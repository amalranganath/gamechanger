<?php

// this file must be stored in:
// protected/components/WebUser.php

class WebUser extends CWebUser {

    // Store model to not repeat query.
    private $User;

    // Return first name.
    // access it by Yii::app()->user->first_name
    public function getFullName() {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->profile->first_name.' '.$user->profile->last_name;
    }

    // Return first name.
    // access it by Yii::app()->user->type
    function getType() {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->usertype;
    }

    // This is a function that checks the field 'role'
    // in the User model to be equal to 1, that means it's admin
    // access it by Yii::app()->user->isAdmin()
    function isAdmin() {
        //die('f');
        $user = $this->loadUser(Yii::app()->user->id);
        return intval($user->superuser) == 1;
    }

    // Load user model.
    protected function loadUser($id = null) {
        if ($this->User === null) {
            if ($id !== null)
                $this->User = User::model()->findByPk($id);
        }
        return $this->User;
    }

}
