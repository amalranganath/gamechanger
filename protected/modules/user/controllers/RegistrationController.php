<?php

class RegistrationController extends Controller {

    public $defaultAction = 'registration';

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * Registration user
     */
    public function actionRegistration() {
        $model = new RegistrationForm;
        $profile = new Profile;
        $profile->regMode = true;

        // ajax validator
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'registration-form') {
            echo UActiveForm::validate(array($model, $profile));
            Yii::app()->end();
        }

        if (Yii::app()->user->id) {
            $this->redirect(Yii::app()->controller->module->profileUrl);
        } else {
            if (isset($_POST['RegistrationForm'])) {

                $model->attributes = $_POST['RegistrationForm'];
                $model->username = time();
                $model->usertype = $_POST['radio'];
                $profile->attributes = ((isset($_POST['Profile']) ? $_POST['Profile'] : array()));
                if ($model->validate() && $profile->validate()) {
                    $soucePassword = $model->password;
                    $model->activkey = UserModule::encrypting(microtime() . $model->password);
                    $model->password = UserModule::encrypting($model->password);
                    $model->verifyPassword = UserModule::encrypting($model->verifyPassword);
                    $model->superuser = 0;
                    $model->status = ((Yii::app()->controller->module->activeAfterRegister) ? User::STATUS_ACTIVE : User::STATUS_NOACTIVE);

                    if ($model->save()) {
                        
                        $profile->user_id = $model->id;
                        $profile->save();
                        if (Yii::app()->controller->module->sendActivationMail) {
                            $activation_url = $this->createAbsoluteUrl('/user/activation/activation', array("activkey" => $model->activkey, "email" => $model->email, "usertype" => $model->usertype));

                            //$from = 'info@gamechangerworldwide.com';
                            $subject = 'Please activate your Game Changer account';
                            $content = 'You have successfully registered at www.gamechangerworldwide.com';
                            $content.='<p>To activate your account click here <a href="' . $activation_url . '" > Activation URL</a></p>';
                            $content.='<p> Thank you.</p>';
                            //$content.='<p> Kind regards,</p>';
                            //$content.='<p> Gamechanger Team.</p>';
                            //$fromname = 'GameChanger';
                            //$toname = '';
                            //$mail = ['to' => $model->email, 'from' => $from, 'subject' => $subject, 'content' => $content, 'fromname' => $fromname, 'toname' => $toname];
                            //self::swiftMailer($mail);
                            self::sendMail($model->email, $subject, $content);
                        }

                        if ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) {
                            $identity = new UserIdentity($model->username, $soucePassword);
                            $identity->authenticate();
                            Yii::app()->user->login($identity, 0);
                            $this->redirect(Yii::app()->controller->module->returnUrl);
                        } else {
                            if (!Yii::app()->controller->module->activeAfterRegister && !Yii::app()->controller->module->sendActivationMail) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Contact Admin to activate your account."));
                            } elseif (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please {{login}}.", array('{{login}}' => CHtml::link(UserModule::t('Login'), Yii::app()->controller->module->loginUrl))));
                            } elseif (Yii::app()->controller->module->loginNotActiv) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please check your email or login."));
                            } else {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for registering. We have sent you an email with the activation link. Please check your email inbox."));
                                $this->redirect(Yii::app()->baseUrl . '/user/login');
                            }
                            $this->refresh();
                        }
                    }
                } else
                    $profile->validate();
            }
            $this->render('/user/registration', array('model' => $model, 'profile' => $profile));
        }
    }

}
