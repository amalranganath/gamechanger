<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
                        
                       
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();
                                        //redirect if not fill info 
                                        
                                         if($this->getUserType()==1 ){
                                            
                                             if(Agent::model()->find('user_id='.Yii::app()->user->id)->completed==1){
                                              $this->redirect(Yii::app()->baseUrl.'/athletes/dashboard');   
                                             }else{
                                             $this->redirect(Yii::app()->baseUrl.'/agent/update');
                                             }
                                         }elseif($this->getUserType()==2 ){
                                              if(Brand::model()->find('user_id='.Yii::app()->user->id)->completed==1){
                                              $this->redirect(Yii::app()->baseUrl.'/brand/marketplace');  
                                              
                                              }else{
                                                   $this->redirect(Yii::app()->baseUrl.'/brand/update');
                                              }
                                         }
                                        
                                        
					if (Yii::app()->user->returnUrl=='/')
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
                                        
                                        //echo  print_r($_POST['UserLogin']);die();
				}
			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}
        
        private function getCompletedStatus($usertype){
                $comp=false;
               // echo Yii::app()->user->id; die();
            if($usertype==1){
                $comp=Agent::model()->find('user_id='.Yii::app()->user->id)->completed;
                
            }elseif($usertype==2){
//                $comp=Brand::model()->find('user_id='.Yii::app()->user->id)->completed;
                $comp=Brand::model()->find('user_id='.Yii::app()->user->id)->completed;
                //echo print_r($comp); die();
            }
            return $comp;
        }

}