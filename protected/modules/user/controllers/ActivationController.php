<?php

class ActivationController extends Controller {

    public $defaultAction = 'activation';

    /**
     * Activation user account
     */
    public function actionActivation() {
        $email = $_GET['email'];
        $activkey = $_GET['activkey'];
        $usertype = $_GET['usertype'];
        if ($email && $activkey) {
            $find = User::model()->notsafe()->findByAttributes(array('email' => $email));
            if (isset($find) && $find->status) {
                $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("You account is active.")));
            } elseif (isset($find->activkey) && ($find->activkey == $activkey)) {

                $find->activkey = UserModule::encrypting(microtime());
                $find->status = 1;
                $find->save();
                $userid = $find->id;
                //$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is activated.")));
                //send emails to super admins
                /*$userTypes = array('admin','agent', 'brand');
                foreach (User::model()->findAll('superuser=1') as $admin) {

                    $content = 'Hi ' . $admin->profile->firstname . ',<br/>';
                    $content.='A new ' . ucfirst($userTypes[$find->usertype]) . ' is registered to Game Changer. <br/>( Reference : User ID = ' . $find->id . ' )';

                    self::sendMail($admin->email, 'New user registration', $content);
                }*/
                
                if ($find->usertype == 1) {
                    $agent = new Agent;
                    $agent->user_id = $userid;

                    if ($agent->save(false)) {
                        Yii::app()->user->setFlash('success-registration', "Your account is activated!");
                        $this->redirect(array('/user/login'));
                    }
                } elseif ($find->usertype == 2) {
                    $brand = new Brand;
                    $brand->user_id = $userid;

                    if ($brand->save(false)) {
                        //$this->defaultImageUpload($brand->imageid, '/images/brands/', $brand->user_id);                                      
                        Yii::app()->user->setFlash('success-registration', "Your account is activated!");
                        $this->redirect(array('/user/login'));
                    }
                }
            } else {
                $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("Incorrect activation URL.")));
            }
        } else {
            $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("Incorrect activation URL.")));
        }
    }

}
