<div id="page-wrapper">
<div class="signup_form">
    <?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
</div>
<?php else: ?>
    <?php echo CHtml::beginForm(); ?>
                
<!--                <div class="">
                    <div class="panel text-center text-uppercase">-->
    <div class="login login-action-login wp-core-ui  locale-en-us dsidx">
        <div id="login">
            <div class="bg">
                    <img class="logo" src="<?php echo Yii::app()->baseUrl;?>/images/logo.png"><br/>
                        <h3 class="text-uppercase text-center">Restore Your Password</h3>
<!--                    </div>
                </div>-->
                    
                <div class="errorMessage">
                <?php echo CHtml::errorSummary($form); ?>
                </div>
    
                <div class="text-center panel-body">
                    <?php echo CHtml::activeTextField($form,'login_or_email',array('class'=>'panel-body','placeholder'=>'Email')) ?>
                     <p class="hint"><?php echo UserModule::t("Please enter your email addres."); ?></p>
                </div>
                
    
                <div class="text-center panel-body">
                  <?php echo CHtml::submitButton(UserModule::t("Restore"),array('class'=>'panel-body bg_color_hover btn')); ?>
                </div>
                
                <div class="text-center panel-body">
                    <p class="hint">
                    <?php echo CHtml::link(UserModule::t("Login"),Yii::app()->getModule('user')->loginUrl); ?> | <?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?>
                    </p>
                </div>

            </div>
        </div>
    </div>
    <?php echo CHtml::endForm(); ?>
    </div>
<?php endif; ?>