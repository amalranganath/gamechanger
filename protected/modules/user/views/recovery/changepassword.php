<!--<div id="page-wrapper">-->
<div class="login login-action-login wp-core-ui  locale-en-us dsidx">
        <div id="login">
<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Change Password"),
);
?>




<div class="form">
<?php echo CHtml::beginForm(); ?>
    <div class="text-center panel-body">
    <img class="logo" src="<?php echo Yii::app()->baseUrl; ?>/images/logo.png"><br/>
    <h3 class="text-uppercase text-center"><?php echo UserModule::t("Change Password"); ?></h3>
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	<?php echo CHtml::errorSummary($form); ?>
        </div>

    <div class="text-center panel-body">
	<?php echo CHtml::activeLabelEx($form,'password'); ?>
	<?php echo CHtml::activePasswordField($form,'password'); ?>
	<p class="hint">
	<?php echo UserModule::t("Minimal password length 4 symbols."); ?>
	</p>
	</div>

    <div class="text-center panel-body">
	<?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
	<?php echo CHtml::activePasswordField($form,'verifyPassword'); ?>
	</div>

    <div class="text-center panel-body">
	<div class="row submit">
	<?php echo CHtml::submitButton(UserModule::t("Save")); ?>
	</div>
	</div>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->
</div>
</div>