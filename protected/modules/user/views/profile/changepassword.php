<?php
//$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
//$this->breadcrumbs=array(
//	UserModule::t("Profile") => array('/user/profile'),
//	UserModule::t("Change Password"),
//);
//$this->menu=array(
//	((UserModule::isAdmin())
//		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'))
//		:array()),
//    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
//    array('label'=>UserModule::t('Profile'), 'url'=>array('/user/profile')),
//    array('label'=>UserModule::t('Edit'), 'url'=>array('edit')),
//    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout')),
//);
?>
<div id="page-wrapper">

    <div class="signup_form">

        <h1 class="page-header text-uppercase"><?php echo UserModule::t("Change Password"); ?></h1>

        <div class="row">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'changepassword-form',
                'enableAjaxValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>


            <!--<p class="note">--><?php //echo UserModule::t('Fields with <span class="required">*</span> are required.');       ?><!--</p>-->
            <!--	--><?php //echo $form->errorSummary($model);       ?>

            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo $form->passwordField($model, 'oldPassword', array('placeholder' => 'Old Password', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'oldPassword'); ?>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group ">
                    <?php echo $form->passwordField($model, 'password', array('placeholder' => 'New Password', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'password'); ?>
                    <p class="hint"><?php echo UserModule::t("Minimal password length 4 symbols."); ?></p>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group ">
                    <?php echo $form->passwordField($model, 'verifyPassword', array('placeholder' => 'Retype New Password', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'verifyPassword'); ?>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                </div>
            </div>

            <div class="col-sm-12 submit">
                <div class="form-group ">
                    <?php echo CHtml::submitButton(UserModule::t("Save"), array('class' => 'bg_color_hover btn')); ?>
                </div>
            </div>

            <?php $this->endWidget(); ?>
        </div><!-- form -->

    </div>
</div>