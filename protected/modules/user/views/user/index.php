<?php
            if(empty(Yii::app()->user->id)) {
                    $this->redirect(Yii::app()->createAbsoluteUrl('/user/login'));
                }
                else {
                    
                    if($this->getUserType() == 1) {
                        $this->redirect(Yii::app()->createAbsoluteUrl('athletes/dashboard'));
//                        $this->redirect(array('athletes/dashboard'));
                    }
                    if($this->getUserType() == 2) {
                        $this->redirect(Yii::app()->createAbsoluteUrl('brand/marketplace'));
//                        $this->redirect(array('brand/marketplace'));
                    }
                }
$this->breadcrumbs=array(
	UserModule::t("Users"),
//    \\SARANGA-PC\htdocs\endors\protected\modules\user\views\user\index.php
);
if(UserModule::isAdmin()) {
	$this->layout='//layouts/column2';
	$this->menu=array(
	    array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
	    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
	);
}
?>

<h1><?php echo UserModule::t("List User"); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id))',
		),
		'create_at',
		'lastvisit_at',
	),
)); ?>
