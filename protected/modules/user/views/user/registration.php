
<div id="page-wrapper">
    <?php if (Yii::app()->user->hasFlash('registration')): ?>
        <div class="col-lg-12">
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('registration'); ?>
                <!--<button type="button" class="btn btn-outline btn-primary btn-lg btn-block">Back to Home</button>-->
            </div>
        </div>
    <?php else: ?>

        <?php
        $form = $this->beginWidget('UActiveForm', array(
            'id' => 'registration-form',
            'enableAjaxValidation' => true,
            'disableAjaxValidationAttributes' => array('RegistrationForm_verifyCode'),
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>
        <?php // echo $form->errorSummary(array($model,$profile));  ?>
        <div class="login login-action-login wp-core-ui locale-en-us dsidx">
            <div id="login">
                <div class="bg">
                    <img class="logo" src="<?php echo Yii::app()->baseUrl . '/images/logo.png'; ?>"><br/>
                    <h3 class="text-uppercase text-center">Registration</h3>

                    <div class="text-center">
                        <?php echo $form->errorSummary($model, null, '', array('class' => 'alert alert-warning')); ?>
                    </div>
                    <div class="col-sm-6 text-center panel-body">
                        <input name="radio" type="radio" value="2" id="optionone"
                        <?php
                        if (empty($_POST['radio'])) {
                            ?> checked = "checked" <?php
                               }
                               if (isset($_POST['radio'])) {
                                   if ($_POST['radio'] == 2) {
                                       ?> checked = "checked"
                                       <?php
                                   }
                               }
                               ?>    
                               >
                        <label for="optionone" class="bg-info panel-body">MARKETER</label>
                    </div>
                    <div class="col-sm-6 text-center panel-body">
                        <input name="radio" type="radio" value="1" id="optiontwo" <?php
                        if (isset($_POST['radio'])) {
                            if ($_POST['radio'] == 1) {
                                ?> checked = "checked" <?php
                                   }
                               }
                               ?> >
                        <label for="optiontwo" class="right bg-info panel-body">AGENT</label>
                    </div>

                    <div class="text-center panel-body">
                        <!--<input type="text" placeholder="Email" class="panel-body">-->
                        <h4>Email</h4>
                        <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                        <?php echo $form->error($model, 'email', array('class' => 'alert-warning')); ?>
                    </div>

                    <div class="text-center panel-body">
                        <h4>Password</h4>
                        <?php echo $form->passwordField($model, 'password', array('class' => 'panel-body', 'placeholder' => 'Password')); ?>
                        <?php echo $form->error($model, 'password', array('class' => 'alert-warning')); ?>
                        <!--<input type="password" placeholder="Password" class="panel-body">-->
                    </div>

                    <div class="text-center panel-body">
                        <h4>Confirm-Password</h4>
                        <?php echo $form->passwordField($model, 'verifyPassword', array('class' => 'panel-body', 'placeholder' => 'Confirm-Password')); ?>
                        <?php echo $form->error($model, 'verifyPassword', array('class' => 'alert-warning')); ?>
                        <!--<input type="password" placeholder="Re Enter Password" class="panel-body">-->
                    </div>

                    <div class="text-center panel-body">
                        <?php echo CHtml::submitButton(UserModule::t("Register"), array('class' => 'panel-body bg_color_hover btn')); ?>
                        <!--<input type="submit" value="SIGN UP" class="panel-body bg_color_hover btn">-->
                    </div>

                    <div class="text-center panel-body">
                        <p>
                            By signing up, you agree to our <a href="<?php echo Yii::app()->createUrl('/site/terms') ?>" target="_blank" class="font_color">Terms of Service</a> &amp; <a href="<?php echo Yii::app()->createUrl('/site/policy') ?>" target="_blank" class="font_color">Privacy&nbsp;Policy</a>
                        </p>
                    </div>
                    <div class="text-center panel-body">
                        <p class="hint">
                            <?php echo CHtml::link(UserModule::t("Login"), Yii::app()->getModule('user')->loginUrl); ?> | <?php echo CHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl); ?>
                        </p>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <!-- /.container-fluid -->
            </div>
        </div>
    <?php endif; ?>
</div>
<!-- /#page-wrapper -->
