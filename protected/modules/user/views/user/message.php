<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login"); ?>

<h1 class="page-header text-uppercase"><?php echo $title; ?></h1>

<div class="login login-action-login wp-core-ui  locale-en-us dsidx">
    <div id="login" class="signup_form">
        <div class="panel text-center">
            <div class="alert alert-info">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div><!-- yiiForm -->