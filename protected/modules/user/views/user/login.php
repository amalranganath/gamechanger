
<?php if (Yii::app()->user->hasFlash('loginMessage')): ?>

    <div class="success">
        <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
    </div>
<?php endif; ?>

<div class="login login-action-login wp-core-ui  locale-en-us dsidx">
    <div id="login">
        <?php echo CHtml::beginForm(); ?>
        <img class="logo" src="<?php echo Yii::app()->baseUrl; ?>/images/logo.png"><br/>
        <h3 class="text-uppercase">login</h3>
        <div class="">
            <div class="panel text-center">
                <?php if (Yii::app()->user->hasFlash('success-registration')): ?>
                    <div class="alert alert-success">
                        <?php echo Yii::app()->user->getFlash('success-registration'); ?>
                    </div>
                <?php endif; ?>
                <?php if (Yii::app()->user->hasFlash('registration')): ?>
                    <div class="alert alert-warning">
                        <?php echo Yii::app()->user->getFlash('registration'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="text-center">
            <?php echo CHtml::errorSummary($model, null, '', array('class'=>'alert alert-warning')); ?>
        </div>
        <div class="text-center panel-body">
            <h4>Email</h4>
            <?php echo CHtml::activeTextField($model, 'username', array('class' => 'panel-body', 'placeholder' => 'Email')) ?>
        </div>

        <div class="text-center panel-body">
            <h4>Password</h4>
            <?php echo CHtml::activePasswordField($model, 'password', array('class' => 'panel-body', 'placeholder' => 'Password')) ?>
        </div>

        <div class="text-center panel-body">

            <?php echo CHtml::submitButton(UserModule::t("Login"), array('class' => 'panel-body bg_color_hover btn')); ?>
        </div>

        <div class="text-center panel-body">
            <p class="hint">
                <?php echo CHtml::link(UserModule::t("Register"), Yii::app()->getModule('user')->registrationUrl); ?> | <?php echo CHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl); ?>
            </p>
        </div>
        <?php echo CHtml::endForm(); ?>

    </div>
</div>

