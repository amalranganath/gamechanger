<?php

/**
 * Command to run Cron Job for making expiration pending deals by ordered date 
 */
class CronCommand extends CConsoleCommand {

    const DEAL_PENDING = 0;
    const DEAL_EXPIRED = 3;
    const ORDER_OPEN = 0;
    const ORDER_CLOSE = 1;
    const SERVER_URL = 'http://localhost/gamechanger';
    const EXP_DAYS = 2;
    

    public function run($args) {
        // here we are doing what we need to do
        //echo "hellow this is cron";
        $_SERVER['SERVER_NAME'] = 'localhost';

        $pendingDeals = Deals::model()->with('order')->findAll(array(
            'condition' => 'dealstatusid=:status AND order.status=:orderStatus',
            'params' => array(':status' => self::DEAL_PENDING, ':orderStatus' => self::ORDER_OPEN),
        ));
        $currentDate = date("Y-m-d");
        //dateofpaymenttoagent
        foreach ($pendingDeals as $deal) {
            $dateDiff = date_diff(date_create($currentDate), date_create($deal->dealrequesteddate));
                var_dump($dateDiff->format('%a'));
            if ($dateDiff->format('%a') >= self::EXP_DAYS) {
                //If the last deal of a order
                if ($deal->isLastDeal()) {
                    $paymentResult = Yii::app()->paypal->DoVoid(array('AUTHORIZATIONID' => $deal->order->payment->transactionid));
                    //Detect payment errors
                    if (!Yii::app()->paypal->isCallSucceeded($paymentResult)) {
                        if (Yii::app()->paypal->apiLive === true) {
                            //Live mode basic error message
                            $error = 'We were unable to process your request. Please try again later (live mode)';
                        } else {
                            //Sandbox output the actual error message to dive in.
                            $error = $paymentResult['L_LONGMESSAGE0'];
                        }
                        $deal->order->payment->msg = $error;
                        $deal->order->payment->save();
                        //Yii::app()->user->setFlash('deals-error', $error);
                        //$this->redirect(array('view', 'id' => $dealId));
                        echo $error;
                        //Yii::app()->end();
                    } else {
                        //voide captured payment completed successfully 
                        //$model->order->payment->msg = $paymentResult['MSGSUBID'];
                        $deal->order->payment->status = self::DEAL_EXPIRED;
                        $deal->order->payment->save();

                        $deal->order->status = self::ORDER_CLOSE;
                        $deal->order->save();
                    }
                }
                
                $deal->dealstatusid = self::DEAL_EXPIRED;
                $deal->save(false);
            }
        }
        $email = 'amal@livingdreams.lk';//The email address the cron job will reach when successful.
        $subject = 'Gamechanger Cron Job';
        $body = 'Hi, This email is automatically generated to confirm that the cron job has been run successfully: /console.php - @'.date('Y-m-d H:i');
        $this->sendMail($email, $subject, $body);
        //var_dump($this);
    }
    
    //send email using YiiMail extension
    private static function sendMail($to, $subject, $content = '') {
        //use YiiMailMessage
        $message = new YiiMailMessage;
        $message->addFrom('noreply@localhost.com','Game Changer');
        $message->subject = $subject;
        
        $message->setBody($content, 'text/html');
        $message->addTo($to);
        //Send the email
        if (Yii::app()->mail->send($message)) {
            ///Yii::app()->user->setFlash('success', 'Successfully sent the email');
            return true;
        } else {
            echo 'Error sending the email!';
            return false;
        }
    }

}
