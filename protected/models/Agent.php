<?php

/**
 * This is the model class for table "agent".
 *
 * The followings are the available columns in table 'agent':
 * @property integer $id
 * @property integer $user_id
 * @property string $firstname
 * @property string $lastname
 * @property string $addressline1
 * @property string $addressline2
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property double $agentshare
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class Agent extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'agent';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, firstname, lastname, addressline1, addressline2, city, state, zipcode, agentshare', 'required'),
            array('user_id,agentshare', 'numerical', 'integerOnly' => false),
            array('agentshare', 'numerical'),
            array('firstname, lastname, addressline1, addressline2', 'length', 'max' => 255),
            array('city, state', 'length', 'max' => 50),
            array('zipcode', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, firstname, lastname, addressline1, addressline2, city, state, zipcode, agentshare', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'addressline1' => 'Address line1',
            'addressline2' => 'Address line2',
            'city' => 'City',
            'state' => 'State',
            'zipcode' => 'Zip code',
            'agentshare' => 'Agent share',
            'completed' => 'Completed',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('firstname', $this->firstname, true);
        $criteria->compare('lastname', $this->lastname, true);
        $criteria->compare('addressline1', $this->addressline1, true);
        $criteria->compare('addressline2', $this->addressline2, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('state', $this->state, true);
        $criteria->compare('zipcode', $this->zipcode, true);
        $criteria->compare('agentshare', $this->agentshare);
        $criteria->compare('completed', $this->completed);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Agent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Returns the total MarketReach & Engagement separately
     */
    public function getTotal() {
        return Yii::app()->db->createCommand()
                            ->select('sum(reach) as MarketReach, sum(engagement) as Engagement')
                            ->from('athletes')
                            ->where('userid = ' . $this->user_id)
                            ->queryRow();
        //CVarDumper::dump($Earned, 1000, true);
    }

    /**
     * Returns the total earned with agent share
     */
    public function getTotalEarned() {
        $Earned = Deals::model()->findAll('agentid=' . $this->user_id . ' AND dealstatusid=5');
        //CVarDumper::dump($Earned, 1000, true);
        $TotalEarned = 0;
        //$TotalEarnedoriginal = 0;
        foreach ($Earned as $earned) {
            $TotalEarned += ( $earned['cost'] * ($this->agentshare / 100));
            //$TotalEarned = $TotalEarnedoriginal;
        }
        //die;
        return $TotalEarned;
    }

    /**
     * Returns the total earned with agent share
     */
    public function getDealCount() {
        $Earned = Deals::model()->findAll('agentid=' . $this->user_id . ''); // AND dealstatusid!=4
        //CVarDumper::dump($Earned, 1000, true);
        return count($Earned);
    }

}
