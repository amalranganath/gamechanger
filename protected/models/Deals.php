<?php

/**
 * This is the model class for table "deals".
 *
 * The followings are the available columns in table 'deals':
 * @property integer $dealid
 * @property integer $orderid
 * @property integer $dealstatusid
 * @property integer $athleteid
 * @property integer $dealtype
 * @property integer $cost
 * @property string $dealrequesteddate
 * @property string $image
 * @property string $description
 * @property string $campaigndate
 * @property string $campaigntime
 * @property string $dealapproveddate
 * @property string $dealfullfilleddate
 * @property string $dealaccpeteddate Removed
 * @property string $dateofpaymentbybrand
 * @property string $dateofpaymenttoagent
 * @property integer $agentid
 * @property string $agentsharepercentage
 * @property double $agentshare
 * @property double $fulfill_engagement
 * @property double $fulfill_reach
 * @property integer $fullfill_paidtoagent
 */
class Deals extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'deals';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                    array('orderid, dealstatusid, athleteid, dealtype, cost, dealrequesteddate, image, description, campaigndate, campaigntime, dealapproveddate, dealfullfilleddate, agentid, agentsharepercentage, agentshare, fulfill_engagement, fulfill_reach, fullfill_paidtoagent', 'required'),
                    array('orderid, dealstatusid, athleteid, dealtype, cost, agentid, fullfill_paidtoagent', 'numerical', 'integerOnly'=>true),
                    array('agentshare, fulfill_engagement, fulfill_reach', 'numerical'),
                    array('image', 'length', 'max'=>100),
                    array('description', 'length', 'max'=>250),
                    array('agentsharepercentage', 'length', 'max'=>11),
                    array('dateofpaymentbybrand, dateofpaymenttoagent', 'safe'),
                    // The following rule is used by search().
                    // @todo Please remove those attributes that should not be searched.
                    array('dealid, orderid, dealstatusid, athleteid, dealtype, cost, dealrequesteddate, image, description, campaigndate, campaigntime, dealapproveddate, dealfullfilleddate, dateofpaymentbybrand, dateofpaymenttoagent, agentid, agentsharepercentage, agentshare, fulfill_engagement, fulfill_reach, fullfill_paidtoagent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'status' => array(self::BELONGS_TO, 'Dealstatus', 'dealstatusid'),
                    'user' => array(self::BELONGS_TO, 'Users', 'agentid'),
                    'agent' => array(self::HAS_ONE, 'Agent', array('id'=>'user_id'), 'through'=>'user'),
                    'athlete' => array(self::BELONGS_TO, 'Athletes', 'athleteid'),
                    'order' => array(self::BELONGS_TO, 'Orders', 'orderid'),
                    'image' => array(self::BELONGS_TO, 'Lookupimages', 'imageid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                    'dealid' => 'Deal ID',
                    'orderid' => 'Order ID',
                    'dealstatusid' => 'Deal Status  ID',
                    'athleteid' => 'Athlete Id',
                    'dealtype' => 'Deal Type',
                    'cost' => 'Cost',
                    'dealrequesteddate' => 'Deal Requested Date',
                    'image' => 'Image',
                    'description' => 'Description',
                    'campaigndate' => 'Campaign Date',
                    'campaigntime' => 'Campaign Time',
                    'dealapproveddate' => 'Deal Approved Date',
                    'dealfullfilleddate' => 'Deal Fulfilled Date',
                    'dateofpaymentbybrand' => 'Date of payment by brand',
                    'dateofpaymenttoagent' => 'Date of payment to agent',
                    'agentid' => 'Agentid',
                    'agentsharepercentage' => 'Agent share percentage',
                    'agentshare' => 'Agentshare',
                    'fulfill_engagement' => 'Fulfill Engagement',
                    'fulfill_reach' => 'Fulfill Reach',
                    'fullfill_paidtoagent' => 'Fulfill Paidtoagent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dealid',$this->dealid);
		$criteria->compare('orderid',$this->orderid);
		$criteria->compare('dealstatusid',$this->dealstatusid);
		$criteria->compare('athleteid',$this->athleteid);
		$criteria->compare('dealtype',$this->dealtype);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('dealrequesteddate',$this->dealrequesteddate,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('campaigndate',$this->campaigndate,true);
		$criteria->compare('campaigntime',$this->campaigntime,true);
		$criteria->compare('dealapproveddate',$this->dealapproveddate,true);
		$criteria->compare('dealfullfilleddate',$this->dealfullfilleddate,true);
		$criteria->compare('dateofpaymentbybrand',$this->dateofpaymentbybrand,true);
		$criteria->compare('dateofpaymenttoagent',$this->dateofpaymenttoagent,true);
		$criteria->compare('agentid',$this->agentid);
		$criteria->compare('agentsharepercentage',$this->agentsharepercentage,true);
		$criteria->compare('agentshare',$this->agentshare);
		$criteria->compare('fulfill_engagement',$this->fulfill_engagement);
		$criteria->compare('fulfill_reach',$this->fulfill_reach);
		$criteria->compare('fullfill_paidtoagent',$this->fullfill_paidtoagent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function isLastDeal($status = 0){
            if($this->findAll(array("condition"=>"orderid =  $this->orderid AND dealid != $this->dealid AND dealstatusid= $status",)))
                return false;
            else
                return true;
        }
        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Deals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /*
         * 
         */
        public function getAgentName() {  
            return ($this->agent) ? $this->agent->firstname.' '.$this->agent->lastname : '';
        } 
                
        public function getAthleteName() {         
            return $this->athlete->firstname.' '.$this->athlete->lastname;
        } 
        
        public function getDealType() {
            return ($this->dealtype == 0) ?'Twitter' : 'Instagram';
        }
}
