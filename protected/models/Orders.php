<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $orderid
 * @property integer $userid
 * @property integer $paymentid
 * @property string $orderdate
 * 
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('orderid, userid, orderdate', 'required'),
			array('orderid, userid, paymentid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('orderid, userid, paymentid, orderdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user' => array(self::BELONGS_TO, 'Users', 'userid'),
                    'brand' => array(self::HAS_ONE, 'Brand', array('id'=>'user_id'), 'through'=>'user'),
                    'payment' =>array(self::BELONGS_TO, 'Payment', 'paymentid'),
                    'deals' =>array(self::HAS_MANY, 'Deals', 'orderid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'orderid' => 'Order ID',
			'userid' => 'User ID',
			'paymentid' => 'Payment ID',
			'orderdate' => 'Order Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('orderid',$this->orderid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('paymentid',$this->paymentid);
		$criteria->compare('orderdate',$this->orderdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchUser()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                //$criteria->with = array('user','brand', 'payment');
                
		$criteria->compare('t.orderid',$this->orderid);
		//$criteria->compare('userid',$this->userid);
		$criteria->compare('t.paymentid',$this->paymentid);
		$criteria->compare('t.orderdate',$this->orderdate,true);

                $criteria->addCondition('t.userid='.Yii::app()->user->id);
                $criteria->order = 't.orderid DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
