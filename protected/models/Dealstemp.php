<?php

/**
 * This is the model class for table "dealstemp".
 *
 * The followings are the available columns in table 'dealstemp':
 * @property integer $dealid
 * @property integer $athleteid
 * @property integer $agentid
 * @property integer $userid
 * @property integer $imageid
 * @property integer $dealtype
 * @property double $cost
 * @property string $description
 * @property string $campaigndate
 * @property string $campaigntime
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Athletes $athlete
 * @property Users $agent
 * @property Lookupimages $image
 */
class Dealstemp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealstemp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('athleteid, agentid, userid', 'required'),
			array('athleteid, agentid, userid, imageid, dealtype', 'numerical', 'integerOnly'=>true),
			array('cost', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dealid, athleteid, agentid, userid, imageid, dealtype,costpertwitter,costperinstagram, description, campaigndate, campaigntime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
                        'agent' => array(self::HAS_ONE, 'Agent', array('id'=>'user_id'), 'through'=>'user'),
			'athlete' => array(self::BELONGS_TO, 'Athletes', 'athleteid'),
			'image' => array(self::BELONGS_TO, 'Lookupimages', 'imageid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dealid' => 'Dealid',
			'athleteid' => 'Athleteid',
			'agentid' => 'Agentid',
			'userid' => 'Userid',
			'imageid' => 'Imageid',
			'dealtype' => 'Dealtype',
			'cost' => 'Cost',
			'description' => 'Description',
			'campaigndate' => 'Campaigndate',
			'campaigntime' => 'Campaigntime',
                        'costpertwitter' =>'CostPerTwitter',
                        'costperinstagram'=>'CostPerInstagram',
                        'completed'=>'Completed'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dealid',$this->dealid);
		$criteria->compare('athleteid',$this->athleteid);
		$criteria->compare('agentid',$this->agentid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('imageid',$this->imageid);
		$criteria->compare('dealtype',$this->dealtype);
                $criteria->compare('costpertwitter',$this->costpertwitter);
                $criteria->compare('costperinstagram',$this->costperinstagram);
                $criteria->compare('description',$this->description,true);
                $criteria->compare('campaigndate',$this->campaigndate,true);
                $criteria->compare('campaigntime',$this->campaigntime,true);
                $criteria->compare('completed',$this->completed);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dealstemp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
