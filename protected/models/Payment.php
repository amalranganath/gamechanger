<?php

/**
 * This is the model class for table "payment".
 *
 * The followings are the available columns in table 'payment':
 * @property integer $paymentid
 * @property integer $userid
 * @property string $token
 * @property string $payerid
 * @property string $transactionid
 * @property string $ipaddress
 * @property double $amount
 * @property string $msg
 * @property string $time
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class Payment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, token, payerid, ipaddress, amount, time, status', 'required'),
			array('userid, status', 'numerical', 'integerOnly'=>true),
			array('amount, captured', 'numerical'),
			array('token, payerid, transactionid', 'length', 'max'=>32),
			array('ipaddress', 'length', 'max'=>16),
			array('msg', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('paymentid, userid, token, payerid, transactionid, ipaddress, amount, captured, msg, time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'order' => array(self::HAS_ONE, 'Orders', 'paymentid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'paymentid' => 'Payment ID',
			'userid' => 'User ID',
			'token' => 'Token',
			'payerid' => 'Payer ID',
			'transactionid' => 'Transaction ID',
			'ipaddress' => 'Ip Address',
			'amount' => 'Total Amount',
			'captured' => 'Charged Amount',
			'msg' => 'Message',
			'time' => 'Time',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('paymentid',$this->paymentid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('payerid',$this->payerid,true);
		$criteria->compare('transactionid',$this->transactionid,true);
		$criteria->compare('ipaddress',$this->ipaddress,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('msg',$this->msg,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchUser()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('order');
                
		$criteria->compare('paymentid',$this->paymentid);
		//$criteria->compare('userid',$this->userid);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('payerid',$this->payerid,true);
		$criteria->compare('transactionid',$this->transactionid,true);
		$criteria->compare('ipaddress',$this->ipaddress,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('msg',$this->msg,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('status',$this->status);
                $criteria->addCondition('t.userid='.Yii::app()->user->id);
                $criteria->order = 't.paymentid DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
                        
        /*
         * @return currunt status name and  list of statuses
         */
        public function getStatus($list=false){
            $status = array('Failed', 'Authorised', 'Capturing', 'Completed');
            return $list ? $status : $status[$this->status];
        }

        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
