<?php

/**
 * This is the model class for table "lookupimages".
 *
 * The followings are the available columns in table 'lookupimages':
 * @property integer $imageid
 * @property string $imagename
 * @property string $type
 * @property string $created_at
 */
class Lookupimages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lookupimages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('imagename, type, created_at', 'required'),
			array('imagename, type', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('imageid, imagename, type, created_at', 'safe', 'on'=>'search'),
                        
                        // Custom rules
                        // Image handle
                        array('imagename', 'file', 
                            'allowEmpty'=>TRUE, 
//                            'safe'=>true,
//                            'types'=>'jpg, gif, png', 
                            'types'=>'jpg', 
//                            'on'=>'insert',//scenario
//                            'except'=>'update',
//                            'message' => 'Upload Valid Image!',  // Error message
                            'wrongType'=>'File type is invalid',
                            'maxSize'=>1024 * 1024 * 5, // 5MB
//                            'maxSize'=>1024,
//                            'maxFiles'=>4,
                            'tooLarge'=>'Image too large',//Error Message
//                            'tooSmall'=>'Image too small',//Error Message
//                            'tooMany'=>'Too Many Files Uploaded',//Error Message                                
                        ),
                    
                        // Dimension validation
                        array('imagename','dimensionValidation'),
		);
	}
        
        public function dimensionValidation($attribute,$param){
//            $minWidth = 800;//800px
//            $minHeight = 600;//600px
           
            $minWidth = 400;
            $minHeight = 400;
            $ratio = $minWidth."px:".$minHeight."px"; // width : height
            if(is_object($this->imagename)){
                list($width, $height) = getimagesize($this->imagename->tempname);
                if($width != $height || $width < $minWidth || $height < $minHeight || ($height/$width) < ($minHeight/$minWidth)) {
                     //die('img validation');
                    $this->addError('imagename','Image does not comply with the system standards. Use ' .$ratio. ' (width:height) ratio.');
                }
            }	
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'imageid' => 'Imageid',
			'imagename' => 'Imagename',
			'type' => 'Type',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('imageid',$this->imageid);
		$criteria->compare('imagename',$this->imagename,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lookupimages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
