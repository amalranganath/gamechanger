ALTER TABLE `dealstemp` CHANGE `dealtype` `dealtype` TINYINT NULL COMMENT 'tweet=0 or instagram=1';
ALTER TABLE `deals` CHANGE `cost` `cost` DOUBLE NOT NULL;
ALTER TABLE `deals` DROP `dealaccpeteddate`;

ALTER TABLE `orders` ADD `status` TINYINT NOT NULL AFTER `orderdate`;
