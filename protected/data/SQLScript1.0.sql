
ALTER TABLE `athletes` ADD `costpertweetimg` DOUBLE NOT NULL AFTER `costpertweet`, ADD `costperinstagramimg` DOUBLE NOT NULL AFTER `costperinstagram`;

ALTER TABLE `dealstemp` ADD `costpertweetimg` DOUBLE NOT NULL AFTER `costperinstagram`, ADD `costperinstagramimg` DOUBLE NOT NULL AFTER `costpertweetimg`;

ALTER TABLE `deals` ADD `costpertweet` DOUBLE NOT NULL AFTER `dealtype`, ADD `costperinstagram` DOUBLE NOT NULL AFTER `costpertweet`, ADD `costpertweetimg` DOUBLE NOT NULL AFTER `costperinstagram`, ADD `costperinstagramimg` DOUBLE NOT NULL AFTER `costpertweetimg`;

