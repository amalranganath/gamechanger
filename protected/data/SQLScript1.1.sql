
--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
`paymentid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `payerid` varchar(32) NOT NULL,
  `transactionid` varchar(32) NOT NULL,
  `ipaddress` varchar(16) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `captured` DOUBLE(10, 2) NULL,
  `msg` text,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL COMMENT '0 => ''failed'', 1 => ''Authorised'', 2 => ''capturing'', 3 => ''completed'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
 ADD PRIMARY KEY (`paymentid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
MODIFY `paymentid` int(11) NOT NULL AUTO_INCREMENT;

--
-- Alter deals
-- 

ALTER TABLE `orders` ADD `paymentid` INT(11) NULL AFTER `userid`, ADD INDEX (`paymentid`) ;
ALTER TABLE `orders` ADD FOREIGN KEY (`paymentid`) REFERENCES `gamechanger`.`payment`(`paymentid`) ON DELETE CASCADE ON UPDATE NO ACTION;
