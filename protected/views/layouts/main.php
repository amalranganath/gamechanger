<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/sb-admin.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/plugins/morris.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery-ui.min.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.ptTimeSelect.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/realsite.css" />
        <!--<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->theme->baseUrl;          ?>/css/homeland.css" />-->

        <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'/>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>
    <body <?php if (!Yii::app()->user->id) { ?> class="login-body" style="background-image: url('<?php echo Yii::app()->baseUrl . '/images/background.jpg'; ?>'); "<?php } ?>>
        <?php if (Yii::app()->user->id) { ?>

            <div id="wrapper">
                <!-- Navigation -->
                <?php require_once 'navi.php'; ?>
                <!---End Navigation---->

                <?php
                if (isset($this->breadcrumbs)):
                    $this->widget('zii.widgets.CBreadcrumbs', array(
                        'links' => $this->breadcrumbs,
                    ));
                endif
                ?>
                <?php if ($flashes = Yii::app()->user->getFlashes()) { ?>
                    <div class="container">
                        <div class="col-md-12" id="user-alerts">
                            <?php
                            foreach ($flashes as $key => $message)
                                printf('<div class="alert alert-%s fade in"><a class="close" data-dismiss="alert" href="#"></a>%s</div>', $key, $message);
                            ?>
                        </div>
                    </div>
                <?php } ?>
                
                <?php echo $content; ?>

                <div class="clear"></div>

                <!--<div id="footer">
                    <div class="howl">
                        <div class="alert howl-slot howl-has-icon" id="universal-msg">
                            <div class="howl-message howl-danger">
                                <button href="#" class="close howl-close" data-dismiss="alert" aria-label="close">&times;</button>
                                <button class="close howl-close">×</button> 
                                <div class="howl-message-inner"><h5 class="howl-title">Default Message</h5>
                                    Lorem ipsum dolor sit amet, consect adipisicing elit.
                                </div>
                                <i class="howl-icon fa fa-ban"></i>
                            </div>
                        </div>
                    </div>

                    Copyright &copy; <?php // echo date('Y');        ?> by My Company.<br/>
                    All Rights Reserved.<br/>
                <?php // echo Yii::powered();      ?>
                </div>-->
                <!-- footer -->
            </div>
            <!-- page -->
            <?php
        } // end user login check
        else {
            echo $content;
        }
        ?>
    </body>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.ptTimeSelect.js"></script>
    <!-- Morris Charts JavaScript -->
    <!--<script src="<?php //echo Yii::app()->theme->baseUrl;         ?>/js/plugins/morris/raphael.min.js"></script>-->
    <!--<script src="<?php //echo Yii::app()->theme->baseUrl;         ?>/js/plugins/morris/morris.min.js"></script>-->
    <!--<script src="<?php //echo Yii::app()->theme->baseUrl;         ?>/js/plugins/morris/morris-data.js"></script>-->
    <script>

        //popover
        //$('[data-toggle="popover"]').popover({trigger: "hover"});
        $("#user-alerts .alert").fadeTo(5000, 500).slideUp(500, function(){
            //$(this).alert('close');
        });
        jQuery(function ($) {
            var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
            $active.find('a').prepend('<i class="glyphicon glyphicon-minus"></i>');
            $('#accordion .panel-heading').not($active).find('a').prepend('<i class="glyphicon glyphicon-plus"></i>');

            $('#accordion').on('show.bs.collapse', function (e) {
                $('#accordion .panel-heading.active').removeClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
                $(e.target).prev().addClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
            });

        });

    </script>

</body>
</html>
