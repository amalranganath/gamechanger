<?php
//$url=Yii::app()->request->url;
//$baseurl=Yii::app()->controller;
//$pending="";

$actions = array('pending', 'approved', 'fullfilled', 'disapproved', 'expired');

if(Yii::app()->controller->id=='deals') {
    ?>

    <ul class="nav navbar-nav navbar-right sub_nav">
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/deals/pending">Pending</a>
        </li>
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/deals/approved">Approved</a>
        </li>
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/deals/disapproved">Disapproved</a>
        </li>
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/deals/fulfilled">Fulfilled</a>
        </li>
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/deals/expired">Expired</a>
        </li>
    </ul>

<?php
}elseif(Yii::app()->controller->id=='brand' && in_array(Yii::app()->controller->action->id, $actions) ){
    ?>
    <ul class="nav navbar-nav navbar-right sub_nav">
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/brand/pending">Pending</a>
        </li>
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/brand/approved">Approved</a>
        </li>

        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/brand/fullfilled">Fulfilled</a>
        </li>
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/brand/disapproved">Disapproved</a>
        </li>
        <li>
            <a class="page-scroll" href="<?php echo Yii::app()->baseUrl; ?>/brand/expired">Expired</a>
        </li>
    </ul>

<?php
}
    ?>