<?php
if ($this->getUserType() == 2) {//if brand
    $getbrand = Brand::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id));

    if (!empty($getbrand)) {

        if ($getbrand[0]['completed'] == 0) {//if agent
            if (Yii::app()->controller->id == 'brand' && Yii::app()->controller->action->id == 'marketplace') {
                $this->redirect(Yii::app()->baseUrl . '/brand/update');
            } elseif (Yii::app()->controller->id == 'brand' && Yii::app()->controller->action->id == 'dashboard') {
                $this->redirect(Yii::app()->baseUrl . '/brand/update');
            } elseif (Yii::app()->controller->id == 'brand' && Yii::app()->controller->action->id == 'pending') {
                $this->redirect(Yii::app()->baseUrl . '/brand/update');
            } elseif (Yii::app()->controller->id == 'brand' && Yii::app()->controller->action->id == 'mycart') {
                $this->redirect(Yii::app()->baseUrl . '/brand/update');
            }
        }
    }
}
?>

<?php
if ($this->getUserType() == 1) {//if agent
    $getagent = Agent::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id));

    if (!empty($getagent)) {

        if ($getagent[0]['completed'] == 0) {//if agent
            if (Yii::app()->controller->id == 'agent' && Yii::app()->controller->action->id == 'brands') {
                $this->redirect(Yii::app()->baseUrl . '/agent/update');
            } elseif (Yii::app()->controller->id == 'agent' && Yii::app()->controller->action->id == 'dashboard') {
                $this->redirect(Yii::app()->baseUrl . '/agent/update');
            } elseif (Yii::app()->controller->id == 'deals' && Yii::app()->controller->action->id == 'pending') {
                $this->redirect(Yii::app()->baseUrl . '/agent/update');
            }
        }
    }
}
?>

<!------------------------------------------------------------>
<div class="header-main">

    <section class="header-block">
        <div class="inside clear" id="yvk">
            <!--SOCIAL-->
            <div class="social">
                <ul class="clear">
                    <li class="twitter"><a href="http://twitter.com/mabuc" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li class="facebook"><a href="http://facebook.com/mabuc" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li class="youtube"><a href="http://youtube.com" target="_blank"><i class="fa fa-youtube"></i></a></li>
                    <li class="linkedin"><a href="http://linkedin.com" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li class="pinterest"><a href="http://pinterest.com/mabuc" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    <li class="dribbble"><a href="http://dribbble.com/mabuc" target="_blank"><i class="fa fa-dribbble"></i></a></li>
                    <li class="gplus"><a href="http://google.com" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <li class="instagram"><a href="http://instagram.com/envato" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li class="rss"><a href="http://feedburner.com" target="_blank"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
            <!--CALL INFO.-->
            <div class="call-info">

                <?php
                // open cart
                if ($this->getUserType() == 2) {
                    $this->renderPartial('//brand/_cart_menu');
                }
                ?>

                <a href="<?php echo Yii::app()->createUrl('/site/faq') ; ?>" class="dropdown-toggle login" data-toggle="tooltip" data-placement="bottom" title="FAQ"> <i class="fa fa-question-circle"></i> </a>
                <a href="#" class="dropdown-toggle login" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gear"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <a href="<?php echo Yii::app()->baseUrl; echo $this->getUserType() == 2 ? '/brand' : '/agent'; ?>/update"><i class="fa fa-fw fa-user"></i> My Profile</a>
                    </li>
                    <?php if ($this->getUserType() == 2) { ?>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('/brand/payment') ; ?>"> <i class="fa fa-fw fa-money"></i> My Payments</a>
                        </li>
                    <?php } ?>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('/user/profile/changepassword'); ?>"> <i class="fa fa-fw fa-key"></i> Change Password</a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->request->getBaseUrl(true) ?>/user/logout" ><i class="fa fa-fw fa-power-off"></i>Log out</a>
                    </li>

                </ul>

            </div>
        </div>
    </section>

    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <!-- <a class="navbar-brand page-scroll" href="#page-top">Start Bootstrap</a> -->
                <!-- ############### -->
                <aside class="logo clear">
                    <!-- <h1> -->
                    <a href="<?php echo Yii::app()->getBaseUrl(true); ?>">
                        <img class="logo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" />
                    </a>
                    <!-- </h1> -->
                </aside>

                <!-- ########## -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <!--collapse navbar-collapse-->
            <div class="" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden active">
                        <a href="#page-top"></a>
                    </li>
                    <?php include_once '_navi_sub.php'; ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->

        <div class="container">

            <?php include_once('_order_status.php'); ?>

        </div>

    </nav>

</div>




<script>
    //var base_url="/endorsxpress.com/";
    var base_url = "http://lankabird.com";
    //var base_url="http://localhost";

    $(document).ready(function () {
        //  alert("###");
        if ($("#deals").hasClass("active")) {
            $("#deal_top_nav").toggleClass("show");
        }
    });

    $('.navbar-nav li').each(function () {
        var link = jQuery(this).find('a').attr('href');
        if (link === window.location.pathname) {
            $(this).addClass('active');
        }
    });
    $('#deal_top_nav li').each(function () {
        var link = jQuery(this).find('a').attr('href');
        if (link === window.location.pathname) {
            $(this).addClass('active');
            $("#deals").addClass("active");
        }
    });
    $(document).ready(function () {
        $(".btn_cart").click(function () {
            setTimeout(function () {
                $('.top_cart').addClass('open');
            }, 10);
        });

    });



</script>


