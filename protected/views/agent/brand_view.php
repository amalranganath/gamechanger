
<?php //echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
<div class="col-sm-3 col-xs-12">
    <div class="property-box-simple">
        <div class="property-box-image ">
            <div class="property-box-simple-image-inner">
                <?php $imageid=Lookupimages::model()->find('imageid='.$data->imageid);?>
                <img src="<?php echo Yii::app()->baseUrl;?>/images/brands/<?php if($imageid){ echo $imageid->imagename;}else{echo 'logo.jpg';}?>" class="attachment-post-thumbnail wp-post-image" alt="17">
            </div>
        </div>
        <div class="property-box-header">
            <h3><?php echo $data->brandname;?></h3>
        </div>
        <footer class="follower-foot">
            <ul>
                <li> <a href="<?php echo $data->siteurl;?>" target="_blank"><i class="fa fa-list-alt fa-2x font_color"></i></a></li>
                <li><a href="#" data-toggle="modal" data-target=".brand8<?php echo $data->id;?>"><i class="fa fa-info-circle fa-2x font_color"></i></a></li>
                <li><a href="#" data-toggle="modal" data-target="#pitch_box8" class="btn btn-success pitch-btn" id="" data-id="<?php echo $data->id;?>">PITCH</a></li>
            </ul>
        </footer>
        
        <div class="modal fade brand8<?php echo $data->id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title text-center" id="mySmallModalLabel">DESCRIPTION OF <?php echo $data->brandname; ?><a class="anchorjs-link" href="#mySmallModalLabel"><span class="anchorjs-icon"></span></a></h4>
                    </div>
                    <div class="modal-body">
                        <?php echo $data->description;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>