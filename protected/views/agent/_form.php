<?php
/* @var $this AgentController */
/* @var $model Agent */
/* @var $form CActiveForm */
?>

<div id="page-wrapper">
    <div class="signup_form">
        <h1 class="page-header text-uppercase">Registration Details</h1>
        <div class="form">
            <?php
            //if(isset($_GET['usertype']) && isset($_GET['userid'])){
            ?>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'agent-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => false,
            ));
            ?>

                <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php // echo $form->errorSummary($model);  ?>

            <?php
            //$getagent = Agent::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id));

            if ($model->completed == 0) {//if agent
                echo '<div class="alert alert-warning fade in"> Please fill your account detail to enable navigation</div>';
                // Yii::app()->user->setFlash('complete', "please fill your account detail to enable Navigation");             
            }
            ?>
            <h3><?php echo 'Email: ' . $model->user->email; ?></h3>
            <div class="row">
                <div class="col-sm-6 panel-body">
                    <?php echo $form->labelEx($model,'firstname');  ?>
                    <?php echo $form->textField($model, 'firstname', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'First Name', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'firstname'); ?>
                </div>

                <div class="col-sm-6 panel-body">
                    <?php echo $form->labelEx($model,'lastname');  ?>
                    <?php echo $form->textField($model, 'lastname', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Last Name', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'lastname'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 panel-body">
                    <?php echo $form->labelEx($model,'addressline1');  ?>
                    <?php echo $form->textField($model, 'addressline1', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Address Line 01', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'addressline1'); ?>
                </div>

                <div class="col-sm-6 panel-body">
                    <?php echo $form->labelEx($model,'addressline2');  ?>
                    <?php echo $form->textField($model, 'addressline2', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Address Line 02', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'addressline2'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 panel-body">
                    <?php echo $form->labelEx($model,'city');  ?>
                    <?php echo $form->textField($model, 'city', array('size' => 50, 'maxlength' => 50, 'placeholder' => 'City', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'city'); ?>
                </div>

                <div class="col-sm-6 panel-body">
                    <?php echo $form->labelEx($model,'state');  ?>
                    <?php echo $form->textField($model, 'state', array('size' => 50, 'maxlength' => 50, 'placeholder' => 'State', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'state'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 panel-body">
                    <?php echo $form->labelEx($model,'zipcode');  ?>
                    <?php echo $form->textField($model, 'zipcode', array('size' => 20, 'maxlength' => 20, 'placeholder' => 'Zip Code', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'zipcode'); ?>
                </div>

                <div class="col-sm-6 panel-body">
                    <!--		--><?php //// echo $form->labelEx($model,'agentshare');      ?>
                    <!--		--><?php //echo $form->textField($model,'agentshare',array('placeholder'=>'Agent Share','class'=>'panel-body'));      ?>
                    <!--		--><?php //echo $form->error($model,'agentshare');      ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 panel-body">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'panel-body bg_color_hover btn')); ?>
                </div>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>