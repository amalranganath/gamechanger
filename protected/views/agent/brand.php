
    <div class="container-fluid">
        <div class="container cover-search">
            <div class="row ">
                <div class="col-md-12 panel-body">
                    <div class="col-md-6">
                        <form class="form-inline">
                            <input type="text" class="form-control" name="Brand[brandname]" id="txtval"  placeholder="Search..." >

                            <span>
                                <?php
                                echo CHtml::ajaxSubmitButton('Search', CHtml::normalizeUrl(array('/agent/brands')), array(
                                    'update' => '#brand-search',
                                    'type' => 'POST',
                                        //'success'=>'',
                                        ), array('name' => 'run', 'class' => 'btn btn-lg btn-block', 'id' => 'txtval-btn')
                                );
                                ?>
                            </span>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <?php if (Yii::app()->user->hasFlash('email_success')) { ?>
                        <div class="alert alert-success fade in">
                            <?php echo Yii::app()->user->getFlash('email_success'); ?>
                        </div>
                    <?php } ?>
                    <?php if (Yii::app()->user->hasFlash('email_fail')) { ?>
                        <div class="alert alert-warning fade in">
                            <?php echo Yii::app()->user->getFlash('email_fail'); ?>
                        </div>
                    <?php } ?>
                    <?php if (Yii::app()->user->hasFlash('email_req')) { ?>
                        <div class="alert alert-danger fade in">
                            <?php echo Yii::app()->user->getFlash('email_req'); ?>
                        </div>
                    <?php } ?>
                    <div id="brand-search">
                        <?php $this->renderPartial('_brands', array('dataProvider' => $dataProvider)); ?>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.container-fluid -->
    </div>


