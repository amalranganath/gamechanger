
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => 'brand_view',
));
?>

<div class="modal fade" id="pitch_box8" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title text-center" id="mySmallModalLabel">PITCH TO A BRAND<a class="anchorjs-link" href="#mySmallModalLabel"><span class="anchorjs-icon"></span></a></h4>
            </div>
            <div class="modal-body">
                <form action="pitchform" method="post" name="pitch">
                    <?php
                    $Athlete = Athletes::model()->findAll(array(
                        'select' => 'firstname,lastname',
                        'condition' => 'userid=' . Yii::app()->user->id,
                    ));
                    // echo '<pre>'.print_r($Athlete,1).'</pre>'; die();
                    $athletes = array();
                    foreach ($Athlete as $athlete) {
                        $athletename = $athlete['firstname'] . ' ' . $athlete['lastname'];
                        $athletes[$athletename] = $athletename;
                    }
                    echo CHtml::dropDownList('athlete', '', $athletes, array('class' => 'form-control', 'empty' => '--Select an Athlete--', 'required' => true));
                    echo CHtml::hiddenField('brandid', '', array('id' => 'brandid'));
                    echo '<br>';
                    echo CHtml::textArea('pitch2brand', '', array('id' => 'widget',
                        'class' => 'form-control',
                        'required' => true,
                    ));
                    ?>
                    <br>
                    <button class="btn btn-info" data-dismiss="modal">CANCEL</button>

                    <input type="submit" class="btn btn-success" style="width: 11%" value="PITCH" onsubmit="this.submit();
                                    this.reset();">

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade brand" id="pitch_box" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">PITCH TO A BRAND<a class="anchorjs-link" href="#mySmallModalLabel"><span class="anchorjs-icon"></span></a></h4>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>
<script>
    $(document).on("click", ".pitch-btn", function () {
        var brandid = $(this).data('id');

        document.getElementById('brandid').value = brandid;

    });
</script>

