<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error';
//$this->breadcrumbs = array(
//    'Error',
//);
?>
<div class="container-fluid">
    <div class="container">
        <div class="signup_form">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel text-center text-uppercase">
                    <h1>Error <?php echo $code; ?></h1>

                </div>
            </div>
            <div class="error">
                <p><?php echo CHtml::encode($message); ?></p>
            </div>
        </div>
        </div>
    </div>
</div>