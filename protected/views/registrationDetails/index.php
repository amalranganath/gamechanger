<div id="landing_wrapper">
<?php if(Yii::app()->user->hasFlash('registration')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>
<?php else: ?>

        <div id="page-wrapper">
            <?php $form=$this->beginWidget('UActiveForm', array(
	'id'=>'registration-form',
	'enableAjaxValidation'=>true,
	'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>
            <?php // echo $form->errorSummary(array($model,$profile)); ?>
            <div class="signup_form">
                <div class="col-sm-12">
                    <div class="panel text-center text-uppercase">
                        <h1>Registration</h1>
                    </div>
                </div>

               
                
                <div class="col-sm-12 text-center panel-body">
                    <!--<input type="text" placeholder="Email" class="panel-body">-->
                    <?php echo $form->textField($model,'email',array('class'=>'panel-body','placeholder'=>'First Name')); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>
                <div class="col-sm-12 text-center panel-body">
                    <!--<input type="text" placeholder="Email" class="panel-body">-->
                    <?php echo $form->textField($model,'email',array('class'=>'panel-body','placeholder'=>'Last Name')); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>
                
                <div class="col-sm-6 text-center panel-body">
                    <?php echo $form->passwordField($model,'password',array('class'=>'panel-body','placeholder'=>'Password')); ?>
                    <?php echo $form->error($model,'password'); ?>
                    <!--<input type="password" placeholder="Password" class="panel-body">-->
                </div>
                
                <div class="col-sm-6 text-center panel-body">
                    <?php // echo $form->passwordField($model,'verifyPassword',array('class'=>'panel-body','placeholder'=>'Confirm-Password')); ?>
                    <?php // echo $form->error($model,'verifyPassword'); ?>
                    <!--<input type="password" placeholder="Re Enter Password" class="panel-body">-->
                </div>
                
                <div class="col-sm-12 text-center panel-body">
                    <?php echo CHtml::submitButton(UserModule::t("Register"),array('class'=>'panel-body bg_color_hover btn')); ?>
                    <!--<input type="submit" value="SIGN UP" class="panel-body bg_color_hover btn">-->
                </div>
                
                <div class="col-sm-12 text-center panel-body">
                    <p>
                        By signing up, you agree to our <a href="/Content/terms.html" target="_blank" class="font_color">Terms of Service</a> &amp; <a href="/Content/privacy.html" target="_blank" class="font_color">Privacy Policy</a>
                    </p>
                </div>

            </div>
            <?php $this->endWidget(); ?>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
<?php endif; ?>