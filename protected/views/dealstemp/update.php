<?php
/* @var $this DealstempController */
/* @var $model Dealstemp */

$this->breadcrumbs=array(
	'Dealstemps'=>array('index'),
	$model->dealid=>array('view','id'=>$model->dealid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Dealstemp', 'url'=>array('index')),
	array('label'=>'Create Dealstemp', 'url'=>array('create')),
	array('label'=>'View Dealstemp', 'url'=>array('view', 'id'=>$model->dealid)),
	array('label'=>'Manage Dealstemp', 'url'=>array('admin')),
);
?>

<h1>Update Dealstemp <?php echo $model->dealid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>