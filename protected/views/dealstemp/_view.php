<?php
/* @var $this DealstempController */
/* @var $data Dealstemp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->dealid), array('view', 'id'=>$data->dealid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('athleteid')); ?>:</b>
	<?php echo CHtml::encode($data->athleteid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agentid')); ?>:</b>
	<?php echo CHtml::encode($data->agentid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imageid')); ?>:</b>
	<?php echo CHtml::encode($data->imageid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealtype')); ?>:</b>
	<?php echo CHtml::encode($data->dealtype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cost')); ?>:</b>
	<?php echo CHtml::encode($data->cost); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaigndate')); ?>:</b>
	<?php echo CHtml::encode($data->campaigndate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaigntime')); ?>:</b>
	<?php echo CHtml::encode($data->campaigntime); ?>
	<br />

	*/ ?>

</div>