<?php
/* @var $this DealstempController */
/* @var $model Dealstemp */

$this->breadcrumbs=array(
	'Dealstemps'=>array('index'),
	$model->dealid,
);

$this->menu=array(
	array('label'=>'List Dealstemp', 'url'=>array('index')),
	array('label'=>'Create Dealstemp', 'url'=>array('create')),
	array('label'=>'Update Dealstemp', 'url'=>array('update', 'id'=>$model->dealid)),
	array('label'=>'Delete Dealstemp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->dealid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dealstemp', 'url'=>array('admin')),
);
?>

<h1>View Dealstemp #<?php echo $model->dealid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'dealid',
		'athleteid',
		'agentid',
		'userid',
		'imageid',
		'dealtype',
		'cost',
		'description',
		'campaigndate',
		'campaigntime',
	),
)); ?>
