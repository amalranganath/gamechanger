<?php
/* @var $this DealstempController */
/* @var $model Dealstemp */

$this->breadcrumbs=array(
	'Dealstemps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Dealstemp', 'url'=>array('index')),
	array('label'=>'Manage Dealstemp', 'url'=>array('admin')),
);
?>

<h1>Create Dealstemp</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>