<?php
/* @var $this DealstempController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dealstemps',
);

$this->menu=array(
	array('label'=>'Create Dealstemp', 'url'=>array('create')),
	array('label'=>'Manage Dealstemp', 'url'=>array('admin')),
);
?>

<h1>Dealstemps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
