<?php
/* @var $this DealstempController */
/* @var $model Dealstemp */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'dealid'); ?>
		<?php echo $form->textField($model,'dealid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'athleteid'); ?>
		<?php echo $form->textField($model,'athleteid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agentid'); ?>
		<?php echo $form->textField($model,'agentid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'imageid'); ?>
		<?php echo $form->textField($model,'imageid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealtype'); ?>
		<?php echo $form->textField($model,'dealtype'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cost'); ?>
		<?php echo $form->textField($model,'cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'campaigndate'); ?>
		<?php echo $form->textField($model,'campaigndate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'campaigntime'); ?>
		<?php echo $form->textField($model,'campaigntime'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->