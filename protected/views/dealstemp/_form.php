<?php
/* @var $this DealstempController */
/* @var $model Dealstemp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dealstemp-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'athleteid'); ?>
		<?php echo $form->textField($model,'athleteid'); ?>
		<?php echo $form->error($model,'athleteid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agentid'); ?>
		<?php echo $form->textField($model,'agentid'); ?>
		<?php echo $form->error($model,'agentid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
		<?php echo $form->error($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'imageid'); ?>
		<?php echo $form->textField($model,'imageid'); ?>
		<?php echo $form->error($model,'imageid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealtype'); ?>
		<?php echo $form->textField($model,'dealtype'); ?>
		<?php echo $form->error($model,'dealtype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost'); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campaigndate'); ?>
		<?php echo $form->textField($model,'campaigndate'); ?>
		<?php echo $form->error($model,'campaigndate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campaigntime'); ?>
		<?php echo $form->textField($model,'campaigntime'); ?>
		<?php echo $form->error($model,'campaigntime'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->