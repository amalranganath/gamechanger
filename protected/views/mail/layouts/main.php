<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
    </head>
    <body style="margin:0px;padding:0px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px;">
        <table style="width:100% !important; min-width: 100%; margin:0; padding:0;">
            <tr>
                <td align="center" valign="top">
                    <center>
                        <table style="background: #D9F0FC; padding: 0px;width: 100%;position: relative;">
                            <tr>
                                <td align="center" style="width: 100%; min-width: 580px;">

                                    <table style="width:580px;margin:0 auto;text-align:inherit">
                                        <tr>
                                            <td style="padding:10px 20px 0px 0px;padding-right:0px">

                                                <table style="width:580px">
                                                    <tr>
                                                        <td style="text-align:left">
                                                            <?php echo CHtml::image(Yii::app()->getBaseUrl(true) . '/images/email-logo.png', 'Logo'); ?>                                                                      
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>                                                            

                                </td>
                            </tr>
                        </table>

                        <table style="width: 580px;margin: 0 auto;text-align: inherit;">
                            <tr>
                                <td>

                                    <table style="padding: 0px;width:100%;position: relative;">
                                        <tr>
                                            <td class="wrapper last" style="padding: 10px 20px 0px 0px; position: relative;">

                                                <table class="twelve columns" style="width: 580px;">
                                                    <tr>
                                                        <td>
                                                            <?php echo $content; ?>
                                                        </td> 
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>

                                    <table class="row footer" style="padding: 0px;  width: 100%; position: relative;">
                                        <tr>
                                            <td class="wrapper" style="padding: 10px 20px 0px 0px; position: relative; vertical-align:top;">

                                                <table class="six columns" style="vertical-align:top;">
                                                    <tr>
                                                        <td class="left-text-pad">

                                                            <table class="six columns">
                                                                <tr>
                                                                    <td class="last right-text-pad contact-info">                                                          
                                                                        <p><?php //echo CHtml::image(Yii::app()->getBaseUrl(true) . '/img/email-logo-admin.png', 'Logo');    ?></p>
                                                                        <p><?php //echo CHtml::image(Yii::app()->getBaseUrl(true) . '/img/email-logo-wording.png', 'Logo');    ?></p>																		
                                                                        <p style="margin-bottom:5px; margin-top:0px"> <span style="margin-right:20px;color:#29b6f6; font-weight:bold;">address</span>No 125 The Strand Road, Colombo, Sri Lanka</p>
                                                                        <p style="margin-bottom:5px; margin-top:0px"> <span style="margin-right:20px;color:#29b6f6; font-weight:bold;">telephone</span>+94 9 377 1985</p>
                                                                        <p style="margin-bottom:5px; margin-top:0px"> <span style="margin-right:20px;color:#29b6f6; font-weight:bold;">fax</span>+94 21 861 697</p>
                                                                        <p style="margin-bottom:5px; margin-top:0px"> <span style="margin-right:20px;color:#29b6f6; font-weight:bold;">email</span><a href="mailto:admin@gamechangerworldwide.com">admin@gamechangerworldwide.com</a></p>                                                                        
                                                                        <p style="margin-bottom:5px; margin-top:0px"> <span style="margin-right:20px;color:#29b6f6; font-weight:bold;">web site</span><a href="http://gamechangerworldwide.com/">gamechangerworldwide.com</a></p> 

                                                                    </td>
                                                                    <td class="expander"></td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                        <td class="expander"></td>
                                                    </tr>
                                                </table>

                                            </td>
                                            <td class="wrapper last" style="padding: 10px 20px 0px 0px; position: relative; vertical-align:top;">
                                            </td>
                                        </tr>
                                    </table>



                                    <!-- container end below -->
                                </td>
                            </tr>
                        </table>
                        
                        <table style="background: #D9F0FC; padding: 0px;width: 100%;position: relative;">
                            <tr>
                                <td align="center" style="width: 100%; min-width: 580px;">

                                    <table style="width:580px;margin:0 auto;text-align:inherit">
                                        <tr>
                                            <td class="wrapper last" style="padding: 10px 20px 0px 0px; position: relative; vertical-align:top; width:100%;">
                                                <table style="position: relative; vertical-align:top; width:100%;">
                                                    <tr>
                                                        <td align="center">
                                                            <center>
                                                                <p class="disclaimer" style="margin:0 0 0 5px;">Copyright © 2015 Game Changer World Wide.</p>
                                                            </center>
                                                        </td>
                                                        <td class="expander"></td>
                                                    </tr>
                                                </table>
                                                <table class="twelve columns" style="position: relative; vertical-align:top; width:100%;">
                                                    <tr>
                                                        <td align="center" style="position: relative; vertical-align:top; width:100%;">
                                                            <center>
                                                                <p style="text-align:center; margin:0px;text-decoration:none;">
                                                                    <a href="<?php echo Yii::app()->createAbsoluteUrl('site/contact') ?>" style="margin:0px;text-decoration:none;">Contact Us</a> | <a href="<?php echo Yii::app()->createAbsoluteUrl('site/policy') ?>" style="margin:0px;text-decoration:none;">Policy</a> 
                                                                </p>
                                                            </center>
                                                        </td>
                                                        <td class="expander"></td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                        
                    </center>
                </td>
            </tr>
        </table>
    </body>
</html>