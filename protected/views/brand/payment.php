
<?php
/* @var $this PaymentController */
/* @var $model Payment */

//$this->breadcrumbs = array( 'Payments' => array('index'), 'Manage', );
//$this->menu = array(
//    array('label' => 'List Payment', 'url' => array('index')),
//    array('label' => 'Create Payment', 'url' => array('create')),
//);
?>

<div class="container-fluid">
    <div class="container brand_chart">

        <div class="col-md-12 panel-body">

            <h1 class="page-header text-center text-uppercase">My Payments</h1>
            <?php
            $this->widget('EGridView', array(
                'id' => 'payment-grid',
                'dataProvider' => $orders->searchUser(),
                //'filter' => $orders,
                'rowDisplayRelated' => array('model' => new Deals, 'on' => 'orderid',
                    'columns' => array(
                        'dealid', 'dealType', array('cost',array('class' => 'text-right')), 'agentName', 'AthleteName', 'status.dealstatus'),
                ),
                'columns' => array(                   
                    array(
                        'name' => 'orderid',
                        'filter' => false,
                    ),
                    //'paymentid',
                    //'userid',                    
                    array(
                        'name' => 'payment.time',
                        'header' => 'Date & Time',
                        'filter' => false,
                    ),
                    //'payment.token',
                    //'payerid',
                    array(
                        'name' => 'payment.transactionid',
                        'filter' => false,
                    ),
                    //'ipaddress',
                    array(
                        'name' => 'payment.amount',
                        'header' => 'Deal Cost (AUD)',
                        'htmlOptions' => array('class' => 'text-right'),
                        'filter' => false,
                    ),
                    array(
                        'name' => 'payment.captured',
                        'header' => 'Charged Amount (AUD)',
                        'htmlOptions' => array('class' => 'text-right'),
                        'filter' => false,
                    ),
                    array(
                        //'name' => '',
                        'header' => 'Released Amount (AUD)',
                        'value' => '($data->payment->status > 2) ? $data->payment->amount - $data->payment->captured : "" ',
                        'htmlOptions' => array('class' => 'text-right'),
                    ),
                    //'msg',
                    array(
                        'name' => 'payment.status',
                        'value' => '$data->payment->getStatus()',
                        //'filter' => $model->getStatus(true),
                    ),
                //array(
                //    'class' => 'CButtonColumn',
                //),
                ),
            ));
            ?>
        </div>
    </div>
</div>