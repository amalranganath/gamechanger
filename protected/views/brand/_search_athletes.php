
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
    <?php
    $link = Yii::app()->createUrl('/athletes/profile/', array('id'=> $data->athleteid));
    $image = ($data->image) ? Yii::app()->baseUrl . '/images/athlete/' . $data->image->imagename : Yii::app()->baseUrl . '/images/default/default.jpg';
    ?>
    <div class="property-box-simple">
        <div class="property-box-image ">
            <a href="<?php echo $link ?>" class="property-box-simple-image-inner">
                <img src="<?php echo $image; ?>" class="attachment-post-thumbnail wp-post-image" alt="17">
                <span class="property-badge col-xs-12 property-table-actions">
                    <span class="col-xs-6"><i class="fa fa-twitter fa-lg success"></i>&nbsp;&nbsp;<?php echo '$ ' . $data->costpertweet; ?></span>
                    <span class="col-xs-6"><i class="fa fa-instagram fa-lg success"></i>&nbsp;&nbsp;<?php echo '$ ' . $data->costperinstagram; ?></span>
                </span>
            </a>

            <div class="property-box-simple-actions">
                <a href="<?php echo $link ?>"><h5 class="text-uppercase text-center"><i class="fa fa-link fa-lg"></i>&nbsp;View Profile</h5></a>

            </div><!-- /.property-box-simple-actions -->
        </div><!-- /.property-box-image -->

        <div class="property-box-simple-header">
            <h2><a href="<?php echo $link ?>"><?php echo $data->firstname . ' ' . $data->lastname; ?></a></h2>
        </div><!-- /.property-box-simple-header -->
        <div class="property-box-simple-meta">

            <ul>
                <li><i class="fa fa-wifi fa-2x font_color"></i>Reach<strong><?php echo ($data->reach > 1000) ? floor($data->reach / 1000) . 'K' : $data->reach; ?></strong></li>
                <li><i class="fa fa-hand-o-left fa-2x font_color"></i><span>Engagement</span><strong><?php echo $data->engagement ?></strong></li>
            </ul>
        </div> <!--/.property-box-simple-meta -->
    </div><!-- /.property-box-simple -->
</div>