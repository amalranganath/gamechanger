<?php
//$Athlete = Athletes::model()->find('athleteid=' . $data->athleteid);
//$Athleteimgname = Lookupimages::model()->find('imageid=' . $Athlete->imageid)->imagename;
?>
<div class="panel-body  directory-info-row">
    <div class="list-group">
        <div class="row">
            <div class="col-lg-12 col-xs-12 pull-left div-space">
                <h3 class="panel-body img-name white-color"><?php echo $data->athlete->firstname . ' ' . $data->athlete->lastname; ?>
                    <i class="fa fa-dollar icon-place-cart">
                        <?php echo ($data->dealtype == 0) ? $data->costpertweet + $data->costpertweetimg : $data->costperinstagram + $data->costperinstagramimg; ?>
                    </i>
                </h3>
                <img src="<?php echo Yii::app()->baseUrl; ?>/images/athlete/<?php echo $data->athlete->image->imagename; ?>" alt="<?php echo $data->athlete->firstname . ' ' . $data->athlete->lastname; ?>" class="img-profile img-thumbnail" style="left: 10px">
                <div class="bottomer panel-body">
                    <div class="col-md-3 col-xs-4">
                        <img src="<?php echo Yii::app()->baseUrl.'/images/'; echo $data->image ? 'deals/' . $data->image->imagename : 'upload_img.png'; ?>" alt="Deal Image" class="img-deals img-thumbnail" id="">
                    </div>
                    <div class="col-md-9 col-xs-8">
                        <div class="col-lg-12 col-xs-12">
                            <?php
                            echo ($data->dealtype == 0) ? '<i class="fa fa-twitter fa-3x"></i> ' : '<i class="fa fa-instagram fa-3x"></i>';
                            ?>
                        </div>
                        <div class="col-xs-12 border"><h3><?php echo $data->campaigndate . ' ' . substr($data->campaigntime, 0, 5) . ' ' . $data->ampm; ?></h3> </div>
                        <div class="col-xs-12 border"><h4></h4><p><?php echo $data->description; ?></p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>