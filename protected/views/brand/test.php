<?php $data=5; $data2=50; ?>
<div class="container-fluid">
    <div class="container brand_chart">
        <div class="col-md-12 panel-body">
            <ul class="nav nav-pills nav-justified text-uppercase" role="tablist" id="myTab">
                <li role="presentation" class="active build_top"><a href="#build" aria-controls="build" role="tab" data-toggle="tab">Build</a></li>
                <li role="presentation" class="preview_top"><a href="#preview" aria-controls="preview" role="tab" data-toggle="tab">Preview</a></li>
            </ul>

        </div>
        <div class="col-md-12 panel-body">
            <div class="col-xs-6"><h4 class="lead">7 Athletes</h4></div>
            <div class="col-xs-6"><h4 class="pull-right lead">$1,200.00</h4></div>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="build">

                <!------Loop begin-------->


                    <div class="alert alert-dismissible fade in" role="alert">
                        <i class="fa fa-times-circle fa-2x close" data-dismiss="alert" aria-label="Close"></i>

                        <div class="col-md-12  panel panel-default">
                            <div class="vcenter">
                                <div class="col-md-2 col-sm-4 panel-body">
                                    <img src="images/sample1.jpg" class="thumbnail img-rounded">
                                </div>
                                <div class="col-md-8"><h4 class="text-uppercase"> Adam Smith</h4></div>
                                <div class="col-md-2"><h4>$0.00</h4></div>
                            </div>
                            <div class="panel-group" id="accordion1">
                                <div class="panel panel-default">
                                    <div class="panel-heading bg_color_dark collapsed" data-toggle="collapse"
                                         data-parent="#accordion" data-target="#collapse<?php echo $data2; ?>"
                                         aria-expanded="false">
                                        <h4 class="panel-title">
                                            <a> <i class="glyphicon glyphicon-plus"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?php echo $data2; ?>" class="panel-collapse collapse"
                                         aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <div id="myCarousel<?php echo $data2; ?>" class="carousel slide"
                                                 data-ride="carousel">


                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner">

                                                    <div class="item active">

                                                        <div class="panel-body">
                                                            <div class="col-md-12">
                                                                <h3 class="text-center">What type of deal would you like
                                                                    to create?</h3>
                                                            </div>
                                                            <div class="col-md-6 panel-body font_color text-center">
                                                                <i class="fa fa-twitter fa-5x"></i>

                                                                <h3>$139.61</h3>
                                                            </div>
                                                            <div class="col-md-6 panel-body font_color text-center">
                                                                <i class="fa fa-instagram fa-5x"></i>

                                                                <h3>$139.61</h3>

                                                                <p class="small text-danger">This athlete has not
                                                                    activated their Instagram account with us.</p>
                                                            </div>

                                                        </div>


                                                    </div>
                                                    <!-- End Item -->


                                                    <div class="item ">
                                                        <div class="panel-body">
                                                            <div class="col-md-12">
                                                                <h3 class="text-center">Would you like to add an
                                                                    image?</h3>
                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <a href="#"><img class="thumbnail center-block"
                                                                                 src="images/uplod_img.png"></a>
                                                                <br>

                                                                <p><i class="fa fa-info-circle text-info"></i> Adding an
                                                                    image will increase the price of the deal to $36!
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End Item -->
                                                    <div class="item">
                                                        <div class="panel-body">
                                                            <div class="col-md-12">
                                                                <h3 class="text-center">What would you like the athlete
                                                                    to say?</h3>
                                                            </div>

                                                            <div class="col-md-12 panel-body text-center">
                                                                <textarea rows="10" style="width:100%;"></textarea>

                                                                <p><i class="fa fa-info-circle text-info"></i> Must
                                                                    contain #ad</p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End Item -->
                                                    <div class="item">
                                                        <div class="panel-body signup_form">
                                                            <div class="col-md-12 panel-body text-center">
                                                                <h3>When would you like this deal to go out?</h3>
                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <label>Date:</label>
                                                                <input class="form-control" type="date">
                                                                <label>Time:</label>
                                                                <input class="form-control" type="time">

                                                                <p>All times are Central Standard Time.</p>
                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <p>Need it to go out as soon as the athlete accepts
                                                                    it?</p>

                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <button type="button"
                                                                        class="btn btn-default text-uppercase">INSTANT
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!-- End Item -->

                                                </div>
                                                <!-- End Carousel Inner -->

                                                <div class="col-xs-6 font_color" id="item-button">
                                                    <button type="button"
                                                            class="btn btn-default btn-default text-uppercase delete"
                                                            data-target="#myCarousel<?php echo $data2; ?>" role="button"
                                                            style="display: none;">Delete
                                                    </button>
                                                    <button type="button"
                                                            class="btn btn-default btn-default text-uppercase back"
                                                            data-target="#myCarousel<?php echo $data2; ?>" role="button" data-slide="prev"
                                                            style="">Back
                                                    </button>

                                                </div>
                                                <div class="col-xs-6 font_color" id="item-button">
                                                    <button type="button"
                                                            class="btn btn-default btn-default pull-right text-uppercase next"
                                                            data-target="#myCarousel<?php echo $data2; ?>" role="button"
                                                            data-slide="next" style="display: block;">next
                                                    </button>
                                                    <button type="button"
                                                            class="btn btn-default btn-default pull-right text-uppercase finish"
                                                            data-target="#myCarousel<?php echo $data2; ?>" role="button"
                                                            data-slide="next" style="display: none;">finish
                                                    </button>
                                                </div>


                                            </div>
                                            <!-- End Carousel -->

                                        </div>
                                        <!-- End accordion -->
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="alert alert-dismissible fade in" role="alert">
                        <i class="fa fa-times-circle fa-2x close" data-dismiss="alert" aria-label="Close"></i>

                        <div class="col-md-12  panel panel-default">
                            <div class="vcenter">
                                <div class="col-md-2 col-sm-4 panel-body">
                                    <img src="images/sample1.jpg" class="thumbnail img-rounded">
                                </div>
                                <div class="col-md-8"><h4 class="text-uppercase"> Adam Smith</h4></div>
                                <div class="col-md-2"><h4>$0.00</h4></div>
                            </div>
                            <div class="panel-group" id="accordion1">
                                <div class="panel panel-default">
                                    <div class="panel-heading bg_color_dark collapsed" data-toggle="collapse"
                                         data-parent="#accordion" data-target="#collapse<?php echo $data; ?>"
                                         aria-expanded="false">
                                        <h4 class="panel-title">
                                            <a> <i class="glyphicon glyphicon-plus"></i></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?php echo $data; ?>" class="panel-collapse collapse"
                                         aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <div id="myCarousel<?php echo $data; ?>" class="carousel slide"
                                                 data-ride="carousel">


                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner">

                                                    <div class="item active">

                                                        <div class="panel-body">
                                                            <div class="col-md-12">
                                                                <h3 class="text-center">What type of deal would you like
                                                                    to create?</h3>
                                                            </div>
                                                            <div class="col-md-6 panel-body font_color text-center">
                                                                <i class="fa fa-twitter fa-5x"></i>

                                                                <h3>$139.61</h3>
                                                            </div>
                                                            <div class="col-md-6 panel-body font_color text-center">
                                                                <i class="fa fa-instagram fa-5x"></i>

                                                                <h3>$139.61</h3>

                                                                <p class="small text-danger">This athlete has not
                                                                    activated their Instagram account with us.</p>
                                                            </div>

                                                        </div>


                                                    </div>
                                                    <!-- End Item -->


                                                    <div class="item ">
                                                        <div class="panel-body">
                                                            <div class="col-md-12">
                                                                <h3 class="text-center">Would you like to add an
                                                                    image?</h3>
                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <a href="#"><img class="thumbnail center-block"
                                                                                 src="images/uplod_img.png"></a>
                                                                <br>

                                                                <p><i class="fa fa-info-circle text-info"></i> Adding an
                                                                    image will increase the price of the deal to $36!
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End Item -->
                                                    <div class="item">
                                                        <div class="panel-body">
                                                            <div class="col-md-12">
                                                                <h3 class="text-center">What would you like the athlete
                                                                    to say?</h3>
                                                            </div>

                                                            <div class="col-md-12 panel-body text-center">
                                                                <textarea rows="10" style="width:100%;"></textarea>

                                                                <p><i class="fa fa-info-circle text-info"></i> Must
                                                                    contain #ad</p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End Item -->
                                                    <div class="item">
                                                        <div class="panel-body signup_form">
                                                            <div class="col-md-12 panel-body text-center">
                                                                <h3>When would you like this deal to go out?</h3>
                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <label>Date:</label>
                                                                <input class="form-control" type="date">
                                                                <label>Time:</label>
                                                                <input class="form-control" type="time">

                                                                <p>All times are Central Standard Time.</p>
                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <p>Need it to go out as soon as the athlete accepts
                                                                    it?</p>

                                                            </div>
                                                            <div class="col-md-12 panel-body text-center">
                                                                <button type="button"
                                                                        class="btn btn-default text-uppercase">INSTANT
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!-- End Item -->

                                                </div>
                                                <!-- End Carousel Inner -->

                                                <div class="col-xs-6 font_color" id="item-button">
                                                    <button type="button"
                                                            class="btn btn-default btn-default text-uppercase delete"
                                                            data-target="#myCarousel" role="button"
                                                            style="display: none;">Delete
                                                    </button>
                                                    <button type="button"
                                                            class="btn btn-default btn-default text-uppercase back"
                                                            data-target="#myCarousel" role="button" data-slide="prev"
                                                            style="">Back
                                                    </button>

                                                </div>
                                                <div class="col-xs-6 font_color" id="item-button">
                                                    <button type="button"
                                                            class="btn btn-default btn-default pull-right text-uppercase next"
                                                            data-target="#myCarousel<?php echo $data; ?>" role="button"
                                                            data-slide="next" style="display: block;">next
                                                    </button>
                                                    <button type="button"
                                                            class="btn btn-default btn-default pull-right text-uppercase finish"
                                                            data-target="#myCarousel<?php echo $data; ?>" role="button"
                                                            data-slide="next" style="display: none;">finish
                                                    </button>
                                                </div>


                                            </div>
                                            <!-- End Carousel -->

                                        </div>
                                        <!-- End accordion -->
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                    <!--loop end-->



            </div>
            <div role="tabpanel" class="tab-pane" id="preview">
                <div class="panel-body">
                    <div class="list-group">
                        <div class="col-sm-12 list-group-item vcenter">
                            <div class="col-md-2 col-sm-4 panel-body">
                                <img src="images/sample1.jpg" class="thumbnail img-rounded">
                            </div>
                            <div class="col-md-8"><h4 class="text-uppercase">Adam Smith</h4></div>
                            <div class="col-md-2"><h4>$0.00</h4></div>
                        </div>
                        <div class="col-xs-12 list-group-item">

                            <div class="col-md-12 text-center"><img src="images/flower.jpg" alt="flower" class="img-rounded thumbnail center-block"></div>
                            <div class="col-md-12 text-center"><h4>#ad description</h4></div>
                            <div class="col-xs-6 panel-body"><i class="fa fa-twitter fa-3x"></i></div>
                            <div class="col-xs-6 panel-body"><h3 class="pull-right">Instant </h3></div>

                        </div>


                    </div>






                </div>
            </div>

        </div>


        <div class="panel-body">
            <div class="col-xs-6 font_color">
                <a href="brand_marketplace.php" class="market btn btn-default btn-default text-uppercase" type="button">Market</a>
            </div>
            <div class="col-xs-6 font_color">
                <button type="button" class="one btn btn-default btn-default pull-right text-uppercase preview" aria-controls="Preview" role="tab" data-toggle="tab" data-target="#preview">next</button>
                <button type="button" class="two btn btn-default btn-default pull-right text-uppercase build" aria-controls="Build" role="tab" data-toggle="tab" data-target="#build">Back</button>
            </div>
        </div>





    </div>
    <!-- /.container -->
</div>

<script>


    //********* Market and next buttons ***********
    $('.one').click(function() {
        if ($(".one").is(":hidden")) {
            $(".one").show();
        } else {
            $(".one").hide();
            $(".two").show();
//   $(".market").hide();
        }

    });

    $('.two').click(function() {
        if ($(".two").is(":hidden")) {
            $(".two").show();
        } else {
            $(".two").hide();
            $(".one").show();
//    $(".market").show();
        }
    });



    //********* end of Market and next buttons ***********
    //********* carousel buttons ***********

    $('.next').click(function() {
        if ($(".next").is(":hidden")) {
            $(".next").show();
        } else {
            $(".delete").hide();
            $(".back").show();
        }

    });

    $('.back').click(function() {
        if ($(".carousel-inner .item:nth-child(2)").hasClass("active")) {
            $(".back").hide();
            $(".delete").show();

        } else if ($(".carousel-inner .item:nth-child(4)").hasClass("active")) {
            $(".next").show();
            $(".finish").hide();

        }
    });


    $('.next').click(function() {
        if ($(".carousel-inner .item:nth-child(3)").hasClass("active")) {
            $(".next").hide();
            $(".finish").show();
        }
    });


    //********* end of carousel buttons ***********


    $('.carousel').carousel({
        interval: false,
        wrap: false
    }) ;

    $(".build").click(function(){
        $(".preview_top").removeClass("active");
        $(".build_top").addClass("active");
    });

    $(".preview").click(function(){
        $(".build_top").removeClass("active");
        $(".preview_top").addClass("active");
    });



</script>