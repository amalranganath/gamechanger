<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */
?>
<div id="landing_wrapper">
    <div id="page-wrapper">
        <div class="signup_form">
            <div class="col-sm-12">
                <div class="panel text-center text-uppercase">
                    <h1>Registration Details</h1>
                </div>
            </div>
            <div class="form">
                <?php
                if (isset($_GET['usertype']) && isset($_GET['userid'])) {
                    ?>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'brand-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation' => false,
                    ));
                    ?>

                <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

                    <?php // echo $form->errorSummary($model);  ?>

                    <!--<div class="col-sm-6 text-center panel-body">-->
                    <?php $model->user_id = $_GET['userid']; ?>
                    <?php echo $form->hiddenField($model, 'user_id', array('placeholder' => 'user_id', 'class' => 'panel-body')); ?>
                    <?php // echo $form->error($model,'user_id');  ?>
                    <!--</div>-->

                    <div class="col-sm-6 text-center panel-body">
                        <?php // echo $form->labelEx($model,'firstname'); ?>
                        <?php echo $form->textField($model, 'firstname', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'First Name', 'class' => 'panel-body')); ?>
                        <?php echo $form->error($model, 'firstname'); ?>
                    </div>

                    <div class="col-sm-6 text-center panel-body">
                        <?php // echo $form->labelEx($model,'lastname'); ?>
                        <?php echo $form->textField($model, 'lastname', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Last Name', 'class' => 'panel-body')); ?>
                        <?php echo $form->error($model, 'lastname'); ?>
                    </div>

                    <div class="col-sm-6 text-center panel-body">
                        <?php // echo $form->labelEx($model,'brandname'); ?>
                        <?php echo $form->textField($model, 'brandname', array('size' => 60, 'maxlength' => 100, 'placeholder' => 'Brand Name', 'class' => 'panel-body')); ?>
                        <?php echo $form->error($model, 'brandname'); ?>
                    </div>

                    <div class="col-sm-6 text-center panel-body">
                        <?php // echo $form->labelEx($model,'siteurl'); ?>
                        <?php echo $form->textField($model, 'siteurl', array('size' => 60, 'maxlength' => 100, 'placeholder' => 'Site Url', 'class' => 'panel-body')); ?>
                        <?php echo $form->error($model, 'siteurl'); ?>
                    </div>

                    <div class="col-sm-6 text-center panel-body">
                        <?php // echo $form->labelEx($model,'description'); ?>
                        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 66, 'placeholder' => 'Description', 'class' => 'panel-body')); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </div>

                    <div class="row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'panel-body bg_color_hover btn')); ?>
                    </div>

                    <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
        </div>
        <?php
    } else {
        echo 'Please relink';
    }
    ?>