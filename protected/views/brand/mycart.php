<?php
$next = false;
if ($dataProvider->totalItemCount == $dataProvider2->totalItemCount) {
    $next = true;
}
?>
<div class="container-fluid">
    <div class="container brand_chart">
        <div class="row">
            <div class="col-md-12 panel-body">
                <ul class="nav nav-pills nav-justified text-uppercase" role="tablist" id="myTab">
                    <li role="presentation" class="active build_top first"><a href="#build" aria-controls="build" role="tab" data-toggle="tab">Step 1 - Build</a></li>
                    <li role="presentation" class="build_top second" ><a href="#<?php echo $next ? 'preview' : '' ?>" aria-controls="preview" role="tab" data-toggle="tab">Step 2 - Preview</a></li>
                    <li role="presentation" class="third"><a href="#<?php echo $next ? 'payment' : '' ?>" aria-controls="payment" role="tab" data-toggle="tab">Step 3 - Payment</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 panel-body">
                <div class="col-md-3 col-xs-12">
                    <span class="round-box">
                        <?php echo $dataProvider->totalItemCount; ?>
                    </span>
                    <span class="cart-count">&nbsp;Athlete<?php echo $dataProvider->totalItemCount>1 ? 's':''?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="build">
                    <!------Loop begin-------->
                    <div class="col-md-12">
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider,
                            'itemView' => '_mycart_view',
                            'template' => '{sorter} {items} {pager}', //{summary} {sorter} {items} {pager}
                        ));
                        ?>
                    </div>
                    <div class="clear"></div>
                    <div class="panel-body panel">
                        <div class="col-xs-6 font_color">
                            <a href="<?php echo Yii::app()->baseUrl; ?>/brand/marketplace" class="market btn btn-default btn-default text-uppercase" type="button">Market</a>
                        </div>
                        <div class="col-xs-6 font_color">
                            <?php if ($next) { ?>
                                <button type="button" class="preview btn btn-default pull-right" aria-controls="Preview" role="tab" data-toggle="tab" data-target="#preview">next</button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="preview">
                    <div class="col-md-12">

                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider2,
                            'itemView' => '_deal_preview',
                            'template' => '{sorter} {items} {pager}',
                        ));
                        ?>

                    </div>
                    <div class="panel-body">
                        <div class="col-xs-6 font_color">
                            <button type="button" class="build btn btn-default" aria-controls="Build" role="tab" data-toggle="tab" data-target="#build">Back</button>
                        </div>
                        <div class="col-xs-6 font_color">
                            <button type="button" class="payment btn btn-default pull-right" rel="tootip" title="Make Payment" aria-controls="Payment" role="tab" data-toggle="tab" data-target="#payment">next</button>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="payment">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <p>By clicking on Pay Now you will be directed to the Paypal secure site to authorize your payment. Please note your credit card will not be charged as this moment. It will only be charged at the acceptance of the Deal by the Agent. In the event of a Deal being rejected by the Agent, it will be refunded to you at the time the Order is being closed. </p>
                            <p class="note">Note:Orders are closed at the stage where all the deals in the order is being acted upon by the Agents..</p>

                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-xs-6 font_color">
                            <button type="button" class="preview btn btn-default" aria-controls="Preview" role="tab" data-toggle="tab" data-target="#preview">Back</button>
                        </div>
                        <div class="col-xs-6 font_color">
                            <?php
                            $Dealtemp = Dealstemp::model()->findAll(array(
                                'condition' => 'userid=' . Yii::app()->user->id . ' AND  completed=1 AND placetoorder=0',
                            ));

                            if ($Dealtemp) {
                                echo CHtml::link(
                                        'Pay Now', array('paypal/buy'), array('confirm' => 'Are you sure you want to pay for this order?', 'class' => 'btn btn-success pull-right', 'title' => 'Make a PayPal Payment')
                                );
                            } else {
                                echo CHtml::link(
                                        'Pay Now', array('#'), array('rel' => 'tooltip', 'class' => 'btn btn-success pull-right', 'title' => 'You have no any completed order to process')
                                );
                                //echo '<a href="#" class="btn btn-success" rel="tooltip" data-toggle="tooltip" title="You have not any completed order to process ">Place to Order</a>';
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.container -->
    </div>
</div>
<div class="howl" id="universal-msg" ></div>
<script>
    $(function () {
        $("[rel='tooltip']").tooltip();

        $('input[name="campaigndate"]').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('input[name="campaigntime"]').ptTimeSelect();
    });

    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah' + id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    //********* Market and next buttons ***********
    $('.one').click(function () {
        if ($(".one").is(":hidden")) {
            $(".one").show();
        } else {
            $(".one").hide();
            $(".two").show();
            //   $(".market").hide();
        }

    });

    $('.two').click(function () {
        if ($(".two").is(":hidden")) {
            $(".two").show();
        } else {
            $(".two").hide();
            $(".one").show();
            //    $(".market").show();
        }
    });


    //********* end of Market and next buttons ***********
    //********* carousel buttons ***********

    $('.back').click(function () {
        var btn_id = $(this).attr('href');
        //    alert(btn_id);
        if ($(btn_id + ' .carousel-inner .item:nth-child(2)').hasClass('active')) {
            $(btn_id + ' .back').hide();
            $(btn_id + ' .delete').show();

        } else if ($(btn_id + ' .carousel-inner .item:nth-child(4)').hasClass('active')) {
            $(btn_id + ' .next').show();
            $(btn_id + ' .finish').hide();

        }
    });


    $('.next').click(function () {
        var btn_id = $(this).attr('href');
        //     alert(btn_id+' .delete');
        if ($(btn_id + ' .carousel-inner .item:nth-child(3)').hasClass('active')) {
            $(btn_id + ' .next').hide();
            $(btn_id + ' .finish').show();
        }

        else if ($(btn_id + ' .carousel-inner .item:nth-child(1)').hasClass('active')) {
            $(btn_id + ' .delete').hide();
            $(btn_id + ' .back').show();
        }
    });


    //********* end of carousel buttons ***********

    //    $('.carousel').carousel({
    //        interval: false,
    //        wrap: false
    //    });

    $(".build").click(function () {
        $(".second").removeClass("active");
        $(".first").addClass("active");
    });

    $(".preview").click(function () {
        $(".first").removeClass("active");
        $(".second").addClass("active");
        $(".third").removeClass("active");
    });

    $(".payment").click(function () {
        $(".second").removeClass("active");
        $(".third").addClass("active");
    });
    //for form submit success message

</script>