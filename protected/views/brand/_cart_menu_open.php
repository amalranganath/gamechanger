
<li class="dropdown top_cart list-unstyled open">
    <?php
    $Dealtemp = Dealstemp::model()->findAllByAttributes(array(
        'userid' => Yii::app()->user->id,
        'placetoorder' => 0,
    ));
    $count = ($Dealtemp == true) ? count($Dealtemp) : '0';
    ?>
    <script type="text/javascript">

        $(document).ready(function () {

            window.setTimeout(function () {
                $("#already-in-cart").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 900);

        });
    </script>

    <a href="#" class="dropdown-menu-right" data-toggle="dropdown" aria-expanded="false">
        <span class="cart_count"><?php echo $count; ?></span><i class="fa fa-shopping-cart"></i><b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
        <?php
        if ($already_in == true) {

            echo '<li><div class="alert alert-warning" id="already-in-cart" style="margin-left: 10%; margin-right: 10%">
                        <center>Already in cart !</center>
                       </div>
                  </li>';
        }
        ?>
        <?php
        foreach ($Dealtemp as $key => $value) {
            //$Athlete = Athletes::model()->find('athleteid=' . $value["athleteid"]);
            //$imagename = Lookupimages::model()->find('imageid=' . $Athlete->imageid);
            $image = ($value->athlete->image) ? Yii::app()->baseUrl . '/images/athlete/200x150/' . $value->athlete->image->imagename : Yii::app()->baseUrl . '/images/default/default.jpg';
            ?>

            <li class="list-unstyled">
                <div class="panel-body">
                    <div class="col-xs-4">
                        <img class="center-block" src="<?php echo $image; ?>" alt="<?php echo $value->athlete->firstname . ' ' . $value->athlete->lastname; ?>">
                    </div>
                    <div class="col-xs-8">
                        <div class="row">
                            <div class="col-xs-12 text-left">
                                <p class="text-left">
                                    <a href="#"><?php echo $value->athlete->firstname . ' ' . $value->athlete->lastname; ?></a>   
                                </p>
                            </div>
                            <div class="col-xs-6 text-left">
                                <i class="fa fa-twitter"></i><?php echo '$' . $value->athlete->costpertweet; ?>
                            </div>
                            <div class="col-xs-6">
                                <i class="fa fa-instagram"></i><?php echo '$' . $value->athlete->costperinstagram; ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="border"></div>
            </li>
            <?php
        }
        ?>
        <?php
        if ($count == 0) {
            echo '<li><center><span class="label label-default">Your cart is empty</span><center></li><br>';
        }
        ?>
        <li class="list-unstyled">
            <div class="panel-body">

                <div class="col-xs-12 text-center">
                    <a href="<?php echo Yii::app()->baseUrl; ?>/index.php/brand/mycart"
                       class="btn btn-link viewcart" style="">View Cart</a>
                </div>
            </div>
        </li>

    </ul>
</li>