<span class="call-us">

    <div id="shopping-cart-update">
        <li class="dropdown top_cart list-unstyled ">
            <?php
            $Dealtemp = Dealstemp::model()->findAllByAttributes(array(
                'userid' => Yii::app()->user->id,
                'placetoorder' => 0,
            ));
            $count = ($Dealtemp == true) ? count($Dealtemp) : 0;
            ?>

            <a href="#" class="dropdown-menu-right" data-toggle="dropdown"aria-expanded="false">
                <span class="cart_count"><?php echo $count; ?></span><i class="fa fa-shopping-cart"></i><b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <?php
                foreach ($Dealtemp as $key => $value) {
                    //CVarDumper::dump($value->athlete, 10000, true);
                    //$Athlete = Athletes::model()->find('athleteid=' . $value["athleteid"]);
                    //$imagename = Lookupimages::model()->find('imageid=' . $Athlete->imageid);
                    $image = ($value->athlete->image) ? Yii::app()->baseUrl . '/images/athlete/200x150/' . $value->athlete->image->imagename : Yii::app()->baseUrl . '/images/default/default.jpg';
                    ?>

                    <li class="list-unstyled">
                        <div class="panel-body">
                            <div class="col-xs-4">
                                <img class="center-block" src="<?php echo $image; ?>" alt="<?php echo $value->athlete->firstname . ' ' . $value->athlete->lastname; ?>">
                            </div>
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-xs-12 text-left">
                                        <a href="#"><?php echo $value->athlete->firstname . ' ' . $value->athlete->lastname; ?></a>                                        
                                    </div>
                                    <div class="col-xs-6 text-left">
                                        <i class="fa fa-twitter"></i><?php echo '$' . $value->athlete->costpertweet; ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <i class="fa fa-instagram"></i><?php echo '$' . $value->athlete->costperinstagram; ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="border"></div>
                    </li>
                <?php } ?>
                <?php if ($count == 0) { ?>
                    <li class="text-center">
                        <span class="label label-default">Your cart is empty</span>
                    </li>
                <?php } else { ?>
                    <li class="list-unstyled">
                        <div class="panel-body">
                            <div class="col-xs-12 text-center">
                                <a href="<?php echo Yii::app()->baseUrl; ?>/brand/mycart" class="btn btn-link viewcart" style="">View Cart</a>
                            </div>
                        </div>
                    </li>
                <?php } ?>

            </ul>
        </li>
    </div>
</span>
