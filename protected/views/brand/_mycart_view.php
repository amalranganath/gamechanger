<?php /* */ ?>
<!--<div id="display" xmlns="http://www.w3.org/1999/html"></div>-->
<div class="alert alert-dismissable fade in" role="alert">
    <i class="fa fa-times-circle fa-2x close" data-dismiss="alert" aria-label="Close" id="alert<?php echo $data->dealid; ?>"></i>

    <script type="text/javascript">
        $('#alert<?php echo $data->dealid; ?>').on('click', function () {

            $.ajax({
                url: 'AjaxDelete',
                type: 'POST',
                cache: false,
                async: true,
                data: {dealid:<?php echo $data->dealid; ?>, athleteid:<?php echo $data->athleteid; ?>},
                success: function (result) {
                    $(this).remove();
                    $('#universal-msg').html('<div class="howl"><div class="alert howl-slot howl-has-icon" id="alertsuccess">' +
                            ' <div class="howl-message howl-success">' +
                            ' <button href="#" class="close howl-close" data-dismiss="alert" aria-label="close">&times;</button>' +
                            '<div class="howl-message-inner"><h5 class="howl-title">Success Alert</h5>' +
                            'Athlete has been removed from cart </div> <i class="howl-icon fa fa-check-square-o"></i>' +
                            '</div>' +
                            '</div></div>');
                    $("#alertsuccess").fadeTo(5000, 500).slideUp(500, function () {
                        location.reload();
                        //$("#alertsuccess").alert('close');
                    });

                    //$('#shopping-cart-update').html(result);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $('#display').html('<div class="alert alert-danger" id="alertfail" role="alert">Error! ' + textStatus + '</div>');
                    $("#alertfail").fadeTo(2000, 500).slideUp(500, function () {
                        //$("#alertfail").alert('close');
                    });
                }

            });
            //  }
        });

    </script>


    <div class="panel-default panel-body header-color">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <?php
                //CVarDumper::dump($data, 1000, true);
                //$Athlete = Athletes::model()->find('athleteid=' . $data->athleteid);
                //$imagename = Lookupimages::model()->find('imageid=' . $data->athlete->imageid);
                $image = ($data->athlete->image->imagename) ? '/images/athlete/400x300/' . $data->athlete->image->imagename : '/images/default/default.jpg';
                ?>
                <div class="property-box-simple">
                    <div class="property-box-image property-box-simple-image-inner">
                        <img src="<?php echo Yii::app()->baseUrl . $image; ?>" class="property-box-simple-image-inner" alt="Athlete Image">
                    </div>

                    <div class="property-box-simple-header">
                        <h2><a href="#"><?php echo $data->athlete->firstname . ' ' . $data->athlete->lastname; ?></a></h2>
                    </div>
                    <div class="follower-foot">
                        <div class="row">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-3"><strong>Cost</strong></div>
                            <div class="col-xs-5"><strong>Image Cost</strong></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 text-center"><i class="fa fa-twitter fa-2x font_color"></i> </div>
                            <div class="col-xs-3">$ <?php echo $data->athlete->costpertweet; ?></div>
                            <div class="col-xs-5">$ <?php echo $data->athlete->costpertweetimg; ?></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 text-center"><i class="fa fa-instagram fa-2x font_color"></i> </div>
                            <div class="col-xs-3">$ <?php echo $data->athlete->costperinstagram; ?></div>
                            <div class="col-xs-5">$ <?php echo $data->athlete->costperinstagramimg; ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-6">
                <form method="post" enctype="multipart/form-data" id="form<?php echo $data->athleteid; ?>" class="" action="">
                    <div class="row">
                        <div class="col-lg-4 col-md-5 col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center">What type of deal would you like to create?</h3>
                                </div>
                                <div class="weather-border">
                                    <ul>
                                        <?php if ($data->athlete->costpertweet != 0) { ?>
                                            <li class="active">
                                                <input name="radio<?php echo $data->dealid; ?>" type="radio" value="twitter" id="optionone<?php echo $data->dealid; ?>" <?php echo $data->completed && $data->dealtype == 0 ? 'checked="checked"' : ''; ?>>
                                                    <label for="optionone<?php echo $data->dealid; ?>" class="font_color panel-body">
                                                        <i class="fa fa-twitter fa-3x"></i>
                                                    </label>
                                            </li>
                                        <?php } ?>
                                        <?php if ($data->athlete->costperinstagram != 0) { ?>
                                            <li class="border_left">
                                                <input name="radio<?php echo $data->dealid; ?>" type="radio" value="instagram" id="optiontwo<?php echo $data->dealid; ?>" <?php echo $data->completed && $data->dealtype == 1 ? 'checked="checked"' : ''; ?>>
                                                    <label for="optiontwo<?php echo $data->dealid; ?>" class="font_color panel-body">
                                                        <i class="fa fa-instagram fa-3x"></i>
                                                    </label>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-8">
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center"><a href="#" data-toggle="tooltip" data-placement="top" title="only JPG & PNG files are allowed"><i class="fa fa-info-circle"></i></a>&nbsp; Would you like to add an image? <span id="image-cost" class=""></span></h3>
                                </div>
                                <div class="upload_image">
                                    <img class="center-block" src="<?php
                                    echo Yii::app()->baseUrl . '/images/';
                                    echo $data->image ? 'deals/' . $data->image->imagename : 'upload_img.png'
                                    ?>" id="blah<?php echo $data->dealid; ?>">
                                        <input type="file" name="image" id="imgInp<?php echo $data->dealid; ?>" multiple="" />
                                        <input type="text" hidden="hidden" name="dealid" value="<?php echo $data->dealid; ?>" />
                                </div>
                                <p style="color: #F5AEAE;margin: 0 auto 11px; text-align: center"><i class="fa fa-info-circle"></i> only JPG &amp; PNG files are allowed</p>
                                <!--<h6 class="text-center"><i class="fa fa-info-circle text-info"></i></h6>-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center">What would you like the athlete to say?</h3>
                                </div>
                                <div class="form-group panel-body">
                                <!--<textarea name="txtarea" rows="10" style="width:100%;" required="required"></textarea>-->
                                    <textarea name="txtarea" class="form-control" placeholder="Description" style="overflow: hidden; word-wrap: break-word;" rows="3" ><?php echo $data->description; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center">When would you like this deal to go out?</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-lg-6 col-md-12 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="campaigndate" value="<?php echo $data->completed ? $data->campaigndate : ''; ?>" readonly placeholder="Date" />
                                            <!--<input  class="form-control" name="subject" placeholder="Date" type="date"  />-->
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="campaigntime" value="<?php echo $data->completed ? substr($data->campaigntime, 0, 5) . ' ' . $data->ampm : ''; ?>"  readonly placeholder="Time" />
                                            <!--<input  class="form-control" name="subject" placeholder="Time" type="time"  />-->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-default btn-default pull-right text-uppercase finish" onclick="$(this).closest('form#form<?php echo $data->athleteid; ?>').submit()"  href="#myCarousel" data-slide="next" style="display:none">finish</a>

                            <button type="button" onclick="$(this).closest('form#form<?php echo $data->athleteid; ?>').submit()"  class="pull-right btn btn-danger text-uppercase center-block" aria-controls="Preview" ><?php echo $data->completed ? 'Update' : 'Save' ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!--close-->

</div>
<!------ajax form submit js------->
<script type="text/javascript">

    $("#imgInp<?php echo $data->dealid; ?>").change(function () {
        readURL(this,<?php echo $data->dealid; ?>);
    });

    //form submit from ajax
    $("form#form<?php echo $data->athleteid; ?>").submit(function () {

        var formData = new FormData($(this)[0]);

        $.ajax({
            url: 'FormSubmit',
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {

                var json = JSON.parse(data);
                //                alert(json["type"]);

                if (json["type"] == "success") {
                    //alert(json["type"]);
                    $('#universal-msg').html('<div class="alert alert-success" id="formsuccess-msg" role="">' + json["alert"] + '</div>');
                    $("#universal-msg").fadeTo(5000, 800).slideUp(1000, function () {
                        //$("#universal-msg").alert('close');
                        location.reload();
                    });
                    //setTimeout(function () {
                    //location.reload();
                    //}, 3000);

                } else {
                    //alert(json["type"]);
                    $('#universal-msg').html('<div class="alert alert-danger" id="formsuccess-msg" role="">' + json["alert"] + '</div>');
                    $("#universal-msg .alert").fadeTo(5000, 800).slideUp(1000, function () {
                        //$(this).alert('close');
                    });
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });

</script>