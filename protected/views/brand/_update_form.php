<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */
?>

<div id="page-wrapper">
    <div class="signup_form">
        <h1 class="page-header text-uppercase">Registration Details</h1>

        <div class="form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'brand-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => false,
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data'),
            ));
            ?>

            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php // echo $form->errorSummary($model);  ?>

            <!--<div class="col-sm-6 text-center panel-body">-->
            <?php //$model->user_id=$_GET['userid']; ?>
            <?php // echo $form->textField($model,'user_id',array('placeholder'=>'user_id','class'=>'panel-body')); ?>
            <?php //echo $form->hiddenField($model, 'user_id'); ?>
            <?php //  echo $form->error($model,'user_id');  ?>
            <!--</div>-->
            <?php
            $getagent = Brand::model()->findAllByAttributes(array('user_id' => Yii::app()->user->id));
            //Yii::app()->user->setFlash('complete', "please fill your account detail to enable Navigation");             

            if (!empty($getagent)) {
                if ($getagent[0]['completed'] == 0) {//if agent
                    echo '<div class="alert alert-info fade in">Please fill your account detail to enable navigation</div>';
                    //Yii::app()->user->setFlash('complete', "please fill your account detail to enable Navigation");             
                }
            }
            ?>
            <h3><?php echo 'Email: ' . $model->user->email; ?></h3>

            <div class="row">
                <div class="col-sm-6 text-center panel-body">
                    <?php echo $form->labelEx($model, 'firstname'); ?>
                    <?php echo $form->textField($model, 'firstname', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'First Name', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'firstname'); ?>
                </div>

                <div class="col-sm-6 text-center panel-body">
                    <?php echo $form->labelEx($model, 'lastname'); ?>
                    <?php echo $form->textField($model, 'lastname', array('size' => 60, 'maxlength' => 255, 'placeholder' => 'Last Name', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'lastname'); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6 text-center panel-body">
                    <?php echo $form->labelEx($model, 'brandname'); ?>
                    <?php echo $form->textField($model, 'brandname', array('size' => 60, 'maxlength' => 100, 'placeholder' => 'Brand Name', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'brandname'); ?>
                </div>

                <div class="col-sm-6 text-center panel-body">
                    <?php echo $form->labelEx($model, 'siteurl'); ?>
                    <?php echo $form->textField($model, 'siteurl', array('size' => 60, 'maxlength' => 100, 'placeholder' => 'Site Url', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'siteurl'); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model,'description');  ?>
                    <?php echo $form->textArea($model, 'description', array('rows' => 5, 'class' => 'form-control panel-body', 'placeholder' => 'Description',)); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
            </div>

            <div class="row">   
                <div class="col-sm-12  panel-body"><?php echo $form->labelEx($model,'imageid', array());  ?></div>
                <div class="col-sm-6 text-center ">
                    <?php echo CHtml::activeFileField($lookupimages, 'imagename', array('class' => 'form-control',)); ?>
                    <span>Please use only (400px X 400px) ratio.</span>
                    <?php echo $form->error($lookupimages, 'imagename'); ?>
                    <?php
//         $this->widget('CMultiFileUpload', array(
//                        'model'=>$model,
//                        'name' => 'Name',
//                        'max'=>5,
//                        'accept' =>'jpg',
//                        'duplicate' => 'Duplicate file!',
//                        'denied' => 'Invalid file type',
////                        'options'=>array(
////                            'onFileSelect'=>'function(e ,v ,m){
////                                var fileSize=$("#Name")[0].files[0].size;
////                                if(fileSize>1024){//5MB
////                                    alert("Image too large");
////                                    $("#importprice-form").reset();
////                                    return false;
////                                }
////                            }'
////                        ),
//                        'htmlOptions'=>array(
//                            'class'=>'form-control',
//                            'style'=>'width:100%',
//                            'onchange'=>'$gettxt',
//
//
//                        ),
//                    ));
                    ?>
                </div>                
                <div class="col-sm-6 text-center ">
                    <?php
                    if ($model->image) {
                        ?>
                        <div class="col-sm-12 text-center panel-body"> 
                            <div class="col-sm-12">
                                <img src="<?php echo Yii::app()->baseUrl . '/images/brands/' . $model->image->imagename; ?>"> 
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">        
            <div class="col-sm-12 text-center panel-body"> 
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'panel-body bg_color_hover btn')); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>

