<div class="col-lg-12 panel-body">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-uppercase text-center">Athletes with similar reach</h3>
        </div>
        <div class="panel-body">


            <!--        <div class="col-md-3">-->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item text-center active">

                        <div class="col-lg-2 col-xs-4 col-lg-offset-1">
                            <div class="slide-img-lable">
                                <?php
                                //                        echo $data->firstname.' '.$data->lastname;
                                $Lookupimage=Lookupimages::model()->find('imageid='.$data->imageid)
                                ?>

                                <a href=""><img src="<?php echo Yii::app()->baseUrl.'/images/athlete/'.$Lookupimage->imagename;   ?>" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>
                                </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                           </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                           </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                          </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                          </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item text-center">
                        <div class="col-lg-2 col-xs-4 col-lg-offset-1">
                            <div class="slide-img-lable">
                                <?php
                                //                        echo $data->firstname.' '.$data->lastname;
                                $Lookupimage=Lookupimages::model()->find('imageid='.$data->imageid)
                                ?>

                                <a href=""><img src="<?php echo Yii::app()->baseUrl.'/images/athlete/'.$Lookupimage->imagename;   ?>" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>
                                </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                          </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                          </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                          </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <div class="slide-img-lable">

                                <a href=""><img src="/endorsnew/images/athlete/9671433147440.jpg" style="display: inline-block;"> </a>
                                <div class="topper text-uppercase">
                                    <?php echo $data->firstname.' '.$data->lastname;?>                          </div>
                                <div class="bottomer">
                                    Athlete Profile
                                </div>
                            </div>
                        </div>




                    </div>


                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!--        </div> -->

            </div>
        </div>




    </div>
</div>
