<?php
//CVarDumper::dump($data->order->brand, 1000, true);
//$brandid = Orders::model()->find('orderid=' . $data->orderid)->userid;
//$brand = Brand::model()->find('user_id=' . $data->order->userid);
//$Lookupimage = Lookupimages::model()->find('imageid=' . $data->imageid);
if ($data->image) {
    $imgurl = Yii::app()->baseUrl . '/images/deals/' . $data->image->imagename;
} else {
    $imgurl = Yii::app()->baseUrl . '/images/default/noimage.png';
}
//$Athlete = Athletes::model()->find('athleteid=' . $data->athleteid);
//CVarDumper::dump($data, 1000, true);
?>
<div class="col-xs-12 col-sm-6 pull-left margin_top">
    <h3 class="panel-body img-name white-color">With <?php echo $data->agentName; ?>
        <?php
        echo($data->dealtype == 0) ? '<i class="fa fa-twitter icon-place"> $ ' . $data->cost . '</i>' : '<i class="fa fa-instagram icon-place"> $ ' . $data->cost . '</i>';
        ?>
    </h3>
    <a href="<?php echo Yii::app()->createUrl('brand/marketplace_profile', array('id' => $data->athlete->athleteid)) ?>" class="anchor-profile">
        <img src="<?php echo Yii::app()->baseUrl . '/images/athlete/200x150/' . $data->athlete->image->imagename; ?>" alt="<?php echo $data->athleteName; ?>" class="img-profile img-thumbnail" id="">
    </a>
    <div class="bottomer panel-body">
        <div class="col-md-4 col-xs-12">
            <img src="<?php echo $imgurl; ?>" alt="deal-<?php echo $data->dealid; ?>" class="img-deals img-thumbnail img-responsive" id="">
        </div>

        <div class="col-md-8 col-xs-12">
            <div class="row">
                <div class="col-sm-6 col-xs-6"><h5>Deal ID</h5> </div>
                <div class="col-sm-6 col-xs-6"><h5> : <?php echo $data->dealid; ?> </h5> </div>

                <div class="col-sm-6 col-xs-6 border"><h5> Created Date</h5> </div>
                <div class="col-sm-6 col-xs-6 border"><h5> : <?php echo $data->dealrequesteddate; ?> </h5> </div>

                <div class="col-sm-6 col-xs-6 border"><h5>Campaign Date</h5></div>
                <div class="col-sm-6 col-xs-6 border"><h5> : <?php echo $data->campaigndate; ?></h5>
                </div>

                <div class="col-sm-6 col-xs-6 border"><h5>Campaign Time</h5></div>
                <div class="col-sm-6 col-xs-6 border"><h5> : <?php echo substr($data->campaigntime, 0, 5) . ' ' . $data->ampm; ?>
                    </h5>
                </div>
            </div>

        </div>
        <div class="col-sm-12 col-xs-12 "><h5>Description</h5></div>
        <div class="col-sm-12 col-xs-12 border">
            <h6><?php echo $data->description; ?></h6>
        </div>
    </div>
</div>