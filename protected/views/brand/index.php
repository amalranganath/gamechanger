
<div class="container-fluid">

    <div class="container brand_deals panel-body">
        <div class="row">
            <div class="col-sm-12 cover-search">
                <div class="row">
                    <div class="col-sm-12 cover-search">
                        <div class="col-md-6 col-xs-12">
                            <?php echo CHtml::beginForm('', '', array('class' => 'form-inline active')) ?>
                            <?php echo CHtml::textField('Athlete[name]', '', array('id' => 'txtval', 'class' => 'form-control', 'placeholder' => 'Search...')) ?>
                            <?php
                            echo CHtml::ajaxSubmitButton('Search', CHtml::normalizeUrl(array('/brand/SearchAthlete')), array(
                                'update' => '#search-results',
                                'type' => 'POST',
                                'success' => 'function(html){$("#search-results").html(html); '
                                . ' $(".brand-list .active").removeClass("active");'
                                . '$("#search").addClass("active");'
                                . '$("ul.nav li.active").removeClass("active")}',
                                    ), array('name' => 'run', 'class' => 'btn btn-lg btn-block', 'type' => 'submit', 'id' => 'txtval-btn')
                            );
                            ?>
                            <?php echo CHtml::endForm(); ?>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <ul class="nav nav-pills nav-justified " role="tablist" id="myTab">
                                <li role="presentation" class="active text-uppercase build_top"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"> View Trending</a></li>
                                <li role="presentation" class="text-uppercase"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">View Recently Added</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="tab-content brand-list row">
                    <div role="tabpanel" class="tab-pane" id="search">
                        <div id="search-results" class="col-sm-12 panel-body"></div>
                    </div>
                    <!--tab-TRENDING-->
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="col-sm-12 panel-body">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $dataProvider_trend,
                                'itemView' => '_search_athletes',
                                    //'enablePagination'=>true,
                            ));
                            ?>
                        </div>
                    </div>
                    <!--tab-RECENTLY ADDED-->
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <div class="col-sm-12 panel-body">
                            <?php
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $dataProvider_recent,
                                //'enablePagination' => false,
                                'itemView' => '_search_athletes',
                            ));
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>        

</div>
<!-- /.container-fluid -->
