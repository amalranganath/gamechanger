<?php
/* @var $this DealsController */
/* @var $model Deals */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'dealid'); ?>
		<?php echo $form->textField($model,'dealid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orderid'); ?>
		<?php echo $form->textField($model,'orderid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealstatusid'); ?>
		<?php echo $form->textField($model,'dealstatusid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'athleteid'); ?>
		<?php echo $form->textField($model,'athleteid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealtype'); ?>
		<?php echo $form->textField($model,'dealtype'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cost'); ?>
		<?php echo $form->textField($model,'cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealrequesteddate'); ?>
		<?php echo $form->textField($model,'dealrequesteddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'image'); ?>
		<?php echo $form->textField($model,'image',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'campaigndate'); ?>
		<?php echo $form->textField($model,'campaigndate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'campaigntime'); ?>
		<?php echo $form->textField($model,'campaigntime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealapproveddate'); ?>
		<?php echo $form->textField($model,'dealapproveddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealfullfilleddate'); ?>
		<?php echo $form->textField($model,'dealfullfilleddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateofpaymentbybrand'); ?>
		<?php echo $form->textField($model,'dateofpaymentbybrand'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateofpaymenttoagent'); ?>
		<?php echo $form->textField($model,'dateofpaymenttoagent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agentid'); ?>
		<?php echo $form->textField($model,'agentid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agentsharepercentage'); ?>
		<?php echo $form->textField($model,'agentsharepercentage',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agentshare'); ?>
		<?php echo $form->textField($model,'agentshare'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fulfill_engagement'); ?>
		<?php echo $form->textField($model,'fulfill_engagement'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fulfill_reach'); ?>
		<?php echo $form->textField($model,'fulfill_reach'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fullfill_paidtoagent'); ?>
		<?php echo $form->textField($model,'fullfill_paidtoagent'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->