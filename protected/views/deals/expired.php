
<div class="container directory-info-row agent_orders">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="fonr-color text-center text-uppercase panel-body"> Expired Deals</h1>
            <div class="list-group margin_bottom">
                <div class="row">
                    <?php if (Yii::app()->user->hasFlash('success')) {
                        ?>
                        <div class="alert alert-success fade in">
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                        <?php }
                    ?>
                    <?php
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider' => $dataProvider,
                        'itemView' => '_deal',
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>


