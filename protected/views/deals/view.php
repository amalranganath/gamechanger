<?php
/* @var $this DealsController */
/* @var $model Deals */

$this->breadcrumbs=array(
	'Deals'=>array('index'),
	$model->dealid,
);

$this->menu=array(
	array('label'=>'List Deals', 'url'=>array('index')),
	array('label'=>'Create Deals', 'url'=>array('create')),
	array('label'=>'Update Deals', 'url'=>array('update', 'id'=>$model->dealid)),
	array('label'=>'Delete Deals', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->dealid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Deals', 'url'=>array('admin')),
);
?>

<h1>View Deals #<?php echo $model->dealid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'dealid',
		'orderid',
		'dealstatusid',
		'athleteid',
		'dealtype',
		'cost',
		'dealrequesteddate',
		'image',
		'description',
		'campaigndate',
		'campaigntime',
		'dealapproveddate',
		'dealfullfilleddate',
		'dateofpaymentbybrand',
		'dateofpaymenttoagent',
		'agentid',
		'agentsharepercentage',
		'agentshare',
		'fulfill_engagement',
		'fulfill_reach',
		'fullfill_paidtoagent',
	),
)); ?>
