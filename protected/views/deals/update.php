<?php
/* @var $this DealsController */
/* @var $model Deals */

$this->breadcrumbs=array(
	'Deals'=>array('index'),
	$model->dealid=>array('view','id'=>$model->dealid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Deals', 'url'=>array('index')),
	array('label'=>'Create Deals', 'url'=>array('create')),
	array('label'=>'View Deals', 'url'=>array('view', 'id'=>$model->dealid)),
	array('label'=>'Manage Deals', 'url'=>array('admin')),
);
?>

<h1>Update Deals <?php echo $model->dealid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>