<?php
/* @var $this DealsController */
/* @var $model Deals */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'deals-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'orderid'); ?>
		<?php echo $form->textField($model,'orderid'); ?>
		<?php echo $form->error($model,'orderid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealstatusid'); ?>
		<?php echo $form->textField($model,'dealstatusid'); ?>
		<?php echo $form->error($model,'dealstatusid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'athleteid'); ?>
		<?php echo $form->textField($model,'athleteid'); ?>
		<?php echo $form->error($model,'athleteid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealtype'); ?>
		<?php echo $form->textField($model,'dealtype'); ?>
		<?php echo $form->error($model,'dealtype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost'); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealrequesteddate'); ?>
		<?php echo $form->textField($model,'dealrequesteddate'); ?>
		<?php echo $form->error($model,'dealrequesteddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->textField($model,'image',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campaigndate'); ?>
		<?php echo $form->textField($model,'campaigndate'); ?>
		<?php echo $form->error($model,'campaigndate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'campaigntime'); ?>
		<?php echo $form->textField($model,'campaigntime'); ?>
		<?php echo $form->error($model,'campaigntime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealapproveddate'); ?>
		<?php echo $form->textField($model,'dealapproveddate'); ?>
		<?php echo $form->error($model,'dealapproveddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealfullfilleddate'); ?>
		<?php echo $form->textField($model,'dealfullfilleddate'); ?>
		<?php echo $form->error($model,'dealfullfilleddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateofpaymentbybrand'); ?>
		<?php echo $form->textField($model,'dateofpaymentbybrand'); ?>
		<?php echo $form->error($model,'dateofpaymentbybrand'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateofpaymenttoagent'); ?>
		<?php echo $form->textField($model,'dateofpaymenttoagent'); ?>
		<?php echo $form->error($model,'dateofpaymenttoagent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agentid'); ?>
		<?php echo $form->textField($model,'agentid'); ?>
		<?php echo $form->error($model,'agentid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agentsharepercentage'); ?>
		<?php echo $form->textField($model,'agentsharepercentage',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'agentsharepercentage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agentshare'); ?>
		<?php echo $form->textField($model,'agentshare'); ?>
		<?php echo $form->error($model,'agentshare'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fulfill_engagement'); ?>
		<?php echo $form->textField($model,'fulfill_engagement'); ?>
		<?php echo $form->error($model,'fulfill_engagement'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fulfill_reach'); ?>
		<?php echo $form->textField($model,'fulfill_reach'); ?>
		<?php echo $form->error($model,'fulfill_reach'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fullfill_paidtoagent'); ?>
		<?php echo $form->textField($model,'fullfill_paidtoagent'); ?>
		<?php echo $form->error($model,'fullfill_paidtoagent'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->