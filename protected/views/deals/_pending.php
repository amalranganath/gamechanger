<?php
//$brandid=Orders::model()->find('orderid='.$data->orderid)->userid;
$brand = Brand::model()->find('user_id=' . $data->order->userid);
//$Lookupimage= Lookupimages::model()->find('imageid='.$data->imageid);
//CVarDumper::dump($data->athlete, 10000, true);
if ($data->image) {
    $imgurl = Yii::app()->baseUrl . '/images/deals/' . $data->image->imagename;
} else {
    $imgurl = Yii::app()->baseUrl . '/images/default/noimage.png';
}
//$Athlete=Athletes::model()->find('athleteid='.$data->athleteid);
//$athleteimageid=$Athlete->imageid;
//$athlete_imagename= Lookupimages::model()->find('imageid='.$athleteimageid)->imagename;
?>
<div class="col-xs-12 col-md-6 pull-left margin_top">
    <h3 class="panel-body img-name white-color"><?php echo $brand->brandname; ?>
<!--        <i class="fa fa-instagram icon-place">$120</i>-->
        <?php
        echo ($data->dealtype == 0) ? '<i class="fa fa-twitter icon-place"> $ ' . $data->cost . '</i>' : '<i class="fa fa-instagram icon-place"> $ ' . $data->cost . '</i>';
        ?>
    </h3>
    <a href="<?php echo Yii::app()->createUrl('/athletes/profile/', array('id' => $data->athlete->athleteid)); ?>" class="anchor-profile">
        <img src="<?php echo Yii::app()->baseUrl . '/images/athlete/200x150/' . $data->athlete->image->imagename; ?>" alt="<?php echo $data->AthleteName ?>" class="img-profile img-thumbnail" id="">
    </a>
    <div class="bottomer panel-body">
        <div class="col-sm-4 col-xs-12">
            <img src="<?php echo $imgurl; ?>" alt="deal-<?php echo $data->dealid; ?>" class="img-deals img-thumbnail img-responsive" id="">
        </div>

        <div class="col-sm-8 col-xs-12">
            <div class="row">
                <div class="col-sm-6 col-xs-6"><h5>Deal ID</h5> </div>
                <div class="col-sm-6 col-xs-6"><h5> : <?php echo $data->dealid; ?> </h5> </div>

                <div class="col-sm-6 col-xs-6 border"><h5> Created Date</h5> </div>
                <div class="col-sm-6 col-xs-6 border"><h5> : <?php echo $data->dealrequesteddate; ?> </h5> </div>

                <div class="col-sm-6 col-xs-6 border"><h5>Campaign Date</h5></div>
                <div class="col-sm-6 col-xs-6 border"><h5> : <?php echo $data->campaigndate; ?></h5>  </div>

                <div class="col-sm-6 col-xs-6 border"><h5>Campaign Time</h5></div>
                <div class="col-sm-6 col-xs-6 border"><h5> : <?php echo substr($data->campaigntime, 0, 5) . ' ' . $data->ampm; ?></h5> </div>
            </div>

        </div>
        <div class="col-sm-12 col-xs-12 "><h5>Description</h5></div>
        <div class="col-sm-12 col-xs-12 border">
            <h5><?php echo $data->description; ?></h5>
        </div>
        <div class="col-sm-12 col-xs-12">
            <?php echo CHtml::link('Approve', array('deals/approval', array('dealid' => $data->dealid)), array('class' => 'btn btn-success', 'confirm' => 'Are you sure?')); ?>
            <?php echo CHtml::link('Disapprove', array('deals/disapproval', array('dealid' => $data->dealid)), array('class' => 'btn btn-danger', 'confirm' => 'Are you sure?')); ?>
        </div>
    </div>
</div>