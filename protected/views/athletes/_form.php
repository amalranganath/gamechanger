<div id="landing_wrapper">
    <div id="page-wrapper">

        <div class="form">

            <?php
            //CVarDumper::dump(Yii::app()->request->urlReferrer, 10000, true);
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'athletes-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation' => false,
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',),
            ));
            ?>

            <!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

            <?php // echo $form->errorSummary($model);  ?>
            <div class="col-sm-2 buttons">
                <a href="<?php echo Yii::app()->request->urlReferrer; ?>"  class="btn btn-dafault bg_color"><i class="fa fa-caret-left"></i> Back</a>
            </div>

            <div class="signup_form">
                <div class="col-sm-12">
                    <?php if (Yii::app()->user->hasFlash('updateSuccess')) { ?>
                        <div class="alert alert-success fade in"> 
                            <?php echo Yii::app()->user->getFlash('updateSuccess'); ?>
                        </div>
                    <?php } ?>
                    <div class="panel text-center text-uppercase">
                        <h1><?php echo ($model->isNewRecord) ? 'Add' : ' Edit'; ?> Your Athlete</h1>
                    </div>
                </div>
                <div class="row">
                    <?php $model->userid = Yii::app()->user->id; ?>
                    <?php // echo $form->labelEx($model,'userid');  ?>
                    <?php echo $form->hiddenField($model, 'userid'); ?>
                    <?php // echo $form->error($model,'userid');  ?>
                </div>

                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'First Name '); ?>
                    <?php echo $form->textField($model, 'firstname', array('size' => 25, 'maxlength' => 25, 'class' => 'panel-body', 'placeholder' => 'First Name')); ?>
                    <?php echo $form->error($model, 'firstname'); ?>
                </div>

                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Last Name '); ?>
                    <?php echo $form->textField($model, 'lastname', array('size' => 25, 'maxlength' => 25, 'class' => 'panel-body', 'placeholder' => 'Last Name')); ?>
                    <?php echo $form->error($model, 'lastname'); ?>
                </div>

                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Sports Category'); ?>
                    <?php $list = CHtml::listData(Sportscatagory::model()->findAll(array('order' => 'sportscatagory_id')), 'sportscatagory_id', 'name'); ?>
                    <?php echo $form->dropDownList($model, 'sportscategoryid', $list, array('prompt' => '--Select Sport Category--', 'class' => 'panel-body', 'style' => 'width:100%')); ?>
                    <?php echo $form->error($model, 'sportscategoryid'); ?>
                </div>
                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Team '); ?>
                    <?php echo $form->textField($model, 'team', array('size' => 25, 'maxlength' => 25, 'class' => 'panel-body', 'placeholder' => 'Team')); ?>
                    <?php echo $form->error($model, 'team'); ?>

                </div>

                <?php
//                echo 'Dateofbirth'.$model->dob;
                $year = $month = $day = '';
                if (!empty($model->athleteid)) {
                    $model->dob = Athletes::model()->findByPk($model->athleteid)->dob;
                }

                if (!empty($model->dob) && !is_array($model->dob)) {
                    //2015-02-03
                    list($year, $month, $day) = explode('-', $model->dob);
                }
                ?>

                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Date of Birth'); ?>
                </div>
                <div class="col-sm-4 panel-body">
                    <?php // echo CHtml::dropDownList('Athletes[dob][0]',$year,array('Year'=>'Year','1980'=>'1980','1981'=>'1981','1982'=>'1982','1983'=>'1983'),array('class'=>'panel-body','style'=>'width: 100%;')); ?>
                    <?php
                    echo CHtml::dropDownList('Athletes[dob][0]', (!empty($year)) ? $year : '', Yii::app()->params['dob_years'], array(
                        'prompt' => 'Year',
                        'class' => 'panel-body',
                        'style' => 'width: 100%;'
                            )
                    );
                    ?>
                </div>
                <div class="col-sm-4 panel-body">
                    <?php // echo CHTML::dropDownList('Athletes[dob][1]',$month,array('01'=>'01','02'=>'02','03'=>'03','04'=>'04'),array('class'=>'panel-body','style'=>'width: 100%;')); ?>
                    <?php
                    echo CHtml::dropDownList('Athletes[dob][1]', (!empty($month)) ? $month : '', Yii::app()->params['dob_months'], array(
                        'prompt' => 'Month',
                        'class' => 'panel-body',
                        'style' => 'width: 100%;'
                            )
                    );
                    ?>

                </div>    
                <div class="col-sm-4 panel-body">
                    <?php // echo CHTML::dropDownList('Athletes[dob][2]',$day,array('Day'=>'Day', '01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07'),array('class'=>'panel-body','style'=>'width: 100%;')); ?>
                    <?php
                    echo CHtml::dropDownList('Athletes[dob][2]', (!empty($day)) ? $day : '', Yii::app()->params['dob_days'], array(
                        'prompt' => 'Day',
                        'class' => 'panel-body',
                        'style' => 'width: 100%;'
                            )
                    );
                    ?>
                </div>    
                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->error($model, 'dob'); ?>
                </div>

                <!--	</div>-->

                <div class="col-sm-12 text-center panel-body">

                    <?php echo $form->labelEx($model, 'sex '); ?>
                    <?php //echo $form->textField($model,'StarRating',array('size'=>60,'maxlength'=>255,'class'=>'form-control',)); ?>
                    <?php
                    echo $form->dropDownList($model, 'sex', array('1' => 'Male', '2' => 'Female'), array('empty' => 'Sex', 'class' => 'panel-body', 'style' => 'width:100%'));
                    ?>
                    <?php echo $form->error($model, 'sex'); ?>
                </div>


                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Athlete Image '); ?>
                    <?php
                    echo CHtml::activeFileField($lookupimages, 'imagename', array(
                        'class' => 'form-control',
                    ));
                    ?>
                    <?php echo '<span> (Allows only .jpg and use 400px:400px (width:height) ratio)</span>'; ?>
                    <?php echo $form->error($lookupimages, 'imagename'); ?>

                    <?php
//                $this->widget('CMultiFileUpload', array(
//                        'model'=>$model,
//                        'name' => 'Name',
//                        'max'=>5,
//                        'accept' =>'jpg',
//                        'duplicate' => 'Duplicate file!', 
//                        'denied' => 'Invalid file type',
////                        'options'=>array(
////                            'onFileSelect'=>'function(e ,v ,m){
////                                var fileSize=$("#Name")[0].files[0].size;
////                                if(fileSize>1024){//5MB
////                                    alert("Image too large");
////                                    $("#importprice-form").reset();
////                                    return false;
////                                }
////                            }'  
////                        ),
//                        'htmlOptions'=>array(
//                            'class'=>'form-control',
//                            'onchange'=>'$gettxt',
//                            
//                           
//                        ),
//                    ));
                    ?>
 <!--<input class="form-control" onchange="$gettxt" id="Name" onkeyup="" type="file" value="" name="Name[]" hidden="hidden">-->
                    <?php // echo $form->hiddenField($model,'imageid');  ?>
                    <?php // echo $form->error($model,'imageid'); ?>
                </div>


                <?php
                if ($model->imageid != '') {
                    ?>
                    <div class="col-sm-12 text-center panel-body"> 
                        <div class="col-sm-3">
                            <?php
                            $imagename = Lookupimages::model()->find('imageid=' . $model->imageid)->imagename;
                            if ($imagename) {
                                ?><img src="<?php echo Yii::app()->baseUrl . '/images/athlete/' . $imagename; ?>"> <?php
                            }
                            ?>


                        </div>
                    </div>
                    <?php
                }
                ?>


                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Cost Per Tweet'); ?>
                    <?php // echo $form->textField($model,'costpertweet',array('size'=>25,'maxlength'=>25,'class'=>'panel-body','placeholder'=>'Cost per tweet $')); ?>
                    <?php echo $form->numberField($model, 'costpertweet', array('size' => 25, 'maxlength' => 25, 'class' => 'panel-body', 'placeholder' => 'Cost per tweet $')); ?>
                    <?php echo $form->error($model, 'costpertweet'); ?>
                </div>
                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Cost Per Tweet Image'); ?>
                    <?php echo $form->numberField($model, 'costpertweetimg', array('size' => 25, 'maxlength' => 25, 'class' => 'panel-body', 'placeholder' => 'Cost per tweet image $')); ?>
                    <?php echo $form->error($model, 'costpertweetimg'); ?>
                </div>

                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Cost Per Instagram'); ?>
                    <?php echo $form->numberField($model, 'costperinstagram', array('size' => 25, 'maxlength' => 25, 'class' => 'panel-body', 'placeholder' => 'Cost per instagram $')); ?>
                    <?php echo $form->error($model, 'costperinstagram'); ?>
                </div>
                <div class="col-sm-12 text-center panel-body">
                    <?php echo $form->labelEx($model, 'Cost Per Instagram Image'); ?>
                    <?php echo $form->numberField($model, 'costperinstagramimg', array('size' => 25, 'maxlength' => 25, 'class' => 'panel-body', 'placeholder' => 'Cost per instagram image $')); ?>
                    <?php echo $form->error($model, 'costperinstagramimg'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'updateddate');  ?>
                    <?php // echo $form->textField($model,'updateddate'); ?>
                    <?php // echo $form->error($model,'updateddate'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'score');  ?>
                    <?php // echo $form->textField($model,'score'); ?>
                    <?php // echo $form->error($model,'score'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'reach');  ?>
                    <?php // echo $form->textField($model,'reach',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'reach'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'audience');  ?>
                    <?php // echo $form->textField($model,'audience',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'audience'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'trust');  ?>
                    <?php // echo $form->textField($model,'trust',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'trust'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'engagement');  ?>
                    <?php // echo $form->textField($model,'engagement',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'engagement'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace');  ?>
                    <?php // echo $form->textField($model,'marketplace',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'marketplace'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'reach_followers_active');  ?>
                    <?php // echo $form->textField($model,'reach_followers_active',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'reach_followers_active'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'reach_followers_inactive');  ?>
                    <?php // echo $form->textField($model,'reach_followers_inactive',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'reach_followers_inactive'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'reach_followers_fake');  ?>
                    <?php // echo $form->textField($model,'reach_followers_fake',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'reach_followers_fake'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'reach_viralpotential');  ?>
                    <?php // echo $form->textField($model,'reach_viralpotential',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'reach_viralpotential'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'audience_gender_male');  ?>
                    <?php // echo $form->textField($model,'audience_gender_male',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'audience_gender_male'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'audience_gender_female');  ?>
                    <?php // echo $form->textField($model,'audience_gender_female',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'audience_gender_female'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'audience_followers_interests');  ?>
                    <?php // echo $form->textField($model,'audience_followers_interests',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'audience_followers_interests'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'audience_quality');  ?>
                    <?php // echo $form->textField($model,'audience_quality',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'audience_quality'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'audience_extendedreach');  ?>
                    <?php // echo $form->textField($model,'audience_extendedreach',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'audience_extendedreach'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'trust_sentiment_positive');  ?>
                    <?php // echo $form->textField($model,'trust_sentiment_positive',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'trust_sentiment_positive'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'trust_sentiment_neutral');  ?>
                    <?php // echo $form->textField($model,'trust_sentiment_neutral',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'trust_sentiment_neutral'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'trust_sentiment_negative');  ?>
                    <?php // echo $form->textField($model,'trust_sentiment_negative',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'trust_sentiment_negative'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'trust_klout');  ?>
                    <?php // echo $form->textField($model,'trust_klout',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'trust_klout'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'engagement_engagement_projectedengagementrate');  ?>
                    <?php // echo $form->textField($model,'engagement_engagement_projectedengagementrate',array('size'=>25,'maxlength'=>25)); ?>
                    <?php // echo $form->error($model,'engagement_engagement_projectedengagementrate'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'engagement_engagement_projectedengagement');  ?>
                    <?php // echo $form->textField($model,'engagement_engagement_projectedengagement',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'engagement_engagement_projectedengagement'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'engagment_engagementrating');  ?>
                    <?php // echo $form->textField($model,'engagment_engagementrating',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'engagment_engagementrating'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'engagement_twitteractivity_averageretweetsperpost');  ?>
                    <?php // echo $form->textField($model,'engagement_twitteractivity_averageretweetsperpost',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'engagement_twitteractivity_averageretweetsperpost'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'engagement_engagement_twitteractivity_averagefavoritesperpost');  ?>
                    <?php // echo $form->textField($model,'engagement_engagement_twitteractivity_averagefavoritesperpost',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'engagement_engagement_twitteractivity_averagefavoritesperpost'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_activity_profileviews');  ?>
                    <?php // echo $form->textField($model,'marketplace_activity_profileviews',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_activity_profileviews'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_activity_completeddeals');  ?>
                    <?php // echo $form->textField($model,'marketplace_activity_completeddeals',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_activity_completeddeals'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_acceptancerate');  ?>
                    <?php // echo $form->textField($model,'marketplace_acceptancerate',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_acceptancerate'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_response_averageagentresponsetime');  ?>
                    <?php // echo $form->textField($model,'marketplace_response_averageagentresponsetime',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_response_averageagentresponsetime'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_response_averageinfluencerresponsetime');  ?>
                    <?php // echo $form->textField($model,'marketplace_response_averageinfluencerresponsetime',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_response_averageinfluencerresponsetime'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_costperengagement');  ?>
                    <?php // echo $form->textField($model,'marketplace_costperengagement',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_costperengagement'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_costperthousands');  ?>
                    <?php // echo $form->textField($model,'marketplace_costperthousands',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_costperthousands'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'marketplace_opportunityvalue');  ?>
                    <?php // echo $form->textField($model,'marketplace_opportunityvalue',array('size'=>10,'maxlength'=>10)); ?>
                    <?php // echo $form->error($model,'marketplace_opportunityvalue'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'trending');  ?>
                    <?php // echo $form->textField($model,'trending'); ?>
                    <?php // echo $form->error($model,'trending'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'lastupdatedadminid');  ?>
                    <?php // echo $form->textField($model,'lastupdatedadminid',array('size'=>4,'maxlength'=>4)); ?>
                    <?php // echo $form->error($model,'lastupdatedadminid'); ?>
                </div>

                <div class="row">
                    <?php // echo $form->labelEx($model,'lastadminupdatedtime');  ?>
                    <?php // echo $form->textField($model,'lastadminupdatedtime'); ?>
                    <?php // echo $form->error($model,'lastadminupdatedtime'); ?>
                </div>

                <div class="col-sm-12 buttons">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default bg_color')); ?>
                </div>
                <br/><br/>

                <?php $this->endWidget(); ?>

            </div><!-- form --></div>
    </div><!-- form --></div>

