<div class="col-md-3 col-xs-12">
    <div class="property-box-simple">
        <div class="property-box-image property-box-simple-image-inner">
            <img src="<?php echo Yii::app()->baseUrl . '/images/athlete/'; echo $data->image ? $data->image->imagename : 'usain.jpg'; ?>" class="attachment-post-thumbnail wp-post-image property-box-simple-image-inner" alt="17">
        </div>
        <div class="property-box-header">
            <a href="<?php echo Yii::app()->baseUrl; ?>/athletes/update?id=<?php echo $data->athleteid; ?>" id="athleteid-<?php echo $data->athleteid; ?>" title="Double click to edit" data-toggle="tooltip">  <h3><?php
                    echo CHtml::encode($data->firstname);
                    echo ' ' . CHtml::encode($data->lastname);
                    ?></h3></a>
        </div>
        <footer class="follower-foot">
            <ul>
                <li><i class="fa fa-twitter fa-2x font_color"></i> <?php echo ($data->costpertweet == 0) ? "" : "  $" . $data->costpertweet; ?> </li>
                <li><i class="fa fa-instagram fa-2x font_color"></i><?php echo ($data->costperinstagram == 0) ? "" : "  $" . $data->costperinstagram; ?></li>
                <li>
                    <i class="fa fa-wifi font_color"><?php echo $data->reach; ?> </i><span> reach &nbsp;&nbsp;&nbsp;</span>
                    <i class="fa fa-location-arrow font_color"> <?php echo $data->marketplace_costperengagement; ?></i> <span> cpe &nbsp;&nbsp;&nbsp;</span>
                    <i class="fa fa-location-arrow font_color"><?php echo $data->marketplace_costperthousands; ?> </i> <span> cpm &nbsp;&nbsp;&nbsp;</span>
                </li>
            </ul>
        </footer>
    </div>
</div>

<script>
    jQuery(function ($) {
        $('#athleteid-<?php echo $data->athleteid; ?>').click(function () {
            return false;
        }).dblclick(function () {
            window.location = this.href;
            return false;
        });
    });
</script>