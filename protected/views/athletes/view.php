<?php
/* @var $this AthletesController */
/* @var $model Athletes */

$this->breadcrumbs=array(
	'Athletes'=>array('index'),
	$model->athleteid,
);

$this->menu=array(
	array('label'=>'List Athletes', 'url'=>array('index')),
	array('label'=>'Create Athletes', 'url'=>array('create')),
	array('label'=>'Update Athletes', 'url'=>array('update', 'id'=>$model->athleteid)),
	array('label'=>'Delete Athletes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->athleteid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Athletes', 'url'=>array('admin')),
);
?>

<h1>View Athletes #<?php echo $model->athleteid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'athleteid',
		'userid',
		'firstname',
		'lastname',
		'sportscategoryid',
		'dob',
		'sex',
		'costpertweet',
		'costperinstagram',
		'updateddate',
		'score',
		'reach',
		'audience',
		'trust',
		'engagement',
		'marketplace',
		'reach_followers_active',
		'reach_followers_inactive',
		'reach_followers_fake',
		'reach_viralpotential',
		'audience_gender_male',
		'audience_gender_female',
		'audience_followers_interests',
		'audience_quality',
		'audience_extendedreach',
		'trust_sentiment_positive',
		'trust_sentiment_neutral',
		'trust_sentiment_negative',
		'trust_klout',
		'engagement_engagement_projectedengagementrate',
		'engagement_engagement_projectedengagement',
		'engagment_engagementrating',
		'engagement_twitteractivity_averageretweetsperpost',
		'engagement_engagement_twitteractivity_averagefavoritesperpost',
		'marketplace_activity_profileviews',
		'marketplace_activity_completeddeals',
		'marketplace_acceptancerate',
		'marketplace_response_averageagentresponsetime',
		'marketplace_response_averageinfluencerresponsetime',
		'marketplace_costperengagement',
		'marketplace_costperthousands',
		'marketplace_opportunityvalue',
		'trending',
		'lastupdatedadminid',
		'lastadminupdatedtime',
	),
)); ?>
