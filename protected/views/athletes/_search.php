<?php
/* @var $this AthletesController */
/* @var $model Athletes */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'athleteid'); ?>
		<?php echo $form->textField($model,'athleteid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sportscategoryid'); ?>
		<?php echo $form->textField($model,'sportscategoryid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dob'); ?>
		<?php echo $form->textField($model,'dob'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sex'); ?>
		<?php echo $form->textField($model,'sex',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costpertweet'); ?>
		<?php echo $form->textField($model,'costpertweet',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costperinstagram'); ?>
		<?php echo $form->textField($model,'costperinstagram',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updateddate'); ?>
		<?php echo $form->textField($model,'updateddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'score'); ?>
		<?php echo $form->textField($model,'score'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reach'); ?>
		<?php echo $form->textField($model,'reach',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'audience'); ?>
		<?php echo $form->textField($model,'audience',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trust'); ?>
		<?php echo $form->textField($model,'trust',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engagement'); ?>
		<?php echo $form->textField($model,'engagement',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace'); ?>
		<?php echo $form->textField($model,'marketplace',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reach_followers_active'); ?>
		<?php echo $form->textField($model,'reach_followers_active',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reach_followers_inactive'); ?>
		<?php echo $form->textField($model,'reach_followers_inactive',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reach_followers_fake'); ?>
		<?php echo $form->textField($model,'reach_followers_fake',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reach_viralpotential'); ?>
		<?php echo $form->textField($model,'reach_viralpotential',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'audience_gender_male'); ?>
		<?php echo $form->textField($model,'audience_gender_male',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'audience_gender_female'); ?>
		<?php echo $form->textField($model,'audience_gender_female',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'audience_followers_interests'); ?>
		<?php echo $form->textField($model,'audience_followers_interests',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'audience_quality'); ?>
		<?php echo $form->textField($model,'audience_quality',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'audience_extendedreach'); ?>
		<?php echo $form->textField($model,'audience_extendedreach',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trust_sentiment_positive'); ?>
		<?php echo $form->textField($model,'trust_sentiment_positive',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trust_sentiment_neutral'); ?>
		<?php echo $form->textField($model,'trust_sentiment_neutral',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trust_sentiment_negative'); ?>
		<?php echo $form->textField($model,'trust_sentiment_negative',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trust_klout'); ?>
		<?php echo $form->textField($model,'trust_klout',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engagement_engagement_projectedengagementrate'); ?>
		<?php echo $form->textField($model,'engagement_engagement_projectedengagementrate',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engagement_engagement_projectedengagement'); ?>
		<?php echo $form->textField($model,'engagement_engagement_projectedengagement',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engagment_engagementrating'); ?>
		<?php echo $form->textField($model,'engagment_engagementrating',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engagement_twitteractivity_averageretweetsperpost'); ?>
		<?php echo $form->textField($model,'engagement_twitteractivity_averageretweetsperpost',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'engagement_engagement_twitteractivity_averagefavoritesperpost'); ?>
		<?php echo $form->textField($model,'engagement_engagement_twitteractivity_averagefavoritesperpost',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_activity_profileviews'); ?>
		<?php echo $form->textField($model,'marketplace_activity_profileviews',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_activity_completeddeals'); ?>
		<?php echo $form->textField($model,'marketplace_activity_completeddeals',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_acceptancerate'); ?>
		<?php echo $form->textField($model,'marketplace_acceptancerate',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_response_averageagentresponsetime'); ?>
		<?php echo $form->textField($model,'marketplace_response_averageagentresponsetime',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_response_averageinfluencerresponsetime'); ?>
		<?php echo $form->textField($model,'marketplace_response_averageinfluencerresponsetime',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_costperengagement'); ?>
		<?php echo $form->textField($model,'marketplace_costperengagement',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_costperthousands'); ?>
		<?php echo $form->textField($model,'marketplace_costperthousands',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marketplace_opportunityvalue'); ?>
		<?php echo $form->textField($model,'marketplace_opportunityvalue',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trending'); ?>
		<?php echo $form->textField($model,'trending'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastupdatedadminid'); ?>
		<?php echo $form->textField($model,'lastupdatedadminid',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastadminupdatedtime'); ?>
		<?php echo $form->textField($model,'lastadminupdatedtime'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->