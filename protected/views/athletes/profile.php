<?php
/**
 * Athlete Profile
 */
?>
<div id="wrapper">

    <div class="container-fluid">              

        <div class="container brand_profile">

            <!-- Page Heading -->
            <div class="row panel-body">

                <div class="col-sm-12 buttons">
                    <a href="javascript:window.history.back();" class="btn btn-link back"><i class="fa fa-caret-left"></i> Back</a>
                    <?php
                    if (Yii::app()->user->type == 2) {
                        echo CHtml::ajaxLink('<i class="fa fa-plus-circle"></i> add to cart', array('/brand/addtocart'), array('type' => 'POST',
                            'data' => array('athleteid' => $model->athleteid,),
                            'update' => '#shopping-cart-update'), array('class' => 'btn btn-lg btn-default text-uppercase btn_cart pull-right', 'id' => 'ajaxup' . $model->athleteid));
                    }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="panel">
                            <h3 class="" class="img-name"><i class="fonr-color"><?php echo $model->firstname . ' ' . $model->lastname; ?></i></h3>
                            <?php //$imagename = Lookupimages::model()->find('imageid=' . $model->imageid)->imagename; ?>
                            <img src="<?php echo Yii::app()->baseUrl; ?>/images/athlete/<?php echo $model->image->imagename; ?>" alt="<?php echo $model->firstname . ' ' . $model->lastname; ?>" class="img-profile img-thumbnail" style="bottom: 55px;">
                            <div class="bottomer">
                                <?php
                                $athleteage = date('Y-m-d') - $model->dob;
                                ?>
                                <span>Age :&nbsp;<?php echo $athleteage; ?>&nbsp;</span><br/>
                                <span>Sex :&nbsp;<?php echo ($model->sex == 1) ? 'Male' : 'Female'; ?>&nbsp;</span><br/>
                                <span>Sport Category :&nbsp;<?php echo Sportscatagory::model()->find('sportscatagory_id=' . $model->sportscategoryid)->name; ?>&nbsp;</span><br>
                                <span>Team :&nbsp;<?php echo $model->team; ?>&nbsp;</span>
                                <?php if (Yii::app()->user->type == 2) { ?>
                                    <br/><span>Agent :&nbsp;<?php echo $model->agent->firstname . ' ' . $model->agent->lastname; ?>&nbsp;</span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 panel">
                        <div class="weather-bg">
                            <i class="fa fa-instagram fa-3x"></i>
                            <span class="lead">Instagram</span>
                        </div>

                        <div class="weather-category">
                            <ul>
                                <li class="active">
                                    <h3>SCORE</h3>
                                    <h5><?php echo $model->score; ?></h5>
                                </li>
                                <li class="border_left">
                                    <h3>COST</h3>
                                    <h5>$ <?php echo $model->costperinstagram; ?></h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class=" col-md-3 col-sm-6 panel">
                        <div class="weather-bg">
                            <i class="fa fa-twitter fa-3x"></i>
                            <span class="lead">Twitter</span>
                        </div>

                        <div class="weather-category">
                            <ul>
                                <li class="active">
                                    <h3>SCORE</h3>
                                    <h5><?php echo $model->score; ?></h5>
                                </li>
                                <li class="border_left">
                                    <h3>COST</h3>
                                    <h5>$<?php echo $model->costpertweet; ?></h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row state-overview panel-body">
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <!--                                        <div class="panel-heading">-->
                        <div class="col-xs-3 symbol terques">
                            <i class="fa fa-wifi"></i>
                        </div>
                        <div class="col-xs-9 value">
                            <h1 class="count text-uppercase">Reach &nbsp;<i class="fa fa-hand-o-right"></i> <?php echo $model->reach ?></h1>

                            <p>How far can this athlete take my message?</p>
                        </div>
                        <!--                                        </div>-->
                        <div class="panel-body chart"  id="reach">
                            <div class="col-sm-4" data-toggle="popover" data-placement="top" data-original-title="FOLLOWERS" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>Total follower count comes from the athlete's Twitter account directly.<br/>HOW TO USE IT:<br/>While most brands make decisions based on the athlete's total followers, we recommend looking first at the athlete's active followers, which will give you an estimate of the active, real consumers that make up the athlete's audience.">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Followers</h3>
                                    </div>
                                    <div class="chart_height">
                                        <div id="donut-example1"></div>
                                    </div>
                                    <script>
                                        Morris.Donut({
                                            element: 'donut-example1',
                                            data: [
                                                {label: "Fake", value: <?php echo $model->reach_followers_fake ?>},
                                                {label: "Inactive", value: <?php echo $model->reach_followers_inactive ?>},
                                                {label: "Active", value: <?php echo $model->reach_followers_active ?>}
                                            ],
                                            resize: true
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-4" data-toggle="popover" data-placement="top" data-original-title="FOLLOWER GROWTH" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> Every time an athlete is updated, a snapshot of their follower growth is recorded and displayed over time.<br/>HOW TO USE IT:<br/>">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Follower Growth</h3>
                                    </div>
                                    <div class="chart_height">
                                        <div class="text-center panel-body">
                                            <h1 class="font_color">
                                                <?php
                                                if ($model->reach_followers_reach > 1000) {
                                                    $result = floor($model->reach_followers_reach / 1000) . 'K';
                                                } else {
                                                    $result = $model->reach_followers_reach;
                                                }
                                                echo $result;
                                                ?>
                                            </h1>
                                            <p>Percentile</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4" data-toggle="popover" data-placement="top" data-original-title="VIRAL POTENTIAL" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> A 0 - 100 comparison to their peers (where 100 is a better Viral Rating than every other athlete on the system), based on both the athlete's follower count and the estimated audience reached through sharing (e.g. Retweets), aggregated from Twitter directly.<br/>HOW TO USE IT:<br/>Using an athlete with a high viral potential rating will help your endorsement reach a larger audience than you're paying for, boosting the impact of your campaign.">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Viral Potential</h3>
                                    </div>
                                    <div class="chart_height">
                                        <div class="text-center panel-body">
                                            <h1 class="font_color"><?php echo $model->reach_viralpotential ?></h1>
                                            <p>Percentile</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="col-sm-3 col-xs-12 symbol terques">
                            <i class="fa fa-hand-o-left"></i>
                        </div>
                        <div class="col-sm-9 col-xs-12 value">
                            <h1 class="count text-uppercase">Engagement &nbsp;<i class="fa fa-hand-o-right"></i>  <?php echo $model->engagement; ?></h1>
                            <p>Can this athlete influence consumers to take action?</p>
                        </div>

                        <div class="panel-body chart">
                            <div id="Engagement">

                                <div class="col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Engagement</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="text-center panel-body">

                                                <h1 class="font_color" data-toggle="popover" data-placement="top" data-original-title="PROJECTED ENGAGEMENT RATE" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> The athlete's estimated engagement rate is multiplied by the number of active followers.<br/>HOW TO USE IT:<br/>HOW TO USE IT:Get a feel for how many users will be interacting with your brand or visiting your promoted content.">

                                                    <?php
                                                    if (!empty($model->engagement_engagement_projectedengagement)) {
                                                        echo $model->engagement_engagement_projectedengagementrate . ' % ';
                                                    } else {
                                                        echo 'N/A';
                                                    }
                                                    ?>
                                                </h1>

                                                <p>Projected Engagement Rate</p>

                                                <h1 class="font_color" data-toggle="popover" data-placement="top" data-original-title="PROJECTED ENGAGEMENT" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> The athlete's estimated engagement rate is multiplied by the number of active followers.<br/>HOW TO USE IT:<br/>Get a feel for how many users will be interacting with your brand or visiting your promoted content.">

                                                    <?php
                                                    if (!empty($model->engagement_engagement_projectedengagement)) {
                                                        echo $model->engagement_engagement_projectedengagement . ' ';
                                                    } else {
                                                        echo 'N/A';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Projected Engagements</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4" data-toggle="popover" data-placement="top" data-original-title="ENGAGEMENT RATING" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> A 0 - 100 comparison to their peers (where 100 is a better Engagement Rating than every other athlete on the system), based on the athlete's estimated engagement rate.<br/>HOW TO USE IT:<br/>Athletes with a high engagement rating have an active, excitable audience. Their audience is more likely to click on links, share your content, and get involved with your brand.">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Engagement Rating</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="text-center panel-body">
                                                <h1 class="font_color"><?php echo $model->engagment_engagementrating ?></h1>
                                                <p>Percentile</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Twitter Activity</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="text-center panel-body">

                                                <h1 class="font_color" data-toggle="popover" data-placement="top" data-original-title="AVERAGE RETWEETS" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>This is the average number of Retweets an athlete will get on any Tweet (even non-advertisements) from Twitter directly. <br/>HOW TO USE IT:<br/>If an athlete receives a large amount of retweets per post, it's an indication the share quality content with their audience, and your endorsement is more likely to go viral.">
                                                    <?php
                                                    if (!empty($model->engagement_twitteractivity_averageretweetsperpost)) {
                                                        echo $model->engagement_twitteractivity_averageretweetsperpost;
                                                    } else {
                                                        echo 'N/A';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Average Retweets Per Post</p>

                                                <h1 class="font_color"data-toggle="popover" data-placement="top" data-original-title="AVERAGE FAVORITES" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>This is the average number of Favorites an athlete will get on any Tweet (even non-advertisements) from Twitter directly.<br/>HOW TO USE IT:<br/>Athletes who receive a large amount of favorites per post typically share relatable content with their audience. Drafting your endorsement content in a relatable fashion will better align your brand with their audience, leaving a lasting impression on your target audience.">
                                                    <?php
                                                    if (!empty($model->engagement_engagement_twitteractivity_averagefavoritesperpost)) {
                                                        echo $model->engagement_engagement_twitteractivity_averagefavoritesperpost;
                                                    } else {
                                                        echo 'N/A';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Average Favorites Per Post</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>      
                    </div>      
                </div>      
            </div>      

            <div class="row state-overview panel-body">
                <div class="col-md-7">
                    <div class="panel panel-primary">
                        <div class="col-sm-3 col-xs-12 symbol terques">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="col-sm-9 col-xs-12 value">
                            <h1 class="count text-uppercase">Audience &nbsp;<i class="fa fa-hand-o-right"></i> <?php echo $model->audience ?></h1>
                            <p>What kind of consumers does this athlete influence?</p>
                        </div>

                        <div class="panel-body chart">
                            <div id="Audience">
                                <div class="col-sm-3" data-toggle="popover" data-placement="top" data-original-title="GENDER" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>Every individual follower of an athlete is analyzed (from Twitter) to determine whether they are male or female, resulting in a percent out of total followers that are either male or female. <br/>HOW TO USE IT:<br/>Find the athlete that influences your target market by comparing the gender breakdown of their audience, or adjust your deal content to ensure your message resonates well with the athlete's audience.">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Gender</h3>
                                        </div>
                                        <div class="panel-body chart_height">
                                            <div id="donut-gender" class=""></div>
                                            <div class="row">
                                                <div class="col-sm-6 center">
                                                    <p class="p-margin">
                                                        <i style="   color: #0f6084; font-size: 26px;" class="fa fa-male "></i>
                                                    </p>
                                                    <p class="p-margin p-text"><?php echo $model->audience_gender_male ?> %</p>
                                                    <p > Male</p>
                                                </div>

                                                <div class="col-sm-6 center">
                                                    <p class="p-margin">
                                                        <i style="   color: #0f6084; font-size: 26px;" class="fa fa-female"></i>
                                                    </p>
                                                    <p class="p-margin p-text"><?php echo $model->audience_gender_female ?> %</p>
                                                    <p > Female</p>
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            var graphGender = Morris.Donut({
                                                element: 'donut-gender',
                                                data: [
                                                    {label: "Male", value: <?php echo $model->audience_gender_male ?>},
                                                    {label: "Female", value: <?php echo $model->audience_gender_female ?>}
                                                ],
                                                //colors: ['#0f6084','#ef3a55'],
                                            });
                                            $('ul.nav a').on('shown.bs.tab', function (e) {
                                                graphGender.redraw();
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col-sm-3" data-toggle="popover" data-placement="top" data-original-title="FOLLOWER INTEREST" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> Klout determines athlete influences based on undisclosed metrics and we retrieve the data directly from them.<br/>HOW TO USE IT:<br/>Look for topics related to your industry to find an athlete that will naturally influence your core target market.">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Follower Interest</h3>
                                        </div>
                                        <div class="panel-body chart_height">
                                            <div class="text-center panel-body">
                                                <p><?php echo $model->audience_followers_interests ?></p>

                                            </div>
                                            <div class="text-right">
<!--                                                <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3" data-toggle="popover" data-placement="top" data-original-title="QUALITY" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>Every individual follower of an athlete is analyzed (from Twitter) to determine their total reach, with the mean total reach being the basis for analyzing their audience quality percentile. <br/>HOW TO USE IT:<br/>Athletes high audience quality influence other influential social media users. Even if they don't generate a ton of retweets, your message can still reach a large audience.">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Quality</h3>
                                        </div>
                                        <div class="panel-body chart_height">
                                            <div class="text-center panel-body">
                                                <h1 class="font_color">
                                                    <?php
                                                    if ($model->audience_quality > 1000) {
                                                        $result = floor($model->audience_quality / 1000) . 'K';
                                                    } else {
                                                        $result = $model->audience_quality;
                                                    }
                                                    echo $result;
                                                    ?>
                                                </h1>
                                                <p>Average Follower Influence</p>
                                            </div>
                                            <div class="text-right">
<!--                                                <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3" data-toggle="popover" data-placement="top" data-original-title="EXTENDED REACH" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>Every individual follower of an athlete is analyzed (from Twitter) to determine their total reach, with the sum total reach being used to analyze their extended reach percentile. <br/>HOW TO USE IT:<br/>Knowing the extended reach for the athlete will allow you to understand the maximum impact this athlete could have on your brand, compared to other athletes on the platform.">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Extended Reach</h3>
                                        </div>
                                        <div class="panel-body chart_height">
                                            <div class="text-center panel-body">
                                                <h1 class="font_color"><?php echo $model->audience_extendedreach ?>m</h1>
                                                <p>Total Follower Influence</p>
                                            </div>
                                            <div class="text-right">
<!--                                                <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="panel panel-primary">
                        <div class="col-sm-3 col-xs-12 symbol terques">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="col-sm-9 col-xs-12 value">
                            <h1 class="count text-uppercase">Trust &nbsp;<i class="fa fa-hand-o-right"></i> <?php echo $model->trust ?></h1>
                            <p>Has athlete established a powerful presence online?</p>
                        </div>

                        <div class="panel-body chart">
                            <div id="Trust">
                                <div class="col-sm-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Sentiment</h3>
                                        </div>
                                        <div class="panel-body chart_height">
                                            <div id="donut-Sentiment"></div>

                                            <script>
                                                var graphSentiment = Morris.Donut({
                                                    element: 'donut-Sentiment',
                                                    data: [
                                                        {label: "Positive", value: <?php echo $model->trust_sentiment_positive ?>},
                                                        {label: "Negative", value: <?php echo $model->trust_sentiment_negative ?>},
                                                        {label: "Neutral", value: <?php echo $model->trust_sentiment_neutral ?>}
                                                    ],
                                                    colors:
                                                            [
                                                                'rgb(85, 172, 238)',
                                                                'rgb(226, 226, 226)',
                                                                'rgb(119, 119, 119)'
                                                            ],
                                                });
                                                $('ul.nav a').on('shown.bs.tab', function (e) {
                                                    graphSentiment.redraw();
                                                });


                                            </script>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" data-toggle="popover" data-placement="top" data-original-title="FOLLOWER INTEREST" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> Klout determines social media presence based on the accumulation of undisclosed metrics and we retrieve the score directly from their data.<br/>HOW TO USE IT:<br/>If you're using non-athlete influencers, the Klout score is a great metric to compare each individual's power and potential impact for your brand.">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"> Follower Interest</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="text-center panel-body">
                                                <h1 class="font_color"><?php echo $model->trust_klout ?></h1>
                                                <p class="text-center">Klout Score</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div> 


            <div class="row state-overview panel-body">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="col-sm-3 col-xs-12 symbol terques">
                            <i class="fa fa-fw fa-shopping-cart"></i>
                        </div>
                        <div class="col-sm-9 col-xs-12 value">
                            <h1 class="count text-uppercase">Marketplace &nbsp;<i class="fa fa-hand-o-right"></i> <?php echo $model->marketplace; ?></h1>
                            <p>How valuable is this athlete's endorsement?</p>
                        </div>

                        <div class="panel-body chart">
                            <div  id="Marketplace">
                                <div class="col-md-2 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Activity</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="text-center panel-body">

                                                <h1 class="font_color" data-toggle="popover" data-placement="top" data-original-title="PROFILE VIEWS" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> In March 2015, we began tracking the amount of times each athlete's profile is viewed by a marketer. This lifetime count is tracked and displayed here.<br/>HOW TO USE IT:<br/>If you're looking to build a campaign with a popular athlete, take a look at their profile views. The more views, the higher the demand for this athlete's endorsement.">
                                                    <?php
                                                    if (!empty($model->marketplace_activity_profileviews)) {
                                                        echo $model->marketplace_activity_profileviews;
                                                    } else {
                                                        echo 'N/A';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Profile Views</p>

                                                <h1 class="font_color" data-toggle="popover" data-placement="top" data-original-title="COMPLETED DEALS" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>Once an athlete has successfully fulfilled their activation requirements for an endorsement, the deal is determined ompleted and a datapoint is added to the completed deals category<br/>HOW TO USE IT:<br/>Athletes that have previously completed a deal on are more likely to approve future campaigns. For more confident expectations, find athletes with a high completed deals count.">
                                                    <?php
                                                    if (!empty($model->marketplace_acceptancerate)) {
                                                        echo $model->marketplace_activity_completeddeals;
                                                    } else {
                                                        echo 'N/A.';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Completed Deals</p>
                                            </div>
                                            <div class="text-right">
<!--                                                <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Acceptance Rate</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="text-center panel-body">
                                                <h1 class="font_color">
                                                    <?php
                                                    if (!empty($model->marketplace_acceptancerate)) {
                                                        echo $model->marketplace_acceptancerate . ' %';
                                                    } else {
                                                        echo 'N/A.';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Acceptance Rate</p>
                                            </div>

                                            <div class="text-right">
<!--                                                <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">GC Score</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="text-center panel-body">
                                                <img src="<?php echo Yii::app()->baseUrl . '/images/gcscore.png' ?>">
                                                <!--                                                <h4 class="font_color">N/A</h4>
                                                                                                <p>Average Agent Response Time</p>
                                                                                                
                                                                                                <h4 class="font_color">N/A</h4>
                                                                                                <p>Average Influencer Response Time</p>-->
                                                <?php
//                                            if(!empty($model->marketplace_response_averageagentresponsetime ))
//                                            {
//                                                echo $model->marketplace_response_averageagentresponsetime ;
//                                            }
//                                            else
//                                            {
//                                                echo 'N/A.';
//                                            }
                                                ?>

                                            </div>

                                            <div class="text-right">
<!--                                                <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4" data-toggle="popover" data-placement="top" data-original-title="PROJECTED CPE" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>Cost per Engagement (CPE) is the total price of the athlete over the estimated amount of engagements the endorsement will receive. <br/>HOW TO USE IT:<br/>Use cost per engagement to easily calculate projected ROI for your endorsement, and compare these results to cost per click rates on other digital mediums."> 
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"> Cost Per Engagement (cpe)</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="col-md-12 text-center panel-body">
                                                <h1 class="font_color">
                                                    <?php
                                                    if (!empty($model->marketplace_costperengagement)) {
                                                        echo '$ ' . $model->marketplace_costperengagement;
                                                    } else {
                                                        echo 'N/A.';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Projected CPE</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4" data-toggle="popover" data-placement="top" data-original-title="PROJECTED CPM" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/>Cost per Thousand (CPM) is affected by their total marketability (opendorse Score) and other market factors (for example, off-field events that affect an athlete's reputation). <br/>HOW TO USE IT:<br/>If you're looking to reach a large audience, find an athlete with a low CPM. It's a great way to get the most bang for your buck."> 
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Cost Per Thousand (cpm)</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="col-md-12 text-center panel-body">
                                                <h1 class="font_color">
                                                    <?php
                                                    if (!empty($model->marketplace_costperthousands)) {
                                                        echo '$ ' . $model->marketplace_costperthousands;
                                                    } else {
                                                        echo 'N/A.';
                                                    }
                                                    ?>
                                                </h1>
                                                <p>Projected CPM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-4" data-toggle="popover" data-placement="top" data-original-title="OPPORTUNITY VALUE" data-trigger="hover" data-html="true" data-content="HOW IT'S CALCULATED:<br/> A 0 - 100 comparison to their peers (where 100 is a better Opportunity Value than every other athlete on the system), based on the athlete's opendorse score.<br/>HOW TO USE IT:<br/>Looking for the most bang for your buck? Find an athlete with a high opportunity value, as they have most likely adjusted their endorsement costs to provide brands with more impression and engagements for less dollars."> 
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Opportunity Value</h3>
                                        </div>
                                        <div class="chart_height">
                                            <div class="col-md-12 text-center panel-body">
                                                <h1 class="font_color"><?php echo $model->marketplace_opportunityvalue; ?>%</h1>
                                                <p>Percentile</p>

                                                <script>

                                                    var graphOppotunity = Morris.Donut({
                                                        element: 'donut-oppotunity',
                                                        data: [
                                                            {label: "50th %", value: 50},
                                                            {label: "", value: 50}

                                                        ]
                                                    });
                                                    $('ul.nav a').on('shown.bs.tab', function (e) {
                                                        graphOppotunity.redraw();
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#wrapper -->


