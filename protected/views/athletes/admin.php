<?php
/* @var $this AthletesController */
/* @var $model Athletes */

$this->breadcrumbs=array(
	'Athletes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Athletes', 'url'=>array('index')),
	array('label'=>'Create Athletes', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#athletes-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Athletes</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'athletes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'athleteid',
		'userid',
		'firstname',
		'lastname',
		'sportscategoryid',
		'dob',
		/*
		'sex',
		'costpertweet',
		'costperinstagram',
		'updateddate',
		'score',
		'reach',
		'audience',
		'trust',
		'engagement',
		'marketplace',
		'reach_followers_active',
		'reach_followers_inactive',
		'reach_followers_fake',
		'reach_viralpotential',
		'audience_gender_male',
		'audience_gender_female',
		'audience_followers_interests',
		'audience_quality',
		'audience_extendedreach',
		'trust_sentiment_positive',
		'trust_sentiment_neutral',
		'trust_sentiment_negative',
		'trust_klout',
		'engagement_engagement_projectedengagementrate',
		'engagement_engagement_projectedengagement',
		'engagment_engagementrating',
		'engagement_twitteractivity_averageretweetsperpost',
		'engagement_engagement_twitteractivity_averagefavoritesperpost',
		'marketplace_activity_profileviews',
		'marketplace_activity_completeddeals',
		'marketplace_acceptancerate',
		'marketplace_response_averageagentresponsetime',
		'marketplace_response_averageinfluencerresponsetime',
		'marketplace_costperengagement',
		'marketplace_costperthousands',
		'marketplace_opportunityvalue',
		'trending',
		'lastupdatedadminid',
		'lastadminupdatedtime',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
