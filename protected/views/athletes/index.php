
<div class="container-fluid">
    <div class="container agent_dash">
        <div class="row">
            <div class="col-md-12">
                <h1 class="fonr-color text-center text-uppercase"> WELCOME TO YOUR DASHBOARD </h1>
                <?php if (Yii::app()->user->hasFlash('success')) {
                    ?>
                    <div class="alert alert-success fade in">
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php }
                ?>
            </div>
            <div class="col-md-12 panel-body">
                <button onclick="window.location = '<?php echo Yii::app()->baseUrl; ?>/athletes/create';" type="button" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus"></i>
                    <span>Add New Athlete</span></button>
            </div>

            <div class="col-md-12 panel-body">
                <div class="row">
                    <?php //$Agent = Agent::model()->find('user_id=' . Yii::app()->user->id); ?>
                    <?php
//                    $totalMR = Yii::app()->db->createCommand()
//                            ->select('sum(reach) as totalMR')
//                            ->from('athletes')
//                            ->where('userid = ' . Yii::app()->user->id)
//                            ->queryRow();
//                    $totalEng = Yii::app()->db->createCommand()
//                            ->select('sum(engagement) as totalEng')
//                            ->from('athletes')
//                            ->where('userid = ' . Yii::app()->user->id)
//                            ->queryRow();
                    ?>
                    <div class="col-sm-3">
                        <div class="panel mini-box">
                            <span class="box-icon">
                                <i class="fa fa-wifi white-color"></i>
                            </span>
                            <div class="box-info">
                                <p class="size-h2 white-color"><?php echo isset($agent->total['MarketReach']) ? $agent->total['MarketReach'] : '0'; ?></p>
                                <p class="white-color size">Total Market Reach</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="panel mini-box">
                            <span class="box-icon">
                                <i class="fa fa-location-arrow white-color"></i>
                            </span>
                            <div class="box-info">
                                <p class="size-h2 white-color"><?php echo isset($agent->total['Engagement']) ? $agent->total['Engagement'] : '0'; ?> </p>
                                <p class="white-color size">Total Engagement</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="panel mini-box bg-green">

                            <div class="box-info">
                                <p class="size-h2 white-color text-center">
                                    <?php
                                    //Total earned
                                    //$Earned = Deals::model()->findAll('agentid=' . Yii::app()->user->id . ' AND dealstatusid=2');
                                    //$AgentShare = Agent::model()->find('user_id=' . Yii::app()->user->id)->agentshare;
                                    //echo '<pre>'.print_r($Earned,1).'</pre>';
                                    //$TotalEarned = 0;
                                    //$TotalEarnedoriginal = 0;
                                    //foreach ($Earned as $earned) {
                                    //$TotalEarned += ( $earned['cost'] * ($AgentShare / 100));
                                    //$TotalEarned = $TotalEarnedoriginal;
                                    //}

                                    echo '$';  echo isset($agent->totalEarned) ? $agent->totalEarned : '0';

                                    //$criteria = new CDbCriteria(); //
                                    //$criteria->condition = 'agentid=' . Yii::app()->user->id . ' AND dealstatusid !=4'; //
                                    //$getEarns = Deals::model()->findAll($criteria);
                                    // echo Yii::app()->user->id; die();
                                    //$dealcount = count($getEarns);
                                    //CVarDumper::dump($agent->dealCount, 1000, true);  
                                    //$getsum = 0;
                                    //foreach ($getEarns as $key => $getEarn) {
                                    //$getsum += $getEarn->agentshare;
                                    //}
                                    // get count
                                    //if ($getsum > 1000) {
                                    //$result = floor($getsum / 1000) . 'K';
                                    //} else {
                                    //$result = $getsum;
                                    //}
                                    ?>
                                </p>
                                <p class="white-color size text-center">Total Earned</p>
                            </div>
                        </div>
                    </div><div class="col-sm-2">
                        <div class="panel mini-box bg-green">

                            <div class="box-info">
                                <p class="size-h2 white-color text-center"><?php echo isset($agent->dealCount) ? $agent->dealCount : '0'; ?></p>
                                <p class="white-color size text-center">Deal Count</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="panel mini-box bg-green">

                            <div class="box-info">
                                <p class="size-h2 white-color text-center">
                                    <?php
                                    //$criteria = new CDbCriteria(); //
                                    //$criteria->condition = 'userid=' . Yii::app()->user->id; //
                                    //$getEarns = Deals::model()->findAll($criteria);
                                    //$getAthletet = Athletes::model()->count($criteria);
                                    echo isset($dataProvider->TotalItemCount) ? $dataProvider->TotalItemCount : '0';
                                    ?>
                                </p>
                                <p class="white-color size text-center">Total Athletes</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <?php
                    //CVarDumper::dump($dataProvider->TotalItemCount, 1000, true);
                    if ($dataProvider != 'empty') {
                        ?>
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider,
                            'itemView' => '_view',
                        ));
                        ?>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>
