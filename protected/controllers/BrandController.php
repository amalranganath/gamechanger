<?php

/**
 * Brand Controller
 */
//CVarDumper::dump(Yii::app()->getClientScript() , 10000, true);
class BrandController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        //$currentuser=Users::model()->find('id='.Yii::app()->user->id)->usertype;
        $arr = [];
        if ($this->putUserRules(1)) {//if agent
            $arr = array('admin', 'brands', 'tempsavedeals');
        } elseif ($this->putUserRules(2)) {
            $arr = array('marketplace', 'addtocart', 'searchathlete', 'update', 'marketplace_profile', 'mycart', 'payment', 'ajaxdelete', 'FormSubmit', 'PlaceToOrder', 'Pending', 'Approved', 'Fullfilled', 'Dashboard', 'Disapproved','expired');
        }


        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                //'actions'=>array('create','update','admin','marketplace','brands','searchathlete','addtocart','tempsavedeals'),
                'actions' => $arr,
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Brand;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Brand'])) {
            $model->attributes = $_POST['Brand'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $id = Brand::model()->find('user_id=' . Yii::app()->user->id)->id;
        $model = $this->loadModel($id);

        $fileName = '';
        $lookupimages = new Lookupimages;
        if ($model->imageid) {
            $lookupimages = Lookupimages::model()->find(array('condition' => 'imageid = "' . $model->imageid . '"'));
            if ($lookupimages) {
                $fileName = $lookupimages->imagename;
            }
        }

        if (isset($_POST['Brand'])) {
            //echo '<pre>'.print_r($_POST['Brand'],1).'</pre>'; die();
            $model->attributes = $_POST['Brand'];
            $firstVisit = false;
            if ($model->completed == 0) {
                $firstVisit = $model->completed = 1;
            }
            if (empty($fileName)) {
                $lookupimages->imageid = $model->imageid = $this->getNextImageId(); // to fix a Dead lock issue
                $lookupimages->validatorList->add(
                        CValidator::createValidator('required', $lookupimages, 'imagename', array('message' => 'Image cannot be blank'))
                );
            }

            // Image handing
            $uploadedFile = CUploadedFile::getInstance($lookupimages, 'imagename');
            if ($uploadedFile) {
                $lookupimages->imagename = $uploadedFile;
            }

            if ($model->validate() && $lookupimages->validate()) {
                if ($model->save(FALSE)) {
                    // Single image save
                    if ($uploadedFile) {
                        // If an image is selected
                        $this->singleImageUpload($lookupimages, '/images/brands/', $model->id, (!empty($fileName) ? $fileName : ''));
                    }
                    
                    Yii::app()->user->setFlash('success', "You have successfully updated your profile!");
                    if ($firstVisit) {
                        //send emails to super admins
                        foreach (User::model()->findAll('superuser=1') as $admin) {
                            $content = 'Hi ' . $admin->profile->firstname . ',<br/>';
                            $content.='A new Brand (' . ucfirst($model->firstname) . ') has registered at Game Changer. Please <a href="' . Yii::app()->createAbsoluteUrl('gamechanger_admin/brand/update/', array('id' => $model->id)) . '">Click here</a> to view details.<br/>(Reference : Brand ID = ' . $model->user_id . ')';

                            self::sendMail($admin->email, 'Notify Brand registration', $content);
                        }
                        $this->redirect(array('/brand/marketplace'));
                    }
                    //$this->redirect(array('/brand/marketplace'));
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
            'lookupimages' => $lookupimages,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $userid = Yii::app()->user->id;
        // Deals::model()->find()
        $orders = Orders::model()->findAll('userid=' . $userid);
        $arr = [];
        foreach ($orders as $key => $value) {
            $arr[$key] = $value->orderid;
        }
        if ($orders) {
            $model = new Deals;
            $criteria = new CDbCriteria;
            $criteria->addCondition('dealstatusid=2');
            $criteria->addInCondition('orderid', $arr);

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));
            if ($dataProvider->getTotalItemCount() == 0) {
                $this->redirect('marketplace');
            }

            $this->render('dashboard', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('noorder');
        }
    }

    public function actionPending() {

        $userid = Yii::app()->user->id;
        // Deals::model()->find()

        if ($orders = Orders::model()->findAll('userid=' . $userid)) {
            $arr = [];
            foreach ($orders as $key => $value) {
                $arr[$key] = $value->orderid;
            }
            $model = new Deals;
            $criteria = new CDbCriteria;
            //$criteria->with = array('order');
            $criteria->addInCondition('orderid', $arr);
            $criteria->addCondition('dealstatusid=0');

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

            $this->render('pending', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('noorder');
        }
    }

    public function actionApproved() {

        $userid = Yii::app()->user->id;
        // Deals::model()->find()
        $orders = Orders::model()->findAll('userid=' . $userid);
        $arr = [];
        foreach ($orders as $key => $value) {
            $arr[$key] = $value->orderid;
        }
        if ($orders) {
            $model = new Deals;
            $criteria = new CDbCriteria;
            $criteria->with = array('order');
            $criteria->addCondition('dealstatusid=1');
            $criteria->addInCondition('t.orderid', $arr);

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

            $this->render('approved', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('noorder');
        }
    }

    public function actionFullfilled() {

        $userid = Yii::app()->user->id;
        // Deals::model()->find()
        $orders = Orders::model()->findAll('userid=' . $userid);
        $arr = [];
        foreach ($orders as $key => $value) {
            $arr[$key] = $value->orderid;
        }
        if ($orders) {
            $model = new Deals;
            $criteria = new CDbCriteria;
            $criteria->with = array('order');
            $criteria->addCondition('dealstatusid=2 OR dealstatusid=5');
            $criteria->addInCondition('t.orderid', $arr);

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

            $this->render('fullfilled', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('noorder');
        }
    }

    public function actionDisapproved() {
        $userid = Yii::app()->user->id;
        // Deals::model()->find()
        $orders = Orders::model()->findAll('userid=' . $userid);
        $arr = [];
        foreach ($orders as $key => $value) {
            $arr[$key] = $value->orderid;
        }
        if ($orders) {
            $model = new Deals;
            $criteria = new CDbCriteria;
            $criteria->with = array('order');
            $criteria->addCondition('dealstatusid=4');
            $criteria->addInCondition('t.orderid', $arr);

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

            $this->render('disapproved', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('noorder');
        }
    }
    
    public function actionExpired() {
        $userid = Yii::app()->user->id;
        // Deals::model()->find()
        $orders = Orders::model()->findAll('userid=' . $userid);
        $arr = [];
        foreach ($orders as $key => $value) {
            $arr[$key] = $value->orderid;
        }
        if ($orders) {
            $model = new Deals;
            $criteria = new CDbCriteria;
            $criteria->with = array('order');
            $criteria->addCondition('dealstatusid=3');
            $criteria->addInCondition('t.orderid', $arr);

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

            $this->render('expired', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('noorder');
        }
    }

    //Search athleteis
    public function actionSearchAthlete() {
        if (Yii::app()->request->isAjaxRequest) {
            //echo print_r($_POST,1);
            $athlete = new Athletes;
            $page = isset($_GET['Athletes_page']) ? $_GET['Athletes_page'] : 1;
            $criteria = new CDbCriteria;
            //$criteria->with = array('spaortscatagory');
            //compare title
            //$criteria->compare('t.name', $_POST['Athlete']['name'], true);
            //$criteria->addSearchCondition('firstname',$_POST['Athlete']['name']);
            //$criteria->select = "t.*, CONCAT(t.firstname, ' ', t.lastname) AS `t.fullname`";
            $limit = 12;
            $criteria->join = 'INNER JOIN sportscatagory cat ON t.sportscategoryid = cat.sportscatagory_id';
            $criteria->limit = $limit;
            $criteria->offset = ($page-1)*$limit;
            $criteria->addcondition("(firstname LIKE '%" . $_POST['Athlete']['name'] . "%' OR lastname LIKE '%" . $_POST['Athlete']['name'] . "%' OR CONCAT(firstname,' ',lastname) LIKE '%" . $_POST['Athlete']['name'] . "%' OR cat.name LIKE '%" . $_POST['Athlete']['name'] . "%' OR team LIKE '%" . $_POST['Athlete']['name'] . "%')");
            $dataProvider = new CActiveDataProvider('Athletes', array('criteria' => $criteria, 'pagination' => array(
                    'pageSize' => $limit,
                ),));

            $this->renderPartial('search_athletes', array('dataProvider' => $dataProvider), false);
        }
    }

    public function actionMarketplace() {

        if (Brand::model()->find('user_id=' . Yii::app()->user->id)->completed == 1) {

            $athlete = new Athletes;

            $criteria = new CDbCriteria;
            $criteria->addSearchCondition('trending', 1);
            //$criteria->limit = 12;
            $dataProvider_trend = new CActiveDataProvider($athlete, array('criteria' => $criteria,'pagination' => array(
                    'pageSize' => 12,
                )));

            $criteria1 = new CDbCriteria;
            //$criteria1->addBetweenCondition('created_at', $beforemonth, $today, 'AND');
            //$criteria1->limit = 12;
            $criteria1->order = "athleteid DESC";

            $dataProvider_recent = new CActiveDataProvider($athlete, array('criteria' => $criteria1,'pagination' => array(
                    'pageSize' => 12,
                )));

            //$criteria2 = new CDbCriteria;
            //$criteria2->addCondition('costpertweet < 100');
            //$dataProvider_under = new CActiveDataProvider($athlete, array('criteria' => $criteria2));
            //dataProvider=new CActiveDataProvider('Brand');
            $this->render('index', array(
                'dataProvider_trend' => $dataProvider_trend,
                'dataProvider_recent' => $dataProvider_recent,
                    //'dataProvider_under' => $dataProvider_under,
            ));
        } else {
            $this->redirect(array('update'));
        }
    }

    public function actionMarketplace_Profile($id) {

        $model = Athletes::model()->findByPk($id);
        $Athlete = new Athletes;

        //$criteria = new CDbCriteria;
        //$criteria->addCondition('reach=' . $model->reach);
        //$dataProvider = new CActiveDataProvider($Athlete, array('criteria' => $criteria));

        $this->render('_profile_view', array('model' => $model));
    }

    public function actionAddtocart() {

        $dealtemp = new Dealstemp;
        if (isset($_POST['athleteid'])) {
            $athleteid = $_POST['athleteid'];
            $userid = Yii::app()->user->id;

            $athlete = Athletes::model()->find('athleteid=' . $athleteid);

            $dealtemp->agentid = $athlete->userid;
            $dealtemp->athleteid = $athleteid;
            $dealtemp->userid = $userid;
            $Dealstemp = Dealstemp::model()->find('athleteid=' . $athleteid . ' AND userid=' . $userid . ' AND placetoorder=0');
            if ($Dealstemp == false) {
                $dealtemp->save(false);
                $already_in = false;
            } else {
                $already_in = true;
            }
            $this->renderPartial('_cart_menu_open', array('id' => $_POST['athleteid'], 'already_in' => $already_in), false);
        }
    }

    public function actionMyCart() {
        $userid = Yii::app()->user->id;

        $criteria = new CDbCriteria;
        $criteria->with = array('image');
        $criteria->addCondition('placetoorder=0', 'AND');
        $criteria->addSearchCondition('userid', $userid);

        $dataProvider = new CActiveDataProvider('Dealstemp', array('criteria' => $criteria));

        $criteria2 = new CDbCriteria;
        $criteria->with = array('image');
        $criteria2->addCondition('completed=1', 'AND');
        $criteria2->addCondition('placetoorder=0', 'AND');
        $criteria2->addCondition("userid='$userid'");

        $dataProvider2 = new CActiveDataProvider('Dealstemp', array('criteria' => $criteria2));

        $this->render('mycart', array('dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2));
        //$this->render('test',array('dataProvider'=>$dataProvider));
        //$this->redirect(Yii::app()->baseUrl.'/brand/mycart');
    }

    public function actionDashboard() {
        
        $userid = Yii::app()->user->id;
        // Deals::model()->find()
        $orders = Orders::model()->findAll('userid=' . $userid);
        $arr = []; 
        foreach ($orders as $key => $value) {
            $arr[$key] = $value->orderid;
        }
        if ($orders) { 
            $model = new Deals;
            $criteria = new CDbCriteria;
            $criteria->addCondition('dealstatusid=2 OR dealstatusid=5');
            $criteria->addInCondition('orderid', $arr);

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));
            if ($dataProvider->getTotalItemCount() == 0) {
                Yii::app()->user->setFlash('info', 'You do not have any fulfiled deals!');//info               
                $this->redirect('marketplace');
            }

            $this->render('dashboard', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('noorder');
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Brand('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Brand']))
            $model->attributes = $_GET['Brand'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Brand the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Brand::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Brand $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'brand-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjaxDelete() {

        Dealstemp::model()->find('dealid=' . $_POST['dealid'])->delete();
        //        $Dealstemp = Dealstemp::model()->findAllByAttributes(array(
        //            'userid'=> Yii::app()->user->id
        //        ));
        //        $Dealstemp=count($Dealstemp);
        $this->renderPartial('_cart_menu_open', array('id' => $_POST['athleteid'], 'already_in' => false), false);
    }

    //
    public function actionFormSubmit() {
        $error = false;
        $image = '';

        $errors = array();

        if (isset($_POST['radio' . $_POST['dealid']]) == false) {
            $errors[] = 'Deal typ is required!';
            $error = true;
        }
        if ($_POST['txtarea'] == '') {
            $errors[] = 'Description is required!';
            $error = true;
        }
        if ($_POST['campaigndate'] == '') {
            $errors[] = 'Campaign Date is required!';
            $error = true;
        }
        if ($_POST['campaigndate'] != '' && $_POST['campaigndate'] < date('Y-m-d')) {
            $errors[] = 'Campaign Date should be a future date!';
            $error = true;
        }
        if ($_POST['campaigntime'] == '') {
            $errors[] = 'Campaign Time is required!';
            $error = true;
        }

        //        -------------------------image validate-----------------------------

        if ($_FILES["image"]["name"] != '') {

            $uploadOk = 1;
            $extension = strtolower(pathinfo(basename($_FILES["image"]["name"]), PATHINFO_EXTENSION));
            // Allow certain file formats
            if ($extension != "jpg" && $extension != "png" && $extension != "jpeg") {
                $image = "Sorry, only JPG & PNG files are allowed!";
                $uploadOk = 0;
            }

            // Check file size
            if ($_FILES["image"]["size"] > 500000) {
                $image = "Sorry, your file is too large!";
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                $image = "Sorry, your file was not uploaded!";
                $error = true;
                // if everything is ok, try to upload file
            }
        }
        //        -------------------------End image validate---------------------------
        //upload image
        $imageid = 0;
        if ($error == false && $_FILES["image"]["name"] != '') {
            $imagename = time() . '.' . $extension;
            if (move_uploaded_file($_FILES["image"]["tmp_name"], "images/deals/" . "/$imagename")) {
                $Lookupimage = new Lookupimages();
                $Lookupimage->imagename = $imagename;
                if ($Lookupimage->save(false)) {
                    $imageid = $Lookupimage->imageid;
                }
                //end image save
                $imageuploaded = "The file " . basename($_FILES["image"]["name"]) . " has been uploaded.";
            } else {
                $image = "Sorry, there was an error uploading your file!";
                $error = true;
            }
        }

        if ($error == false) {

            $Dealtemp = Dealstemp::model()->find('dealid=' . $_POST['dealid']);
            //$Dealtemp->loadModel($_POST['dealid']);

            $Dealtemp->description = $_POST['txtarea'];
            $Dealtemp->campaigndate = $_POST['campaigndate'];
            $Dealtemp->ampm = substr($_POST['campaigntime'], -2);
            $Dealtemp->campaigntime = $_POST['campaigntime'];

            if (isset($_POST['radio' . $_POST['dealid']])) {

                //$Athlete = Athletes::model()->findByPk($Dealtemp->athleteid);

                if ($_POST['radio' . $_POST['dealid']] == 'twitter') {
                    $Dealtemp->costpertweet = $Dealtemp->athlete->costpertweet;
                    $Dealtemp->costperinstagram = $Dealtemp->costperinstagramimg = 0;
                    $Dealtemp->dealtype = 0; //twitter
                    $Dealtemp->costpertweetimg = ($_FILES["image"]["name"] != '') ? $Dealtemp->athlete->costpertweetimg : 0;
                } else {
                    $Dealtemp->costperinstagram = $Dealtemp->athlete->costperinstagram;
                    $Dealtemp->costpertweet = $Dealtemp->costpertweetimg = 0;
                    $Dealtemp->dealtype = 1; //instagram
                    $Dealtemp->costperinstagramimg = ($_FILES["image"]["name"] != '') ? $Dealtemp->athlete->costperinstagramimg : 0;
                }
            }
            $Dealtemp->completed = 1;
            $Dealtemp->imageid = $imageid;

            if ($Dealtemp->save(false)) {

                $alert = 'Your deal is saved successfully.';

                $arr = ['type' => 'success', 'alert' => $alert];
                echo json_encode($arr);
            }
        } else {//if validated
            $alert= implode('<br/>', $errors);
            $alert.= $image ;

            $arr = ['type' => 'error', 'alert' => $alert];
            echo json_encode($arr);
        }
    }

    public function actionPlaceToOrder($id) {

        $userid = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->addCondition('userid=' . $userid, 'AND');
        $criteria->addCondition('completed="1"');
        $criteria->addCondition('placetoorder="0"');

        $Dealtemp = Dealstemp::model()->findAll($criteria);

        $Order = new Orders;
        $Order->userid = $userid;
        $Order->paymentid = $id;
        $Order->save();
        $Order = Orders::model()->find('paymentid='.$id);
        //CVarDumper::dump($Order, 1000, true); die;
        foreach ($Dealtemp as $dealtmp) {
            //$Athlete = Athletes::model()->find('athleteid='.$dealtmp->athleteid);
            $Agent = Agent::model()->find('user_id=' . $dealtmp->athlete->userid);
            $Deals = new Deals;
            $Deals->orderid = $Order->orderid;
            // $Deals->dealstatusid='';
            $Deals->athleteid = $dealtmp->athleteid;
            $Deals->dealtype = $dealtmp->dealtype;

            if ($dealtmp->dealtype == 0) {
                $cost = $dealtmp->costpertweet + $dealtmp->costpertweetimg;
            } else {
                $cost = $dealtmp->costperinstagram + $dealtmp->costperinstagramimg;
            }

            $Deals->cost = $cost;
            $Deals->dealrequesteddate = date('Y-m-d');
            $Deals->costpertweet = $dealtmp->costpertweet;
            $Deals->costperinstagram = $dealtmp->costperinstagram;
            $Deals->costpertweetimg = $dealtmp->costpertweetimg;
            $Deals->costperinstagramimg = $dealtmp->costperinstagramimg;
            $Deals->imageid = $dealtmp->imageid;
            $Deals->description = $dealtmp->description;
            $Deals->campaigndate = $dealtmp->campaigndate;
            $Deals->campaigntime = $dealtmp->campaigntime;
            $Deals->ampm = $dealtmp->ampm;
            $Deals->agentid = $dealtmp->athlete->userid;
            $Deals->agentsharepercentage = $Agent->agentshare;
            $Deals->dateofpaymentbybrand = $Order->orderdate;
            
            if ($Deals->save(false)) {
                //send an email to agent
                //$from = 'info@gamechangerworldwide.com';
                $content = 'Hi ' . $Deals->agentName . ',<br/>';
                $content.='Brand (' . $Deals->order->brand->brandname . ') has created a new deal for "' . $Deals->dealType . '" on Athlete (' . $Deals->athleteName . ').  Please make your decision within 3 days.<br/>';
                $content.='Please <a href="' . Yii::app()->createAbsoluteUrl('deals/pending') . '">Click here</a> to view pending deals.<br/>( Reference : Deal ID = ' . $Deals->dealid . ' )';

                //$mail = ['to' => $Deals->user->email, 'from' => $from, 'subject' => $subject, 'content' => $content, 'toname' => $Deals->agent->firstname];
                //self::swiftMailer($mail);
                self::sendMail($Deals->user->email, 'You have a new Deal', $content);

                Dealstemp::model()->updateByPk($dealtmp->dealid, array("placetoorder" => 1));
            }

            //echo $dealtmp->description.'<br>';
        }
        Yii::app()->user->setFlash('success', "Thank you for your payment. Your order has being successfully placed.");
        $this->redirect(array('brand/payment'));

        //echo '<pre>'.print_r($Dealtemp,1).'</pre>';
    }

    /**
     * Manages Payment model.
     */
    public function actionPayment() {
        //$model = new Payment('searchUser');
        //$model->unsetAttributes();  // clear any default values   
        //if (isset($_GET['Payment'])) {
        //var_dump($_GET['Payment']);
        //$model->attributes = $_GET['Payment'];
        //}
        //Yii::app()->user->setFlash('error', "Thank you testing!");
        $orders = new Orders('searchUser');
        $orders->unsetAttributes();  // clear any default values
        if (isset($_GET['Orders'])) {
            //var_dump($_GET['Payment']);
            $Orders->attributes = $_GET['Orders'];
        }
        if (isset($_GET['status']))
            $orders->status = $_GET['status'];

        $this->render('payment', array(
            'orders' => $orders,
                //'model' => $model,
        ));
    }

}
