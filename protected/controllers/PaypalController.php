<?php

class PaypalController extends Controller {

    public function actionBuy() {

        //isset()
        if (isset(Yii::app()->user->id))
            $userid = Yii::app()->user->id;
        else
            $this->redirect(array('/user/login'));

        // set
        $criteria = new CDbCriteria;
        $criteria->addCondition('userid=' . $userid, 'AND');
        $criteria->addCondition('completed="1"');
        $criteria->addCondition('placetoorder="0"');
        $Dealtemp = Dealstemp::model()->findAll($criteria);
        $totalCost = 0;
        $desc = array();
        foreach ($Dealtemp as $dealtmp):
            $totalCost += $dealtmp->costpertweet + $dealtmp->costperinstagram + $dealtmp->costpertweetimg + $dealtmp->costperinstagramimg;
            $desc[] = $dealtmp->athlete->firstname . ' ' . $dealtmp->athlete->lastname;
            //$Deals->dealrequesteddate = date('Y-m-d');            
        endforeach;

        $paymentInfo['Order']['paymentType'] = 'Authorization'; //Authorize to be captchered later
        $paymentInfo['Order']['theTotal'] = $totalCost;
        $paymentInfo['Order']['description'] = implode(', ', $desc);
        $paymentInfo['Order']['quantity'] = '1';

        //set session order total cost
        Yii::app()->user->setState("ORDERTOTAL", $totalCost);

        // call paypal 
        $result = Yii::app()->paypal->SetExpressCheckout($paymentInfo);
        //Detect Errors 
        if (!Yii::app()->paypal->isCallSucceeded($result)) {
            if (Yii::app()->paypal->apiLive === true) {
                //Live mode basic error message
                $error = 'We were unable to process your request. Please try again later (live mode)';
            } else {
                //Sandbox output the actual error message to dive in.
                $error = $result['L_LONGMESSAGE0'];
            }
            echo $error;
            Yii::app()->end();
        } else {
            // send user to paypal 
            $token = urldecode($result["TOKEN"]);

            $payPalURL = Yii::app()->paypal->paypalUrl . $token;
            $this->redirect($payPalURL);
        }
    }

    public function actionConfirm() {

        $token = trim($_GET['token']);
        $payerId = trim($_GET['PayerID']);

        $payment = new Payment;
        $payment->userid = Yii::app()->user->id;
        $payment->ipaddress = $_SERVER['SERVER_ADDR'];

        $result = Yii::app()->paypal->GetExpressCheckoutDetails($token);

        $result['PAYMENTACTION'] = 'Authorization';
        $result['PAYERID'] = $payment->payerid = $payerId;
        $result['TOKEN'] = $payment->token = $token;
        $result['ORDERTOTAL'] = $payment->amount = Yii::app()->user->getState('ORDERTOTAL');

        //Detect errors 
        if (!Yii::app()->paypal->isCallSucceeded($result)) {
            if (Yii::app()->paypal->apiLive === true) {
                //Live mode basic error message
                $error = 'We were unable to process your request. Please try again later (live mode)';
            } else {
                //Sandbox output the actual error message to dive in.
                $error = $result['L_LONGMESSAGE0'];
            }
            echo $error;
            $payment->msg = $error;
            $payment->status = 0;
            $payment->save();
            Yii::app()->end();
        } else {

            $paymentResult = Yii::app()->paypal->DoExpressCheckoutPayment($result);

            //Detect errors  
            if (!Yii::app()->paypal->isCallSucceeded($paymentResult)) {
                if (Yii::app()->paypal->apiLive === true) {
                    //Live mode basic error message
                    $error = 'We were unable to process your request. Please try again later (live mode)';
                } else {
                    //Sandbox output the actual error message to dive in.
                    $error = $paymentResult['L_LONGMESSAGE0'];
                }
                echo $error;
                $payment->msg = $error;
                $payment->status = 0;
                $payment->save();
                Yii::app()->end();
            } else {
                //payment was completed successfully
                $payment->transactionid = $paymentResult['TRANSACTIONID'];
                $payment->time = $paymentResult['ORDERTIME'];
                $payment->status = 1;
                $payment->save();
                //$this->render('confirm');
                $this->redirect(array('/brand/placetoorder', 'id' => $payment->getPrimaryKey()));
            }
        }
    }

    public function actionCancel() {
        //The token of the cancelled payment typically used to cancel the payment within your application
        $token = $_GET['token'];
        Yii::app()->user->setFlash('info', "You have canceled the payment ( token = $token) !");
        $this->redirect(array('/brand/mycart'));
        //$this->render('cancel');
    }

    public function actionDirectPayment() {
        $paymentInfo = array('Member' =>
            array(
                'first_name' => 'name_here',
                'last_name' => 'lastName_here',
                'billing_address' => 'address_here',
                'billing_address2' => 'address2_here',
                'billing_country' => 'country_here',
                'billing_city' => 'city_here',
                'billing_state' => 'state_here',
                'billing_zip' => 'zip_here'
            ),
            'CreditCard' =>
            array(
                'payment_type' => '',
                'card_number' => 'number_here',
                'expiration_month' => 'month_here',
                'expiration_year' => 'year_here',
                'cv_code' => 'code_here'
            ),
            'Order' =>
            array('theTotal' => 1.00)
        );

        /*
         * On Success, $result contains [AMT] [CURRENCYCODE] [AVSCODE] [CVV2MATCH]  
         * [TRANSACTIONID] [TIMESTAMP] [CORRELATIONID] [ACK] [VERSION] [BUILD] 
         *  
         * On Fail, $ result contains [AMT] [CURRENCYCODE] [TIMESTAMP] [CORRELATIONID]  
         * [ACK] [VERSION] [BUILD] [L_ERRORCODE0] [L_SHORTMESSAGE0] [L_LONGMESSAGE0]  
         * [L_SEVERITYCODE0]  
         */

        $result = Yii::app()->paypal->DoDirectPayment($paymentInfo);

        //Detect Errors 
        if (!Yii::app()->paypal->isCallSucceeded($result)) {
            if (Yii::app()->paypal->apiLive === true) {
                //Live mode basic error message
                $error = 'We were unable to process your request. Please try again later (live mode)';
            } else {
                //Sandbox output the actual error message to dive in.
                $error = $result['L_LONGMESSAGE0'];
            }
            echo $error;
        } else {
            //Payment was completed successfully, do the rest of your stuff
        }

        Yii::app()->end();
    }

}
