<?php
/**
 * 
 */

class DealsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'pending', 'approved', 'disapproved', 'fulfilled', 'approval', 'disapproval', 'expired', 'Approved', 'Accepted', 'Fulfilled'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Deals;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Deals'])) {
            $model->attributes = $_POST['Deals'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->dealid));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Deals'])) {
            $model->attributes = $_POST['Deals'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->dealid));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Deals');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionApproval() {

        $Deal = Deals::model()->findByPk($_GET[0]['dealid']);
        $isLast = $Deal->isLastDeal();

        //Capture the cost per this deal
        $captureInfo['COMPLETETYPE'] = $isLast ? 'complete' : 'NotComplete'; //when complete paypal do voide automatically
        $captureInfo['AUTHORIZATIONID'] = $Deal->order->payment->transactionid;
        $captureInfo['AMOUNT'] = $Deal->cost;
        $paymentResult = Yii::app()->paypal->DoCapture($captureInfo);
        //CVarDumper::dump($paymentResult, 1000, true);
        //Detect payment errors
        if (!Yii::app()->paypal->isCallSucceeded($paymentResult)) {
            if (Yii::app()->paypal->apiLive === true) {
                //Live mode basic error message
                $error = 'We were unable to process your request. Please try again later (live mode)';
            } else {
                //Sandbox output the actual error message to dive in.
                $error = $paymentResult['L_LONGMESSAGE0'];
            }
            //echo $error;
            $Deal->order->payment->msg = $error;
            $Deal->order->payment->save();
            Yii::app()->user->setFlash('danger', $error);
            $this->redirect(array('/deals/pending'));
            //Yii::app()->end();
        } else {
            //capture payment was completed successfully 
            $Deal->order->payment->captured += $paymentResult['AMT']; //same PARENTTRANSACTIONID
            $Deal->order->payment->msg = $paymentResult['PAYMENTSTATUS'];
            $Deal->order->payment->status = $isLast ? 3 : 2;
            $Deal->order->payment->save();
            if($isLast){
                $Deal->order->status = 1;
                $Deal->order->save();
            }
        }
        //CVarDumper::dump($Deal->order->payment, 10000, true);
        $Deal->dealstatusid = 1;
        $Deal->dealapproveddate = date("Y-m-d H:i:s");
        if ($Deal->save(false)) {
            //send an email to brand
            $content = 'Hi ' . $Deal->order->brand->brandname . ',<br/>';
            $content.='Your deal with agent (' . $Deal->agentName . ') is approved. <br> Charged amount: (AUD) ' . $paymentResult['AMT'] . '.<br/>';
            $content.='Please <a href="' . Yii::app()->createAbsoluteUrl('brand/approved') . '">Click here</a> to view deals.<br/>(Reference : Deal ID = ' . $Deal->dealid . ')';
            //$mail = ['to' => $Deal->order->user->email, 'from' => $from, 'subject' => $subject, 'content' => $content, 'toname' => $Deal->order->brand->brandname];
            //self::swiftMailer($mail);
            self::sendMail($Deal->order->user->email, 'One of Your Deals Approved', $content);

            //send an emails to super admins
            foreach (User::model()->findAll('superuser=1') as $admin) {
                $content = 'Hi ' . $admin->profile->firstname . ',<br/>';
                $content.='Agent (' . $Deal->agentName . ') has approved the deal with Brand ('.$Deal->order->brand->brandname.'). <br> Charged amount: (AUD) ' . $paymentResult['AMT'] . '.<br/>';
                $content.='Please <a href="' . Yii::app()->createAbsoluteUrl('gamechanger_admin/deals/', array('id'=>$Deal->dealid)) . '">Click here</a> to view the deal.<br/>(Reference : Deal ID = ' . $Deal->dealid . ')';
                //$mail = ['to' => $Deal->order->user->email, 'from' => $from, 'subject' => $subject, 'content' => $content, 'toname' => $Deal->order->brand->brandname];
                //self::swiftMailer($mail);
                self::sendMail($admin->email, 'Notification For Deal Approval', $content);
            }

            Yii::app()->user->setFlash('success', "Successfully Approved!");
            $this->redirect(array('/deals/pending'));
        }
    }

    public function actionDisapproval() {

        $Deal = Deals::model()->findByPk($_GET[0]['dealid']);
        //voide if the last deal
        if ($Deal->isLastDeal()) {
            $paymentResult = Yii::app()->paypal->DoVoid(array('AUTHORIZATIONID' => $Deal->order->payment->transactionid));
            //CVarDumper::dump($paymentResult, 1000, true);
            //Detect payment errors
            if (!Yii::app()->paypal->isCallSucceeded($paymentResult)) {
                if (Yii::app()->paypal->apiLive === true) {
                    //Live mode basic error message
                    $error = 'We were unable to process your request. Please try again later (live mode)';
                } else {
                    //Sandbox output the actual error message to dive in.
                    $error = $paymentResult['L_LONGMESSAGE0'];
                }
                //echo $error;
                $Deal->order->payment->msg = $error;
                $Deal->order->payment->save();
                Yii::app()->user->setFlash('danger', $error);
                $this->redirect(array('/deals/pending'));
                //Yii::app()->end();
            } else {
                //voide captured payment completed successfully 
                //$Deal->order->payment->msg = $paymentResult['MSGSUBID'];
                $Deal->order->payment->status = 3;
                $Deal->order->payment->save();
                
                $Deal->order->status = 1;
                $Deal->order->save();
            }
        }
        $Deal->dealstatusid = 4;
        if ($Deal->save(false)) {
            //send an email to brand
            $content = 'Hi ' . $Deal->order->brand->brandname . ',<br/>';
            $content.='Your deal with agent (' . $Deal->agentName . ') has been rejected and cost for the deal will be refunded.<br/>';
            $content.='Please <a href="' . Yii::app()->createAbsoluteUrl('brand/disapproved') . '">Click here</a> to view deals.<br/>(Reference : Deal ID = ' . $Deal->dealid . ')';
            //$mail = ['to' => $Deal->order->user->email, 'from' => $from, 'subject' => $subject, 'content' => $content, 'toname' => $Deal->order->brand->brandname];
            //self::swiftMailer($mail);
            self::sendMail($Deal->order->user->email, 'One of Your Deals Disapproved', $content);

            Yii::app()->user->setFlash('success', "Successfully Disapproved!");
            $this->redirect(array('/deals/pending'));
        }
    }

    public function actionPending() {
        //Yii::app()->user->setFlash('success', "Thank you testing!");
        //$dataProvider=new CActiveDataProvider('Deals');
        $model = new Deals;
        $userid = Yii::app()->user->id;

        $criteria = new CDbCriteria;
        $criteria->addCondition('dealstatusid=0');
        $criteria->addCondition('agentid=' . $userid);

        $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

        $this->render('pending', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionApproved() {
        $model = new Deals;
        $userid = Yii::app()->user->id;

        $criteria = new CDbCriteria;
        $criteria->addCondition('dealstatusid=1');
        $criteria->addCondition('agentid=' . $userid);

        $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

        $this->render('approved', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionDisapproved() {
        $model = new Deals;
        $userid = Yii::app()->user->id;

        $criteria = new CDbCriteria;
        $criteria->addCondition('dealstatusid=4');
        $criteria->addCondition('agentid=' . $userid);

        $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

        $this->render('disapproved', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionExpired() {
        $model = new Deals;
        $userid = Yii::app()->user->id;

        $criteria = new CDbCriteria;
        $criteria->addCondition('dealstatusid=3');
        $criteria->addCondition('agentid=' . $userid);

        $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

        $this->render('expired', array(
            'dataProvider' => $dataProvider,
        ));
    }
    
    public function actionFulfilled() {
        $model = new Deals;
        $userid = Yii::app()->user->id;

        $criteria = new CDbCriteria;
        $criteria->addCondition('dealstatusid=2 OR dealstatusid=5');
        $criteria->addCondition('agentid=' . $userid);

        $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria));

        $this->render('fullfilled', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Deals('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Deals']))
            $model->attributes = $_GET['Deals'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Deals the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Deals::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Deals $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'deals-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
