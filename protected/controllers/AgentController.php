<?php

class AgentController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $arr = [];
        if ($this->putUserRules(1)) {
            $arr = array('create', 'update', 'brands', 'dashboard', 'PitchForm');
        }
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('dashboard','update','brands','create'),
                'actions' => $arr,
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Agent;

        if ($this->getUserType() == 1) {

            if (isset($_POST['Agent'])) {
                $model->attributes = $_POST['Agent'];
                $model->user_id = Yii::app()->user->id;
                if ($model->save())
                    $this->redirect(array('update', 'id' => $model->id));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        }else {
            throw new CHttpException(404, 'The specified post cannot be found.');
        }
    }

    public function actionBrands() {
        $model = new Brand;

        if (isset($_POST['Brand']['brandname'])) {//brand search
            $criteria = new CDbCriteria;
            $criteria->addCondition('t.completed=1');
            $criteria->addCondition('c.status=1');
            $criteria->join = 'LEFT JOIN users c ON c.id=t.user_id';

//                $criteria->addSearchCondition('brandname',$_POST['Brand']['brandname']);
            $criteria->addcondition(" (brandname LIKE '%" . $_POST['Brand']['brandname'] . "%' )");

            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria, 'pagination' => array('pageSize' => '10')));
            $this->renderPartial('_brands', array('dataProvider' => $dataProvider, 'model' => $model), false);
        } else {

            $criteria = new CDbCriteria;
            $criteria->addCondition('t.completed=1');
            $criteria->addCondition('c.status=1');
            $criteria->join = 'LEFT JOIN users c ON c.id=t.user_id';
            $dataProvider = new CActiveDataProvider($model, array('criteria' => $criteria, 'pagination' => array('pageSize' => '10')));

            $this->render('brand', array(
                'dataProvider' => $dataProvider, 'model' => $model,
            ));
        }
    }

    public function actionDashboard() {
//		$model=new Agent;
//
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//
//		if(isset($_POST['Agent']))
//		{
//			$model->attributes=$_POST['Agent'];
//			if($model->save())
//				$this->redirect(array('view','id'=>$model->id));
//		}
//
//		$this->render('create',array(
//			'model'=>$model,
//		));

        $dataProvider = new CActiveDataProvider('Agent');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {
        $id = Agent::model()->find('user_id=' . Yii::app()->user->id)->id;
        $model = $this->loadModel($id);

        if (isset($_POST['Agent'])) {
            $firstVisit = false;
            $model->attributes = $_POST['Agent'];
            if ($model->completed == 0) {
                $firstVisit = $model->completed = 1;
            }
            $model->agentshare = 0;
            //echo '<pre>'.print_r($model->attributes,1).'</pre>';die();

            if ($model->save(true)) {
                Yii::app()->user->setFlash('success', "You have successfully updated your profile!");
                if ($firstVisit) {
                    //send emails to super admins
                    foreach (User::model()->findAll('superuser=1') as $admin) {
                        $content = 'Hi ' . $admin->profile->firstname . ',<br/>';
                        $content.='A new Agent (' . ucfirst($model->firstname) . ') has registered at Game Changer. Please <a href="' . Yii::app()->createAbsoluteUrl('gamechanger_admin/agent/update/', array('id' => $model->id)) . '">Click here</a> to view details.<br/>(Reference : Agent ID = ' . $model->user_id . ')';

                        self::sendMail($admin->email, 'Notify Agent registration', $content);
                    }
                    $this->redirect(array('/athletes/dashboard'));
                }
                //$this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
//		$dataProvider=new CActiveDataProvider('Agent');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
        $model = new Agent;

        // Uncomment the following line if AJAX validation is needed
//		 $this->performAjaxValidation($model);

        if (isset($_POST['Agent'])) {
            // echo '<pre>'. print_r($_POST['Agent'],1).'</pre>';die();
            $model->attributes = $_POST['Agent'];

            if ($model->save()) {
//				$this->redirect(array('view','id'=>$model->id));
                Yii::app()->user->setFlash('success-registration', "Your acount is activated!");
                $this->redirect(array('/user/login'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionPitchForm() {

        if (!empty($_POST['pitch2brand']) && !empty($_POST['athlete'])) {

            $Usersemail = Users::model()->findAll(array('select' => 'email', 'condition' => 'usertype=0 AND superuser=1'));

            $athletename = $_POST['athlete'];
            $message = $_POST['pitch2brand'];
            $branduserid = $_POST['brandid'];

            $Brand = Brand::model()->find('id=' . $branduserid);

            $Agent = Agent::model()->find('user_id=' . Yii::app()->user->id);
            //var_dump($Brand); die;
            //$Users=Users::model()->find('id='.$branduserid);
            //echo '<pre>'.print_r($Users,1).'</pre>';

            /* foreach($Usersemail as $user){

              $toemail=$user['email'];

              $from='info@endorsexpress.com';
              $subject=$Agent->firstname.' '.$Agent->lastname.' to a '.$Brand->brandname;
              $content='<p><b>Brand Name:</b> '.$Brand->brandname.'</p>';
              $content.='<p><b>Athlete Name:</b> '.$athletename.'</p>';
              $content.='<p><b>Brand User Name:</b> '.$Brand->firstname.' '.$Brand->lastname.'</p>';
              $content.='<p><b>Brand Email:</b> '.$Users->email.'</p>';
              $content.='<p><b>Message:</b> '.$message.'</p>';
              // $content.='<p> Kind regards,</p>';
              ///  $content.='<p> Endorsexpress Team.</p>';
              $fromname='Endors Agent';
              $toname='Admin';
              $mail=['to'=>$toemail,'from'=>$from,'subject'=>$subject,'content'=>$content,'fromname'=>$fromname,'toname'=>$toname];
              self::swiftMailer($mail);
              } */
            //send emails to super admins
            foreach (User::model()->findAll('superuser=1') as $admin) {
                $subject = 'Pitch from ' . $Agent->firstname . ' ' . $Agent->lastname . ' to ' . $Brand->brandname;

                $content = '<p><b>Brand Name:</b> ' . $Brand->brandname . '</p>';
                $content.='<p><b>Athlete Name:</b> ' . $athletename . '</p>';
                $content.='<p><b>Brand User Name:</b> ' . $Brand->firstname . ' ' . $Brand->lastname . '</p>';
                $content.='<p><b>Brand Email:</b> ' . $Brand->user->email . '</p>';
                $content.='<p><b>Message:</b> ' . $message . '</p>';

                self::sendMail($admin->email, $subject, $content);
            }
            Yii::app()->user->setFlash('success', "Successfully sent emails to super admins!");
            $this->redirect('brands');
        } else {
            Yii::app()->user->setFlash('danger', "You must fill all fields!");

            $this->redirect('brands');
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Agent('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Agent']))
            $model->attributes = $_GET['Agent'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Agent the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Agent::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Agent $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'agent-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
