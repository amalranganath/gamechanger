<?php

class AthletesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        $arr = [];
        if ($this->putUserRules(1)) {//if agent
            $arr = array('dashboard', 'update', 'create','profile');
        } elseif ($this->putUserRules(2)) {
                     $arr=array('profile');
            //$this->redirect(array('brand/marketplace'));
        }

        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions'=>$arr,
                'actions' => $arr,
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'create'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {//Profile
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }
    
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionProfile($id) {//
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/plugins/morris/raphael.min.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/plugins/morris/morris.min.js');
        $this->render('profile', array(
            'model' => $this->loadModel($id),
        ));
    }
    
    public function actionIndex() {
        $this->redirect(array('brand/marketplace'));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Athletes;
        $lookupimages = new Lookupimages;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Athletes'])) {
            //echo '<pre>'.print_r($_POST['Athletes'],1).'</pre>'; die();
            $model->attributes = $_POST['Athletes'];
            $model->firstname = $_POST['Athletes']['firstname'];
            $model->lastname = $_POST['Athletes']['lastname'];
            $model->team = $_POST['Athletes']['team'];

            if (!is_numeric($_POST['Athletes']['dob']['0']) || !is_numeric($_POST['Athletes']['dob']['1']) || !is_numeric($_POST['Athletes']['dob']['2'])) {
                $model->validatorList->add(
                        CValidator::createValidator('numerical', $model, 'dob')
                );
            } else {
                $dob = $_POST['Athletes']['dob']['0'] . '/' . $_POST['Athletes']['dob']['1'] . '/' . $_POST['Athletes']['dob']['2'];
                $dob = strtotime($dob);
                $dob = date('Y-m-d', $dob);
                $model->dob = $dob;
            }
            // cost per tweet or cost per instagram validation---------------------------------------------
            $costval = 0;
            if (!empty($_POST['Athletes']['costpertweet']) && !empty($_POST['Athletes']['costpertweetimg']) || !empty($_POST['Athletes']['costperinstagram']) && !empty($_POST['Athletes']['costperinstagramimg'])) {

                if (!empty($_POST['Athletes']['costpertweet']) && empty($_POST['Athletes']['costpertweetimg']) || !empty($_POST['Athletes']['costpertweetimg']) && empty($_POST['Athletes']['costpertweet'])) {
                    $costval = 0;
                } else {
                    $costval = 1;
                }
                if (!empty($_POST['Athletes']['costperinstagram']) && empty($_POST['Athletes']['costperinstagramimg']) || !empty($_POST['Athletes']['costperinstagramimg']) && empty($_POST['Athletes']['costperinstagram'])) {
                    $costval = 0;
                } else {
                    $costval = 1;
                }
            }
            if ($costval != 1) {
                if (empty($_POST['Athletes']['costpertweet'])) {
                    //$costval=false;
                    $model->validatorList->add(
                            CValidator::createValidator('required', $model, 'costpertweet')
                    );
                }
                if (empty($_POST['Athletes']['costperinstagram'])) {
                    $model->validatorList->add(
                            CValidator::createValidator('required', $model, 'costperinstagram')
                    );
                }

                if (!empty($_POST['Athletes']['costpertweet'])) {
                    if (empty($_POST['Athletes']['costpertweetimg'])) {
                        $model->validatorList->add(
                                CValidator::createValidator('required', $model, 'costpertweetimg')
                        );
                    }
                } elseif (!empty($_POST['Athletes']['costperinstagram'])) {
                    if (empty($_POST['Athletes']['costperinstagramimg'])) {
                        $model->validatorList->add(
                                CValidator::createValidator('required', $model, 'costperinstagramimg')
                        );
                    }
                }
            }
            // End validation

            $lookupimages->imageid = $model->imageid = $this->getNextImageId(); // to fix a Dead lock issue
            //$lookupimages->attributes = $_POST['Lookupimages'];
            // Image handing
            $uploadedFile = CUploadedFile::getInstance($lookupimages, 'imagename');
            if ($uploadedFile) {
                $lookupimages->imagename = $uploadedFile;
            }

            if ($model->validate() && $lookupimages->validate()) {
                $model->costperinstagramimg = $_POST['Athletes']['costperinstagramimg'];
                $model->costpertweetimg = $_POST['Athletes']['costpertweetimg'];
                $model->team = $_POST['Athletes']['team'];
                if ($model->save(FALSE)) {
                    // Single image save
                    if ($uploadedFile) {
                        // If an image is selected
                        $this->singleImageUpload($lookupimages, '/images/athlete/', $model->athleteid);
                    } else {
                        // If not
                        $this->defaultImageUpload($model->imageid, '/images/athlete/', $model->athleteid);
                    }
                    //send email to super admins
                    foreach (User::model()->findAll('superuser=1') as $admin) {
                        //CVarDumper::dump($admin, 1000, true); die;
                        //$from = 'info@gamechangerworldwide.com';
                        $subject = 'A new Athlete Created';
                        $content = 'Hi ' . $admin->profile->firstname . ',<br/>';
                        $content.='A new Athlete created by Agent (' . $model->agent->firstname . ' '.$model->agent->lastname. '). Please <a href="' . Yii::app()->createAbsoluteUrl('gamechanger_admin/athletes/update/', array('id'=>$model->athleteid)) . '">Click here</a> to view the details.<br/>( Reference : Athlete ID = ' . $model->athleteid . ' )';
                        //$fromname = 'gamechanger world wide';

                        //$mail = ['to' => $admin->email, 'from' => $from, 'subject' => $subject, 'content' => $content, 'toname' => $admin->profile->firstname];
                        //self::swiftMailer($mail);
                        self::sendMail($admin->email, $subject, $content);
                    }
                    Yii::app()->user->setFlash('success', "Athlete created successfully!");
                    $this->redirect(array('athletes/dashboard'));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
            'lookupimages' => $lookupimages,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        $lookupimages = Lookupimages::model()->find(array('condition' => 'imageid = "' . $model->imageid . '"'));
        $fileName = $lookupimages->imagename;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        //CVarDumper::dump($model->agent, 1000, true);
        if (isset($_POST['Athletes'])) {
            // echo '<pre>'.  print_r($_POST['Athletes'], 1).'</pre>';die();

            $model->attributes = $_POST['Athletes'];
            $model->firstname = $_POST['Athletes']['firstname'];
            $model->lastname = $_POST['Athletes']['lastname'];

            if (!is_numeric($_POST['Athletes']['dob']['0']) || !is_numeric($_POST['Athletes']['dob']['1']) || !is_numeric($_POST['Athletes']['dob']['2'])) {
                $model->validatorList->add(
                        CValidator::createValidator('numerical', $model, 'dob')
                );
            } else {
                $dob = $_POST['Athletes']['dob']['0'] . '/' . $_POST['Athletes']['dob']['1'] . '/' . $_POST['Athletes']['dob']['2'];
                $dob = strtotime($dob);
                $dob = date('Y-m-d', $dob);
                $model->dob = $dob;
            }
            // cost per tweet or cost per instagram validation---------------------------------------------
            $costval = 0;
            if (!empty($_POST['Athletes']['costpertweet']) && !empty($_POST['Athletes']['costpertweetimg']) || !empty($_POST['Athletes']['costperinstagram']) && !empty($_POST['Athletes']['costperinstagramimg'])) {

                if (!empty($_POST['Athletes']['costpertweet']) && empty($_POST['Athletes']['costpertweetimg']) || !empty($_POST['Athletes']['costpertweetimg']) && empty($_POST['Athletes']['costpertweet'])) {
                    $costval = 0;
                } else {
                    $costval = 1;
                }
                if (!empty($_POST['Athletes']['costperinstagram']) && empty($_POST['Athletes']['costperinstagramimg']) || !empty($_POST['Athletes']['costperinstagramimg']) && empty($_POST['Athletes']['costperinstagram'])) {
                    $costval = 0;
                } else {
                    $costval = 1;
                }
            }
            if ($costval != 1) {
                if (empty($_POST['Athletes']['costpertweet'])) {
                    //$costval=false;
                    $model->validatorList->add(
                            CValidator::createValidator('required', $model, 'costpertweet')
                    );
                }
                if (empty($_POST['Athletes']['costperinstagram'])) {
                    $model->validatorList->add(
                            CValidator::createValidator('required', $model, 'costperinstagram')
                    );
                }

                if (!empty($_POST['Athletes']['costpertweet'])) {
                    if (empty($_POST['Athletes']['costpertweetimg'])) {
                        $model->validatorList->add(
                                CValidator::createValidator('required', $model, 'costpertweetimg')
                        );
                    }
                } elseif (!empty($_POST['Athletes']['costperinstagram'])) {
                    if (empty($_POST['Athletes']['costperinstagramimg'])) {
                        $model->validatorList->add(
                                CValidator::createValidator('required', $model, 'costperinstagramimg')
                        );
                    }
                }
            }
            // End validation

            $model->updateddate = date('Y-m-d h:i:s');

            // Image handing
            $uploadedFile = CUploadedFile::getInstance($lookupimages, 'imagename');
            if ($uploadedFile) {
                $lookupimages->imagename = $uploadedFile;
            }
            if (empty($_POST['Athletes']['costpertweet']) && empty($_POST['Athletes']['costperinstagram'])) {
                if (empty($_POST['Athletes']['costpertweet'])) {
                    $model->validatorList->add(
                            CValidator::createValidator('required', $model, 'costpertweet')
                    );
                }
                if (empty($_POST['Athletes']['costperinstagram'])) {
                    $model->validatorList->add(
                            CValidator::createValidator('required', $model, 'costperinstagram')
                    );
                }
            }

            if ($model->validate() && $lookupimages->validate()) {

                $model->team = $_POST['Athletes']['team'];

                // echo '<pre>'.print_r($model->attributes,1).'</pre>'; die();
                if ($model->save(FALSE)) {
                    // Single image save
                    if ($uploadedFile) {
                        $this->singleImageUpload($lookupimages, '/images/athlete/', $model->imageid, $fileName);
                        $model->save(FALSE);
                    }
                    Yii::app()->user->setFlash('updateSuccess', "Successfully Updated!");
                    $this->redirect(array('Athletes/update?id=' . $model->athleteid));
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
            'lookupimages' => $lookupimages,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionDashboard() {
        $agent = Agent::model()->find('user_id=' . Yii::app()->user->id);

        if ($agent->completed == 1) {
            //CVarDumper::dump($agent->totalEarned, 1000, true);  
            $model = new Athletes;

            if ($athleties = $model->findAll('userid=' . YII::app()->user->id)) {

                //$dataProvider = new CActiveDataProvider('Athletes');
                $dataProvider = $model->search();
                $this->render('index', array(
                    'agent' => $agent,
                    'dataProvider' => $dataProvider,
                ));
            } else {
                $this->render('index', array(
                    'dataProvider' => 'empty',
                ));
            }
        } else {
            $this->redirect(Yii::app()->baseUrl . '/agent/update');
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Athletes('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Athletes']))
            $model->attributes = $_GET['Athletes'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Athletes the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Athletes::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Athletes $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'athletes-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
