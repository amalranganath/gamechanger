<!--Sidebar Menu Items - These collapse to the responsive navigation menu on small screens--> 

<?php //var_dump(Yii::app()->controller->action->id); ?>
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">

        <li class="<?php echo (Yii::app()->controller->id == 'site') ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('/'); ?>"><i class="fa fa-fw fa-home"></i> Dashboard</a>
        </li>
        <li class="<?php echo (Yii::app()->controller->id == 'payment' && Yii::app()->controller->action->id == 'index') ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('/payment'); ?>"><i class="fa fa-fw fa-credit-card"></i> Orders (Payments)</a>
        </li>
        <li class="<?php echo (Yii::app()->controller->id == 'payment' && Yii::app()->controller->action->id == 'agent') ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('/payment/agent'); ?>"><i class="fa fa-fw fa-bars"></i> Agents Earning Report</a>
        </li>
        <li class="<?php echo (Yii::app()->controller->id == 'athletes') ? 'active' : '' ?> ">
            <a href="<?php echo Yii::app()->createUrl('/athletes'); ?>"><i class="fa fa-fw fa-search"></i> Manage Athletes</a>
        </li> 

        <li class="<?php echo (Yii::app()->controller->id == 'agent') ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('/agent'); ?>"><i class="fa fa-fw fa-search"></i> Manage Agents</a>
        </li>

        <li class="<?php echo (Yii::app()->controller->id == 'brand') ? 'active' : '' ?>" >
            <a href="<?php echo Yii::app()->createUrl('/brand'); ?>"><i class="fa fa-fw fa-search"></i> Manage Brands</a>
        </li>

        <li class="<?php echo (Yii::app()->controller->id == 'deals') ? 'active' : '' ?>" >
            <a href="javascript:;" data-toggle="collapse" data-target="#demo" class=""><i class="fa fa-fw fa-arrows-v"></i> Manage Deals<i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="demo" class="collapse <?php echo (Yii::app()->controller->id == 'deals') ? 'in' : '' ?>">

                <li class="<?php echo (Yii::app()->controller->action->id == 'pending') ? 'active' : '' ?> ">
                    <a href="<?php echo Yii::app()->createUrl('/deals/pending'); ?>">Pending</a>
                </li>
                <li class="<?php echo (Yii::app()->controller->action->id == 'approved') ? 'active' : '' ?> ">
                    <a href="<?php echo Yii::app()->createUrl('/deals/approved'); ?>">Approved</a>
                </li>
                <li class="<?php echo (Yii::app()->controller->action->id == 'disapproved') ? 'active' : '' ?> ">
                    <a href="<?php echo Yii::app()->createUrl('/deals/disapproved'); ?>">Disapproved</a>
                </li>
                <li class="<?php echo (Yii::app()->controller->action->id == 'fullfilled') ? 'active' : '' ?> ">
                    <a href="<?php echo Yii::app()->createUrl('/deals/fullfilled'); ?>">Fulfilled</a>
                </li>
                <li class="<?php echo (Yii::app()->controller->action->id == 'expired') ? 'active' : '' ?> ">
                    <a href="<?php echo Yii::app()->createUrl('/deals/expired'); ?>">Expired</a>
                </li>
            </ul>
        </li>

<!--        <li>
            <a href="<?php //echo Yii::app()->baseUrl; ?>/user/admin"><i class="fa fa-fw fa-arrows-v"></i>All Users</a>
        </li>-->

    </ul>
</div>
<!--/.navbar-collapse--> 
