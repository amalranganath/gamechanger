<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Custom CSS -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/sb-admin.css" rel="stylesheet" />

        <!-- Morris Charts CSS -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/plugins/morris.css" rel="stylesheet" />

        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" rel="stylesheet" />

        <!-- Custom Fonts -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php if (Yii::app()->user->id) { ?>   
            <div id="wrapper">
                <!--Navigation--> 
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <?php
                    require_once('header_navi.php');
                    require_once('sidebar_navi.php');
                    ?>
                </nav>
                <div id="page-wrapper">
                    <div class="container-fluid">
                        <?php echo $content; ?> 
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!--#page-wrapper -->

            </div>
            <!-- /#wrapper -->

            <?php
        } else {

            echo $content;
        }
        ?>

        <!-- jQuery -->
        <!--<script src="<?php //echo Yii::app()->theme->baseUrl;  ?>/js/jquery.js"></script>-->

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/morris/raphael.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/morris/morris.min.js"></script>
        <!--<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/morris/morris-data.js"></script>-->
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    </body>

</html>


