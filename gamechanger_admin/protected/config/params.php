<?php
function getRange($start, $limit, $step = 1, $prepend = FALSE, $is_desc = FALSE) {
    $array = array();
     // in desending order
    for($i = $start; $i<= $limit; $i+=$step) {
//        $array[$i] = ($prepend == TRUE)? sprintf("%02s",$i): $i;
        if ($prepend == TRUE) {
            $i = sprintf("%02s",$i);
        } 
        $array[$i] = $i;
        
    }
    if($is_desc) {
        krsort($array);
    }
    return $array;
}

function getGroupId($start, $limit) {
    $array = array();
    for($i = $start; $i<= $limit; $i++) {
        $array[$i] = $i;
    }
    return $array;
}

return array(
    'adminEmail'=>'webmaster@example.com',
    'dob_years' => getRange((date('Y') - 70), date('Y'), 1, $prepend = TRUE, $is_desc = TRUE),
    'dob_months' => getRange(1, 12, 1, $prepend = TRUE, $is_desc = FALSE),
    'dob_days' => getRange(1, 31, 1, $prepend = TRUE, $is_desc = FALSE),
    'Hours' => getRange(1, 12, 1, $prepend = TRUE, $is_desc = FALSE),
    'pageSize' => 25,
);