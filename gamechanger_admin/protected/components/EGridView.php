<?php

/*
 * @info CGridView is Extended to display, multiple date of a related model and sum of a specific column
 * @author Amal Ranganath
 * @copyright Copyright &copy; 2015 Living Dreams (Pvt) Ltd
 * 
 */


Yii::import('zii.widgets.grid.CGridView');

class EGridView extends CGridView {

    public $rowDisplayRelated;
    /*
     * @param array $rowDisplayRelated
     * @keys => model Object, on String, columns array
     */
    public $relatedColumns = array();
    /*
     * @param array $relatedColumns
     * @keys => columns array the column names of table
     */
    public $rowGetToatal;
    /*
     * @param array $rowGetToatal
     * @keys => on String, refer String
     */
    public $rowCurrent = 0;
    public $dataCount = 0;
    public $total = 0;

    /*
     * @param array $rowGetToatal
     * @keys => on String, column String
     */

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init() {
        parent::init();
        if (isset($this->rowDisplayRelated)) {
            foreach ($this->rowDisplayRelated['columns'] as $k => $rc) {
                if (is_array($rc))
                    $rc = $rc[0];
                $this->relatedColumns[$k] = $rc;
            }
        }
    }

    /**
     * Renders the table body.
     */
    public function renderTableBody() {
        $data = $this->dataProvider->getData();
        $this->dataCount = $n = count($data);
        echo "<tbody>\n";

        if ($n > 0) {
            for ($row = 0; $row < $n; ++$row)
                $this->renderTableRow($row);
        } else {
            echo '<tr><td colspan="' . count($this->columns) . '" class="empty">';
            $this->renderEmptyText();
            echo "</td></tr>\n";
        }
        echo "</tbody>\n";
    }

    /**
     * Renders a table body row.
     * @param integer $row the row number (zero-based).
     */
    public function renderTableRow($row) {
        /*
         * Please Do not edit ! Start####### 
         */
        $htmlOptions = array();
        if ($this->rowHtmlOptionsExpression !== null) {
            $data = $this->dataProvider->data[$row];
            $options = $this->evaluateExpression($this->rowHtmlOptionsExpression, array('row' => $row, 'data' => $data));
            if (is_array($options))
                $htmlOptions = $options;
        }

        if ($this->rowCssClassExpression !== null) {
            $data = $this->dataProvider->data[$row];
            $class = $this->evaluateExpression($this->rowCssClassExpression, array('row' => $row, 'data' => $data));
        } elseif (is_array($this->rowCssClass) && ($n = count($this->rowCssClass)) > 0)
            $class = $this->rowCssClass[$row % $n];

        if (!empty($class)) {
            if (isset($htmlOptions['class']))
                $htmlOptions['class'].=' ' . $class;
            else
                $htmlOptions['class'] = $class;
        }

        echo CHtml::openTag('tr', $htmlOptions) . "\n";
        foreach ($this->columns as $column)
            $column->renderDataCell($row);
        echo "</tr>\n";
        /*
         * End####### Do not edit !
         */


        /*
         * Renders if isset rowGetToatal options
         */
        if (isset($this->rowGetToatal)) {
            //CVarDumper::dump($this->dataProvider->data[$row][$this->rowGetToatal['refer']], 10000, true);
            if ((isset($this->dataProvider->data[$row + 1]) && $this->rowCurrent != $this->dataProvider->data[$row + 1][$this->rowGetToatal['refer']]) || $row + 1 == $this->dataCount) {
                echo CHtml::openTag('tr', $htmlOptions) . "\n";
                if ($this->rowCurrent == 0) {
                    $this->total = $this->dataProvider->data[$row][$this->rowGetToatal['on']];
                } else {
                    $this->total += $this->dataProvider->data[$row][$this->rowGetToatal['on']];
                    foreach ($this->columns as $k => $column) {
                        echo CHtml::openTag('td', $this->rowGetToatal['htmlOptions']);
                        echo ($column->name == $this->rowGetToatal['on']) ? $this->total : (isset($this->columns[$k + 1]) && $this->columns[$k + 1]->name == $this->rowGetToatal['on'] ? 'Total' : '');
                        echo '</td>';
                    }
                    $this->total = 0;
                }
                $this->rowCurrent = $row + 1 < $this->dataCount ? $this->dataProvider->data[$row + 1][$this->rowGetToatal['refer']] : 0;
                echo "</tr>\n";
            } else {
                $this->total += $this->dataProvider->data[$row][$this->rowGetToatal['on']];
            }
        }

        /*
         * Renders if isset rowDisplayRelated options
         */
        if (isset($this->rowDisplayRelated)) {
            //$data = $this->dataProvider->data[$row];
            $relatedOn = $this->rowDisplayRelated['on'];

            if ($related = $this->rowDisplayRelated['model']->findAll($relatedOn . '=' . $this->dataProvider->data[$row]->$relatedOn)) {

                echo CHtml::openTag('tr', $htmlOptions) . "\n";
                echo CHtml::tag('td', array('colspan' => count($this->columns) - count($this->relatedColumns)));
                foreach ($this->relatedColumns as $c)
                    echo CHtml::openTag('td', array('class' => 'column-header')) . $this->rowDisplayRelated['model']->getAttributeLabel($c) . '</td>';
                echo "</tr>\n";
                //CVarDumper::dump($related-getAttributeLabel(), 1000, true); die;
                foreach ($related as $rrow) {
                    echo CHtml::openTag('tr', $htmlOptions) . "\n";
                    echo CHtml::tag('td', array('colspan' => count($this->columns) - count($this->rowDisplayRelated['columns'])));
                    foreach ($this->rowDisplayRelated['columns'] as $rc) {
                        $rhtmlOptions = array();
                        if (is_array($rc)) {
                            $rhtmlOptions = $rc[1];
                            $rc = $rc[0];
                        }
                        if (strpos($rc, '.') !== FALSE) {
                            $items = explode('.', $rc);
                            $obj = $rrow->$items[0];
                            $rc = $items[1];
                        } else {
                            $obj = $rrow;
                        }
                        echo CHtml::openTag('td', $rhtmlOptions);
                        echo isset($obj->$rc) ? $obj->$rc : 'Invalid column!';
                        echo '</td>';
                    }
                    echo "</tr>\n";
                }
            }
        }
    }

}
