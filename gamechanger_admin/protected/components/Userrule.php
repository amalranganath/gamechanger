<?php


class Userrule extends CWebModule
{
	

	static private $_getaccess;

	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'user.models.*',
			'user.components.*',
		));
	}
	
        
        public static function getAccess($userlevel) {
            
            if(is_array($userlevel))
                {
                 if (!self::$_getaccess) 
                 {
                $criteria = new CDbCriteria;
                $criteria->addInCondition('usertype',$userlevel,true); 
                $usernames = Users::model()->findAll($criteria);
                  
                $Access_name = array();
			foreach ($usernames as $username)
				array_push($Access_name,$username->username);
			self::$_getaccess = $Access_name;
               
                     }
//                     throw new CHttpException(403,'Invalid request. You are not allowed to delete this event.');
                     return self::$_getaccess;
                    
                }
         else {
             
             if (!self::$_getaccess) 
                 {
                $criteria = new CDbCriteria;
                $criteria->condition = 'usertype='.$userlevel;
                //Apply To Model
                $usernames = Users::model()->findAll($criteria);
                
          
                  
                $Access_name = array();
			foreach ($usernames as $username)
				array_push($Access_name,$username->username);
			self::$_getaccess = $Access_name;
               
                     }
//                     throw new CHttpException(403,'Invalid request. You are not allowed to delete this event.');
                     return self::$_getaccess;
              }

        }


}
