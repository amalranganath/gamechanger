<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function singleImageUpload($model, $path, $dirId, $fileName = '') {
        $paths = array(
            'filepath' => Yii::getPathOfAlias('webroot') . '/../' . $path . '/',
            '400x300' => Yii::getPathOfAlias('webroot') . '/../' . $path . '/400x300/',
            '200x150' => Yii::getPathOfAlias('webroot') . '/../' . $path . '/200x150/',
        );

        // Check the parent directry already exists
        if (!is_dir($paths['filepath'])) {
            // Create all directries

            foreach ($paths as $key => $value) {
                //mkdir($value, 0755, true); // or 0644
                mkdir($value, 0755, true);
            }
        } else {
            if (!is_dir($paths['400x300']))
                mkdir($paths['400x300'], 0755, true);
            if (!is_dir($paths['200x150']))
                mkdir($paths['200x150'], 0755, true);
        }

        // Create the image name if not given
        if (empty($fileName)) {
            // echo '<pre>'.print_r($model,1).'</pre>'; die();
            $ext = pathinfo($model->imagename->name, PATHINFO_EXTENSION);
            $fileName = $this->getRandomString() . '.' . $ext;
        }

        //return $this->createImage($model, $paths, $fileName);
        return $this->createImage($model, $paths, $fileName);
    }

    public function createImage($model, $paths, $fileName) {
        //$this->printThis($model,1);
        $model->imagename->saveAs($paths['filepath'] . $fileName);
        $this->crateThumbs($paths['filepath'], $paths['400x300'], $fileName, 400);
        $this->crateThumbs($paths['filepath'], $paths['200x150'], $fileName, 200);

        //$model->Name = $fileName;
        // echo 'IMAGEID:'.$model->imageid.' Filename:'.$fileName ; die();
        Lookupimages::model()->updateByPk($model->imageid, array('imagename' => $fileName));
        //if($model->save(FALSE)) {
        //}
        //$this->printThis($model->attributes);
        return $model->imageid;
    }

    public function crateThumbs($filepath, $thumbpath, $file_name, $thumbWidth) {
        $thumb = imagecreatefromjpeg($filepath . $file_name);
        $width = imagesx($thumb);
        $height = imagesy($thumb);
        // calculate thumbnail size
        $new_width = $thumbWidth;
        $new_height = floor($height * ( $thumbWidth / $width ));
        // create a new temporary image
        $tmp_img = imagecreatetruecolor($new_width, $new_height);
        // copy and resize old image into new image
        imagecopyresized($tmp_img, $thumb, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // save thumbnail into a file
        imagejpeg($tmp_img, $thumbpath . $file_name);
    }

    public function getNextImageId() {
        $lookupimages = new Lookupimages;
        $lookupimages->imagename = '';
        if ($lookupimages->save(FALSE)) {
            return $lookupimages->imageid;
        }
    }

    public function defaultImageUpload($ImageId, $path, $dirId) {
        $paths = array(
            'filepath' => Yii::getPathOfAlias('webroot') . $path . $dirId . '/',
            '400x300' => Yii::getPathOfAlias('webroot') . $path . $dirId . '/400x300/',
            '200x150' => Yii::getPathOfAlias('webroot') . $path . $dirId . '/200x150/',
        );

        // Check the parent directry already exists
        if (!is_dir($paths['filepath'])) {
            // Create all directries
            foreach ($paths as $key => $value) {
                mkdir($value, 0755, true); // or 0644
            }
        }

        $fileName = $this->getRandomString() . '.jpg';

        $this->saveDefaultImage($paths['filepath'], $fileName);
        $this->crateThumbs($paths['filepath'], $paths['400x300'], $fileName, 400);
        $this->crateThumbs($paths['filepath'], $paths['200x150'], $fileName, 200);

        $Lookupimages = Lookupimages::model()->findByPk($ImageId);
        $Lookupimages->imagename = $fileName;
        $Lookupimages->save(FALSE);
        //return $Lookupimages->ImageId;
    }

    public function saveDefaultImage($thumbpath, $file_name) {
        $defaultPath = Yii::getPathOfAlias('webroot') . '/images/default/default.jpg';
        $thumb = imagecreatefromjpeg($defaultPath);
        $width = imagesx($thumb);
        $height = imagesy($thumb);

        //$this->printThis($width, 1);
        //list($width, $height) = getimagesize($defaultPath);
        //calculate thumbnail size
        //$new_width = $thumbWidth;
        //$new_height = floor( $height * ( $thumbWidth / $width ) );

        $new_width = $width;
        $new_height = $height;
        // create a new temporary image
        $tmp_img = imagecreatetruecolor($new_width, $new_height);
        // copy and resize old image into new image
        imagecopyresized($tmp_img, $thumb, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // save thumbnail into a file
        imagejpeg($tmp_img, $thumbpath . $file_name);
    }

    public function getRandomString() {
        return rand(0, 9999) . time();
    }

    //send email using YiiMail extension
    public static function sendMail($to, $subject, $content = '') {
        if (isset(Yii::app()->controller))
            $controller = Yii::app()->controller;
        else
            $controller = new CController('YiiMail');
        $oldLayout = $controller->layout;
        $controller->layout = Yii::app()->mail->viewPath . '.layouts.main';

        $message = new YiiMailMessage;
        $message->addFrom('noreply@gamechangerworldwide.com', 'Game Changer');
        $message->subject = $subject;
        $params['content'] = $content;
        //if (php_sapi_name() == "cli") {
        // In cli-mode
        //$layoutPath = Yii::getPathOfAlias(Yii::app()->mail->viewPath.'.layouts.main').'.php';
        //$body = CConsoleCommand::renderFile($layoutPath, array('content'=>$body), true);
        //} else {
        // Not in cli-mode
        $body = $controller->render(Yii::app()->mail->viewPath . '.index', $params, true);
        //}
        $message->setBody($body, 'text/html');
        $message->addTo($to);
        //Send the email
        if (Yii::app()->mail->send($message)) {
            Yii::app()->user->setFlash('success', 'Successfully sent the email');
            return true;
        } else {
            Yii::app()->user->setFlash('failed', 'Error sending the email');
            return false;
        }
        $controller->layout = $oldLayout;
    }

    public function behaviors() {
        return array(
            'exportableGrid' => array(
                'class' => 'application.components.ExportableGridBehavior',
                'filename' => 'report.xls',
                //'csvDelimiter' => ';', //i.e. Excel friendly csv delimiter
        ));
    }

}
