<?php

/**
 * This is the model class for table "athletes".
 *
 * The followings are the available columns in table 'athletes':
 * @property integer $athleteid
 * @property integer $userid
 * @property string $firstname
 * @property string $lastname
 * @property integer $sportscategoryid
 * @property string $dob
 * @property string $sex
 * @property string $costpertweet
 * @property string $costperinstagram
 * @property integer $imageid
 * @property string $updateddate
 * @property double $score
 * @property string $reach
 * @property string $audience
 * @property string $trust
 * @property string $engagement
 * @property string $marketplace
 * @property string $reach_followers_active
 * @property string $reach_followers_inactive
 * @property string $reach_followers_fake
 * @property string $reach_viralpotential
 * @property string $reach_followers_reach
 * @property string $audience_gender_male
 * @property string $audience_gender_female
 * @property string $audience_followers_interests
 * @property string $audience_quality
 * @property string $audience_extendedreach
 * @property string $trust_sentiment_positive
 * @property string $trust_sentiment_neutral
 * @property string $trust_sentiment_negative
 * @property string $trust_klout
 * @property string $engagement_engagement_projectedengagementrate
 * @property string $engagement_engagement_projectedengagement
 * @property string $engagment_engagementrating
 * @property string $engagement_twitteractivity_averageretweetsperpost
 * @property string $engagement_engagement_twitteractivity_averagefavoritesperpost
 * @property string $marketplace_activity_profileviews
 * @property string $marketplace_activity_completeddeals
 * @property string $marketplace_acceptancerate
 * @property string $marketplace_response_averageagentresponsetime
 * @property string $marketplace_response_averageinfluencerresponsetime
 * @property string $marketplace_costperengagement
 * @property string $marketplace_costperthousands
 * @property string $marketplace_opportunityvalue
 * @property integer $trending
 * @property integer $lastupdatedadminid
 * @property string $lastadminupdatedtime
 * @property string $created_at
 */
class Athletes extends CActiveRecord {

    public $valuesArray = array();

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'athletes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('userid, firstname, lastname, sportscategoryid, sex, costpertweet, costperinstagram, updateddate, score, reach, audience, trust, engagement, marketplace, reach_followers_active, reach_followers_inactive, reach_followers_fake, reach_viralpotential, audience_gender_male, audience_gender_female, audience_followers_interests, audience_quality, audience_extendedreach, trust_sentiment_positive, trust_sentiment_neutral, trust_sentiment_negative, trust_klout, engagement_engagement_projectedengagementrate, engagement_engagement_projectedengagement, engagment_engagementrating, engagement_twitteractivity_averageretweetsperpost, engagement_engagement_twitteractivity_averagefavoritesperpost, marketplace_activity_profileviews, marketplace_activity_completeddeals, marketplace_acceptancerate, marketplace_response_averageagentresponsetime, marketplace_response_averageinfluencerresponsetime, marketplace_costperengagement, marketplace_costperthousands, marketplace_opportunityvalue, trending, lastupdatedadminid, lastadminupdatedtime, created_at, reach_followers_reach', 'required'),
            array('firstname, lastname, sportscategoryid, sex, score', 'required'),
            array('score, reach ,reach_followers_active, reach_followers_inactive, reach_followers_fake, reach_viralpotential, reach_followers_reach, audience, audience_gender_male, audience_gender_female, audience_quality, audience_extendedreach, trust, trust_sentiment_positive, trust_sentiment_neutral, trust_sentiment_negative, trust_klout, engagement_engagement_projectedengagementrate ,engagement_engagement_projectedengagement, engagment_engagementrating, engagement_twitteractivity_averageretweetsperpost, engagement_engagement_twitteractivity_averagefavoritesperpost, engagement, marketplace_activity_profileviews, marketplace_activity_completeddeals, marketplace_acceptancerate, marketplace_response_averageagentresponsetime, marketplace_response_averageinfluencerresponsetime, marketplace_costperengagement, marketplace_costperthousands, marketplace_opportunityvalue,  marketplace, costpertweet, costperinstagram', 'numerical'),
            array(
                'audience_followers_interests',
                'match', 'not' => true, 'pattern' => '/^([0-9  ]+)$/',
                'message' => 'Must start with a letter.',
            ),
            array('costpertweet, costperinstagram', 'my_required'),
            //array('userid, sportscategoryid, imageid, trending, lastupdatedadminid', 'numerical', 'integerOnly'=>true),
//			array('score', 'numerical'),
//			array('firstname, lastname, costpertweet, costperinstagram, reach, audience, trust, engagement, marketplace, reach_followers_active, reach_followers_inactive, reach_followers_fake, reach_viralpotential, audience_gender_male, audience_gender_female, audience_followers_interests, audience_quality, audience_extendedreach, trust_sentiment_positive, trust_sentiment_neutral, trust_sentiment_negative, trust_klout, engagement_engagement_projectedengagementrate', 'length', 'max'=>25),
//			array('sex', 'length', 'max'=>6),
//			array('engagement_engagement_projectedengagement, engagment_engagementrating, engagement_twitteractivity_averageretweetsperpost, engagement_engagement_twitteractivity_averagefavoritesperpost, marketplace_activity_profileviews, marketplace_activity_completeddeals, marketplace_acceptancerate, marketplace_response_averageagentresponsetime, marketplace_response_averageinfluencerresponsetime, marketplace_costperengagement, marketplace_costperthousands, marketplace_opportunityvalue', 'length', 'max'=>10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('athleteid, userid, firstname, lastname, sportscategoryid, dob, sex, costpertweet, costperinstagram, imageid, updateddate, score, reach, audience, trust, engagement, marketplace, reach_followers_active, reach_followers_inactive, reach_followers_fake, reach_viralpotential, audience_gender_male, audience_gender_female, audience_followers_interests, audience_quality, audience_extendedreach, trust_sentiment_positive, trust_sentiment_neutral, trust_sentiment_negative, trust_klout, engagement_engagement_projectedengagementrate, engagement_engagement_projectedengagement, engagment_engagementrating, engagement_twitteractivity_averageretweetsperpost, engagement_engagement_twitteractivity_averagefavoritesperpost, marketplace_activity_profileviews, marketplace_activity_completeddeals, marketplace_acceptancerate, marketplace_response_averageagentresponsetime, marketplace_response_averageinfluencerresponsetime, marketplace_costperengagement, marketplace_costperthousands, marketplace_opportunityvalue, trending, lastupdatedadminid, lastadminupdatedtime, created_at, team', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sportscategory' => array(self::BELONGS_TO, 'Sportscatagory', 'sportscategoryid'),
            'image' => array(self::BELONGS_TO, 'Lookupimages', 'imageid'),
            'user' => array(self::BELONGS_TO, 'Users', 'userid'),
            'agent' => array(self::BELONGS_TO, 'Agent', array('id' => 'user_id'), 'through' => 'user'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'athleteid' => 'Athlete ID',
            'userid' => 'Agent Name',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'sportscategoryid' => 'Sports Category',
            'dob' => 'Date Of Birth',
            'sex' => 'Sex',
            'costpertweet' => 'Cost per Tweet',
            'costperinstagram' => 'Cost per Instagram',
            'costpertweetimg' => 'Cost per Tweet Image',
            'costperinstagramimg' => 'Cost per Instagram Image',
            'imageid' => 'Profile Image',
            'updateddate' => 'Updateddate',
            'score' => 'Score',
            'reach' => 'Reach Value',
            'audience' => 'Audience Value',
            'trust' => 'Trust Value',
            'engagement' => 'Engagement Value',
            'marketplace' => 'Market Place Value',
            'reach_followers_active' => 'Reach Followers Active in %',
            'reach_followers_inactive' => 'Reach Followers Inactive in %',
            'reach_followers_fake' => 'Reach Followers Fake in %',
            'reach_viralpotential' => 'Reach Viral Potential',
            'reach_followers_reach' => 'Followers Growth',
            'audience_gender_male' => 'Audience Gender Male in %',
            'audience_gender_female' => 'Audience Gender Female in %',
            'audience_followers_interests' => 'Audience Followers Interests',
            'audience_quality' => 'Audience Quality',
            'audience_extendedreach' => 'Audience Extended Reach',
            'trust_sentiment_positive' => 'Trust Sentiment Positive',
            'trust_sentiment_neutral' => 'Trust Sentiment Neutral',
            'trust_sentiment_negative' => 'Trust Sentiment Negative',
            'trust_klout' => 'Trust Klout',
            'engagement_engagement_projectedengagementrate' => 'Projected Engagement Rate in %',
            'engagement_engagement_projectedengagement' => 'Projected Engagements',
            'engagment_engagementrating' => 'Engagement Rating',
            'engagement_twitteractivity_averageretweetsperpost' => 'Average Retweets Per Post',
            'engagement_engagement_twitteractivity_averagefavoritesperpost' => 'Average Favorites Per Post',
            'marketplace_activity_profileviews' => 'Profile Views',
            'marketplace_activity_completeddeals' => 'Completed Deals',
            'marketplace_acceptancerate' => 'Acceptance Rate in %',
            'marketplace_response_averageagentresponsetime' => 'Average Agent Response Time',
            'marketplace_response_averageinfluencerresponsetime' => 'Average Influencer Response Time',
            'marketplace_costperengagement' => 'Cost Per Engagement',
            'marketplace_costperthousands' => 'Cost Per Thousands',
            'marketplace_opportunityvalue' => 'Opportunity Value',
            'trending' => 'Set Trending',
            'lastupdatedadminid' => 'Last updated admin Id',
            'lastadminupdatedtime' => 'Last admin updated time',
            'created_at' => 'Created At',
            'team' => 'Team',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('athleteid', $this->athleteid);
        $criteria->compare('userid', $this->userid);
        $criteria->compare('firstname', $this->firstname, true);
        $criteria->compare('lastname', $this->lastname, true);
        $criteria->compare('sportscategoryid', $this->sportscategoryid);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('sex', $this->sex, true);
        $criteria->compare('costpertweet', $this->costpertweet, true);
        $criteria->compare('costperinstagram', $this->costperinstagram, true);
        $criteria->compare('imageid', $this->imageid);
        $criteria->compare('updateddate', $this->updateddate, true);
        $criteria->compare('score', $this->score);
        $criteria->compare('reach', $this->reach, true);
        $criteria->compare('audience', $this->audience, true);
        $criteria->compare('trust', $this->trust, true);
        $criteria->compare('engagement', $this->engagement, true);
        $criteria->compare('marketplace', $this->marketplace, true);
        $criteria->compare('reach_followers_active', $this->reach_followers_active, true);
        $criteria->compare('reach_followers_inactive', $this->reach_followers_inactive, true);
        $criteria->compare('reach_followers_fake', $this->reach_followers_fake, true);
        $criteria->compare('reach_viralpotential', $this->reach_viralpotential, true);
        $criteria->compare('reach_followers_reach', $this->reach_followers_reach, true);
        $criteria->compare('audience_gender_male', $this->audience_gender_male, true);
        $criteria->compare('audience_gender_female', $this->audience_gender_female, true);
        $criteria->compare('audience_followers_interests', $this->audience_followers_interests, true);
        $criteria->compare('audience_quality', $this->audience_quality, true);
        $criteria->compare('audience_extendedreach', $this->audience_extendedreach, true);
        $criteria->compare('trust_sentiment_positive', $this->trust_sentiment_positive, true);
        $criteria->compare('trust_sentiment_neutral', $this->trust_sentiment_neutral, true);
        $criteria->compare('trust_sentiment_negative', $this->trust_sentiment_negative, true);
        $criteria->compare('trust_klout', $this->trust_klout, true);
        $criteria->compare('engagement_engagement_projectedengagementrate', $this->engagement_engagement_projectedengagementrate, true);
        $criteria->compare('engagement_engagement_projectedengagement', $this->engagement_engagement_projectedengagement, true);
        $criteria->compare('engagment_engagementrating', $this->engagment_engagementrating, true);
        $criteria->compare('engagement_twitteractivity_averageretweetsperpost', $this->engagement_twitteractivity_averageretweetsperpost, true);
        $criteria->compare('engagement_engagement_twitteractivity_averagefavoritesperpost', $this->engagement_engagement_twitteractivity_averagefavoritesperpost, true);
        $criteria->compare('marketplace_activity_profileviews', $this->marketplace_activity_profileviews, true);
        $criteria->compare('marketplace_activity_completeddeals', $this->marketplace_activity_completeddeals, true);
        $criteria->compare('marketplace_acceptancerate', $this->marketplace_acceptancerate, true);
        $criteria->compare('marketplace_response_averageagentresponsetime', $this->marketplace_response_averageagentresponsetime, true);
        $criteria->compare('marketplace_response_averageinfluencerresponsetime', $this->marketplace_response_averageinfluencerresponsetime, true);
        $criteria->compare('marketplace_costperengagement', $this->marketplace_costperengagement, true);
        $criteria->compare('marketplace_costperthousands', $this->marketplace_costperthousands, true);
        $criteria->compare('marketplace_opportunityvalue', $this->marketplace_opportunityvalue, true);
        $criteria->compare('trending', $this->trending);
        $criteria->compare('lastupdatedadminid', $this->lastupdatedadminid);
        $criteria->compare('lastadminupdatedtime', $this->lastadminupdatedtime, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('team', $this->team, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'athleteid DESC',
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params->pageSize,
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Athletes the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /* public function GetSportCategoryName($param) {

      $sportcategory= Sportscatagory::model()->find('sportscatagory_id ='.$param);
      //   echo '<pre>'.print_r($sportcategory,1).'</pre>';
      //   die();
      if($sportcategory)
      {
      $name=$sportcategory->name;

      }
      $this->valuesArray = array(
      'name'=> $name,

      );

      return 1;
      } */

    public function GetAgentName($param) {

        $agent = Agent::model()->find('user_id =' . $param);

        if ($agent) {
            $firstname = $agent->firstname;
            $lastname = $agent->lastname;
        }
        $this->valuesArray = array(
            'name' => $firstname . ' ' . $lastname,
        );

        return 1;
    }

    public function my_required($attribute_name, $params) {
        if (empty($this->costpertweet) && empty($this->costperinstagram)
        ) {
            $this->addError($attribute_name, Yii::t('user', 'At least 1 of the field must be filled up properly'));

            return false;
        }

        return true;
    }

}
