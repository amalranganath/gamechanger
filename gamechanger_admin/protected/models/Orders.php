<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $orderid
 * @property integer $dealid
 * @property integer $userid
 * @property string $orderdate
 */
class Orders extends CActiveRecord
{
    /**
     * @var date Filter order date statrting from
     */
    public $from;
    
    /**
     * @var date Filter order date ending
     */
    public $to;
    
    /**
     * @var string Filter brand name
     */
    public $brandname;
    
    /**
     * @var string Filter transactionId
     */
    public $transactionId;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paymentid, userid, orderdate', 'required'),
			array('paymentid, userid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('orderid, paymentid, userid, orderdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user' => array(self::BELONGS_TO, 'Users', 'userid'),
                    'brand' => array(self::HAS_ONE, 'Brand', array('id'=>'user_id'), 'through'=>'user'),
                    'payment' =>array(self::BELONGS_TO, 'Payment', 'paymentid'),
                    'deals' =>array(self::HAS_MANY, 'Deals', 'orderid'), 
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'orderid' => 'Order ID',
			'payment' => 'Payment ID',
			'userid' => 'User ID',
			'orderdate' => 'Order Date & Time',
                        'srtatus' => 'Order Status'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
                $criteria->with = array('user','brand', 'payment');
                
                if(!empty($this->from))
                    $criteria->compare('t.orderdate', '>='.$this->from, true);
                if(!empty($this->to))
                    $criteria->compare('t.orderdate', '<='.$this->to.' 23:59:59', true);
		$criteria->compare('t.orderid',$this->orderid);
		$criteria->compare('t.paymentid',$this->paymentid);
		$criteria->compare('brand.brandname',$this->brandname, true);
		$criteria->compare('payment.transactionid', $this->transactionId, true);
		$criteria->compare('t.userid',$this->userid);
		//$criteria->compare('orderdate',$this->orderdate,true);
		$criteria->compare('t.status',$this->status);
                
                $criteria->order = 't.orderid DESC';
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        //'pagination' => array(
                        //    'pageSize' => Yii::app()->params->pageSize,
                        //),
		));
	}
        
        public function searchExport()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
            $pagination = Yii::app()->request->getParam('exportPdf', false) ? false : true;
		$criteria=new CDbCriteria;
                $criteria->with = array('user','brand', 'payment');
                
                if(!empty($this->from))
                    $criteria->compare('t.orderdate', '>='.$this->from, true);
                if(!empty($this->to))
                    $criteria->compare('t.orderdate', '<='.$this->to.' 23:59:59', true);
		//$criteria->compare('t.orderid',$this->orderid);
		//$criteria->compare('t.paymentid',$this->paymentid);
		$criteria->compare('brand.brandname',$this->brandname, true);
		$criteria->compare('payment.transactionid', $this->transactionId, true);
		//$criteria->compare('t.userid',$this->userid);
		//$criteria->compare('orderdate',$this->orderdate,true);
		$criteria->compare('t.status',$this->status);
                
                $criteria->order = 't.orderid DESC';
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=> false,
		));
	}
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        /*
         * @return dates count since the order being placed
         */
        public function getDaysSinceOrdered(){
            $interval = date_diff(date_create(strtotime(new CDbExpression('NOW()'))), date_create($this->orderdate));
            return $interval->format('%a');
        }
        
        /*
         * @return status 
         */
        public function getStatus(){            
            return $this->status ? 'Closed' : 'Open';
        }
}
