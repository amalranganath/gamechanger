<?php

/**
 * This is the model class for table "deals".
 *
 * The followings are the available columns in table 'deals':
 * @property integer $dealid
 * @property integer $orderid
 * @property integer $dealstatusid
 * @property integer $athleteid
 * @property integer $dealtype
 * @property integer $cost
 * @property string $dealrequesteddate
 * @property string $imageid
 * @property string $description
 * @property string $campaigndate
 * @property string $campaigntime
 * @property string $dealapproveddate
 * @property string $dealfullfilleddate
 * @property string $dealaccpeteddate removed
 * @property string $dateofpaymentbybrand
 * @property string $dateofpaymenttoagent
 * @property integer $agentid
 * @property string $agentsharepercentage
 * @property double $agentshare
 * @property double $fulfill_engagement
 * @property double $fulfill_reach
 * @property integer $fullfill_paidtoagent
 * @property integer $ampm
 * @property integer $actual_tweet
 */
class Deals extends CActiveRecord
{
    /**
     * @var date Filter order date statrting from
     */
    public $from;
    
    /**
     * @var date Filter order date ending
     */
    public $to;
    
    /**
     * @var string Filter agent name ending
     */
    public $agentname;
    
         public $valuesArray = array();
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'deals';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('orderid, dealstatusid, athleteid, dealtype, cost, dealrequesteddate, imageid, description, campaigndate, campaigntime, dealapproveddate, dealfullfilleddate, agentid, agentsharepercentage, agentshare, fulfill_engagement, fulfill_reach, fullfill_paidtoagent', 'required'),
			array('orderid, dealstatusid, athleteid, dealtype, cost, agentid, fullfill_paidtoagent', 'numerical', 'integerOnly'=>true),
			//array('agentshare, fulfill_engagement, fulfill_reach', 'numerical'),
			//array('imageid', 'length', 'max'=>100),
			//array('description', 'length', 'max'=>250),
			//array('agentsharepercentage', 'length', 'max'=>11),
                        array('fulfill_engagement, fulfill_reach, actual_tweet', 'numerical','on'=>'numeric_validation'),
			array('dateofpaymentbybrand, dateofpaymenttoagent', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dealid, orderid, dealstatusid, athleteid, dealtype, cost, dealrequesteddate, imageid, description, campaigndate, campaigntime, dealapproveddate, dealfullfilleddate, dateofpaymentbybrand, dateofpaymenttoagent, agentid, agentsharepercentage, agentshare, fulfill_engagement, fulfill_reach, fullfill_paidtoagent, agentname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'status' => array(self::BELONGS_TO, 'Dealstatus', 'dealstatusid'),
                    'user' => array(self::BELONGS_TO, 'Users', 'agentid'),
                    'agent' => array(self::HAS_ONE, 'Agent', array('id'=>'user_id'), 'through'=>'user'),
                    'athlete' => array(self::BELONGS_TO, 'Athletes', 'athleteid'),
                    'order' => array(self::BELONGS_TO, 'Orders', 'orderid'),
                    'image' => array(self::BELONGS_TO, 'Lookupimages', 'imageid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dealid' => 'Deal ID',
			'orderid' => 'Order ID',
			'dealstatusid' => 'Deal Status',
			'athleteid' => 'Athlete Name',
			'dealtype' => 'Deal type',
			'cost' => 'Cost',
			'dealrequesteddate' => 'Deal Requested Date',
			'imageid' => 'Image',
			'description' => 'Description',
			'campaigndate' => 'Campaign Date',
			'campaigntime' => 'Campaign Time',
			'dealapproveddate' => 'Approved Date',
			'dealfullfilleddate' => 'Fulfilled Date',
			'dateofpaymentbybrand' => 'Date of payment by brand',
			'dateofpaymenttoagent' => 'Date of payment to Agent',
			'agentid' => 'Agent Name',
			'agentsharepercentage' => 'Agent Share in %',
			'agentshare' => 'Agent Share',
			'fulfill_engagement' => 'Fulfill Engagement',
			'fulfill_reach' => 'Fulfill Reach',
			'fullfill_paidtoagent' => 'Paid To Agent',
                        'ampm'=> 'ampm',
                        'actual_tweet'=>'Actual Tweet',
                        'agentname' => 'Agent Name'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dealid',$this->dealid);
		$criteria->compare('orderid',$this->orderid);
		$criteria->compare('dealstatusid',$this->dealstatusid);
		$criteria->compare('athleteid',$this->athleteid);
		$criteria->compare('dealtype',$this->dealtype);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('dealrequesteddate',$this->dealrequesteddate,true);
		$criteria->compare('imageid',$this->imageid,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('campaigndate',$this->campaigndate,true);
		$criteria->compare('campaigntime',$this->campaigntime,true);
		$criteria->compare('dealapproveddate',$this->dealapproveddate,true);
		$criteria->compare('dealfullfilleddate',$this->dealfullfilleddate,true);
		$criteria->compare('dateofpaymentbybrand',$this->dateofpaymentbybrand,true);
		$criteria->compare('dateofpaymenttoagent',$this->dateofpaymenttoagent,true);
		$criteria->compare('agentid',$this->agentid);
		$criteria->compare('agentsharepercentage',$this->agentsharepercentage,true);
		$criteria->compare('agentshare',$this->agentshare);
		$criteria->compare('fulfill_engagement',$this->fulfill_engagement);
		$criteria->compare('fulfill_reach',$this->fulfill_reach);
		$criteria->compare('fullfill_paidtoagent',$this->fullfill_paidtoagent);
                $criteria->compare('ampm',$this->ampm);
                $criteria->compare('actual_tweet',$this->actual_tweet);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                        'defaultOrder' => 'dealid DESC', 
                     ),
		));
	}
        
        public function searchAgent(){
            
            $criteria=new CDbCriteria;
            $criteria->with = array('status','user','agent', 'athlete', 'order');
                
                if(!empty($this->from))
                    $criteria->compare('order.orderdate', '>='.$this->from, true);
                if(!empty($this->to))
                    $criteria->compare('order.orderdate', '<='.$this->to.' 23:59:59', true);
                
                $criteria->compare('t.dealid',$this->dealid);
		//$criteria->compare('t.orderid',$this->orderid);
                if($this->dealstatusid!=0)
                    $criteria->compare('t.dealstatusid',$this->dealstatusid);
                
		//$criteria->compare('t.fullfill_paidtoagent',$this->fullfill_paidtoagent);                
		$criteria->compare('t.athleteid',$this->athleteid);
		$criteria->compare('t.dealtype',$this->dealtype);
		$criteria->compare('t.cost',$this->cost);
		$criteria->compare('t.dealrequesteddate',$this->dealrequesteddate,true);
		//$criteria->compare('t.imageid',$this->imageid,true);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.campaigndate',$this->campaigndate,true);
		$criteria->compare('t.campaigntime',$this->campaigntime,true);
		$criteria->compare('t.dealapproveddate',$this->dealapproveddate,true);
		$criteria->compare('t.dealfullfilleddate',$this->dealfullfilleddate,true);
		$criteria->compare('t.dateofpaymentbybrand',$this->dateofpaymentbybrand,true);
		$criteria->compare('t.dateofpaymenttoagent',$this->dateofpaymenttoagent,true);
		$criteria->compare('t.agentid',$this->agentid);
		$criteria->compare('t.agentsharepercentage',$this->agentsharepercentage,true);
		$criteria->compare('t.agentshare',$this->agentshare);
		//$criteria->compare('t.fulfill_engagement',$this->fulfill_engagement);
		//$criteria->compare('t.fulfill_reach',$this->fulfill_reach);
                //$criteria->compare('t.ampm',$this->ampm);
                $criteria->compare('t.actual_tweet',$this->actual_tweet);
                //$criteria->compare('agent.firstname',$this->agentname, true);
                //$criteria->addSearchCondition('agent.firstname', $this->agentname, true);
                //$criteria->addSearchCondition('agent.lastname', $this->agentname, true, "OR");
                $criteria->addCondition('agent.firstname LIKE "%'.$this->agentname.'%" OR agent.lastname LIKE "%'.$this->agentname.'%" OR CONCAT(agent.firstname, " ",agent.lastname) LIKE "%'.$this->agentname.'%"');
                $criteria->order = 't.agentid';
                
                return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'dealid DESC', 
                        ),
                        'pagination' => array(
                            'pageSize' => Yii::app()->params->pageSize,
                        ),
		));
        }
        
        /**
         * 
         */
        public function searchExport(){
            $criteria=new CDbCriteria;
            $criteria->with = array('status','user','agent', 'athlete', 'order');
                
                if(!empty($this->from))
                    $criteria->compare('order.orderdate', '>='.$this->from, true);
                if(!empty($this->to))
                    $criteria->compare('order.orderdate', '<='.$this->to.' 23:59:59', true);
                
                $criteria->compare('t.dealid',$this->dealid);
		//$criteria->compare('t.orderid',$this->orderid);
                if($this->dealstatusid!=0)
                    $criteria->compare('t.dealstatusid',$this->dealstatusid);
                
                $criteria->addSearchCondition('agent.firstname', $this->agentname, true);
                $criteria->addSearchCondition('agent.lastname', $this->agentname, true, "OR");
                $criteria->order = 't.agentid';
                
                return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'order.orderid DESC', 
                        ),
                        'pagination' => false
		));
        }

        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Deals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * Return true if exists deals for given status
         */
        public function isLastDeal($status = 0){
            if($this->findAll(array("condition"=>"orderid =  $this->orderid AND dealid != $this->dealid AND dealstatusid= $status",)))
                return false;
            else
                return true;
        }
        
        public function getBrandName() {
            $brand = Brand::model()->find('user_id='.$this->order->userid);
            ///CVarDumper::dump($brand, 1000, true);
            return ($brand->brandname) ? $brand->brandname : '';
        }        
        
        public function getAgentName() {  
            return ($this->agent) ? $this->agent->firstname.' '.$this->agent->lastname : '';
        }         
                
        public function getAthleteName() {         
            return $this->athlete->firstname.' '.$this->athlete->lastname;
        } 
        
        public function getDealType() {
            return ($this->dealtype == 0) ?'Twitter' : 'Instagram';
        }
        
        /*
         * @return dates count since the order being placed
         */
        public function getDaysSinceOrdered(){
            $interval = date_diff(date_create(strtotime(new CDbExpression('NOW()'))), date_create($this->dealrequesteddate));
            return $interval->format('%a');
        }
        
}
