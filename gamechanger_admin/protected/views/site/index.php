<?php
/* @var $this SiteController */

  $this->pageTitle=Yii::app()->name;

?>

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Dashboard
<!--            <small>Statistics Overview</small>-->
        </h1>
<!--        <ol class="breadcrumb">-->
<!--            <li class="active">-->
<!--                <i class="fa fa-dashboard"></i> Dashboard-->
<!--            </li>-->
<!--        </ol>-->
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Latest Updates</h3>
            </div>
            <div class="panel-body">

                <div class="row">

                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">5 Latest Agents</h3>
                            </div>
                            <div class="panel-body">
                                <ul>
                                <?php
                                if($this->getLatestAgents(5) > 0) {
                                    foreach ($this->getLatestAgents(5) as $key => $value) {
                                        echo '<a href="'.Yii::app()->createUrl('agent/update/'.$value->id).'"><li>'.$value->firstname.' '.$value->lastname.'</li></a>';
                                    }
                                } else {
                                    echo '<li>No records found</li>';
                                }
                                ?>
                                </ul>
                            </div>
                            <a href="<?php echo Yii::app()->createUrl('agent') ?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title">5 Latest Brands</h3>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <?php
                                    if($this->getLatestBrands(5) > 0) {
                                        foreach ($this->getLatestBrands(5) as $key => $value) {
                                            echo '<a href="'.Yii::app()->createUrl('brand/update/'.$value->id).'"><li>'.$value->brandname.'</li></a>';
                                        }
                                    } else {
                                        echo '<li>No records found</li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                            <a href="<?php echo Yii::app()->createUrl('brand') ?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title">5 Latest Athletes</h3>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <?php
                                    if($this->getLatestAthletes(5) > 0) {
                                        foreach ($this->getLatestAthletes(5) as $key => $value) {
                                            echo '<a href="'.Yii::app()->createUrl('athletes/update/'.$value->athleteid).'"><li>'.$value->firstname.' '.$value->lastname.'</li></a>';
                                        }
                                    } else {
                                        echo '<li>No records found</li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                            <a href="<?php echo Yii::app()->createUrl('athletes') ?>">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                <!-- /.row -->

            </div>
        </div>
    </div>
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> At a Glance</h3>
            </div>
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <div class="huge"><?php echo ($count = Athletes::model()->count())? $count: 0; ?></div>
                                        <div>Total number of Athletes</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <div class="huge"><?php echo ($count = Brand::model()->count(array('condition'=>'completed=1')))? $count: 0; ?></div>
                                        <div>Total number of Brands</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <div class="huge"><?php echo ($count = Agent::model()->count(array('condition'=>'completed=1')))? $count: 0; ?></div>
                                        <div>Total number of Agents</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
<!--
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <div class="huge">X</div>
                                        <div>Total Authorized Amount</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="text-center">
                                        <div class="huge"><?php echo '$'.Payment::model()->getTotalCaptured() ?></div>
                                        <div>Total Captured Amount</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> History</h3>
            </div>
            <div class="panel-body">

                <?php

//                echo $this->getDealCost(0, 'week');
//                foreach($this->getDealCost(0, 'week') as $key => $value) {
//                    echo $count = count($this->getDealCost(0, 'week'));
//                    echo '<pre>'.print_r($value->attributes, 1).'</pre>';
//
//                }
                ?>
                <div class="row">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Total number of</th>
                                    <th>Last 7 days</th>
                                    <th>Last 1 month</th>
                                    <th>Last 6 month</th>
                                    <th>Whole history</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Pending Deals with cost</td>
                                    <td><?php echo $this->getDealCount(0, 'week').' | $'.$this->getDealCost(0, 'week'); ?></td>
                                    <td><?php echo $this->getDealCount(0, 'month').' | $'.$this->getDealCost(0, 'month'); ?></td>
                                    <td><?php echo $this->getDealCount(0, '6months').' | $'.$this->getDealCost(0, '6months'); ?></td>
                                    <td><?php echo $this->getDealCount(0, 'all').' | $'.$this->getDealCost(0, 'all'); ?></td>
                                </tr>
                                <tr>
                                    <td>Approved Deals with cost</td>
                                    <td><?php echo $this->getDealCount(1, 'week').' | $'.$this->getDealCost(1, 'week'); ?></td>
                                    <td><?php echo $this->getDealCount(1, 'month').' | $'.$this->getDealCost(1, 'month'); ?></td>
                                    <td><?php echo $this->getDealCount(1, '6months').' | $'.$this->getDealCost(1, '6months'); ?></td>
                                    <td><?php echo $this->getDealCount(1, 'all').' | $'.$this->getDealCost(1, 'all'); ?></td>
                                </tr>
                                <tr>
                                    <td>Fulfilled deals with cost</td>
                                    <td><?php echo $this->getDealCount(3, 'week', null).' | $'.$this->getDealCost(3, 'week', null); ?></td>
                                    <td><?php echo $this->getDealCount(3, 'month', null).' | $'.$this->getDealCost(3, 'month', null); ?></td>
                                    <td><?php echo $this->getDealCount(3, '6months', null).' | $'.$this->getDealCost(3, '6months', null); ?></td>
                                    <td><?php echo $this->getDealCount(3, 'all', null).' | $'.$this->getDealCost(3, 'all', null); ?></td>
                                </tr>
                                <tr>
                                    <td>Fulfilled deals which already paid to agent</td>
                                    <td><?php echo $this->getDealCount(3, 'week', 1).' | $'.$this->getDealCost(3, 'week', 1); ?></td>
                                    <td><?php echo $this->getDealCount(3, 'month', 1).' | $'.$this->getDealCost(3, 'month', 1); ?></td>
                                    <td><?php echo $this->getDealCount(3, '6months', 1).' | $'.$this->getDealCost(3, '6months', 1); ?></td>
                                    <td><?php echo $this->getDealCount(3, 'all', 1).' | $'.$this->getDealCost(3, 'all', 1); ?></td>
                                </tr>
                                <tr>
                                    <td>Fulfilled deals which needs to paid to be agent</td>
                                    <td><?php echo $this->getDealCount(3, 'week', 0).' | $'.$this->getDealCost(3, 'week', 0); ?></td>
                                    <td><?php echo $this->getDealCount(3, 'month', 0).' | $'.$this->getDealCost(3, 'month', 0); ?></td>
                                    <td><?php echo $this->getDealCount(3, '6months', 0).' | $'.$this->getDealCost(3, '6months', 0); ?></td>
                                    <td><?php echo $this->getDealCount(3, 'all', 0).' | $'.$this->getDealCost(3, 'all', 0); ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href=<?php echo Yii::app()->createUrl('payment') ?>>View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>


                </div>
                <!-- /.row -->

            </div>
        </div>
    </div>
</div>
<!-- /.row -->




