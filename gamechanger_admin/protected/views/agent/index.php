<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Agents</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php //echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('agent/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'agent-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
        		//'id',
		'user_id',
		'firstname',
		'lastname',
		'addressline1',
		'addressline2',
		/*
		'city',
		'state',
		'zipcode',
		'agentshare',
		'completed',
		*/
                        array(
                                'class'=>'CButtonColumn',
                                'template'=>'{update}',
                        ),
                ),
        )); ?>
    </div>
</div>
<!-- /.row -->