<?php
/* @var $this AgentController */
/* @var $model Agent */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-uppercase"><?php echo $model->firstname.' '.$model->lastname; ?></h1>
    </div>
</div>
<!-- /.row -->

<?php $this->renderPartial('_update', array('model'=>$model,'user'=>$user)); ?>