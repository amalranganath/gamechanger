<?php
/* @var $this AgentController */
/* @var $model Agent */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript(
        'flashFadeOut', "$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');", CClientScript::POS_READY
);

?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'agent-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        //'enableAjaxValidation'=>false,
        'enableAjaxValidation' => True,
        'clientOptions' => array(
            'validateOnSubmit' => true
        )
    ));
    ?>

    <div class="row">
        <div class="col-lg-12">
            <p class="text-primary">Fields with * are required</p>
            <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('agent'), array('class' => 'btn btn-link back')); ?>
        </div>
    </div>
    <!-- /.row -->

    <?php
    if (Yii::app()->user->hasFlash('agent-form')) {
        ?>
        <div class="row info">
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo Yii::app()->user->getFlash('agent-form'); ?>

                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

<!-- <span>
    <?php
//  if(!empty($form->errorSummary($model))) {
    ?>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php // echo $form->errorSummary($model);  ?>

                        </div>
                    </div>
                </div>
            </div>
    <?php
//   }
    ?>
<span> -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'firstname'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'firstname', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'firstname'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'lastname'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'lastname', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'lastname'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->



    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($user, 'email'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($user, 'email', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($user, 'email'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->


    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'addressline1'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'addressline1', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'addressline1'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'addressline2'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'addressline2', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'addressline2'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'city'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'city', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'city'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'state'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'state', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'state'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'zipcode'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'zipcode', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'zipcode'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'agentshare'); ?> %
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'agentshare', array('class' => 'form-control')); ?>
                <p class="help-block"><?php echo $form->error($model, 'agentshare'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'totalmarketreach'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'totalmarketreach', array('value' => $model->total["MarketReach"], 'class' => 'form-control', 'disabled'=>true)); ?>
                <p class="help-block"><?php //echo $form->error($model, 'totalmarketreach'); ?></p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'totalengagement'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'totalengagement', array('value' => $model->total['Engagement'],'class' => 'form-control', 'disabled'=>true)); ?>
                <p class="help-block"><?php //echo $form->error($model, 'totalengagement'); ?></p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($user, 'status'); ?>
            </div>
            <div class="col-lg-2">
                <?php
                echo $form->dropdownList($user, 'status', array(
                    '0' => 'Deactive',
                    '1' => 'Active',
                        ), array(
                    'class' => 'form-control',
                ));
                ?> 

            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">

            <div class="col-lg-2 col-xs-4 panel-body">
                <?php echo CHtml::submitButton('Update', array('class' => 'bg_color_hover btn')); ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <?php $this->endWidget(); ?>

</div><!-- form -->