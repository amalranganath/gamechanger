<?php

/* @var $this OrdersController */
/* @var $model Orders */
?>
<?php

$grid = $this->widget('EGridView', array(
    'id' => 'orders-export-grid',
    'dataProvider' => $model->searchExport(),
    //'filter' => $model,
    'rowDisplayRelated' => array('model' => new Deals, 'on' => 'orderid',
        'columns' => array(
            'dealid', array('cost', array('class' => 'right')), 'agentName', 'AthleteName', 'dealType', 'status.dealstatus'),
    ),
    'columns' => array(
        'orderid',
        'orderdate',
        'payment.time',
        'payment.transactionid',
        'brand.brandname',
        //'token',
        //'payerid',
        //'ipaddress',
        array(
            'name' => 'payment.amount',
            'header' => 'Authorized Amount (AUD)',
            'htmlOptions' => array('class' => 'right'),
        ),
        array(
            'name' => 'payment.captured',
            'header' => 'Charged Amount (AUD)',
            'htmlOptions' => array('class' => 'right'),
        ),
        array(
            //'name' => 'msg',
            'header' => 'Released Amount (AUD)',
            'value' => '$data->payment->released',
            'htmlOptions' => array('class' => 'right'),
        ),
//                array(
//                    'name' => 'payment.msg',
//                    'filter' => false,
//                ),
        array(
            'name' => 'payment.status',
            'value' => '$data->payment->Status',
        ),
        array(
            'name' => 'daysSinceOrdered',
            'htmlOptions' => array('class' => 'right'),
        ),
    ),
        ));
?>