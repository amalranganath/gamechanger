<?php
/* @var $this OrdersController */
/* @var $model Orders */

Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
    $.fn.yiiGridView.update.yiiGridView('agent-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Agents Earning report
            <span class="pull-right"><?php
            //Export PDF
            $this->renderExportGridButton('agent-grid', 'Export Pdf', array('id' => 'export-pdf', 'class' => 'btn btn-info', 'data-target' => 'exportPdf'));
            ?>
            <?php
            //Export CSV
            $this->renderExportGridButton('agent-grid', 'Export Excel', array('class' => 'btn btn-info '));
            ?></span></h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <?php
        $this->renderPartial('_search_agent', array(
            'model' => $model,
        ));
        ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <?php
        $grid = $this->widget('EGridView', array(
            'id' => 'agent-grid',
            'dataProvider' => $model->searchAgent(),
            //'filter' => $orders,
            'rowGetToatal' => array(
                'on' => 'agentshare',
                'refer' => 'agentid',
                'htmlOptions' => array('class' => 'right')
            ),
            'columns' => array(
                'agentName',
                'athleteName',
                'dealid',
                array(
                    'name' => 'cost',
                    //'header' => 'Brand Name',
                    'htmlOptions' => array('class' => 'right'),
                ),
                array(
                    'name' => 'agentshare',
                    //'header' => 'Authorized Amount (AUD)',
                    'htmlOptions' => array('class' => 'right'),
                ),
                'order.orderdate',
                array(
                    'name' => 'status.dealstatus',
                //'value' => '$data->payment->Status',
                //'filter' => $model->getStatus(true),
                ),
                array(
                    'name' => 'dealfullfilleddate',
                    'header' => 'Fulfilled on',
                //'htmlOptions' => array('class' => 'right'),
                ),
                array(
                    'name' => 'dateofpaymenttoagent',
                    'header' => 'Paid to Agent on',
                //'htmlOptions' => array('class' => 'right'),
                ),
            ),
        ));
        ?>
    </div>
</div>
<!-- /.row -->