<?php
/* @var $this OrdersController */
/* @var $model Orders */

Yii::app()->clientScript->registerScript('search', "

$('.search-form form').submit(function(){
    $.fn.yiiGridView.update.yiiGridView('orders-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Orders (Payments)
        <div class="pull-right">
            <?php
            $this->renderExportGridButton('orders-grid', 'Export Pdf', array('id' => 'export-pdf', 'class' => 'btn btn-info', 'data-target' => 'exportPdf'));
            ?>
            <?php
            //Export CSV
            $this->renderExportGridButton('orders-grid', 'Export Excel', array('class' => 'btn btn-info '));
            ?>
        </div></h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
        $grid = $this->widget('EGridView', array(
            'id' => 'orders-grid',
            'dataProvider' => $model->search(),
            //'filter' => $model,
            'rowDisplayRelated' => array('model' => new Deals, 'on' => 'orderid',
                'columns' => array(
                    'dealid', array('cost', array('class' => 'right')), 'agentName', 'AthleteName', 'dealType', 'status.dealstatus'),
            ),
            'columns' => array(
                'orderid',
                'orderdate',
                'payment.time',
                'payment.transactionid',
                'brand.brandname',
                //'token',
                //'payerid',
                //'ipaddress',
                array(
                    'name' => 'payment.amount',
                    'header' => 'Authorized Amount (AUD)',
                    'htmlOptions' => array('class' => 'right'),
                //'filter' => false,
                ),
                array(
                    'name' => 'payment.captured',
                    'header' => 'Charged Amount (AUD)',
                    'htmlOptions' => array('class' => 'right'),
                ),
                array(
                    //'name' => 'msg',
                    'header' => 'Released Amount (AUD)',
                    'value' => '$data->payment->released',
                    'htmlOptions' => array('class' => 'right'),
                ),
//                array(
//                    'name' => 'payment.msg',
//                    'filter' => false,
//                ),
                'Status',
                array(
                    'name' => 'daysSinceOrdered',
                    'htmlOptions' => array('class' => 'right'),
                ),
//                array(
//                    'class' => 'CButtonColumn',
//                    'header' => 'Actions',
//                    'template' => '{view}',
//                    'buttons' => array
//                        (
//                        'view' => array
//                            (
//                            'label' => 'View',
//                        //'imageUrl' => Yii::app()->request->baseUrl . '/images/email.png',
//                        //'url' => 'Yii::app()->createUrl("users/email", array("id"=>$data->id))',
//                        ),
//                    ),
//                ),
            ),
        ));
        //CVarDumper::dump($grid, 1000, true);
        ?>
    </div>
</div>
<!-- /.row -->