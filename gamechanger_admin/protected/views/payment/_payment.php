
<?php
/* @var $this PaymentControllerController */
/* @var $model Payment */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Payment #<?php echo $model->paymentid; ?></h1>
    </div>
</div>
<!-- /.row -->
<?php
/* @var $this PaymentControllerController */
/* @var $model Payment */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript(
        'flashFadeOut', "$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');", CClientScript::POS_READY
);
?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'payment-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="row">
        <div class="col-lg-12">
            <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('site/payments'), array('class' => 'btn btn-link back')); ?>
        </div>
    </div>
    <!-- /.row -->


    <?php
    if (Yii::app()->user->hasFlash('payment-form')) {
        ?>
        <div class="row info">
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo Yii::app()->user->getFlash('payment-form'); ?>

                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'userid'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'userid'); ?>
                <p class="help-block"><?php echo $form->error($model, 'userid'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'token'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'token', array('size' => 32, 'maxlength' => 32)); ?>
                <p class="help-block"><?php echo $form->error($model, 'token'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'payerid'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'payerid', array('size' => 32, 'maxlength' => 32)); ?>
                <p class="help-block"><?php echo $form->error($model, 'payerid'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'transactionid'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'transactionid', array('size' => 32, 'maxlength' => 32)); ?>
                <p class="help-block"><?php echo $form->error($model, 'transactionid'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'ipaddress'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'ipaddress', array('size' => 16, 'maxlength' => 16)); ?>
                <p class="help-block"><?php echo $form->error($model, 'ipaddress'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'amount'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'amount'); ?>
                <p class="help-block"><?php echo $form->error($model, 'amount'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'captured'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'captured'); ?>
                <p class="help-block"><?php echo $form->error($model, 'captured'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'msg'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textArea($model, 'msg', array('rows' => 6, 'cols' => 50)); ?>
                <p class="help-block"><?php echo $form->error($model, 'msg'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'time'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'time'); ?>
                <p class="help-block"><?php echo $form->error($model, 'time'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
                <?php echo $form->labelEx($model, 'status'); ?>
            </div>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'status'); ?>
                <p class="help-block"><?php echo $form->error($model, 'status'); ?>
                </p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-10">
                <?php //echo CHtml::submitButton('Update', array('class' => 'btn btn-default', 'onclick' => 'return confirm("Are you sure you want to update?");')); ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <?php $this->endWidget(); ?>

</div><!-- form -->