<?php

/* @var $this OrdersController */
/* @var $model Orders */
?>

<?php

$this->widget('EGridView', array(
    'id' => 'agent-grid',
    'dataProvider' => $model->searchExport(),
    //'filter' => $model,
    'rowGetToatal' => array(
        'on' => 'agentshare',
        'refer' => 'agentid',
        'htmlOptions' => array('class' => 'right')
    ),
    'columns' => array(
        'agentName',
        'athleteName',
        'dealid',
        array(
            'name' => 'cost',
            //'header' => 'Brand Name',
            'htmlOptions' => array('class' => 'right'),
        ),
        array(
            'name' => 'agentshare',
            //'header' => 'Authorized Amount (AUD)',
            'htmlOptions' => array('class' => 'right'),
        ),
        'order.orderdate',
        array(
            'name' => 'status.dealstatus',
        //'value' => '$data->payment->Status',
        //'filter' => $model->getStatus(true),
        ),
        array(
            'name' => 'dealfullfilleddate',
            'header' => 'Full filled on',
        //'htmlOptions' => array('class' => 'right'),
        ),
        array(
            'name' => 'dateofpaymenttoagent',
            'header' => 'Paid to Agent on',
        //'htmlOptions' => array('class' => 'right'),
        ),
    ),
));
?>