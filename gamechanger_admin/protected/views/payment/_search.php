<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>

<div class="search-form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'htmlOptions' => array(
            'class' => 'form-inline row',
        ),
    ));
    ?>
    <div class="col-md-12 panel-body">
        <div class="row">
            <div class="col-sm-5">
                <?php echo CHtml::label('Date from', 'from'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'from',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'maxDate' => date('Y-m-d h:i:s'), // maximum date
                    ),
                    'htmlOptions' => array(
                        'size' => '8', // textField size
                        'maxlength' => '10', // textField maxlength
                        'class' => 'form-control',
                    ),
                ));
                ?>
                <?php echo CHtml::error($model, 'from'); ?>

                <?php echo CHtml::label('to', 'to'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'to',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'maxDate' => date('Y-m-d h:i:s'), // maximum date
                    ),
                    'htmlOptions' => array(
                        'size' => '8',
                        'maxlength' => '10', // textField maxlength
                        'class' => 'form-control',
                    ),
                ));
                ?>
                <?php echo CHtml::error($model, 'to'); ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->label($model, 'status', array('class' => '')); ?>
                <?php echo $form->dropDownList($model, 'status', array('Open', 'Closed'), array('class' => 'form-control', 'empty'=>'All')); ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 ">
        <div class="row">
            <div class="col-sm-5">
                <?php echo $form->label($model, 'brand.brandname', array('class' => '')); ?>
                <?php echo $form->textField($model, 'brandname', array('class' => 'form-control')); ?>
            </div>
            <div class="col-sm-5">
                <?php echo $form->label($model, 'payment.transactionid', array('class' => '')); ?>
                <?php echo $form->textField($model, 'transactionId', array('class' => 'form-control')); ?>
            </div>
            <div class="col-sm-2 buttons">
                <?php echo CHtml::submitButton('Search', array('class' => 'btn btn-default bg_color_hover btn')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- search-form -->
