<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript(
   'flashFadeOut',
   "$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');",
   CClientScript::POS_READY
);
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'brand-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

        <div class="row">
            <div class="col-lg-12">
                 <p class="text-primary">Fields with * are required</p>
                <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('brand'), array('class'=>'btn btn-link back')); ?>
            </div>
	</div>
        <!-- /.row -->

                        

<?php
        if(Yii::app()->user->hasFlash('brand-form')) {
            ?>
            <div class="row info">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo Yii::app()->user->getFlash('brand-form' ); ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
?>
        

<!-- <span>
<?php
    //    if(!empty($form->errorSummary($model))) {
            ?>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php // echo $form->errorSummary($model); ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php
     //   }
?>
<span> -->


        
   
        
                  
         <div class="row">
             <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx(Lookupimages::model(),'imagename'); ?>
                </div>
                <div class="col-lg-4">
                    <?php
                        echo CHtml::activeFileField($lookupimages,'imagename',array(
                            'class'=>'form-control',
                        ));
                    ?>
                    <p class="help-block"><?php echo $form->error($lookupimages,'imagename'); ?></p>
                </div>
            
                <div class="col-lg-6">
                   <div class="form-group">
                        <?php
                            if($model->imageid!=0){
                        ?>
              
                    <div class="col-sm-3">
                        <?php
                            $imagename=Lookupimages::model()->find('imageid='.$model->imageid)->imagename;
                        ?>
                        <img src="<?php echo Yii::app()->baseUrl.'/../images/brands/200x150/'.$imagename;?>" class="img-height"> 
                      
                    </div>
              
                        <?php         
                        }
                        ?>
                 </div>
            </div>
           </div>
       </div>
        <br/>
        
        
        

     
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'firstname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'firstname',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                    <p class="help-block"><?php echo $form->error($model,'firstname'); ?>
</p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'lastname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                    <p class="help-block"><?php echo $form->error($model,'lastname'); ?>
</p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
       
        
        
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'brandname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'brandname',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                    <p class="help-block"><?php echo $form->error($model,'brandname'); ?>
</p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        

        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'siteurl'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'siteurl',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                    <p class="help-block"><?php echo $form->error($model,'siteurl'); ?>
</p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'description'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
                    <p class="help-block"><?php echo $form->error($model,'description'); ?>
</p>
                </div>
            </div>
	</div>
        <!-- /.row -->

        
          <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($user,'status'); ?>
                </div>
                <div class="col-lg-2">
                    <?php echo $form->dropdownList($user,'status',
                             array(
                                                        '0'=>'Deactive',  
                                                        '1'=>'Active',
                                                                                                        
                                              ), array(
                            'class'=>'form-control',
                        )); ?> 
       
        
            
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        
        
        <div class="row">
            <div class="form-group">
                
                <div class="col-lg-2 col-xs-4 panel-body">
                    <?php echo CHtml::submitButton('Update', array('class'=>'bg_color_hover btn','onclick'=>'return confirm("Are you sure you want to update?");')); ?>
                </div>
            </div>
	</div>
        <!-- /.row -->

<?php $this->endWidget(); ?>

</div><!-- form -->