<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'brand-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

        <div class="row">
            <div class="col-lg-12">
                <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('brand'), array('class'=>'btn btn-link back')); ?>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="col-lg-12">
                <p class="note">Fields with <span class="required">*</span> are required.</p>
            </div>
        </div>
        <!-- /.row -->
                        

<!-- <span>
<?php
        if(!empty($form->errorSummary($model))) {
            ?>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $form->errorSummary($model); ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
?>
<span> -->

        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'user_id'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'user_id'); ?>
                    <p class="help-block"><?php echo $form->error($model,'user_id'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'firstname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'firstname',array('size'=>60,'maxlength'=>255)); ?>
                    <p class="help-block"><?php echo $form->error($model,'firstname'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'lastname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>255)); ?>
                    <p class="help-block"><?php echo $form->error($model,'lastname'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'brandname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'brandname',array('size'=>60,'maxlength'=>100)); ?>
                    <p class="help-block"><?php echo $form->error($model,'brandname'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'imageid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'imageid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'imageid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'siteurl'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'siteurl',array('size'=>60,'maxlength'=>100)); ?>
                    <p class="help-block"><?php echo $form->error($model,'siteurl'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'description'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
                    <p class="help-block"><?php echo $form->error($model,'description'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'completed'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'completed'); ?>
                    <p class="help-block"><?php echo $form->error($model,'completed'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-10">
                    <?php echo CHtml::submitButton('Create', array('class'=>'btn btn-default','data-loading-text'=>'Loading...')); ?>
                    <?php echo CHtml::resetButton('Reset', array('class'=>'btn btn-default')); ?>
                </div>
            </div>
	</div>
        <!-- /.row -->

<?php $this->endWidget(); ?>

</div><!-- form -->