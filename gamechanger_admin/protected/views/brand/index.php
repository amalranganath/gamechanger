<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Brands</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php //echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('brand/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'brand-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
        		'user_id',
		
		'firstname',
		'lastname',
		'brandname',
		'siteurl',
		/*'description',
		'completed',
		*/
                        array(
                                'class'=>'CButtonColumn',
                                'template'=>'{update}',
                        ),
                ),
        )); ?>
    </div>
</div>
<!-- /.row -->