<?php
/* @var $this SportscatagoryController */
/* @var $data Sportscatagory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('sportscatagory_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->sportscatagory_id), array('view', 'id'=>$data->sportscatagory_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />


</div>