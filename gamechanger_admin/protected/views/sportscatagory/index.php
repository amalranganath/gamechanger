<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Sportscatagories</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('sportscatagory/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'sportscatagory-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
        		'sportscatagory_id',
		'name',
                        array(
                                'class'=>'CButtonColumn',
                                'template'=>'{view} {delete}',
                        ),
                ),
        )); ?>
    </div>
</div>
<!-- /.row -->