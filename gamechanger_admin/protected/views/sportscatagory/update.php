<?php
/* @var $this SportscatagoryController */
/* @var $model Sportscatagory */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Sportscatagory <?php echo $model->sportscatagory_id; ?></h1>
    </div>
</div>
<!-- /.row -->

<?php $this->renderPartial('_update', array('model'=>$model)); ?>