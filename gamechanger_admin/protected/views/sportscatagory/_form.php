<?php
/* @var $this SportscatagoryController */
/* @var $model Sportscatagory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sportscatagory-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

        <div class="row">
            <div class="col-lg-12">
                <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('sportscatagory'), array('class'=>'btn btn-link back')); ?>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="col-lg-12">
                <p class="note">Fields with <span class="required">*</span> are required.</p>
            </div>
        </div>
        <!-- /.row -->
                        

<!-- <span>
<?php
        if(!empty($form->errorSummary($model))) {
            ?>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $form->errorSummary($model); ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
?>
<span> -->

        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'name'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
                    <p class="help-block"><?php echo $form->error($model,'name'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-10">
                    <?php echo CHtml::submitButton('Create', array('class'=>'btn btn-default','data-loading-text'=>'Loading...')); ?>
                    <?php echo CHtml::resetButton('Reset', array('class'=>'btn btn-default')); ?>
                </div>
            </div>
	</div>
        <!-- /.row -->

<?php $this->endWidget(); ?>

</div><!-- form -->