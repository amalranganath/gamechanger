<?php
/* @var $this SportscatagoryController */
/* @var $model Sportscatagory */

$this->breadcrumbs=array(
	'Sportscatagories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Sportscatagory', 'url'=>array('index')),
	array('label'=>'Create Sportscatagory', 'url'=>array('create')),
	array('label'=>'Update Sportscatagory', 'url'=>array('update', 'id'=>$model->sportscatagory_id)),
	array('label'=>'Delete Sportscatagory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->sportscatagory_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Sportscatagory', 'url'=>array('admin')),
);
?>

<h1>View Sportscatagory #<?php echo $model->sportscatagory_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'sportscatagory_id',
		'name',
	),
)); ?>
