<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Expired Deals</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php //echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('deals/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php
        //CVarDumper::dump($_SERVER, 1000, true);
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'deals-grid',
            'dataProvider' => $dataProvider,
            //'filter'=>$model,
            'columns' => array(
                'dealid',
                //'orderid',
                //'dealstatusid',
                'brandName',
                array(
                    'name' => 'AthleteName',
                    'type' => 'raw',
                //'value' => '$data->GetAthleteName()',
                ),
                array(
                    'name' => 'AgentName',
                    'type' => 'raw',
                //'value' => '$data->GetAgentName()',
                ),
                array(
                    'name' => 'dealType',
                //'type' => 'raw',
                //'value' => 'getdealtype($data->dealtype)',
                ),
                array(
                    'name' => 'cost',
                    'header' => 'Deal Cost (AUD)',
                    'htmlOptions' => array('class' => 'right'),
                ),
                array(
                    'name' => 'dealrequesteddate',
                    'htmlOptions' => array('class' => 'right'),
                ),
                array(
                    'name' => 'daysSinceOrdered',
                    'htmlOptions' => array('class' => 'right'),
                ),
                /* 'image',
                  'description',
                  'campaigndate',
                  'campaigntime',
                  'dealapproveddate',
                  'dealfullfilleddate',
                  'dateofpaymentbybrand',
                  'dateofpaymenttoagent',
                  'agentid',
                  'agentsharepercentage',
                  'agentshare',
                  'fulfill_engagement',
                  'fulfill_reach',
                  'fullfill_paidtoagent',
                 */
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{view}',
                ),
            ),
        ));
        ?>

        <?php

        function getdealtype($dealtype) {

            if ($dealtype == 0) {
                echo 'Twitter';
            } else {
                echo 'Instagram';
            }
        }
        ?>
    </div>
</div>
<!-- /.row -->