<?php
/* @var $this DealsController */
/* @var $model Deals */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript(
        'flashFadeOut', "$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');", CClientScript::POS_READY
);
?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'deals-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
    ));
    ?>

    <div class="row">
        <div class="col-lg-12">

            <?php
            if ($model->dealstatusid == 0) {
                echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('deals/pending'), array('class' => 'btn btn-link back'));
            } else if ($model->dealstatusid == 1) {
                echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('deals/approved'), array('class' => 'btn btn-link back'));
            } else if ($model->dealstatusid == 4) {
                echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('deals/disapproved'), array('class' => 'btn btn-link back'));
            } else {
                echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('deals/fullfilled'), array('class' => 'btn btn-link back'));
            }
            ?>

            <?php //echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('deals'), array('class'=>'btn btn-link back')); ?>
        </div>
    </div>
    <!-- /.row -->

    <?php if (Yii::app()->user->hasFlash('deals-form')) { ?>
        <div class="row info">
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo Yii::app()->user->getFlash('deals-form'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if (Yii::app()->user->hasFlash('deals-error')) { ?>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo Yii::app()->user->getFlash('deals-error'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php
    //$brandid = Orders::model()->find('orderid=' . $model->orderid)->userid;
    $brand = Brand::model()->find('user_id=' . $model->order->userid);
    ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title text-center"><?php echo $brand->brandname; ?></h2>
                </div>

                <div class="panel-body">  
                    <div class="row">
                        <div class="form-group panel-body">
                            <div class="col-sm-3 col-xs-7">
                                <?php echo $form->labelEx($model, 'dealstatusid'); ?>
                            </div>
                            <div class="col-sm-3 col-xs-5">
                                <div class="deal-status">
                                    <?php echo $model->status->dealstatus ?>
                                </div> 
                            </div> 
                            <?php
                            if ($model->image) {
                                //$imagename=Lookupimages::model()->find('imageid='.$model->imageid)->imagename;
                                ?>
                                <div class="col-sm-6 col-xs-12 text-center">
                                    <img src="<?php echo Yii::app()->baseUrl . '/../images/deals/' . $model->image->imagename; ?>" class="img-height">
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class="row">
                        <div class="form-group panel-body">
                            <div class="col-sm-3 col-xs-7">
                                <?php echo $form->labelEx($model, 'dealtype'); ?>
                            </div>
                            <div class="col-sm-3 col-xs-5">
                                <?php echo $model->dealtype == 0 ? 'Twitter' : 'Instagram'; ?>
                            </div>
                            <div class="col-sm-3 col-xs-7">
                                <?php echo $form->labelEx($model, 'cost'); ?>
                            </div>
                            <div class="col-sm-3 col-xs-5">
                                <?php echo $model->cost; ?>

                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="form-group panel-body">
                            <div class="col-sm-3 col-xs-7">
                                <?php echo $form->labelEx($model, 'dealrequesteddate'); ?>
                            </div>
                            <div class="col-sm-3 col-xs-5">
                                <?php echo $model->dealrequesteddate; ?>
                            </div>
                            <div class="col-sm-3 col-xs-7">
                                <?php echo $form->labelEx($model, 'description'); ?>
                            </div>
                            <div class="col-sm-3 col-xs-5">
                                <p><?php echo $model->description; ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="form-group panel-body">
                            <div class="col-sm-3 col-xs-7">
                                <label>Campaign Date & Time</label>
                            </div>
                            <div class="col-sm-3 col-xs-5">
                                <?php echo $model->campaigndate . ' ' . $model->campaigntime . ' ' . $model->ampm; ?>

                            </div>
                            <?php if ($model->dealstatusid == 4) { ?>
                            </div>
                        </div>
                    <?php } else if ($model->dealstatusid == 0) { ?>
                    </div>
                </div>

                <div class="col-lg-2 col-xs-4 panel-body">
                    <?php
                    echo CHtml::link("Set as Approved", array("deals/approvedByAdmin", "dealId" => $model->dealid), array("confirm" => "Do you wish to mark this as Approved?", 'class' => 'btn btn-success'));
                    ?>
                </div>
                <div class="col-lg-2 col-xs-4 panel-body">
                    <?php
                    echo CHtml::link("Set as Disapproved", array("deals/disapprovedByAdmin", "dealId" => $model->dealid), array("confirm" => "Do you wish to mark this as Disapproved?", 'class' => 'btn btn-danger'));
                    ?>
                </div>
            <?php } else if ($model->dealstatusid == 1) { ?>

                <!--        <div class="row">
                            <div class="form-group panel-body">-->
                <div class="col-sm-3 col-xs-7">
                    <?php echo $form->labelEx($model, 'dealapproveddate'); ?>
                </div>
                <div class="col-sm-3 col-xs-5">
                    <?php echo $model->dealapproveddate; ?>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <div class="col-lg-2 col-xs-4 panel-body">
            <?php
            echo CHtml::link("Set as Fullfilled", array("deals/fullfilledByAdmin", array("DealId" => $model->dealid)), array("confirm" => "Do you wish to mark this as Fullfilled?", 'class' => 'btn btn-info'));
            ?>
        </div>

        <?php
    } else if (($model->dealstatusid == 2)) {//&& ($model->fullfill_paidtoagent == 0)
        ?>

        <div class="col-sm-3 col-xs-7">
            <?php echo $form->labelEx($model, 'dealapproveddate'); ?>
        </div>
        <div class="col-sm-3 col-xs-5">
            <?php echo $model->dealapproveddate; ?>
        </div>
    </div>
    </div><!-- /.form-group panel-body-->
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'dealfullfilleddate'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $model->dealfullfilleddate; ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'agentsharepercentage'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $model->agentsharepercentage; ?>
            </div>
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'fulfill_engagement'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $form->textField($model, 'fulfill_engagement'); ?>
                <p class="help-block"><?php echo $form->error($model, 'fulfill_engagement'); ?></p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'fulfill_reach'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $form->textField($model, 'fulfill_reach'); ?>
                <p class="help-block"><?php echo $form->error($model, 'fulfill_reach'); ?></p>
            </div>
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'fullfill_paidtoagent'); ?>
            </div>
            <div class="col-sm-1 col-xs-1">
                <?php echo $form->checkBox($model, 'fullfill_paidtoagent', array('value' => 1, 'uncheckValue' => 0)); ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">

            <div class="col-lg-2 col-xs-4 panel-body">
                <?php echo CHtml::submitButton('Update', array('class' => 'bg_color_hover btn', 'onclick' => 'return confirm("Are you sure you want to update?");')); ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

<?php } else if ($model->dealstatusid == 5) { //) && ($model->fullfill_paidtoagent == 1)?>
    <div class="col-sm-3 col-xs-7">
        <?php echo $form->labelEx($model, 'dealapproveddate'); ?>
    </div>
    <div class="col-sm-3 col-xs-5">
        <?php echo $model->dealapproveddate; ?>
    </div>
    </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'dealfullfilleddate'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $model->dealfullfilleddate; ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'agentsharepercentage'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $model->agentsharepercentage; ?>
            </div>
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'agentshare'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $model->agentshare; ?>
            </div>
        </div>
    </div>
    <!-- /.row --> 

    <div class="row">
        <div class="form-group panel-body">
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'fulfill_engagement'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $form->textField($model, 'fulfill_engagement'); ?>
                <p class="help-block"><?php echo $form->error($model, 'fulfill_engagement'); ?></p>
            </div>
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'fulfill_reach'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $form->textField($model, 'fulfill_reach'); ?>
                <p class="help-block"><?php echo $form->error($model, 'fulfill_reach'); ?></p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">
            <div class="col-sm-3 col-xs-7">
                <?php echo $form->labelEx($model, 'actual_tweet'); ?>
            </div>
            <div class="col-sm-3 col-xs-5">
                <?php echo $form->textField($model, 'actual_tweet'); ?>
                <p class="help-block"><?php echo $form->error($model, 'actual_tweet'); ?></p>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="form-group panel-body">

            <div class="col-lg-2 col-xs-4 panel-body">
                <?php echo CHtml::submitButton('Update', array('class' => 'bg_color_hover btn', 'onclick' => 'return confirm("Are you sure you want to update?");')); ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

<?php } ?>    

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title text-center">Other Details</h4>
            </div>
            <div class="panel-body">  
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-3 col-xs-7">
                            <?php echo $form->labelEx($model, 'athleteid'); ?>
                        </div>
                        <div class="col-sm-4 col-xs-5">
                            <?php
                            if ($model->athlete->image) {
                                //$athleteDetails=Athletes::model()->find('athleteid='.$model->athleteid);
                                //$imagename=Lookupimages::model()->find('imageid='.$athleteDetails->imageid)->imagename;
                                echo $model->athlete->firstname . ' ' . $model->athlete->lastname;
                            }
                            ?>
                        </div>
                        <div class="col-sm-5 col-xs-12">
                            <img src="<?php echo Yii::app()->baseUrl . '/../images/athlete/200x150/' . $model->athlete->image->imagename; ?>"> 
                        </div>
                    </div>
                </div> <br/> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-3 col-xs-7">
                            <?php echo $form->labelEx($model, 'agentid'); ?>
                        </div>
                        <div class="col-sm-3 col-xs-5">
                            <?php
                            //var_dump($model->agent);
                            if ($model->agentid != '') {
                                $agentDetails = Agent::model()->find('user_id=' . $model->agentid);
                                echo $agentDetails->firstname . ' ' . $agentDetails->lastname;
                            }
                            ?>
                        </div>
                    </div>      
                </div>      
            </div>                   
        </div>              
    </div>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->