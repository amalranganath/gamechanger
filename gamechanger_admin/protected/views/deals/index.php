<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Deals</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('deals/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'deals-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
        		'dealid',
		'orderid',
		'dealstatusid',
		'athleteid',
		'dealtype',
		'cost',
		/*
		'dealrequesteddate',
		'image',
		'description',
		'campaigndate',
		'campaigntime',
		'dealapproveddate',
		'dealfullfilleddate',
		'dateofpaymentbybrand',
		'dateofpaymenttoagent',
		'agentid',
		'agentsharepercentage',
		'agentshare',
		'fulfill_engagement',
		'fulfill_reach',
		'fullfill_paidtoagent',
		*/
                        array(
                                'class'=>'CButtonColumn',
                                'template'=>'{view} {delete}',
                        ),
                ),
        )); ?>
    </div>
</div>
<!-- /.row -->