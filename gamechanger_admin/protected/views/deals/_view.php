<?php
/* @var $this DealsController */
/* @var $data Deals */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->dealid), array('view', 'id'=>$data->dealid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderid')); ?>:</b>
	<?php echo CHtml::encode($data->orderid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealstatusid')); ?>:</b>
	<?php echo CHtml::encode($data->dealstatusid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('athleteid')); ?>:</b>
	<?php echo CHtml::encode($data->athleteid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealtype')); ?>:</b>
	<?php echo CHtml::encode($data->dealtype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cost')); ?>:</b>
	<?php echo CHtml::encode($data->cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealrequesteddate')); ?>:</b>
	<?php echo CHtml::encode($data->dealrequesteddate); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaigndate')); ?>:</b>
	<?php echo CHtml::encode($data->campaigndate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('campaigntime')); ?>:</b>
	<?php echo CHtml::encode($data->campaigntime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealapproveddate')); ?>:</b>
	<?php echo CHtml::encode($data->dealapproveddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealfullfilleddate')); ?>:</b>
	<?php echo CHtml::encode($data->dealfullfilleddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateofpaymentbybrand')); ?>:</b>
	<?php echo CHtml::encode($data->dateofpaymentbybrand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateofpaymenttoagent')); ?>:</b>
	<?php echo CHtml::encode($data->dateofpaymenttoagent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agentid')); ?>:</b>
	<?php echo CHtml::encode($data->agentid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agentsharepercentage')); ?>:</b>
	<?php echo CHtml::encode($data->agentsharepercentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agentshare')); ?>:</b>
	<?php echo CHtml::encode($data->agentshare); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fulfill_engagement')); ?>:</b>
	<?php echo CHtml::encode($data->fulfill_engagement); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fulfill_reach')); ?>:</b>
	<?php echo CHtml::encode($data->fulfill_reach); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullfill_paidtoagent')); ?>:</b>
	<?php echo CHtml::encode($data->fullfill_paidtoagent); ?>
	<br />

	*/ ?>

</div>