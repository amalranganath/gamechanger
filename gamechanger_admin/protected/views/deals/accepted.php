<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Accepted Orders</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php //echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('deals/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'deals-grid',
                'dataProvider'=>$dataProvider,
                //'filter'=>$model,
                'columns'=>array(
        		'dealid',
		//'orderid',
		//'dealstatusid',
		
                 array(
                            'name'=>'athleteid',
                            'type'=>'raw',
                           
                            'value'=>function($data,$row) use (&$model){
                                $result = Deals::model()->GetAthleteName($data->athleteid);
                                return Deals::model()->valuesArray['name'];
                             },
                      
                      
                      ),   
	     
                array(
                            'name'=>'agentid',
                            'type'=>'raw',
                           
                            'value'=>function($data,$row) use (&$model){
                                $result = Deals::model()->GetAgentName($data->agentid);
                                return Deals::model()->valuesArray['name'];
                             },
                      
                      
                        ),
                                     
                 array(
                            'name'=>'dealtype',
                            'type'=>'raw',
                            'value'=>'getdealtype($data->dealtype)',
                      
                        ),                     
		'cost',
                'dealrequesteddate',    
                                     
                array(
                    'name'=>'dealid',
                    'header'=>'Fullfill',
                    'type' => 'raw',
                    'value'=> 'setAsFullfilled($data->dealid)',
                    'filter'=>FALSE ,
                ),  
		/*
		'dealrequesteddate',
		'image',
		'description',
		'campaigndate',
		'campaigntime',
		'dealapproveddate',
		'dealfullfilleddate',
		'dateofpaymentbybrand',
		'dateofpaymenttoagent',
		'agentid',
		'agentsharepercentage',
		'agentshare',
		'fulfill_engagement',
		'fulfill_reach',
		'fullfill_paidtoagent',
		*/
                        array(
                                'class'=>'CButtonColumn',
                                'template'=>'{view}',
                        ),
                ),
        )); ?>
        
        
          <?php
        function setAsFullfilled($dealid) {
                $deals = Deals::model()->find('dealid = '.$dealid.' AND dealstatusid=2' );
                if($deals) {
                     echo CHtml::link("Set as Fullfilled",array("deals/fullfilledByAdmin",array("DealId"=>$dealid)),array("confirm" => "Do you wish to mark this as Fullfilled?",'class'=>'btn btn-info')); 
                } 
            }
        
            function getdealtype($dealtype) {
                
                if($dealtype==0) {
                     echo 'Twitter';
                } else {  
                     echo  'Instagram';
                }
            }
        
        ?>
    </div>
</div>
<!-- /.row -->