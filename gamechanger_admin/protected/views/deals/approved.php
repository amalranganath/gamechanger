<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Approved Deals</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php //echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('deals/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'deals-grid',
            'dataProvider' => $dataProvider,
            //'filter'=>$model,
            'columns' => array(
                'dealid',
                //'orderid',
                //'dealstatusid',
                'brandName',
                array(
                    'name' => 'athleteid',
                    'type' => 'raw',
                    'value' => '$data->GetAthleteName()',
                ),
                array(
                    'name' => 'agentid',
                    'type' => 'raw',
                    'value' => '$data->GetAgentName()',
                ),
                array(
                    'name' => 'dealtype',
                    'type' => 'raw',
                    'value' => 'getdealtype($data->dealtype)',
                ),
                array(
                    'name' => 'cost',
                    'header' => 'Deal Cost (AUD)',
                    'htmlOptions' => array('class' => 'right'),
                ),
                array(
                    'name' => 'dealrequesteddate',
                    'htmlOptions' => array('class' => 'right'),
                ),
                array(
                    'name' => 'dealid',
                    'header' => 'Fulfill',
                    'type' => 'raw',
                    'value' => 'setAsFullfilled($data->dealid)',
                    'filter' => FALSE,
                ),
                /*
                  'dealrequesteddate',
                  'image',
                  'description',
                  'campaigndate',
                  'campaigntime',
                  'dealapproveddate',
                  'dealfullfilleddate',
                  'dateofpaymentbybrand',
                  'dateofpaymenttoagent',
                  'agentid',
                  'agentsharepercentage',
                  'agentshare',
                  'fulfill_engagement',
                  'fulfill_reach',
                  'fullfill_paidtoagent',
                 */
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{view}',
                ),
            ),
        ));
        ?>

        <?php

        function setAsFullfilled($dealid) {
            $deals = Deals::model()->find('dealid = ' . $dealid . ' AND dealstatusid=1');
            if ($deals) {
                echo CHtml::link("Set as Fulfilled", array("deals/fullfilledByAdmin", array("DealId" => $dealid)), array("confirm" => "Do you wish to mark this as Fulfilled?", 'class' => 'btn btn-info'));
            }
        }

        function getdealtype($dealtype) {

            if ($dealtype == 0) {
                echo 'Twitter';
            } else {
                echo 'Instagram';
            }
        }
        ?>

    </div>
</div>
<!-- /.row -->