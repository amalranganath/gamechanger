<?php
/* @var $this DealsController */
/* @var $model Deals */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'deals-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

        <div class="row">
            <div class="col-lg-12">
                <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('deals'), array('class'=>'btn btn-link back')); ?>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="col-lg-12">
                <p class="note">Fields with <span class="required">*</span> are required.</p>
            </div>
        </div>
        <!-- /.row -->
                        

<!-- <span>
<?php
        if(!empty($form->errorSummary($model))) {
            ?>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $form->errorSummary($model); ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
?>
<span> -->

        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'orderid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'orderid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'orderid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dealstatusid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dealstatusid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dealstatusid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'athleteid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'athleteid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'athleteid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dealtype'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dealtype'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dealtype'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'cost'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'cost'); ?>
                    <p class="help-block"><?php echo $form->error($model,'cost'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dealrequesteddate'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dealrequesteddate'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dealrequesteddate'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'image'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'image',array('size'=>60,'maxlength'=>100)); ?>
                    <p class="help-block"><?php echo $form->error($model,'image'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'description'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>250)); ?>
                    <p class="help-block"><?php echo $form->error($model,'description'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'campaigndate'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'campaigndate'); ?>
                    <p class="help-block"><?php echo $form->error($model,'campaigndate'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'campaigntime'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'campaigntime'); ?>
                    <p class="help-block"><?php echo $form->error($model,'campaigntime'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dealapproveddate'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dealapproveddate'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dealapproveddate'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dealfullfilleddate'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dealfullfilleddate'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dealfullfilleddate'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
                
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dateofpaymentbybrand'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dateofpaymentbybrand'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dateofpaymentbybrand'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dateofpaymenttoagent'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dateofpaymenttoagent'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dateofpaymenttoagent'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'agentid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'agentid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'agentid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'agentsharepercentage'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'agentsharepercentage',array('size'=>11,'maxlength'=>11)); ?>
                    <p class="help-block"><?php echo $form->error($model,'agentsharepercentage'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'agentshare'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'agentshare'); ?>
                    <p class="help-block"><?php echo $form->error($model,'agentshare'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'fulfill_engagement'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'fulfill_engagement'); ?>
                    <p class="help-block"><?php echo $form->error($model,'fulfill_engagement'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'fulfill_reach'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'fulfill_reach'); ?>
                    <p class="help-block"><?php echo $form->error($model,'fulfill_reach'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'fullfill_paidtoagent'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'fullfill_paidtoagent'); ?>
                    <p class="help-block"><?php echo $form->error($model,'fullfill_paidtoagent'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-10">
                    <?php echo CHtml::submitButton('Create', array('class'=>'btn btn-default','data-loading-text'=>'Loading...')); ?>
                    <?php echo CHtml::resetButton('Reset', array('class'=>'btn btn-default')); ?>
                </div>
            </div>
	</div>
        <!-- /.row -->

<?php $this->endWidget(); ?>

</div><!-- form -->