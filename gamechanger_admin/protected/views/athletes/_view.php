<?php
/* @var $this AthletesController */
/* @var $data Athletes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('athleteid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->athleteid), array('view', 'id'=>$data->athleteid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firstname')); ?>:</b>
	<?php echo CHtml::encode($data->firstname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastname')); ?>:</b>
	<?php echo CHtml::encode($data->lastname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sportscategoryid')); ?>:</b>
	<?php echo CHtml::encode($data->sportscategoryid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dob')); ?>:</b>
	<?php echo CHtml::encode($data->dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sex')); ?>:</b>
	<?php echo CHtml::encode($data->sex); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('costpertweet')); ?>:</b>
	<?php echo CHtml::encode($data->costpertweet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costperinstagram')); ?>:</b>
	<?php echo CHtml::encode($data->costperinstagram); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imageid')); ?>:</b>
	<?php echo CHtml::encode($data->imageid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updateddate')); ?>:</b>
	<?php echo CHtml::encode($data->updateddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('score')); ?>:</b>
	<?php echo CHtml::encode($data->score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reach')); ?>:</b>
	<?php echo CHtml::encode($data->reach); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audience')); ?>:</b>
	<?php echo CHtml::encode($data->audience); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trust')); ?>:</b>
	<?php echo CHtml::encode($data->trust); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engagement')); ?>:</b>
	<?php echo CHtml::encode($data->engagement); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reach_followers_active')); ?>:</b>
	<?php echo CHtml::encode($data->reach_followers_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reach_followers_inactive')); ?>:</b>
	<?php echo CHtml::encode($data->reach_followers_inactive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reach_followers_fake')); ?>:</b>
	<?php echo CHtml::encode($data->reach_followers_fake); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reach_viralpotential')); ?>:</b>
	<?php echo CHtml::encode($data->reach_viralpotential); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audience_gender_male')); ?>:</b>
	<?php echo CHtml::encode($data->audience_gender_male); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audience_gender_female')); ?>:</b>
	<?php echo CHtml::encode($data->audience_gender_female); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audience_followers_interests')); ?>:</b>
	<?php echo CHtml::encode($data->audience_followers_interests); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audience_quality')); ?>:</b>
	<?php echo CHtml::encode($data->audience_quality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audience_extendedreach')); ?>:</b>
	<?php echo CHtml::encode($data->audience_extendedreach); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trust_sentiment_positive')); ?>:</b>
	<?php echo CHtml::encode($data->trust_sentiment_positive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trust_sentiment_neutral')); ?>:</b>
	<?php echo CHtml::encode($data->trust_sentiment_neutral); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trust_sentiment_negative')); ?>:</b>
	<?php echo CHtml::encode($data->trust_sentiment_negative); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trust_klout')); ?>:</b>
	<?php echo CHtml::encode($data->trust_klout); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engagement_engagement_projectedengagementrate')); ?>:</b>
	<?php echo CHtml::encode($data->engagement_engagement_projectedengagementrate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engagement_engagement_projectedengagement')); ?>:</b>
	<?php echo CHtml::encode($data->engagement_engagement_projectedengagement); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engagment_engagementrating')); ?>:</b>
	<?php echo CHtml::encode($data->engagment_engagementrating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engagement_twitteractivity_averageretweetsperpost')); ?>:</b>
	<?php echo CHtml::encode($data->engagement_twitteractivity_averageretweetsperpost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engagement_engagement_twitteractivity_averagefavoritesperpost')); ?>:</b>
	<?php echo CHtml::encode($data->engagement_engagement_twitteractivity_averagefavoritesperpost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_activity_profileviews')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_activity_profileviews); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_activity_completeddeals')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_activity_completeddeals); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_acceptancerate')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_acceptancerate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_response_averageagentresponsetime')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_response_averageagentresponsetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_response_averageinfluencerresponsetime')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_response_averageinfluencerresponsetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_costperengagement')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_costperengagement); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_costperthousands')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_costperthousands); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketplace_opportunityvalue')); ?>:</b>
	<?php echo CHtml::encode($data->marketplace_opportunityvalue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trending')); ?>:</b>
	<?php echo CHtml::encode($data->trending); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastupdatedadminid')); ?>:</b>
	<?php echo CHtml::encode($data->lastupdatedadminid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastadminupdatedtime')); ?>:</b>
	<?php echo CHtml::encode($data->lastadminupdatedtime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	*/ ?>

</div>