<?php
/* @var $this AthletesController */
/* @var $model Athletes */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'athletes-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

        <div class="row">
            <div class="col-lg-12">
                <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('athletes'), array('class'=>'btn btn-link back')); ?>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="col-lg-12">
                <p class="note">Fields with <span class="required">*</span> are required.</p>
            </div>
        </div>
        <!-- /.row -->
                        

<!-- <span>
<?php
        if(!empty($form->errorSummary($model))) {
            ?>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $form->errorSummary($model); ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
?>
<span> -->

        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'userid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'userid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'userid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'firstname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'firstname',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'firstname'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'lastname'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'lastname',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'lastname'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'sportscategoryid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'sportscategoryid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'sportscategoryid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'dob'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'dob'); ?>
                    <p class="help-block"><?php echo $form->error($model,'dob'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'sex'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'sex',array('size'=>6,'maxlength'=>6)); ?>
                    <p class="help-block"><?php echo $form->error($model,'sex'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'costpertweet'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'costpertweet',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'costpertweet'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'costperinstagram'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'costperinstagram',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'costperinstagram'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'imageid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'imageid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'imageid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'updateddate'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'updateddate'); ?>
                    <p class="help-block"><?php echo $form->error($model,'updateddate'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'score'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'score'); ?>
                    <p class="help-block"><?php echo $form->error($model,'score'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'reach'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'reach',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'reach'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'audience'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'audience',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'audience'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'trust'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'trust',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'trust'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'engagement'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'engagement',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'engagement'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'reach_followers_active'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'reach_followers_active',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'reach_followers_active'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'reach_followers_inactive'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'reach_followers_inactive',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'reach_followers_inactive'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'reach_followers_fake'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'reach_followers_fake',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'reach_followers_fake'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'reach_viralpotential'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'reach_viralpotential',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'reach_viralpotential'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'audience_gender_male'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'audience_gender_male',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'audience_gender_male'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'audience_gender_female'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'audience_gender_female',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'audience_gender_female'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'audience_followers_interests'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'audience_followers_interests',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'audience_followers_interests'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'audience_quality'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'audience_quality',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'audience_quality'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'audience_extendedreach'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'audience_extendedreach',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'audience_extendedreach'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'trust_sentiment_positive'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'trust_sentiment_positive',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'trust_sentiment_positive'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'trust_sentiment_neutral'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'trust_sentiment_neutral',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'trust_sentiment_neutral'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'trust_sentiment_negative'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'trust_sentiment_negative',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'trust_sentiment_negative'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'trust_klout'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'trust_klout',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'trust_klout'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'engagement_engagement_projectedengagementrate'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'engagement_engagement_projectedengagementrate',array('size'=>25,'maxlength'=>25)); ?>
                    <p class="help-block"><?php echo $form->error($model,'engagement_engagement_projectedengagementrate'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'engagement_engagement_projectedengagement'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'engagement_engagement_projectedengagement',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'engagement_engagement_projectedengagement'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'engagment_engagementrating'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'engagment_engagementrating',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'engagment_engagementrating'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'engagement_twitteractivity_averageretweetsperpost'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'engagement_twitteractivity_averageretweetsperpost',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'engagement_twitteractivity_averageretweetsperpost'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'engagement_engagement_twitteractivity_averagefavoritesperpost'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'engagement_engagement_twitteractivity_averagefavoritesperpost',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'engagement_engagement_twitteractivity_averagefavoritesperpost'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_activity_profileviews'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_activity_profileviews',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_activity_profileviews'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_activity_completeddeals'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_activity_completeddeals',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_activity_completeddeals'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_acceptancerate'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_acceptancerate',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_acceptancerate'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_response_averageagentresponsetime'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_response_averageagentresponsetime',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_response_averageagentresponsetime'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_response_averageinfluencerresponsetime'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_response_averageinfluencerresponsetime',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_response_averageinfluencerresponsetime'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_costperengagement'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_costperengagement',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_costperengagement'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_costperthousands'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_costperthousands',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_costperthousands'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'marketplace_opportunityvalue'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'marketplace_opportunityvalue',array('size'=>10,'maxlength'=>10)); ?>
                    <p class="help-block"><?php echo $form->error($model,'marketplace_opportunityvalue'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'trending'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'trending'); ?>
                    <p class="help-block"><?php echo $form->error($model,'trending'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'lastupdatedadminid'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'lastupdatedadminid'); ?>
                    <p class="help-block"><?php echo $form->error($model,'lastupdatedadminid'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'lastadminupdatedtime'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'lastadminupdatedtime'); ?>
                    <p class="help-block"><?php echo $form->error($model,'lastadminupdatedtime'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'created_at'); ?>
                </div>
                <div class="col-lg-10">
                    <?php echo $form->textField($model,'created_at'); ?>
                    <p class="help-block"><?php echo $form->error($model,'created_at'); ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-10">
                    <?php echo CHtml::submitButton('Create', array('class'=>'btn btn-default','data-loading-text'=>'Loading...')); ?>
                    <?php echo CHtml::resetButton('Reset', array('class'=>'btn btn-default')); ?>
                </div>
            </div>
	</div>
        <!-- /.row -->

<?php $this->endWidget(); ?>

</div><!-- form -->