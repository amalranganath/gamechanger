<?php
/* @var $this AthletesController */
/* @var $model Athletes */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript(
        'flashFadeOut', "$('.info').animate({opacity: 1.0}, 3000).fadeOut('slow');", CClientScript::POS_READY
);
?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'athletes-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    ?>

    <div class="row">
        <div class="col-lg-12">
            <p class="text-primary">Fields with * are required</p>
            <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Back', Yii::app()->createUrl('athletes'), array('class' => 'btn btn-link back')); ?>
        </div>
    </div>
    <!-- /.row -->

    <?php if (Yii::app()->user->hasFlash('athletes-form')) { ?>
        <div class="row info">
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo Yii::app()->user->getFlash('athletes-form'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <span>
        <?php if ($form->errorSummary($model)) { ?>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $form->errorSummary($model); ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </span>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-center">Athlete Basic Details</h4>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($lookupimages, 'imagename'); ?>
                            </div>
                            <div class="col-lg-4">
                                <?php echo CHtml::activeFileField($lookupimages, 'imagename', array('class' => 'form-control')); ?>
                                <p class="help-block"><?php echo $form->error($lookupimages, 'imagename'); ?></p>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php
                                    if ($model->imageid != '') {
                                        ?>
                                        <div class=""> 
                                            <div class="col-sm-3">
                                                <?php
                                                $imagename = Lookupimages::model()->find('imageid=' . $model->imageid)->imagename;
                                                ?>
                                                <img src="<?php echo Yii::app()->baseUrl . '/../images/athlete/200x150/' . $imagename; ?>" class="img-height"> 

                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <br/>

                    <!--         <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-2">
                    <?php //echo $form->labelEx(Lookupimages::model(), 'imagename'); ?>
                                    </div>
                                    <div class="col-lg-10">
                    <?php //echo CHtml::activeFileField(Lookupimages::model(), 'imagename', array('class' => 'form-control')); ?>
                                        <p class="help-block"><?php //echo $form->error(Lookupimages::model(), 'imagename');           ?></p>
                                    </div>
                                </div>
                            </div>-->
                    <!-- /.row -->

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'firstname'); ?>
                            </div>
                            <div class="col-lg-10">
                                <?php echo $form->textField($model, 'firstname', array('size' => 25, 'maxlength' => 25, 'class' => 'form-control')); ?>
                            </div>
                            <div class="col-lg-12">
                                <p class="help-block"><?php echo $form->error($model, 'firstname'); ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->


                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'lastname'); ?>
                            </div>
                            <div class="col-lg-10">
                                <?php echo $form->textField($model, 'lastname', array('size' => 25, 'maxlength' => 25, 'class' => 'form-control')); ?>
                            </div>
                            <div class="col-lg-12">
                                <p class="help-block"><?php echo $form->error($model, 'lastname'); ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->


                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'sportscategoryid'); ?>
                            </div>
                            <div class="col-lg-4">
                                <?php
                                echo $form->dropdownList($model, 'sportscategoryid', CHtml::listData(Sportscatagory::model()->findAll(), 'sportscatagory_id', 'name'), array('class' => 'form-control', 'prompt' => '--Select Sport Category--'
                                ));
                                ?>

                            </div>
                            <div class="col-lg-12">
                                <p class="help-block"><?php echo $form->error($model, 'sportscategoryid'); ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'score'); ?>
                            </div>
                            <div class="col-lg-10">
                                <?php echo $form->textField($model, 'score', array('class' => 'form-control')); ?>
                            </div>
                            <div class="col-lg-12">
                                <p class="help-block"><?php echo $form->error($model, 'score'); ?></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'dob'); ?>
                            </div>
                            <!--                <div class="col-lg-10">-->

                            <?php
                            /* if($model->dob!=''){

                              $year=  substr($model->dob,0,-6 );
                              $month=  substr($model->dob,5,-3 );
                              $day=  substr($model->dob,-2 );


                              }else{
                              $year='';$month='';$day='';
                              } */

                            // echo 'Dateofbirth'.$model->dob;
                            $year = $month = $day = '';

                            if (!empty($model->dob) && !is_array($model->dob)) {
                                //2015-02-03
                                list($year, $month, $day) = explode('-', $model->dob);
                            }
                            ?>
                            <div class="form-group">
                                <div class="col-lg-2">
                                    <?php //echo CHtml::dropDownList('Athletes[dob][0]',$year,array('Year'=>'Year','1980'=>'1980','1981'=>'1981','1982'=>'1982','1983'=>'1983'),array('class'=>'form-control','style'=>'')); ?>
                                    <?php
                                    echo CHtml::dropDownList('Athletes[dob][0]', (!empty($year)) ? $year : '', Yii::app()->params['dob_years'], array(
                                        'prompt' => 'Year',
                                        'class' => 'form-control',
                                        'style' => 'width: 100%;'
                                            )
                                    );
                                    ?>   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-2">
                                    <?php //echo CHTML::dropDownList('Athletes[dob][1]',$month,array('Month'=>'Month','01'=>'01','02'=>'02','03'=>'03','04'=>'04'),array('class'=>'form-control','style'=>'')); ?>
                                    <?php
                                    echo CHtml::dropDownList('Athletes[dob][1]', (!empty($month)) ? $month : '', Yii::app()->params['dob_months'], array(
                                        'prompt' => 'Month',
                                        'class' => 'form-control',
                                        'style' => 'width: 100%;'
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-2">
                                    <?php //echo CHTML::dropDownList('Athletes[dob][2]',$day,array('Day'=>'Day', '01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07'),array('class'=>'form-control','style'=>'')); ?>

                                    <?php
                                    echo CHtml::dropDownList('Athletes[dob][2]', (!empty($day)) ? $day : '', Yii::app()->params['dob_days'], array(
                                        'prompt' => 'Day',
                                        'class' => 'form-control',
                                        'style' => 'width: 100%;'
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-12">  
                                <p class="help-block"><?php echo $form->error($model, 'dob'); ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <br/>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'sex'); ?>
                            </div>
                            <div class="col-lg-2">
                                <?php
                                echo $form->dropdownList($model, 'sex', array('1' => 'Male', '2' => 'Female'), array('class' => 'form-control')
                                );
                                ?>

                            </div>
                            <div class="col-lg-12">
                                <p class="help-block"><?php echo $form->error($model, 'sex'); ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'team'); ?>
                            </div>
                            <div class="col-lg-2">
                                <?php echo $form->textField($model, 'team', array('class' => 'form-control')); ?>

                            </div>
                            <div class="col-lg-12">
                                <p class="help-block"><?php echo $form->error($model, 'team'); ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->


                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-2">
                                <?php echo $form->labelEx($model, 'trending'); ?>
                            </div>
                            <div class="col-lg-2">
                                <?php
                                echo $form->dropDownList($model, 'trending', array(
                                    '1' => 'Enable',
                                    '0' => 'Disable',
                                        ), array(
                                    'class' => 'form-control',
                                ));
                                ?>  
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div> <!-------------End Basic Details------------>
    <br/>   

    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-center">Tweets Details</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <?php echo $form->labelEx($model, 'costpertweet'); ?>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $form->textField($model, 'costpertweet', array('class' => 'form-control')); ?>
                        </div>
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo $form->error($model, 'costpertweet'); ?></p>
                        </div>        
                    </div>     
                    <div class="form-group">
                        <div class="col-lg-6">
                            <?php echo $form->labelEx($model, 'costpertweetimg'); ?>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $form->textField($model, 'costpertweetimg', array('class' => 'form-control')); ?>
                        </div>
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo $form->error($model, 'costpertweetimg'); ?></p>
                        </div>
                    </div>   
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-center">Instagram Details</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <?php echo $form->labelEx($model, 'costperinstagram'); ?>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $form->textField($model, 'costperinstagram', array('class' => 'form-control')); ?>
                        </div>
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo $form->error($model, 'costperinstagram'); ?></p>
                        </div>
                    </div> 

                    <div class="form-group">
                        <div class="col-lg-6">
                            <?php echo $form->labelEx($model, 'costperinstagramimg'); ?>
                        </div>
                        <div class="col-lg-6">
                            <?php echo $form->textField($model, 'costperinstagramimg', array('class' => 'form-control')); ?>
                        </div>
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo $form->error($model, 'costperinstagramimg'); ?></p>
                        </div>
                    </div> 
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center">Reach Details</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'reach'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'reach', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'reach'); ?></p>
                                </div>
                            </div>
                        </div>

                        <!-- /.row -->       

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'reach_followers_active'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'reach_followers_active', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'reach_followers_active'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'reach_followers_inactive'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'reach_followers_inactive', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'reach_followers_inactive'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'reach_followers_fake'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'reach_followers_fake', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'reach_followers_fake'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'reach_viralpotential'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'reach_viralpotential', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'reach_viralpotential'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->  


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'reach_followers_reach'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'reach_followers_reach', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'reach_followers_reach'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->  
                    </div><!-- /panel-body--> 
                </div>
            </div>   

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center">Audience Details</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'audience'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'audience', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'audience'); ?></p>
                                </div>
                            </div>
                        </div>

                        <!-- /.row -->
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'audience_gender_male'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'audience_gender_male', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'audience_gender_male'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'audience_gender_female'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'audience_gender_female', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'audience_gender_female'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'audience_followers_interests'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'audience_followers_interests', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'audience_followers_interests'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'audience_quality'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'audience_quality', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'audience_quality'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'audience_extendedreach'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'audience_extendedreach', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'audience_extendedreach'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div> 
        </div><!-- /.row -->


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center">Trust Details</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'trust'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'trust', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'trust'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'trust_sentiment_positive'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'trust_sentiment_positive', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'trust_sentiment_positive'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'trust_sentiment_neutral'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'trust_sentiment_neutral', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'trust_sentiment_neutral'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'trust_sentiment_negative'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'trust_sentiment_negative', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'trust_sentiment_negative'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'trust_klout'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'trust_klout', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'trust_klout'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->            
                    </div>
                </div>
            </div>


            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center">Engagement Details</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'engagement'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'engagement', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'engagement'); ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'engagement_engagement_projectedengagementrate'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'engagement_engagement_projectedengagementrate', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'engagement_engagement_projectedengagementrate'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'engagement_engagement_projectedengagement'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'engagement_engagement_projectedengagement', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'engagement_engagement_projectedengagement'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'engagment_engagementrating'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'engagment_engagementrating', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'engagment_engagementrating'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'engagement_twitteractivity_averageretweetsperpost'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'engagement_twitteractivity_averageretweetsperpost', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'engagement_twitteractivity_averageretweetsperpost'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'engagement_engagement_twitteractivity_averagefavoritesperpost'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'engagement_engagement_twitteractivity_averagefavoritesperpost', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'engagement_engagement_twitteractivity_averagefavoritesperpost'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div>
        </div><!-- /.row -->


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center">Market Details</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_activity_profileviews'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_activity_profileviews', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_activity_profileviews'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_activity_completeddeals'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_activity_completeddeals', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_activity_completeddeals'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_acceptancerate'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_acceptancerate', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_acceptancerate'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_response_averageagentresponsetime'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_response_averageagentresponsetime', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_response_averageagentresponsetime'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_response_averageinfluencerresponsetime'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_response_averageinfluencerresponsetime', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_response_averageinfluencerresponsetime'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_costperengagement'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_costperengagement', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_costperengagement'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_costperthousands'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_costperthousands', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_costperthousands'); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->


                        <div class="row">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <?php echo $form->labelEx($model, 'marketplace_opportunityvalue'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php echo $form->textField($model, 'marketplace_opportunityvalue', array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-lg-12">
                                    <p class="help-block"><?php echo $form->error($model, 'marketplace_opportunityvalue'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->


        <div class="row">
            <div class="form-group">

                <div class="col-lg-2 col-xs-4 panel-body">
                    <?php echo CHtml::submitButton('Update', array('class' => 'bg_color_hover btn', 'onclick' => 'return confirm("Are you sure you want to update?");')); ?>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <?php $this->endWidget(); ?>

    </div><!-- form -->