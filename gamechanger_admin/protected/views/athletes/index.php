<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Athletes</h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        
        <?php // print_r( str_replace("endors_admin/index.php","index.php/deals/accepted",Yii::app()->createAbsoluteUrl('')) ) ?>
        <?php //echo CHtml::link('<i class="fa fa-file-o"></i> Create', Yii::app()->createUrl('athletes/create'), array('class'=>'btn btn-default')); ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'athletes-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
        		'athleteid',
		//'userid',
                    
                    array(
                            'name'=>'userid',
                            'type'=>'raw',
                            'filter'=>false,
                            'value'=>function($data,$row) use (&$model){
                                $result = $model->GetAgentName($data->userid);
                                return $model->valuesArray['name'];
                             },
                      
                      
                        ),
                     
                    
                    
		'firstname',
		'lastname',
		
                  array(
                            'name'=>'sportscategoryid',
                            'type'=>'raw',
                            // 'filter' => array('1'=>'Foot Ball','2'=>'Cricket','3'=>'Table Tennis','4'=>'Rugby','5'=>'Soccer','6'=>'Golf','7'=>'Car Racing','8'=>'Marathon'), 
                            'filter'=> CHtml::listData(Sportscatagory::model()->findAll(), 'sportscatagory_id', 'name'),
                            /*'value'=>function($data,$row) use (&$model){
                                $result = $model->GetSportCategoryName($data->sportscategoryid);
                                return $model->valuesArray['name'];
                             },*/
                             'value'=>'$data->sportscategory->name',
                                     
                                     
                        ),
                    
                    
                'team',    
		'dob',
		/*
		'sex',
		'costpertweet',
		'costperinstagram',
		'imageid',
		'updateddate',
		'score',
		'reach',
		'audience',
		'trust',
		'engagement',
		'marketplace',
		'reach_followers_active',
		'reach_followers_inactive',
		'reach_followers_fake',
		'reach_viralpotential',
		'audience_gender_male',
		'audience_gender_female',
		'audience_followers_interests',
		'audience_quality',
		'audience_extendedreach',
		'trust_sentiment_positive',
		'trust_sentiment_neutral',
		'trust_sentiment_negative',
		'trust_klout',
		'engagement_engagement_projectedengagementrate',
		'engagement_engagement_projectedengagement',
		'engagment_engagementrating',
		'engagement_twitteractivity_averageretweetsperpost',
		'engagement_engagement_twitteractivity_averagefavoritesperpost',
		'marketplace_activity_profileviews',
		'marketplace_activity_completeddeals',
		'marketplace_acceptancerate',
		'marketplace_response_averageagentresponsetime',
		'marketplace_response_averageinfluencerresponsetime',
		'marketplace_costperengagement',
		'marketplace_costperthousands',
		'marketplace_opportunityvalue',
		'trending',
		'lastupdatedadminid',
		'lastadminupdatedtime',
		'created_at',
		*/
                        array(
                                'class'=>'CButtonColumn',
                                'template'=>'{update}',
                        ),
                ),
        )); ?>
        
       <?php
   /*  function GetSportCategoryName() {
          $model= new Athletes;
          $sportcategory= Sportscatagory::model()->find('sportscatagory_id ="2"');
          if($sportcategory)
          {
             
              
          }
           return $sportcategory->name;
     } 
     
      */
       
       
       ?>
        
        
    </div>
</div>
<!-- /.row -->