<?php

class ActivationController extends Controller
{
	public $defaultAction = 'activation';

	
	/**
	 * Activation user account
	 */
	public function actionActivation () {
		$email = $_GET['email'];
		$activkey = $_GET['activkey'];
                $usertype= $_GET['usertype'];
		if ($email&&$activkey) {
			$find = User::model()->notsafe()->findByAttributes(array('email'=>$email));
			if (isset($find)&&$find->status) {
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is active.")));
			} elseif(isset($find->activkey) && ($find->activkey==$activkey)) {
                            
				$find->activkey = UserModule::encrypting(microtime());
				$find->status = 1;
				$find->save();
                                $userid=$find->id; 
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is activated.")));
                                    
                                   if($usertype == 1){
                                        $agent = new Agent;
                                        $agent->user_id=$userid;
                                      
                                        if($agent->save()){
                                        $this->redirect( array('/Agent/index','usertype'=>'1','userid'=>$userid ));
                                        
                                        }

                                    }elseif($usertype ==2){
                                        $brand = new Brand;
                                        $brand->user_id=$userid;
                                        if($brand->save()){    
                                        $this->redirect( array('/Brand/index','usertype'=>'2','userid'=>$userid ));
                                        
                                        }
                                       
                                    }
			} else {
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
			}
		} else {
			$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
		}
	}

}