<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
$this->breadcrumbs=array(
	UserModule::t("Profile") => array('/user/profile'),
	UserModule::t("Change Password"),
);
/*$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'))
		:array()),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
    array('label'=>UserModule::t('Profile'), 'url'=>array('/user/profile')),
    array('label'=>UserModule::t('Edit'), 'url'=>array('edit')),
    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout')),
);*/
?>

<h1 class="page-header"><?php echo UserModule::t("Change password"); ?></h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'changepassword-form',
	'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="text-primary"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	<?php //echo $form->errorSummary($model); ?>
	
	<div class="row">
            <div class="form-group panel-body">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'oldPassword'); ?>
                </div>
                <div class="col-lg-6">
                    <?php echo $form->passwordField($model,'oldPassword',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'oldPassword'); ?>
                </div>
            </div>
	</div>
	
	<div class="row">
            <div class="form-group panel-body">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'password'); ?>
                </div>
                <div class="col-lg-6">
                    <?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'password'); ?>
                    <p class="hint"><?php echo UserModule::t("Minimal password length 4 symbols."); ?></p>
                </div>
            </div>
        </div>
        
	<div class="row">
            <div class="form-group panel-body">
                <div class="col-lg-2">
                    <?php echo $form->labelEx($model,'verifyPassword'); ?>
                </div>
                <div class="col-lg-6">
                    <?php echo $form->passwordField($model,'verifyPassword',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'verifyPassword'); ?>
                </div>
            </div>
	</div>
        
	<div class="row submit">
            <div class="form-group panel-body">
                <div class="col-lg-2 col-xs-4">
                    <?php echo CHtml::submitButton(UserModule::t("Save"),array('class'=>'bg_color_hover btn')); ?>
                </div>
            </div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->