<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
   
</div>
<?php endif; ?>


<div>
<div class="signup_form">
    <?php echo CHtml::beginForm(); ?>
                <div class="col-sm-12">
                    <div class="panel text-center text-uppercase">
                    <?php if(Yii::app()->user->hasFlash('success-registration')):?>
                        <div class="alert alert-success">
                            <?php echo Yii::app()->user->getFlash('success-registration'); ?>
                        </div>
                    <?php endif; ?>

                        <h1>login</h1>
                    </div>
                </div>
                <?php echo CHtml::errorSummary($model); ?>
                <div class="col-sm-12 text-center panel-body">
                    
                    <?php echo CHtml::activeTextField($model,'username', array('class'=>'panel-body','placeholder'=>'Email')) ?>
                </div>
                
                <div class="col-sm-12 text-center panel-body">
                  
                    <?php echo CHtml::activePasswordField($model,'password', array('class'=>'panel-body','placeholder'=>'Password')) ?>
                </div>
                
                
                <div class="col-sm-12 text-center panel-body">
                  
                    <?php echo CHtml::submitButton(UserModule::t("Login"),array('class'=>'panel-body bg_color_hover btn')); ?>
                </div>
                
                <div class="col-sm-12 text-center panel-body">
                    <p class="hint">
					<?php //echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?> <?php //echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
		</p>
                    <p>
                        <!--By signing up, you agree to our <a href="/Content/terms.html" target="_blank" class="font_color">Terms of Service</a> &amp; <a href="/Content/privacy.html" target="_blank" class="font_color">Privacy Policy</a>-->
                    </p>
                </div>

            </div>
    <?php echo CHtml::endForm(); ?>
  </div>