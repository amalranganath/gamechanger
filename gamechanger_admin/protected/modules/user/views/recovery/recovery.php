<div id="page-wrapper">
<div class="signup_form">
    <?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
</div>
<?php else: ?>
    <?php echo CHtml::beginForm(); ?>
    <?php echo CHtml::errorSummary($form); ?>
                <div class="col-sm-12">
                    <div class="panel text-center text-uppercase">
                        <h1>Restore</h1>
                    </div>
                </div>
                
                <div class="col-sm-12 text-center panel-body">
                    <?php echo CHtml::activeTextField($form,'login_or_email',array('class'=>'panel-body','placeholder'=>'Email')) ?>
                     <p class="hint"><?php echo UserModule::t("Please enter your login or email addres."); ?></p>
                </div>
                
    
                <div class="col-sm-12 text-center panel-body">
                  <?php echo CHtml::submitButton(UserModule::t("Restore"),array('class'=>'panel-body bg_color_hover btn')); ?>
                </div>
                
                <div class="col-sm-12 text-center panel-body">
                    <p class="hint">
		<?php // echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?>  <?php // echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
		</p>
                    <p>
                        <!--By signing up, you agree to our <a href="/Content/terms.html" target="_blank" class="font_color">Terms of Service</a> &amp; <a href="/Content/privacy.html" target="_blank" class="font_color">Privacy Policy</a>-->
                    </p>
                </div>

            </div>
    <?php echo CHtml::endForm(); ?>
    </div>
<?php endif; ?>