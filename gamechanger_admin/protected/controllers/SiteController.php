<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
     
        
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
	
            
            // IF NOT LOGGED IN, GO TO LOGIN SCREEN
            if(Yii::app()->user->isGuest)
            {
              $this->redirect(Yii::app()->homeUrl);
            // IF NOT LOGGED IN, GO TO LOGIN SCREEN
            }
            else {

                 $this->render('index');

            }
	}


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function getLatestAgents($limit = 5)
	{
		$model = Agent::model()->findAll(
			array(
                            'condition' => 'completed="1"',
                            'limit'=> $limit,
                            'order'=> 'id DESC'
			)
		);

		return (!empty($model))? $model: 0;
	}

	public function getLatestBrands($limit = 5)
	{
		$model = Brand::model()->findAll(
			array(
                            'condition' => 'completed="1"',
                            'limit'=> $limit,
                            'order'=> 'id DESC'
			)
		);

		return (!empty($model))? $model: 0;
	}
	public function getLatestAthletes($limit = 5)
	{
		$model = Athletes::model()->findAll(
			array(
				'limit'=> $limit,
				'order'=> 'athleteid DESC'
			)
		);

		return (!empty($model))? $model: 0;
	}

	public function getDeals($status = 0, $time = 'all', $is_paid = null)
	{
		$criteria = new CDbCriteria();

		// check for the status
		$criteria->condition = 'dealstatusid = '. $status;


		$datetype = '';
		switch($status) {
			case 0:
				// pending
				$datetype = 't.dealrequesteddate';
				break;
			case 1:
				// approved
				$datetype = 't.dealapproveddate';
				break;
			case 3:
				// fulfilled
				$datetype = 't.dealfullfilleddate';
				break;
		}

		// check for the time
		switch($time) {
			case 'week':
//				$criteria->condition = '('.$datetype.' >= curdate()-7) AND ('.$datetype.' < curdate())';
				$criteria->addCondition('('.$datetype.' >= curdate()-7) AND ('.$datetype.' < curdate())', 'AND');
				break;

			case 'month':
//				$criteria->condition = '('.$datetype.' >= curdate()-30) AND ('.$datetype.' < curdate())';
				$criteria->addCondition('('.$datetype.' >= curdate()-30) AND ('.$datetype.' < curdate())', 'AND');
				break;

			case '6months':
//				$criteria->condition = '('.$datetype.' >= curdate()-30*6) AND ('.$datetype.' < curdate())';
				$criteria->addCondition('('.$datetype.' >= curdate()-(30*6)) AND ('.$datetype.' < curdate())', 'AND');
				break;

			case 'all':
				$criteria->addCondition($datetype.' < curdate()', 'AND');
				break;
		}


		if($status == 3 && !is_null($is_paid)) {
			$criteria->addCondition('fullfill_paidtoagent = '. $is_paid, 'AND');
		}


		return $model = Deals::model()->findAll($criteria);
	}

    public function getDealCount($status = 0, $time = 'all', $is_paid = null)
    {
		$model = $this->getDeals($status, $time, $is_paid);

        return (!empty($model))? count($model): 0;
    }

    public function getDealCost($status = 0, $time = 'all', $is_paid = null)
    {
		$model = $this->getDeals($status, $time, $is_paid);

		$total = 0;
		foreach($model as $key => $value) {
			$total += floatval($value->cost);
		}

//		return number_format($total, 2);
        return (!empty($total))? number_format($total, 2): 0;
    }

}