<?php

class AthletesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','update'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
				'actions'=>array('create'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
				'actions'=>array('delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionUpdate($id)
	{
//		$this->render('view',array(
//			'model'=>$this->loadModel($id),
//		));
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

                 $lookupimages = Lookupimages::model()->find(array('condition'=>'imageid = "'.$model->imageid.'"'));
                 $fileName = $lookupimages->imagename;
                
		if(isset($_POST['Athletes']))
		{
                    
                        //echo '<pre>'.print_r($_POST['Athletes'],1).'</pre>';
                        //die();
   
			$model->attributes=$_POST['Athletes'];
                         
                        $model->firstname = $_POST['Athletes']['firstname'];
                        $model->lastname = $_POST['Athletes']['lastname'];
                        $model->sportscategoryid = $_POST['Athletes']['sportscategoryid'];
                        $model->lastname = $_POST['Athletes']['lastname'];
                        
                        $dob=$_POST['Athletes']['dob']['0'].'/'.$_POST['Athletes']['dob']['1'].'/'.$_POST['Athletes']['dob']['2'];
                        $dob=  strtotime($dob);        
                        $dob= date('Y-m-d',$dob);
                        $model->dob=$dob;
        
                        $model->sex = $_POST['Athletes']['sex'];
                        $model->costpertweet = $_POST['Athletes']['costpertweet'];
                        $model->firstname = $_POST['Athletes']['firstname'];
                        $model->lastname = $_POST['Athletes']['lastname'];
                        $model->sportscategoryid = $_POST['Athletes']['sportscategoryid'];
                        $model->lastname = $_POST['Athletes']['lastname'];
                        $model->score = $_POST['Athletes']['score'];
                        $model->reach = $_POST['Athletes']['reach'];
                        $model->audience = $_POST['Athletes']['audience'];
                        $model->trust = $_POST['Athletes']['trust'];
                        $model->engagement = $_POST['Athletes']['engagement'];
                        $model->marketplace = $_POST['Athletes']['marketplace'];
                        
                        $model->reach_followers_active = $_POST['Athletes']['reach_followers_active'];
                        $model->reach_followers_inactive = $_POST['Athletes']['reach_followers_inactive'];
                        $model->reach_followers_fake = $_POST['Athletes']['reach_followers_fake'];
                        $model->reach_viralpotential= $_POST['Athletes']['reach_viralpotential'];
                        $model->reach_followers_reach= $_POST['Athletes']['reach_followers_reach'];
                        
                        $model->audience_gender_male = $_POST['Athletes']['audience_gender_male'];
                        $model->audience_gender_female = $_POST['Athletes']['audience_gender_female'];
                        $model->audience_followers_interests = $_POST['Athletes']['audience_followers_interests'];
                        $model->audience_quality = $_POST['Athletes']['audience_quality'];
                        $model->audience_extendedreach = $_POST['Athletes']['audience_extendedreach'];
                              
                        $model->trust_sentiment_positive = $_POST['Athletes']['trust_sentiment_positive'];
                        $model->trust_sentiment_neutral = $_POST['Athletes']['trust_sentiment_neutral'];
                        $model->trust_sentiment_negative = $_POST['Athletes']['trust_sentiment_negative'];
                        $model->trust_klout = $_POST['Athletes']['trust_klout'];
                        
                        $model->engagement_engagement_projectedengagementrate= $_POST['Athletes']['engagement_engagement_projectedengagementrate'];
                        $model->engagement_engagement_projectedengagement= $_POST['Athletes']['engagement_engagement_projectedengagement'];
                        $model->engagment_engagementrating = $_POST['Athletes']['engagment_engagementrating'];
                        $model->engagement_twitteractivity_averageretweetsperpost = $_POST['Athletes']['engagement_twitteractivity_averageretweetsperpost'];
                        $model->engagement_engagement_twitteractivity_averagefavoritesperpost = $_POST['Athletes']['engagement_engagement_twitteractivity_averagefavoritesperpost'];
                         
                        $model->marketplace_activity_profileviews = $_POST['Athletes']['marketplace_activity_profileviews'];
                        $model->marketplace_activity_completeddeals= $_POST['Athletes']['marketplace_activity_completeddeals'];
                        $model->marketplace_acceptancerate= $_POST['Athletes']['marketplace_acceptancerate'];
                        $model->marketplace_response_averageagentresponsetime = $_POST['Athletes']['marketplace_response_averageagentresponsetime'];
                        $model->marketplace_response_averageinfluencerresponsetime= $_POST['Athletes']['marketplace_response_averageinfluencerresponsetime'];
                        $model->marketplace_costperengagement= $_POST['Athletes']['marketplace_costperengagement'];
                        $model->marketplace_costperthousands= $_POST['Athletes']['marketplace_costperthousands'];
                        $model->marketplace_opportunityvalue= $_POST['Athletes']['marketplace_opportunityvalue'];
                                               
                        $model->trending=$_POST['Athletes']['trending'];
                        $model->team=$_POST['Athletes']['team'];
                        
                        $model->lastupdatedadminid= Yii::app()->user->getId();
                        $model->lastadminupdatedtime = new CDbExpression('NOW()'); // "YYYY-MM-DD HH:MM:SS"
                        //echo '<pre>'.print_r($model->attributes,1).'</pre>';
                        //die();
   
                       
                       
                        
                      /*  $lookupimages = Lookupimages::model()->find(array('condition'=>'imageid = "'.$model->imageid.'"'));
                
                        $fileName = '';

                        if($lookupimages) 
                        {
                            $fileName = $lookupimages->imagename;
                           
                        }
                         
                        else 
                            
                        {
                            $lookupimages = new Lookupimages;
                           
                        }  
                        
                         $uploadedFile=CUploadedFile::getInstance($lookupimages,'imagename');
                         $ext = pathinfo($uploadedFile, PATHINFO_EXTENSION);
                         $filename= pathinfo($uploadedFile, PATHINFO_FILENAME);

                        $newName = date("m-d-Y-h-i-s", time())."-".$filename.'.'.$ext;*/
                        
                          
      //                  if($lookupimages->validate())
      //                  {
                              
                              $uploadedFile=CUploadedFile::getInstance($lookupimages,'imagename');
                              if($uploadedFile){
                                  $lookupimages->imagename = $uploadedFile;
                               }   
                                 
                              /*   if($model->validate()){
                                       if($uploadedFile){
                                                 
                                                  $lookupimages->imagename = $uploadedFile;
                                                  $model->imageid=$this->singleImageUpload($lookupimages, '/images/athlete/',$newName);
                                                  unlink(Yii::getPathOfAlias('webroot').'/../images/athlete/'. $fileName);
                                                  unlink(Yii::getPathOfAlias('webroot').'/../images/athlete/400x300/'. $fileName);
                                                  unlink(Yii::getPathOfAlias('webroot').'/../images/athlete/200x150/'. $fileName);
                                                 
                                        }
                                         else
                                         {
                                                $model->imageid=$lookupimages->imageid;

                                         }
                                       
                                        if($model->save(FALSE)) {

                                                Yii::app()->user->setFlash('athletes-form', "Successfully Updated!");
                                                $this->redirect(array('update','id'=>$model->athleteid));
                                        }

                               }*/
                               
                         if($model->validate()&& $lookupimages->validate()){
                        
                        
                            if($uploadedFile){
                                $this->singleImageUpload($lookupimages, '/images/athlete/', $model->imageid, $fileName);
                               
                            }
                        
                       
                           if($model->save(false)) {
                                 Yii::app()->user->setFlash('athletes-form', "Successfully Updated!");
                                 $this->redirect(array('update','id'=>$model->athleteid));
                           }
                    }
                               
                               
                               
       //           }
                        
                          
		}

		$this->render('update',array(
			'model'=>$model,
                        'lookupimages'=>$lookupimages,
		));                
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Athletes;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
               

		if(isset($_POST['Athletes']))
		{
			
                        $model->attributes=$_POST['Athletes'];
			if($model->save()) {
                                Yii::app()->user->setFlash('athletes-form', "Successfully Inserted!");
				$this->redirect(array('view','id'=>$model->athleteid));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
//	public function actionUpdate($id)
//	{
//		$model=$this->loadModel($id);
//
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//
//		if(isset($_POST['Athletes']))
//		{
//			$model->attributes=$_POST['Athletes'];
//			if($model->save())
//				$this->redirect(array('view','id'=>$model->athleteid));
//		}
//
//		$this->render('update',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		$dataProvider=new CActiveDataProvider('Athletes');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
                $model=new Athletes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Athletes']))
			$model->attributes=$_GET['Athletes'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
//	public function actionAdmin()
//	{
//		$model=new Athletes('search');
//		$model->unsetAttributes();  // clear any default values
//		if(isset($_GET['Athletes']))
//			$model->attributes=$_GET['Athletes'];
//
//		$this->render('admin',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Athletes the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Athletes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Athletes $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='athletes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
