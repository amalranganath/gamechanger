<?php

class DealsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','pending','approved','expired','disapproved','fullfilled','approvedByAdmin','disapprovedByAdmin','fullfilledByAdmin','paidToAgent'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
				'actions'=>array('create'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
				'actions'=>array('delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
//		$this->render('view',array(
//			'model'=>$this->loadModel($id),
//		));
		$model=$this->loadModel($id);
                
                $model->setScenario('numeric_validation');
                $this->performAjaxValidation($model);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Deals']))
		{
			$model->attributes=$_POST['Deals'];
                        if($model->validate()){
                            if($model->save(false)) {
                                    Yii::app()->user->setFlash('deals-form', "Successfully Updated!");
                                    $this->redirect(array('view','id'=>$model->dealid));
                            }
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));                
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Deals;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Deals']))
		{
			$model->attributes=$_POST['Deals'];
			if($model->save()) {
                                Yii::app()->user->setFlash('deals-form', "Successfully Inserted!");
				$this->redirect(array('view','id'=>$model->dealid));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
//	public function actionUpdate($id)
//	{
//		$model=$this->loadModel($id);
//
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//
//		if(isset($_POST['Deals']))
//		{
//			$model->attributes=$_POST['Deals'];
//			if($model->save())
//				$this->redirect(array('view','id'=>$model->dealid));
//		}
//
//		$this->render('update',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		$dataProvider=new CActiveDataProvider('Deals');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
                $model=new Deals('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Deals']))
			$model->attributes=$_GET['Deals'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
//	public function actionAdmin()
//	{
//		$model=new Deals('search');
//		$model->unsetAttributes();  // clear any default values
//		if(isset($_GET['Deals']))
//			$model->attributes=$_GET['Deals'];
//
//		$this->render('admin',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Deals the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Deals::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Deals $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='deals-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
       public function actionPending()
	{
               $criteria=new CDbCriteria;
               $criteria->condition= "dealstatusid='0'";
               $criteria->order = 'dealid DESC';
               $dataProvider=new CActiveDataProvider('Deals',array('criteria'=>$criteria ,'pagination'=>array('pageSize'=>Yii::app()->params->pageSize)));

		$this->render('pending',array(
			'dataProvider'=>$dataProvider,                                              
		));
	}
               
        public function actionExpired()
	{
               $criteria=new CDbCriteria;
               $criteria->condition= "dealstatusid='3'";
               $criteria->order = 'dealid DESC';
               $dataProvider=new CActiveDataProvider('Deals',array('criteria'=>$criteria ,'pagination'=>array('pageSize'=>Yii::app()->params->pageSize)));

		$this->render('expired',array(
			'dataProvider'=>$dataProvider,                                              
		));
	}
               
        public function actionApproved()
	{
               $criteria=new CDbCriteria;
               $criteria->condition= "dealstatusid='1'";
               $criteria->order = 'dealid DESC';
               $dataProvider=new CActiveDataProvider('Deals',array('criteria'=>$criteria ,'pagination'=>array('pageSize'=>Yii::app()->params->pageSize)));

		$this->render('approved',array(
			'dataProvider'=>$dataProvider,
                       
		));
	}
        
        public function actionDisapproved()
	{                           
               $criteria=new CDbCriteria;
               $criteria->condition= "dealstatusid='4'";
               $criteria->order = 'dealid DESC';
               $dataProvider=new CActiveDataProvider('Deals',array('criteria'=>$criteria ,'pagination'=>array('pageSize'=>Yii::app()->params->pageSize)));

		$this->render('disapproved',array(
			'dataProvider'=>$dataProvider,                       
		));
	}
        
        
        public function actionFullfilled()
	{              
               $criteria=new CDbCriteria;
               $criteria->condition= "dealstatusid='2' OR dealstatusid='5'";
               $criteria->order = 'dealid DESC';
               $dataProvider=new CActiveDataProvider('Deals',array('criteria'=>$criteria ,'pagination'=>array('pageSize'=>Yii::app()->params->pageSize)));

		$this->render('fullfilled',array(
			'dataProvider'=>$dataProvider,
                       
		));
	}
        
        public function actionApprovedByAdmin($dealId) {
                                       
            $model = $this->loadModel($dealId);
            $isLast = $model->isLastDeal();
        
            //Capture the cost per this deal
            $captureInfo['COMPLETETYPE'] = $isLast ? 'complete' : 'NotComplete';//when complete paypal do voide automatically
            $captureInfo['AUTHORIZATIONID'] = $model->order->payment->transactionid;
            $captureInfo['AMOUNT'] = $model->cost;
            $paymentResult = Yii::app()->paypal->DoCapture($captureInfo);
            //CVarDumper::dump($paymentResult, 1000, true);
            //Detect payment errors
            if (!Yii::app()->paypal->isCallSucceeded($paymentResult)) {
                if (Yii::app()->paypal->apiLive === true) {
                    //Live mode basic error message
                    $error = 'We were unable to process your request. Please try again later (live mode)';
                } else {
                    //Sandbox output the actual error message to dive in.
                    $error = $paymentResult['L_LONGMESSAGE0'];
                }
                $model->order->payment->msg = $error;
                $model->order->payment->save();                
                Yii::app()->user->setFlash('deals-error', $error);
                $this->redirect(array('view','id'=>$dealId));
                //Yii::app()->end();
            } else {
                //capture payment was completed successfully 
                $model->order->payment->captured += $paymentResult['AMT']; //same PARENTTRANSACTIONID
                $model->order->payment->msg = $paymentResult['PAYMENTSTATUS'];
                $model->order->payment->status = $isLast ? 3 : 2;
                $model->order->payment->save();
                if($isLast){
                    $model->order->status = 1;
                    $model->order->save();
                }
            }
             $model->dealstatusid = 1;
             $model->dealapproveddate = new CDbExpression('NOW()'); // "YYYY-MM-DD HH:MM:SS"
      	
            if($model->save(false)) {
                //send an email to the agent
                //$from='info@gamechangerworldwide.com';
                $subject = 'Notification For Deal Approval';
                $content='Hi '.$model->agentName.',<br/>';
                $content.='The deal placed on '.$model->athleteName.' has been approved by the super admin. <a href="'.str_replace("endors_admin/","deals/approved",Yii::app()->createAbsoluteUrl('')).'">Click here</a> to view your accepted deals.<br/>(Reference : Order ID = '.$model->orderid.')';   
                //$fromname='gamechanger world wide';
                //$mail=['to'=>$agent_mail,'from'=>$from,'subject'=>$subject,'content'=>$content,'fromname'=>$fromname,'toname'=>$agent_name];
                //self::swiftMailer($mail);
                self::sendMail($model->user->email, $subject, $content);
                                                          
                $this->redirect(array('view','id'=>$dealId));            
            }
                        
        }
        
        public function actionDisapprovedByAdmin($dealId) {
                                       
            $model = $this->loadModel($dealId);
        
            //voide if the last deal
            if ($model->isLastDeal()) {
                $paymentResult = Yii::app()->paypal->DoVoid(array('AUTHORIZATIONID'=> $model->order->payment->transactionid));
                //Detect payment errors
                if (!Yii::app()->paypal->isCallSucceeded($paymentResult)) {
                    if (Yii::app()->paypal->apiLive === true) {
                        //Live mode basic error message
                        $error = 'We were unable to process your request. Please try again later (live mode)';
                    } else {
                        //Sandbox output the actual error message to dive in.
                        $error = $paymentResult['L_LONGMESSAGE0'];
                    }
                    //echo $error;
                    $model->order->payment->msg = $error;
                    $model->order->payment->save();
                    Yii::app()->user->setFlash('deals-error', $error);
                    $this->redirect(array('view','id'=>$dealId));
                    //Yii::app()->end();
                } else {
                    //voide captured payment completed successfully 
                    //$model->order->payment->msg = $paymentResult['MSGSUBID'];
                    $model->order->payment->status = 3;
                    $model->order->payment->save();
                    
                    $model->order->status = 1;
                    $model->order->save();
                }
            }
            $model->dealstatusid = 4;
                   	
            if($model->save(false)) {
                 
                //$from='info@gamechangerworldwide.com';
                $subject='Notification For Disapproval';
                $content='Hi '.$model->agentName.',<br/>';
                $content.='The deal placed on '.$model->athleteName.' has been disapproved by the super admin. <a href="'.str_replace("endors_admin/","deals/disapproved",Yii::app()->createAbsoluteUrl('')).'">Click here</a> to view your disapproved deals.<br/>(Reference : Order ID = '.$model->orderid.')';
                //$fromname='gamechanger world wide';
                //$mail=['to'=>$agent_mail,'from'=>$from,'subject'=>$subject,'content'=>$content,'fromname'=>$fromname,'toname'=>$agent_name];
                //self::swiftMailer($mail);
                self::sendMail($model->user->email, $subject, $content);
                                                          
                $this->redirect(array('view','id'=>$dealId));            
            }
                        
        }
               
                
        public function actionFullfilledByAdmin() {
             
             $model = $this->loadModel($_GET[0]['DealId']);
             $model->dealstatusid = 2;
             $model->dealfullfilleddate = new CDbExpression('NOW()'); // "YYYY-MM-DD HH:MM:SS"
            // $model->dateofpaymenttoagent = new CDbExpression('NOW()'); // "YYYY-MM-DD HH:MM:SS"
             if($model->save(false)) {
                $this->redirect(array('view','id'=>$_GET[0]['DealId']));
            }
        }
        
        public function actionPaidToAgent() {
                           
             $model = $this->loadModel($_GET[0]['DealId']);
          
             $model->dateofpaymenttoagent = new CDbExpression('NOW()'); // "YYYY-MM-DD HH:MM:SS"
             $model->dealstatusid = 5;
             $model->fullfill_paidtoagent=1;
             $model->agentshare=($model->cost)*($model->agentsharepercentage/100);
             if($model->save(false)) {
                $this->redirect(array('view','id'=>$_GET[0]['DealId']));
            }
        }
        
}
