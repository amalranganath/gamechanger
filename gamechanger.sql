-- phpMyAdmin SQL Dump
-- version 4.2.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2015 at 03:54 PM
-- Server version: 5.5.44-0ubuntu0.14.10.1
-- PHP Version: 5.5.12-2ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gamechanger`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`adminid` int(11) NOT NULL,
  `adminname` varchar(10) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE IF NOT EXISTS `agent` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `addressline1` varchar(255) NOT NULL,
  `addressline2` varchar(255) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `agentshare` double DEFAULT NULL COMMENT 'this value is a %',
  `totalmarketreach` double NOT NULL,
  `totalengagement` double NOT NULL,
  `completed` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `agent`
--

INSERT INTO `agent` (`id`, `user_id`, `firstname`, `lastname`, `addressline1`, `addressline2`, `city`, `state`, `zipcode`, `agentshare`, `totalmarketreach`, `totalengagement`, `completed`) VALUES
(18, 3, 'suranga', 'ramanayake', 'Temple lane', 'Colombo 10', 'Colombo', 'sri lanka', '85488', 50, 50, 32, 1),
(19, 6, 'Thanuja', 'Madana', 'line 1', 'line 2', 'Kurunagala', 'Colomboo', '025', 19, 56, 44, 1),
(20, 8, '', '', '', '', '', '', '', NULL, 0, 0, 0),
(21, 11, 'Naduni', 'Jayasuriya', 'Pollwatta road', 'Poramba', 'Ambalangoda', 'Galle', '012', 20, 50, 85, 1);

-- --------------------------------------------------------

--
-- Table structure for table `athletes`
--

CREATE TABLE IF NOT EXISTS `athletes` (
`athleteid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `sportscategoryid` int(11) NOT NULL,
  `team` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `sex` varchar(6) NOT NULL,
  `costpertweet` varchar(25) NOT NULL,
  `costpertweetimg` double NOT NULL,
  `costperinstagram` varchar(25) NOT NULL,
  `costperinstagramimg` double NOT NULL,
  `imageid` int(11) NOT NULL,
  `updateddate` date NOT NULL,
  `score` float NOT NULL,
  `reach` float NOT NULL,
  `audience` float NOT NULL,
  `trust` float NOT NULL,
  `engagement` float NOT NULL,
  `marketplace` float NOT NULL,
  `reach_followers_active` varchar(25) NOT NULL,
  `reach_followers_inactive` varchar(25) NOT NULL,
  `reach_followers_fake` varchar(25) NOT NULL,
  `reach_viralpotential` varchar(25) NOT NULL,
  `reach_followers_reach` double NOT NULL,
  `audience_gender_male` varchar(25) NOT NULL,
  `audience_gender_female` varchar(25) NOT NULL,
  `audience_followers_interests` varchar(25) NOT NULL,
  `audience_quality` varchar(25) NOT NULL,
  `audience_extendedreach` varchar(25) NOT NULL,
  `trust_sentiment_positive` varchar(25) NOT NULL,
  `trust_sentiment_neutral` varchar(25) NOT NULL,
  `trust_sentiment_negative` varchar(25) NOT NULL,
  `trust_klout` varchar(25) NOT NULL,
  `engagement_engagement_projectedengagementrate` varchar(25) NOT NULL,
  `engagement_engagement_projectedengagement` varchar(10) NOT NULL,
  `engagment_engagementrating` varchar(10) NOT NULL,
  `engagement_twitteractivity_averageretweetsperpost` varchar(10) NOT NULL,
  `engagement_engagement_twitteractivity_averagefavoritesperpost` varchar(10) NOT NULL,
  `marketplace_activity_profileviews` varchar(10) NOT NULL,
  `marketplace_activity_completeddeals` varchar(10) NOT NULL,
  `marketplace_acceptancerate` varchar(10) NOT NULL,
  `marketplace_response_averageagentresponsetime` varchar(10) NOT NULL,
  `marketplace_response_averageinfluencerresponsetime` varchar(10) NOT NULL,
  `marketplace_costperengagement` varchar(10) NOT NULL,
  `marketplace_costperthousands` varchar(10) NOT NULL,
  `marketplace_opportunityvalue` varchar(10) NOT NULL,
  `trending` tinyint(4) NOT NULL,
  `lastupdatedadminid` int(11) NOT NULL,
  `lastadminupdatedtime` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `athletes`
--

INSERT INTO `athletes` (`athleteid`, `userid`, `firstname`, `lastname`, `sportscategoryid`, `team`, `dob`, `sex`, `costpertweet`, `costpertweetimg`, `costperinstagram`, `costperinstagramimg`, `imageid`, `updateddate`, `score`, `reach`, `audience`, `trust`, `engagement`, `marketplace`, `reach_followers_active`, `reach_followers_inactive`, `reach_followers_fake`, `reach_viralpotential`, `reach_followers_reach`, `audience_gender_male`, `audience_gender_female`, `audience_followers_interests`, `audience_quality`, `audience_extendedreach`, `trust_sentiment_positive`, `trust_sentiment_neutral`, `trust_sentiment_negative`, `trust_klout`, `engagement_engagement_projectedengagementrate`, `engagement_engagement_projectedengagement`, `engagment_engagementrating`, `engagement_twitteractivity_averageretweetsperpost`, `engagement_engagement_twitteractivity_averagefavoritesperpost`, `marketplace_activity_profileviews`, `marketplace_activity_completeddeals`, `marketplace_acceptancerate`, `marketplace_response_averageagentresponsetime`, `marketplace_response_averageinfluencerresponsetime`, `marketplace_costperengagement`, `marketplace_costperthousands`, `marketplace_opportunityvalue`, `trending`, `lastupdatedadminid`, `lastadminupdatedtime`, `created_at`) VALUES
(50, 3, 'Paul', 'Gallen', 1, 'Cronulla Sharks', '1981-08-14', '1', '750', 0, '950', 0, 144, '2015-07-02', 80, 60, 82, 85, 60, 50, '60', '10', '2', '50', 47, '60', '20', 'State of Origin, Surfing,', '57', '83', '90', '5', '0', '85.5', '25', '50', '74', '40', '65', '67', '10', '90', '24', '5', '12', '20', '50', 1, 1, '2015-07-06 17:55:18', '2015-05-29 04:29:22'),
(51, 6, 'Adam', 'Ashley-Cooper', 2, 'NSW Waratahs', '1984-03-27', '1', '500', 0, '1000', 0, 151, '2015-05-29', 1000, 50.5, 85, 90, 52, 26, '50', '40', '10', '25', 79, '12', '19', 'Waratahs Rugby', '53', '80', '63', '25', '45.5', '52.4', '10', '49', '63', '25', '98', '32', '10', '23.5', '18', '16', '93', '28', '27', 1, 1, '2015-07-06 17:35:50', '2015-05-29 07:05:44'),
(52, 6, 'Serena', 'Williams', 5, 'ABC', '1960-10-10', '2', '500', 0, '1000', 0, 156, '0000-00-00', 0, 30, 0, 0, 0, 0, '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '52', '20', '', 1, 1, '2015-07-06 17:27:53', '2015-05-29 07:35:50'),
(53, 3, 'Daly Cherry', 'Evans', 1, 'Manly Sea Eagles', '1989-01-20', '1', '1000', 20, '1300', 30, 180, '2015-07-02', 0, 60, 80, 76, 75, 75, '70', '10', '2', '70', 20, '62', '23', 'Gold Coast Titans, Manly ', '65', '40', '80', '20', '0', '75', '54', '37', '70', '122', '155', '56', '8', '100', '20', '4', '20', '30', '75', 1, 1, '2015-07-06 17:52:27', '2015-06-01 08:30:40'),
(54, 3, 'Lance (buddy)', 'Franklin', 13, 'Sydney Swans', '1987-01-30', '1', '300', 0, '100', 0, 208, '2015-07-02', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, 1, '2015-07-06 17:18:45', '2015-06-09 11:40:15'),
(55, 3, 'SALLY ', 'PEARSON', 12, 'WWE', '1988-04-05', '1', '25911', 0, '100', 0, 212, '2015-07-02', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '0000-00-00 00:00:00', '2015-06-23 09:46:39'),
(56, 11, 'Sally', 'Pearson', 10, 'Hurdles', '1986-09-19', '2', '1500', 100, '300', 10, 228, '2015-08-28', 100, 10, 24, 14, 42, 30, '12', '45', '45', '.35', 87, '40', '535', 'Watching', '35', '24', '35', '74', '12', '15', '23', '27', '15', '45', '12', '50', '30', '40', '45', '12', '46', '35', '24', 1, 1, '2015-06-26 16:52:21', '2015-06-26 10:39:55'),
(57, 11, 'Stephene', 'Gilmore', 4, 'Surf Best', '1988-01-29', '2', '450', 20, '100', 10, 233, '2015-08-28', 1000, 10, 20, 60, 74, 10, '20.5', '50', '80', '40.2', 75, '60', '23.5', 'Washington', '40', '45', '20', '31', '32', '52', '41', '52', '63', '97', '20', '15', '46', '52.3', '14', '15', '78', '72', '56', 1, 1, '2015-06-29 14:36:14', '2015-06-29 05:27:27'),
(58, 11, 'Quade', 'Cooper', 2, 'Rugby Union', '1988-04-05', '1', '300', 20, '250', 10, 237, '2015-08-28', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '0000-00-00 00:00:00', '2015-07-02 06:37:50'),
(59, 11, 'Greg ', 'Inglis', 1, 'South Sydney Rabbitohs', '1987-01-15', '1', '550', 30, '650', 40, 240, '2015-08-28', 0, 76, 68, 66, 45, 78, '50', '10', '2', '30', 0, '60', '34', 'State of Origin, Queensla', '55', '20', '60', '55', '3', '67', '80', '30', '45', '78', '90', '45', '5', '100', '48', '4', '20', '4.1', '', 1, 1, '2015-07-06 17:59:28', '2015-07-06 11:50:05'),
(60, 11, 'Mitchell', 'Pearce', 1, 'Sydney Roosters', '1989-04-07', '1', '300', 20, '650', 30, 251, '2015-08-28', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '0000-00-00 00:00:00', '2015-07-31 13:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `brandname` varchar(100) NOT NULL,
  `imageid` int(11) NOT NULL,
  `siteurl` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `completed` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `user_id`, `firstname`, `lastname`, `brandname`, `imageid`, `siteurl`, `description`, `completed`) VALUES
(28, 2, 'saranga', 'ranaweea', 'ABANS', 141, 'http://www.abans.lk', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ', 1),
(29, 5, 'Naduni', 'Nayanthara', 'Bag Sporty', 145, 'https://www.google.lk/', 'Hi, this is my first brand. ', 1),
(30, 7, 'gayans', 'sanjeewas', 'Zearhy1s', 189, 'http://www.zearagroup.com1s', 'dasdsadas', 1),
(31, 9, 'saranga', 'ranaweera', 'dfsdf', 211, 'http://www.gfgff.com', 'sdfsdfsdf', 1),
(32, 10, 'gayans', 'sanjeewas', 'Zearhy1s', 189, 'http://www.zearagroup.com1s', 'dasdsadas', 1),
(33, 12, 'Thanuja', 'Maadana', 'NewSporty', 220, 'https://www.google.lk/', 'This is my first experience', 1),
(34, 13, 'Lakmini', 'Uthpala', 'NewFortune', 224, 'https://www.google.lk/', 'This is my second brand', 1),
(35, 16, 'Gen', 'Griffin', 'Tester', 249, 'http://www.eliteathleticpro.com', 'compression wear', 1),
(36, 18, '', '', '', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `deals`
--

CREATE TABLE IF NOT EXISTS `deals` (
`dealid` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `dealstatusid` int(11) NOT NULL,
  `athleteid` int(11) NOT NULL,
  `dealtype` tinyint(2) NOT NULL COMMENT '0-Twitter 1-Intagram',
  `costpertweet` double NOT NULL,
  `costperinstagram` double NOT NULL,
  `costpertweetimg` double NOT NULL,
  `costperinstagramimg` double NOT NULL,
  `cost` double NOT NULL,
  `dealrequesteddate` date NOT NULL,
  `imageid` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `campaigndate` date NOT NULL,
  `campaigntime` time NOT NULL,
  `ampm` varchar(2) NOT NULL,
  `dealapproveddate` datetime NOT NULL,
  `dealfullfilleddate` datetime NOT NULL,
  `dealaccpeteddate` datetime NOT NULL,
  `dateofpaymentbybrand` datetime NOT NULL,
  `dateofpaymenttoagent` datetime NOT NULL,
  `agentid` int(11) NOT NULL,
  `agentsharepercentage` decimal(11,0) NOT NULL,
  `agentshare` float NOT NULL,
  `fulfill_engagement` float NOT NULL,
  `fulfill_reach` float NOT NULL,
  `actual_tweet` int(11) NOT NULL,
  `fullfill_paidtoagent` int(1) NOT NULL COMMENT '1-Paid'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `deals`
--

INSERT INTO `deals` (`dealid`, `orderid`, `dealstatusid`, `athleteid`, `dealtype`, `costpertweet`, `costperinstagram`, `costpertweetimg`, `costperinstagramimg`, `cost`, `dealrequesteddate`, `imageid`, `description`, `campaigndate`, `campaigntime`, `ampm`, `dealapproveddate`, `dealfullfilleddate`, `dealaccpeteddate`, `dateofpaymentbybrand`, `dateofpaymenttoagent`, `agentid`, `agentsharepercentage`, `agentshare`, `fulfill_engagement`, `fulfill_reach`, `actual_tweet`, `fullfill_paidtoagent`) VALUES
(1, 1, 3, 58, 1, 0, 250, 0, 10, 260, '2015-08-28', 264, 'I like the athlete to say ', '2015-09-30', '08:30:00', 'AM', '2015-08-31 10:48:56', '2015-09-02 15:06:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 20, 0, 0, 0, 0, 0),
(2, 1, 0, 56, 0, 1500, 0, 100, 0, 1600, '2015-08-28', 265, 'Sally Pearson', '2015-09-30', '02:00:00', 'PM', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 20, 0, 0, 0, 0, 0),
(3, 2, 2, 57, 0, 450, 0, 20, 0, 470, '2015-09-01', 266, 'Stephene Gilmore', '2015-08-31', '03:00:00', 'PM', '2015-09-01 12:54:48', '2015-09-02 15:11:29', '0000-00-00 00:00:00', '2015-09-01 07:24:00', '0000-00-00 00:00:00', 11, 20, 0, 0, 0, 0, 0),
(4, 3, 1, 51, 0, 500, 0, 0, 0, 500, '2015-09-01', 267, 'Adam', '2015-09-30', '12:00:00', 'PM', '2015-09-01 17:52:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-09-01 10:55:08', '0000-00-00 00:00:00', 6, 19, 0, 0, 0, 0, 0),
(5, 3, 1, 60, 1, 0, 650, 0, 30, 680, '2015-09-01', 268, 'cost', '2015-09-30', '04:40:00', 'PM', '2015-09-01 16:26:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-09-01 10:55:08', '0000-00-00 00:00:00', 11, 20, 0, 0, 0, 0, 0),
(6, 4, 1, 53, 0, 1000, 0, 20, 0, 1020, '2015-09-01', 269, 'af af', '2015-09-30', '08:00:00', 'PM', '2015-09-01 18:05:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-09-01 12:32:45', '0000-00-00 00:00:00', 3, 50, 0, 0, 0, 0, 0),
(7, 4, 1, 54, 1, 0, 100, 0, 0, 100, '2015-09-01', 0, 'zsg srete', '2015-09-30', '03:00:00', 'PM', '2015-09-01 18:07:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-09-01 12:32:45', '0000-00-00 00:00:00', 3, 50, 0, 0, 0, 0, 0),
(8, 5, 4, 58, 1, 0, 250, 0, 0, 260, '2015-09-01', 0, 'Testing disapproval', '2015-10-29', '02:00:00', 'PM', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-09-01 12:44:48', '0000-00-00 00:00:00', 11, 20, 0, 0, 0, 0, 0),
(9, 5, 1, 59, 0, 550, 0, 30, 0, 580, '2015-09-01', 270, 'Testing approval', '2015-09-30', '08:00:00', 'AM', '2015-09-01 18:15:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-09-01 12:44:48', '0000-00-00 00:00:00', 11, 20, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dealstatus`
--

CREATE TABLE IF NOT EXISTS `dealstatus` (
  `dealstatusid` tinyint(1) NOT NULL,
  `dealstatus` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealstatus`
--

INSERT INTO `dealstatus` (`dealstatusid`, `dealstatus`) VALUES
(0, 'Pending'),
(1, 'Approved'),
(2, 'Fullfilled'),
(3, 'Closed'),
(4, 'Disapproved');

-- --------------------------------------------------------

--
-- Table structure for table `dealstemp`
--

CREATE TABLE IF NOT EXISTS `dealstemp` (
`dealid` int(11) NOT NULL,
  `athleteid` int(11) NOT NULL,
  `agentid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `imageid` int(11) NOT NULL,
  `dealtype` tinyint(4) DEFAULT NULL COMMENT 'tweet=0 or instagram=1',
  `costpertweet` double NOT NULL,
  `costperinstagram` double NOT NULL,
  `costpertweetimg` double NOT NULL,
  `costperinstagramimg` double NOT NULL,
  `description` text NOT NULL,
  `campaigndate` date NOT NULL,
  `campaigntime` time NOT NULL,
  `ampm` varchar(2) NOT NULL,
  `completed` tinyint(4) NOT NULL,
  `placetoorder` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `dealstemp`
--

INSERT INTO `dealstemp` (`dealid`, `athleteid`, `agentid`, `userid`, `imageid`, `dealtype`, `costpertweet`, `costperinstagram`, `costpertweetimg`, `costperinstagramimg`, `description`, `campaigndate`, `campaigntime`, `ampm`, `completed`, `placetoorder`) VALUES
(1, 58, 11, 12, 264, 1, 0, 250, 0, 10, 'I like the athlete to say ', '2015-09-30', '08:30:00', 'AM', 1, 1),
(2, 56, 11, 12, 265, 0, 1500, 0, 100, 0, 'Sally Pearson', '2015-09-30', '02:00:00', 'PM', 1, 1),
(3, 57, 11, 12, 266, 0, 450, 0, 20, 0, 'Stephene Gilmore', '2015-08-31', '03:00:00', 'PM', 1, 1),
(4, 51, 6, 12, 267, 0, 500, 0, 0, 0, 'Adam', '2015-09-30', '12:00:00', 'PM', 1, 1),
(5, 60, 11, 12, 268, 1, 0, 650, 0, 30, 'cost', '2015-09-30', '04:40:00', 'PM', 1, 1),
(6, 53, 3, 12, 269, 0, 1000, 0, 20, 0, 'af af', '2015-09-30', '08:00:00', 'PM', 1, 1),
(7, 54, 3, 12, 0, 1, 0, 100, 0, 0, 'zsg srete', '2015-09-30', '03:00:00', 'PM', 1, 1),
(10, 58, 11, 12, 0, 1, 0, 250, 0, 0, 'Testing disapproval', '2015-10-29', '02:00:00', 'PM', 1, 1),
(11, 59, 11, 12, 270, 0, 550, 0, 30, 0, 'Testing approval', '2015-09-30', '08:00:00', 'AM', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lookupimages`
--

CREATE TABLE IF NOT EXISTS `lookupimages` (
`imageid` int(11) NOT NULL,
  `imagename` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=271 ;

--
-- Dumping data for table `lookupimages`
--

INSERT INTO `lookupimages` (`imageid`, `imagename`, `type`, `created_at`) VALUES
(141, '7951432871929.jpg', '', '2015-05-29 03:58:49'),
(144, '39101432873762.jpg', '', '2015-05-29 04:29:22'),
(145, '33271432879358.jpg', '', '2015-05-29 06:02:38'),
(146, '1432882509.png', '', '2015-05-29 06:55:09'),
(151, '93091432883144.jpg', '', '2015-05-29 07:05:44'),
(152, '1432883431.jpg', '', '2015-05-29 07:10:31'),
(156, '91641432884950.jpg', '', '2015-05-29 07:35:50'),
(158, '1432886881.jpg', '', '2015-05-29 08:08:01'),
(159, '1432894864.jpg', '', '2015-05-29 10:21:04'),
(160, '1432897114.jpg', '', '2015-05-29 10:58:34'),
(161, '1433136035.jpg', '', '2015-06-01 05:20:35'),
(162, '1433136434.jpg', '', '2015-06-01 05:27:14'),
(163, '1433136736.jpg', '', '2015-06-01 05:32:16'),
(164, '1433136980.jpg', '', '2015-06-01 05:36:20'),
(165, '1433137123.jpg', '', '2015-06-01 05:38:43'),
(166, '1433137333.jpg', '', '2015-06-01 05:42:13'),
(167, '1433138380.png', '', '2015-06-01 05:59:40'),
(168, '1433139431.jpg', '', '2015-06-01 06:17:11'),
(169, '1433139778.jpg', '', '2015-06-01 06:22:58'),
(170, '1433139812.jpg', '', '2015-06-01 06:23:32'),
(171, '1433140011.jpg', '', '2015-06-01 06:26:51'),
(172, '1433140057.jpg', '', '2015-06-01 06:27:37'),
(173, '1433140758.jpg', '', '2015-06-01 06:39:18'),
(174, '1433140795.jpg', '', '2015-06-01 06:39:55'),
(175, '1433140969.jpg', '', '2015-06-01 06:42:49'),
(176, '1433140997.jpg', '', '2015-06-01 06:43:17'),
(177, '1433144354.png', '', '2015-06-01 07:39:14'),
(178, '1433144637.jpg', '', '2015-06-01 07:43:57'),
(179, '', '', '2015-06-01 07:52:29'),
(180, '9671433147440.jpg', '', '2015-06-01 08:30:40'),
(181, '1433152805.jpg', '', '2015-06-01 10:00:05'),
(182, '1433309802.png', '', '2015-06-03 05:36:42'),
(183, '', '', '2015-06-03 08:44:35'),
(184, '', '', '2015-06-03 08:44:53'),
(185, '', '', '2015-06-03 08:45:40'),
(186, '8641433321152.jpg', '', '2015-06-03 08:45:52'),
(187, '', '', '2015-06-03 08:46:01'),
(188, '', '', '2015-06-03 08:48:24'),
(189, '8641433321152.jpg', '', '2015-06-03 08:48:36'),
(190, '1433474845.jpg', '', '2015-06-05 03:27:25'),
(191, '1433474881.png', '', '2015-06-05 03:28:01'),
(192, '', '', '2015-06-09 04:08:13'),
(193, '', '', '2015-06-09 04:13:30'),
(194, '', '', '2015-06-09 04:13:59'),
(195, '', '', '2015-06-09 04:14:21'),
(196, '', '', '2015-06-09 04:14:49'),
(197, '', '', '2015-06-09 04:15:15'),
(198, '', '', '2015-06-09 04:15:58'),
(199, '', '', '2015-06-09 04:16:35'),
(200, '', '', '2015-06-09 04:18:29'),
(201, '', '', '2015-06-09 04:18:45'),
(202, '', '', '2015-06-09 04:20:54'),
(203, '', '', '2015-06-09 04:21:40'),
(204, '', '', '2015-06-09 04:22:29'),
(205, '', '', '2015-06-09 04:24:00'),
(206, '', '', '2015-06-09 11:39:25'),
(207, '', '', '2015-06-09 11:40:04'),
(208, '34431433850016.jpg', '', '2015-06-09 11:40:15'),
(209, '', '', '2015-06-11 10:29:19'),
(210, '', '', '2015-06-11 10:29:28'),
(211, '63601434018575.jpg', '', '2015-06-11 10:29:36'),
(212, '89901435052799.jpg', '', '2015-06-23 09:46:39'),
(213, '1435216847.jpg', '', '2015-06-25 07:20:47'),
(214, '1435216903.jpg', '', '2015-06-25 07:21:43'),
(215, '1435229992.jpg', '', '2015-06-25 10:59:52'),
(216, '', '', '2015-06-26 07:48:52'),
(217, '', '', '2015-06-26 08:28:06'),
(218, '', '', '2015-06-26 08:35:40'),
(219, '', '', '2015-06-26 08:36:18'),
(220, '77961435308092.jpg', '', '2015-06-26 08:41:32'),
(221, '', '', '2015-06-26 09:01:23'),
(222, '', '', '2015-06-26 09:26:35'),
(223, '', '', '2015-06-26 09:32:43'),
(224, '71001435311188.jpg', '', '2015-06-26 09:33:08'),
(225, '', '', '2015-06-26 09:56:53'),
(226, '', '', '2015-06-26 10:05:18'),
(227, '', '', '2015-06-26 10:39:15'),
(228, '5981435315196.jpg', '', '2015-06-26 10:39:56'),
(229, '1435320280.jpg', '', '2015-06-26 12:04:40'),
(230, '1435551996.jpg', '', '2015-06-29 04:26:36'),
(231, '', '', '2015-06-29 05:23:42'),
(232, '', '', '2015-06-29 05:26:53'),
(233, '63061435555647.jpg', '', '2015-06-29 05:27:27'),
(234, '1435566055.jpg', '', '2015-06-29 08:20:55'),
(235, '1435569630.jpg', '', '2015-06-29 09:20:30'),
(236, '1435643066.jpg', '', '2015-06-30 05:44:26'),
(237, '25601435819070.jpg', '', '2015-07-02 06:37:50'),
(238, '1435820320.jpg', '', '2015-07-02 06:58:40'),
(239, '1435829349.jpg', '', '2015-07-02 09:29:09'),
(240, '79041436183405.jpg', '', '2015-07-06 11:50:05'),
(241, '1436231367.jpg', '', '2015-07-07 01:09:27'),
(242, '1436231504.jpg', '', '2015-07-07 01:11:44'),
(243, '', '', '2015-07-29 00:49:55'),
(244, '', '', '2015-07-29 00:50:06'),
(245, '', '', '2015-07-29 00:52:41'),
(246, '', '', '2015-07-29 00:52:47'),
(247, '', '', '2015-07-29 00:53:46'),
(248, '', '', '2015-07-29 00:53:49'),
(249, '8321438131318.jpg', '', '2015-07-29 00:55:18'),
(250, '1438133417.jpg', '', '2015-07-29 01:30:17'),
(251, '16251438350199.jpg', '', '2015-07-31 13:43:19'),
(252, '1440387360.jpg', '', '2015-08-24 03:36:00'),
(253, '1440562618.jpg', '', '2015-08-26 04:16:58'),
(254, '', '', '2015-08-26 08:59:53'),
(255, '', '', '2015-08-26 09:00:03'),
(256, '', '', '2015-08-26 09:00:14'),
(257, '', '', '2015-08-26 09:00:21'),
(258, '', '', '2015-08-26 09:00:27'),
(259, '', '', '2015-08-26 09:00:49'),
(260, '', '', '2015-08-26 10:44:39'),
(261, '', '', '2015-08-26 10:45:18'),
(262, '1440589108.jpg', '', '2015-08-26 11:38:28'),
(263, '1440671083.jpg', '', '2015-08-27 10:24:43'),
(264, '1440737488.jpg', '', '2015-08-28 04:51:28'),
(265, '1440737552.jpg', '', '2015-08-28 04:52:32'),
(266, '1440750994.jpg', '', '2015-08-28 08:36:34'),
(267, '1441102457.jpg', '', '2015-09-01 10:14:17'),
(268, '1441102458.jpg', '', '2015-09-01 10:14:18'),
(269, '1441110671.jpg', '', '2015-09-01 12:31:11'),
(270, '1441111439.jpg', '', '2015-09-01 12:43:59');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`orderid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `paymentid` int(11) DEFAULT NULL,
  `orderdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderid`, `userid`, `paymentid`, `orderdate`) VALUES
(1, 12, 1, '2015-08-28 05:00:07'),
(2, 12, 2, '2015-09-01 07:24:00'),
(3, 12, 3, '2015-09-01 10:55:09'),
(4, 12, 4, '2015-09-01 12:32:46'),
(5, 12, 5, '2015-09-01 12:44:49');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
`paymentid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `payerid` varchar(32) NOT NULL,
  `transactionid` varchar(32) DEFAULT NULL,
  `ipaddress` varchar(16) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `captured` double(10,2) DEFAULT NULL,
  `msg` text,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL COMMENT '0 => ''failed'', 1 => ''Authorised'', 2 => ''capturing'', 3 => ''completed'''
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`paymentid`, `userid`, `token`, `payerid`, `transactionid`, `ipaddress`, `amount`, `captured`, `msg`, `time`, `status`) VALUES
(1, 12, 'EC-7FV49121BX6490628', '88K3GEVNC6UXC', '388035547R142003K', '127.0.0.1', 1860.00, NULL, 'Authorization is voided.', '2015-08-27 23:30:07', 2),
(2, 12, 'EC-3NY37467KR8166338', '88K3GEVNC6UXC', '62U807513B3870332', '127.0.0.1', 470.00, 470.00, 'Completed', '2015-09-01 01:54:00', 3),
(3, 12, 'EC-7KP772298J8114453', '88K3GEVNC6UXC', '43H39677J5718760R', '127.0.0.1', 1180.00, 1180.00, 'Completed', '2015-09-01 05:25:08', 3),
(4, 12, 'EC-60580751DL120603W', '88K3GEVNC6UXC', '06934549JV875735Y', '127.0.0.1', 1120.00, 1120.00, 'Completed', '2015-09-01 07:02:45', 3),
(5, 12, 'EC-8BY97877JK364424C', '88K3GEVNC6UXC', '4NG85560U17425012', '127.0.0.1', 830.00, 580.00, 'Completed', '2015-09-01 07:14:48', 3);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`user_id` int(11) NOT NULL,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'admin', 'admin'),
(3, 'saranga', 'ranaweera'),
(4, '', ''),
(5, '', ''),
(6, '', ''),
(7, '', ''),
(8, '', ''),
(9, '', ''),
(10, '', ''),
(11, '', ''),
(12, '', ''),
(13, '', ''),
(14, '', ''),
(15, '', ''),
(16, '', ''),
(17, '', ''),
(18, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `profiles_fields`
--

CREATE TABLE IF NOT EXISTS `profiles_fields` (
`id` int(10) NOT NULL,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profiles_fields`
--

INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sportscatagory`
--

CREATE TABLE IF NOT EXISTS `sportscatagory` (
`sportscatagory_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `sportscatagory`
--

INSERT INTO `sportscatagory` (`sportscatagory_id`, `name`) VALUES
(1, 'Rugby League'),
(2, 'Rugby Union'),
(3, 'Cricket'),
(4, 'Surfing'),
(5, 'Tennis'),
(6, 'Swimmy'),
(7, 'Golf'),
(8, 'Marathon'),
(9, 'Running'),
(10, 'Triathlete'),
(11, 'Iron Man'),
(12, 'Baseball'),
(13, 'AFL');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `usertype` int(11) NOT NULL COMMENT '1=agent , 2=brand'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit_at`, `superuser`, `status`, `usertype`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'thanuja@livingdreams.lk', '9a24eff8c15a6a141ece27eb6947da0f', '2015-05-29 03:46:20', '2015-09-02 10:08:53', 1, 1, 0),
(2, '1432871806', '5b6a47034354b80436637160d0509a30', 'saranga.ranaweera@gmail.com1', '0fa5a0bbbf2f669a86d603cb0e8f7350', '2015-05-29 03:56:46', '2015-06-09 07:50:49', 0, 1, 2),
(3, '1432871910', '5b6a47034354b80436637160d0509a30', 'saranga.livingdreams@gmail.com', '2dfd30ff2883723988c72024e156ee7c', '2015-05-29 03:58:30', '2015-07-02 09:20:19', 0, 1, 1),
(4, '1432878352', '967746089ed2fc8501334acfe58749bc', 'naduni.livingdreams2@gmail.com', 'f6d21987b9ce61dfc24ae9aecbf2bdcd', '2015-05-29 05:45:52', '0000-00-00 00:00:00', 0, 0, 2),
(5, '1432879002', '967746089ed2fc8501334acfe58749bc', 'nnjayasuriya2@yahoo.com', '6518b6d997309a5bf233709512b60ca6', '2015-05-29 05:56:42', '2015-06-26 03:48:40', 0, 1, 2),
(6, '1432881396', 'd69fe619b4561d045819f0e312ccc0c0', 'ldreams082@yahoo.com', '31e7b43466b00cc5f5ef65c312a52d47', '2015-05-29 06:36:36', '2015-06-03 05:05:46', 0, 1, 1),
(7, '1433315964', 'a63791c9c0a875957baeb7666aa90546', 'gayan.livingdreams@gmail.com', '04d85da9fba6cc9dd364ceae6814bee6', '2015-06-03 07:19:24', '2015-06-03 03:55:02', 0, 1, 2),
(8, '1433851431', 'e67d07ea715221fd254c6fe682aec61d', 'nimesha@livingdreams.lk', 'e355f5ceb715ce0246629a750484bd9d', '2015-06-09 12:03:51', '2015-06-09 08:36:27', 0, 1, 1),
(9, '1433851769', '5b6a47034354b80436637160d0509a30', 'saranga.ranaweera@gmail.com', '8b0fcbc73ab4c64b5c2efe20978f9043', '2015-06-09 12:09:29', '2015-08-04 13:41:43', 0, 1, 2),
(10, '1434101170', '81dc9bdb52d04dc20036dbd8313ed055', 'shanika@livingdreams.lk', '2a306ac571b37ca68416fe297fc5caf9', '2015-06-12 09:26:10', '2015-06-12 06:06:58', 0, 0, 2),
(11, '1435303333', '967746089ed2fc8501334acfe58749bc', 'naduni.livingdreams@gmail.com', '554b55fadf04cc7b68a04cc59c590515', '2015-06-26 07:22:14', '2015-09-02 06:30:04', 0, 1, 1),
(12, '1435304423', '967746089ed2fc8501334acfe58749bc', 'nnjayasuriya@yahoo.com', '554b55fadf04cc7b68a04cc59c590515', '2015-06-26 07:40:23', '2015-09-02 09:43:25', 0, 1, 2),
(13, '1435308570', '0d121357cc80d14659cb481fd901a332', 'lakmini.livingdreams@gmail.com', '54896840752fa1b40991bc566ba8efbd', '2015-06-26 08:49:30', '2015-07-02 09:24:50', 0, 1, 2),
(14, '1435812020', 'e10adc3949ba59abbe56e057f20f883e', 'sasadssa@fgfd.com', 'e18730418e14b829f53e9fd2b45da837', '2015-07-02 04:40:20', '0000-00-00 00:00:00', 0, 0, 2),
(15, '1435812389', 'e10adc3949ba59abbe56e057f20f883e', 'saragfgf@fg.com', 'e9a46243d8af37bfcb5f90ccd86e0821', '2015-07-02 04:46:29', '0000-00-00 00:00:00', 0, 0, 2),
(16, '1438130829', '23d86a36f19c0d13cb2e90e37c229380', 'gen.griffin@gmail.com', '6fcd3ce906bf3e9c04b451fcd494be1a', '2015-07-29 00:47:09', '2015-07-29 00:49:19', 0, 1, 2),
(17, '1438134551', 'd7127773331c68a71b85885a570ca219', 'admin@gamechangerworldwide.com', '73d54ac7577f9c97b3c0a018801ed31d', '2015-07-29 01:49:11', '0000-00-00 00:00:00', 0, 0, 1),
(18, '1438134639', 'd5f9c6b52265f57aa69630b0186fc86e', 'media@gamechangerworldwide.com', '2f2ac910a5d4c5766302abdcae042337', '2015-07-29 01:50:39', '2015-07-29 01:51:32', 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE IF NOT EXISTS `usertype` (
  `usertypeid` int(11) NOT NULL,
  `usertype` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`adminid`);

--
-- Indexes for table `agent`
--
ALTER TABLE `agent`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `athletes`
--
ALTER TABLE `athletes`
 ADD PRIMARY KEY (`athleteid`), ADD KEY `userid` (`userid`), ADD KEY `athleteid` (`athleteid`), ADD KEY `sportscategoryid` (`sportscategoryid`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `deals`
--
ALTER TABLE `deals`
 ADD PRIMARY KEY (`dealid`), ADD KEY `orderid` (`orderid`), ADD KEY `dealstatusid` (`dealstatusid`), ADD KEY `dealstatusid_2` (`dealstatusid`), ADD KEY `dealstatusid_3` (`dealstatusid`);

--
-- Indexes for table `dealstatus`
--
ALTER TABLE `dealstatus`
 ADD PRIMARY KEY (`dealstatusid`), ADD KEY `dealstatusid` (`dealstatusid`), ADD KEY `dealstatusid_2` (`dealstatusid`), ADD KEY `dealstatusid_3` (`dealstatusid`);

--
-- Indexes for table `dealstemp`
--
ALTER TABLE `dealstemp`
 ADD PRIMARY KEY (`dealid`), ADD KEY `athleteid` (`athleteid`), ADD KEY `agentid` (`agentid`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `lookupimages`
--
ALTER TABLE `lookupimages`
 ADD PRIMARY KEY (`imageid`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`orderid`), ADD KEY `orderid` (`orderid`), ADD KEY `paymentid` (`paymentid`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
 ADD PRIMARY KEY (`paymentid`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
 ADD PRIMARY KEY (`id`), ADD KEY `varname` (`varname`,`widget`,`visible`);

--
-- Indexes for table `sportscatagory`
--
ALTER TABLE `sportscatagory`
 ADD PRIMARY KEY (`sportscatagory_id`), ADD KEY `sportscatagory_id` (`sportscatagory_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `username` (`username`), ADD KEY `status` (`status`), ADD KEY `superuser` (`superuser`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
 ADD PRIMARY KEY (`usertypeid`), ADD KEY `usertypeid` (`usertypeid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `adminid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agent`
--
ALTER TABLE `agent`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `athletes`
--
ALTER TABLE `athletes`
MODIFY `athleteid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `deals`
--
ALTER TABLE `deals`
MODIFY `dealid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `dealstemp`
--
ALTER TABLE `dealstemp`
MODIFY `dealid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `lookupimages`
--
ALTER TABLE `lookupimages`
MODIFY `imageid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=271;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
MODIFY `paymentid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sportscatagory`
--
ALTER TABLE `sportscatagory`
MODIFY `sportscatagory_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `agent`
--
ALTER TABLE `agent`
ADD CONSTRAINT `agent_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `athletes`
--
ALTER TABLE `athletes`
ADD CONSTRAINT `athletes_ibfk_1` FOREIGN KEY (`sportscategoryid`) REFERENCES `sportscatagory` (`sportscatagory_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `brand`
--
ALTER TABLE `brand`
ADD CONSTRAINT `brand_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dealstemp`
--
ALTER TABLE `dealstemp`
ADD CONSTRAINT `dealstemp_ibfk_1` FOREIGN KEY (`athleteid`) REFERENCES `athletes` (`athleteid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `dealstemp_ibfk_2` FOREIGN KEY (`agentid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `dealstemp_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`paymentid`) REFERENCES `payment` (`paymentid`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
ADD CONSTRAINT `profile_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
