<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php echo "<?php echo CHtml::link('<i class=\"fa fa-file-o\"></i> Create', Yii::app()->createUrl('".$this->class2id($this->modelClass)."/create'), array('class'=>'btn btn-default')); ?>\n"; ?>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
        <?php
        $count=0;
        foreach($this->tableSchema->columns as $column)
        {
                if(++$count==7)
                        echo "\t\t/*\n";
                echo "\t\t'".$column->name."',\n";
        }
        if($count>=7)
                echo "\t\t*/\n";
        ?>
                        array(
                                'class'=>'CButtonColumn',
                                'template'=>'{view} {delete}',
                        ),
                ),
        )); ?>
    </div>
</div>
<!-- /.row -->