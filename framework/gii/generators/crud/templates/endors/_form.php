<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="form">

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>\n"; ?>

        <div class="row">
            <div class="col-lg-12">
                <?php echo "<?php echo CHtml::link('<i class=\"fa fa-caret-left\"></i> Back', Yii::app()->createUrl('".$this->class2id($this->modelClass)."'), array('class'=>'btn btn-link back')); ?>\n"; ?>
            </div>
	</div>
        <!-- /.row -->
        
        <div class="row">
            <div class="col-lg-12">
                <p class="note">Fields with <span class="required">*</span> are required.</p>
            </div>
        </div>
        <!-- /.row -->
                        
<?php 
echo "
<!-- <span>
<?php
        if(!empty(\$form->errorSummary(\$model))) {
            ?>
            <div class=\"row\">
                <div class=\"form-group\">
                    <div class=\"col-lg-12\">
                        <div class=\"alert alert-danger alert-dismissable\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                            <?php echo \$form->errorSummary(\$model); ?>\n
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
?>
<span> -->
";
?>

<?php
foreach($this->tableSchema->columns as $column)
{
	if($column->autoIncrement)
		continue;
?>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
                </div>
                <div class="col-lg-10">
                    <?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
                    <p class="help-block"><?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>"; ?></p>
                </div>
            </div>
	</div>
        <!-- /.row -->
        
<?php
}
?>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-10">
                    <?php echo "<?php echo CHtml::submitButton('Create', array('class'=>'btn btn-default','data-loading-text'=>'Loading...')); ?>\n"; ?>
                    <?php echo "<?php echo CHtml::resetButton('Reset', array('class'=>'btn btn-default')); ?>\n"; ?>
                </div>
            </div>
	</div>
        <!-- /.row -->

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div><!-- form -->