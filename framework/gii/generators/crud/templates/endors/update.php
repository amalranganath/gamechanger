<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo $this->modelClass." <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>
    </div>
</div>
<!-- /.row -->

<?php echo "<?php \$this->renderPartial('_update', array('model'=>\$model)); ?>"; ?>