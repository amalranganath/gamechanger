<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create <?php echo $this->modelClass; ?></h1>
    </div>
</div>
<!-- /.row -->

<?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
