<?php

class DistanceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','update','loadcity','test','pdf'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','test'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Distance;
                $cities= new Cities;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Distance']))
		{
			$model->attributes=$_POST['Distance'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->DistanceId));
		}

		$this->render('create',array(
			'model'=>$model,
                    'cities'=>$cities
		));
	}

        public function actionPdf() {
            
            $this->renderPartial('printthis', array());
            
        }
        
        public function actionLoadcity() {
                

            
            
            
                $model=new Distance;
                $cities= new Cities;
                
//                echo '<pre>'. print_r($_POST,1).'</pre>';
//                echo '<pre>'. print_r(Distance::model()->find('FromId='.$_POST['FromId'].' AND ToId='.$_POST['FromId'])->DistanceId,1).'</pre>';
            
            if(isset($_POST['FromId'])){
                
                $maincity_id = $_POST['FromId'];
                
                $usercriteria = new CDbCriteria();
                $usercriteria->select="*";
                $usercriteria->condition="FromId=$maincity_id";
                
                $listdata = Distance::model()->findAll($usercriteria);
                 echo '<table  id="sample">';
                 
                
                 $this->renderPartial('test2', array());
                foreach ($listdata as $key => $value) {
                    if($maincity_id!= $value->ToId) {
//                        echo '<pre>'. print_r($value->attributes,1).'</pre>';
//                    echo '<tr>';
                   
//                         $this->renderPartial('test', array('model'=>$model,'cities'=>$cities));
                         $this->renderPartial('test', array('value'=>$value));
                        
                        
//                        echo '<tr >'
//                            . '<td id="disId:'.$value->DistanceId.'">'.$value->DistanceId.'</td>'
//                            . '<td id="disId:'.$value->DistanceId.'">'. Cities::model()->find('CityId='.$value->ToId)->City.'</td>'
//                            . '<td id="disId:'.$value->DistanceId.'" contenteditable="true">'.$value->Distance.'</td>'
//                        . '</tr>';
//                    echo '</tr>';
//                    
                    }
//                    echo '</table>';
                        
                }
                echo '</table>';
//            $this->renderPartial('offer-detail-view', array('offer'=>$offer),true,true);
                //$this->renderPartial('entrypage',array('model'=>$model,'cities'=>$cities),false,true);
                
            }
            
        }
        
        
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
                
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Distance']))
		{
			$model->attributes=$_POST['Distance'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->DistanceId));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Distance');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Distance('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Distance']))
			$model->attributes=$_GET['Distance'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

        
        public function actionTest() {
            //echo '<pre>'.print_r($_POST,1).'</pre>';
            //echo htmlspecialchars($_POST);
            foreach($_POST as $field_name => $val){
                
                $field_userid = strip_tags(trim($field_name));
               // echo $field_name.''.$val;
                
                $val = strip_tags(trim(mysql_real_escape_string($val)));
               
                $split_data = explode(':', $field_userid);
                //print_r ($split_data);
                $distance_id=$split_data[1];
                
                //echo var_dump($_POST['Distance']);
                
                $model=$this->loadModel($distance_id);
                if(!empty($distance_id)){
                    $model->attributes=array('Distance'=>$val);
                    if($model->save())
                        echo 'Done!'; 
                    
                }
               // echo $distance_id;
                //exit();
            }
            
            
            
//            list($name, $id) = explode(':', $_POST[0]);
//            $model=$this->loadModel($_POST['DistanceId']);
//            echo '<pre>'.print_r($name,1).'</pre>';
//            echo '<pre>'.print_r($id,1).'</pre>';
            
            
            //echo 'Hello World!';
            
        }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Distance the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Distance::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Distance $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='distance-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
