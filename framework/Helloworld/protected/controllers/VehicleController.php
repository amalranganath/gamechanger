<?php

class VehicleController extends Controller
{
	/*public function actionIndex()
	{
		$this->render('index');
	}*/

	public function actionVehicle()
{
    $model=new Vehicle;

    // uncomment the following code to enable ajax-based validation
    /*
    if(isset($_POST['ajax']) && $_POST['ajax']==='vehicle-vehicle-form')
    {
        echo CActiveForm::validate($model);
        Yii::app()->end();
    }
    */

    if(isset($_POST['Vehicle']))
    {
        $model->attributes=$_POST['Vehicle'];
        if($model->validate())
        {
            // form inputs are valid, do something here
            return;
        }
    }
    $this->render('vehicle',array('model'=>$model));
}
}