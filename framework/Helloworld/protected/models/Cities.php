<?php

/**
 * This is the model class for table "cities".
 *
 * The followings are the available columns in table 'cities':
 * @property integer $CityId
 * @property string $City
 * @property integer $ZoneId
 * @property string $SmallDescription
 * @property string $Description
 * @property integer $Longitude
 * @property integer $Latitude
 * @property integer $Distance
 * @property string $CreateDate
 * @property string $ModifiedDate
 * @property string $Status
 */
class Cities extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('City, ZoneId, SmallDescription, Description, Longitude, Latitude, Distance, CreateDate, ModifiedDate', 'required'),
			array('ZoneId, Longitude, Latitude, Distance', 'numerical', 'integerOnly'=>true),
			array('City', 'length', 'max'=>45),
			array('SmallDescription', 'length', 'max'=>255),
			array('Status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CityId, City, ZoneId, SmallDescription, Description, Longitude, Latitude, Distance, CreateDate, ModifiedDate, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CityId' => 'City',
			'City' => 'City',
			'ZoneId' => 'Zone',
			'SmallDescription' => 'Small Description',
			'Description' => 'Description',
			'Longitude' => 'Longitude',
			'Latitude' => 'Latitude',
			'Distance' => 'Distance',
			'CreateDate' => 'Create Date',
			'ModifiedDate' => 'Modified Date',
			'Status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CityId',$this->CityId);
		$criteria->compare('City',$this->City,true);
		$criteria->compare('ZoneId',$this->ZoneId);
		$criteria->compare('SmallDescription',$this->SmallDescription,true);
		$criteria->compare('Description',$this->Description,true);
		$criteria->compare('Longitude',$this->Longitude);
		$criteria->compare('Latitude',$this->Latitude);
		$criteria->compare('Distance',$this->Distance);
		$criteria->compare('CreateDate',$this->CreateDate,true);
		$criteria->compare('ModifiedDate',$this->ModifiedDate,true);
		$criteria->compare('Status',$this->Status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
