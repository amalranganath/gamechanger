<?php
/* @var $this DistanceController */
/* @var $model Distance */

$this->breadcrumbs=array(
	'Distances'=>array('index'),
	$model->DistanceId=>array('view','id'=>$model->DistanceId),
	'Update',
);

$this->menu=array(
	array('label'=>'List Distance', 'url'=>array('index')),
	array('label'=>'Create Distance', 'url'=>array('create')),
	array('label'=>'View Distance', 'url'=>array('view', 'id'=>$model->DistanceId)),
	array('label'=>'Manage Distance', 'url'=>array('admin')),
);
?>

<h1>Update Distance <?php echo $model->DistanceId; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>