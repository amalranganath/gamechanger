<?php
/* @var $this DistanceController */
/* @var $model Distance */

/*$this->breadcrumbs=array(
	'Distances'=>array('index'),
	'Create',
);*/

$this->menu=array(
	array('label'=>'List Distance', 'url'=>array('index')),
	array('label'=>'Manage Distance', 'url'=>array('admin')),
);
?>

<h1>Create Distance</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'cities'=>$cities)); ?>