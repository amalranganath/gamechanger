<?php
/* @var $this DistanceController */
/* @var $data Distance */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('DistanceId')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->DistanceId), array('view', 'id'=>$data->DistanceId)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FromId')); ?>:</b>
	<?php echo CHtml::encode($data->FromId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ToId')); ?>:</b>
	<?php echo CHtml::encode($data->ToId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Distance')); ?>:</b>
	<?php echo CHtml::encode($data->Distance); ?>
	<br />


</div>