<?php
/* @var $this DistanceController */
/* @var $model Distance */

$this->breadcrumbs=array(
	'Distances'=>array('index'),
	$model->DistanceId,
);

$this->menu=array(
//	array('label'=>'List Distance', 'url'=>array('index')),
//	array('label'=>'Create Distance', 'url'=>array('create')),
//	array('label'=>'Update Distance', 'url'=>array('update', 'id'=>$model->DistanceId)),
//	array('label'=>'Delete Distance', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->DistanceId),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage Distance', 'url'=>array('admin')),
);
?>

<h1>View Distance #<?php // echo $model->DistanceId; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'DistanceId',
		'FromId',
		'ToId',
		'Distance',
	),
)); ?>
