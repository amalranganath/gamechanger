<?php
/* @var $this DistanceController */
/* @var $model Distance */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'DistanceId'); ?>
		<?php echo $form->textField($model,'DistanceId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FromId'); ?>
		<?php echo $form->textField($model,'FromId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ToId'); ?>
		<?php echo $form->textField($model,'ToId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Distance'); ?>
		<?php echo $form->textField($model,'Distance'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->