<?php
/* @var $this DistanceController */
/* @var $model Distance */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'distance-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        
        
        <div class="row">
           <?php $main_cities = CHtml::listData(Cities::model()->findAll(),'CityId','City');
                    echo CHtml::activeDropDownList($model,'FromId',$main_cities ,array(

                            'prompt'=>'Select From City',
                            'class'=>'form-control',

                            'ajax' => array(
                            'type'=>'POST', 
                            'url'=>Yii::app()->createUrl('distance/loadcity'), //or $this->createUrl('loadcities') if '$this' extends CController

                            'update'=>'#result_here', //or 'success' => 'function(data){...handle the data in the way you want...}',
//                           'success'=> "function(data)
//                                        {
//                                            $('#biz_sub_category1_id').val(data.biz_category_id);
//                                        } ",

                            'data'=>array(
                                'FromId'=>'js:this.value',
                                'ToId'=>'js:this.value',
                            ),
                                
                                ))); ?>
            
        </div>
        <div class="row">
            <?php // echo CController::createUrl('distance/Test');?>
            <script type="text/javascript">
                 $(function(){
              
                        //acknowledgement message
                        $('td.set').on('dblclick',function(){
                           // $(this).setProperty('contentEditable', true);
                           $(this).attr('contenteditable',true);
                           //$(this).css('font-color', 'red');
                        //});
                        
                    var message_status = $('#status');
                    //$('td[dbl=ready]').addEvent('dblclick', function () {
                    $('td[contenteditable=true]').blur(function(){
                        var field_userid = $(this).attr('id') ;
                        var value = $(this).text() ;
                        $.post('<?php echo Yii::app()->createUrl('distance/Test');?>' , field_userid + '=' + value, function(data){
                            if(data != '')
                                {
                                        message_status.show();
                                        message_status.text(data);
                                        //hide the message
                                        setTimeout(function(){message_status.hide()},1000);
                                        
                                }
                        });
                    });
                   
                });
                });
            </script>
           
        <?php
       // echo '<pre>'.print_r(Yii::app()->createUrl('distance/Test'),1).'</pre>';

                 /*   Yii::app()->clientScript->registerScript('distin', "
           

                $(function(){
                        //acknowledgement message
                    var message_status = $('#status');
                    $('td[contenteditable=true]').blur(function(){
                        var field_userid = $(this).attr('id') ;
                        var value = $(this).text() ;
                        $.post('".  Yii::app()->createUrl('distance/Test')."' , field_userid + '=' + value, function(data){
                            if(data != '')
                                {
                                        message_status.show();
                                        message_status.text(data);
                                        //hide the message
                                        setTimeout(function(){message_status.hide()},3000);
                                }
                        });
                    });
                });

            ");
                 */   
        
        ?>
    </div>
        
        
        
        <div class="row" >
            
            <div id="result_here" class="result-here"></div>
            
        </div>

	<div class="row">
            <b><div id="status" class="flash-error">
                 <?php
      $this->widget('ext.mPrint.mPrint', array(
            'id' => 'mprint1',  // !!!you have to set up this one if you want multiple prints per page
            'title' => 'Some title for page',        //the title of the document. Defaults to the HTML title
            'tooltip' => 'testing the print',    //tooltip message of the print icon. Defaults to 'print'
            'text' => 'Print table 1 - Some name of table', //text which will appear beside the print icon. Defaults to NULL
            'element' => '.result-here',      //the element to be printed.
           // 'exceptions' => array(     //the element/s which will be ignored
               // '.summary',
               // '.search-form'
           // ),
            'publishCss' => true       //publish the CSS for the whole page?
           
        ));
      $name='YES ITS WORKING...';
      ?>
                
            </div></b>
		<?php // echo $form->labelEx($model,'FromId'); ?>
		<?php // echo $form->textField($model,'FromId',array('contenteditable'=>'true')); ?>
		<?php // echo $form->error($model,'FromId'); ?>
<!--            <table>
                <thead></thead>
                <tr>
                    <td id="id:1" contenteditable="true">THIS ONE</td>
                    <td id="iddd:2" contenteditable="true">THIS TWO</td>
                </tr>
                
            </table>-->
	</div>

	<div class="row">
           
		<?php echo $form->labelEx($model,'ToId'); ?>
		<?php echo $form->textField($model,'ToId'); ?>
		<?php echo $form->error($model,'ToId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Distance'); ?>
		<?php echo $form->textField($model,'Distance'); ?>
		<?php echo $form->error($model,'Distance'); ?>
	</div>

        <div class="row">
            <?php echo CHtml::link('LinkText',array('distance/pdf'),array('class'=>'btn_registro')); ?>
        </div>
       
        
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->