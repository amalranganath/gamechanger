<?php
/* @var $this PhoneController */
/* @var $data Phone */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Brand')); ?>:</b>
	<?php echo CHtml::encode($data->Brand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Series')); ?>:</b>
	<?php echo CHtml::encode($data->Series); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Country')); ?>:</b>
	<?php echo CHtml::encode($data->Country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Year')); ?>:</b>
	<?php echo CHtml::encode($data->Year); ?>
	<br />


</div>