<?php
/* @var $this PhoneController */
/* @var $model Phone */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'phone-form',
        'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
         ),
        
    
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Brand'); ?>
		<?php // echo $form->textField($model,'Brand',array('size'=>20,'maxlength'=>20)); ?>
		<?php // echo $form->error($model,'Brand'); ?>
            <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                        'name'=>'Brand',
                    
                        'sourceUrl'=>array('Phone/aclist'),
                        // additional javascript options for the autocomplete plugin
                        'options'=>array(
                           'minLength'=>'1',
                             ),
                         'htmlOptions'=>array(
                          'style'=>'height:20px;',
                             'placeholder' => "Search...",
                                ),
                            ));
            ?>
            <?php echo $form->error($model,'Brand'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Series'); ?>
		<?php echo $form->textField($model,'Series',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'Series'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Country'); ?>
		<?php echo $form->textField($model,'Country',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'Country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Year'); ?>
		<?php echo $form->textField($model,'Year'); ?>
		<?php echo $form->error($model,'Year'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($customer,'Name'); ?>
		<?php echo $form->textField($customer,'Name'); ?>
		<?php echo $form->error($customer,'Name'); ?>
	</div>
       
        
        

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->