<?php
/* @var $this VehicleController */
/* @var $model Vehicle */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vehicle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_no'); ?>
		<?php echo $form->textField($model,'vehicle_no',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'vehicle_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Brand'); ?>
		<?php echo $form->textField($model,'Brand',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'Brand'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Color'); ?>
		<?php echo $form->textField($model,'Color',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'Color'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Capacity'); ?>
		<?php echo $form->textField($model,'Capacity',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'Capacity'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->