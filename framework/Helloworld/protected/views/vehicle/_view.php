<?php
/* @var $this VehicleController */
/* @var $data Vehicle */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_no')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->vehicle_no), array('view', 'id'=>$data->vehicle_no)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Brand')); ?>:</b>
	<?php echo CHtml::encode($data->Brand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Color')); ?>:</b>
	<?php echo CHtml::encode($data->Color); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Capacity')); ?>:</b>
	<?php echo CHtml::encode($data->Capacity); ?>
	<br />


</div>