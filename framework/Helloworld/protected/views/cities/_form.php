<?php
/* @var $this CitiesController */
/* @var $model Cities */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cities-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'City'); ?>
		<?php echo $form->textField($model,'City',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'City'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ZoneId'); ?>
		<?php echo $form->textField($model,'ZoneId'); ?>
		<?php echo $form->error($model,'ZoneId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SmallDescription'); ?>
		<?php echo $form->textField($model,'SmallDescription',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'SmallDescription'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Description'); ?>
		<?php echo $form->textArea($model,'Description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'Description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Longitude'); ?>
		<?php echo $form->textField($model,'Longitude'); ?>
		<?php echo $form->error($model,'Longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Latitude'); ?>
		<?php echo $form->textField($model,'Latitude'); ?>
		<?php echo $form->error($model,'Latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Distance'); ?>
		<?php echo $form->textField($model,'Distance'); ?>
		<?php echo $form->error($model,'Distance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CreateDate'); ?>
		<?php echo $form->textField($model,'CreateDate'); ?>
		<?php echo $form->error($model,'CreateDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ModifiedDate'); ?>
		<?php echo $form->textField($model,'ModifiedDate'); ?>
		<?php echo $form->error($model,'ModifiedDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Status'); ?>
		<?php echo $form->textField($model,'Status',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->