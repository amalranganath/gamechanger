<?php
/* @var $this CitiesController */
/* @var $model Cities */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'CityId'); ?>
		<?php echo $form->textField($model,'CityId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'City'); ?>
		<?php echo $form->textField($model,'City',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ZoneId'); ?>
		<?php echo $form->textField($model,'ZoneId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SmallDescription'); ?>
		<?php echo $form->textField($model,'SmallDescription',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Description'); ?>
		<?php echo $form->textArea($model,'Description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Longitude'); ?>
		<?php echo $form->textField($model,'Longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Latitude'); ?>
		<?php echo $form->textField($model,'Latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Distance'); ?>
		<?php echo $form->textField($model,'Distance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CreateDate'); ?>
		<?php echo $form->textField($model,'CreateDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ModifiedDate'); ?>
		<?php echo $form->textField($model,'ModifiedDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Status'); ?>
		<?php echo $form->textField($model,'Status',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->