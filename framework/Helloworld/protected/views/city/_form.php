<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'city-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php //echo $form->errorSummary($model); ?>
        
        
        <div class="row">
        <?php echo CHtml::dropDownList('region_id','', 
  array(1=>'New England',2=>'Middle Atlantic',3=>'East North Central'),
          //echo CHtml::dropDownList('region_id','', 
//  array($model->city_id),
 
            /*-******/  
                  
                  /*$opts = CHtml::listData(City::model()->findAll(),'city_id','city');
echo $form->dropDownList($model,'city_id',$opts,array('empty'=>''));  */
                  
            /****/
              
                  
  array(
    'prompt'=>'Select Region',
    'ajax' => array(
    'type'=>'POST', 
    'url'=>Yii::app()->createUrl('city/loadcities'), //or $this->createUrl('loadcities') if '$this' extends CController
    'update'=>'#city_name', //or 'success' => 'function(data){...handle the data in the way you want...}',
  'data'=>array('region_id'=>'js:this.value'),
  ))); 
 
 
 
echo CHtml::dropDownList('city_name','', array(), array('prompt'=>'Select City'));


 ?>
        </div> 
        <div class="row">
            <?php Yii::app()->clientScript->registerScript('some-name', "
                        $('select').on('change', function (event) {
                        var url = $(event.currentTarget).val();
                    if (url != 0)
                    $.get(url, function (data) {
                    $('#result').html(data);
                        });
                });", CClientScript::POS_READY) ?>
           

            <?php echo
                CHtml::dropDownList("param", "select here", [
                        'Select here',

                        $this->createUrl('/city/pagea') => "Add class teacher01",
                        $this->createUrl('/city/index2') => "Add class teacher02",
                        $this->createUrl('/city/page') => "Add class teacher03",
                    
//                        $this->createUrl('/city/index') => "colombo"
                        ],array('onChange'=>'changetextbox();')) ?>
<div id="result"></div>
        </div>

	<div class="row">
		<?php echo $form->labelEx($model,'city_id'); ?>
		<?php echo $form->textField($model,'city_id'); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city',array('size'=>30,'maxlength'=>30,'class'=>'test','id'=>'test')); ?>
		<?php // echo $form->error($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Description'); ?>
		<?php echo $form->textField($model,'Description',array('size'=>50,'maxlength'=>50,'class'=>'test')); ?>
		<?php echo $form->error($model,'Description'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->