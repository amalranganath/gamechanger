<?php
/* @var $this PhotosController */
/* @var $model Photos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'photos-form',
        'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
         ),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

      
        
        
	<div class="row">
		<?php echo $form->labelEx($model,'imagePath'); echo Yii::app()->basePath.'/../banner/'; ?>
		<?php // echo $form->textField($model,'imagePath',array('size'=>60,'maxlength'=>100)); ?>
		<?php // echo CHtml::activeFileField($model, 'imagePath'); ?>
                <?php
            $this->widget('CMultiFileUpload', array(
                'name' => 'images',
                'accept' => 'jpeg|jpg|gif|png', // useful for verifying files
                'duplicate' => 'Duplicate file!', // useful, i think
                'denied' => 'Invalid file type', // useful, i think
            ));
                ?>
            
            
		<?php echo $form->error($model,'imagePath'); ?>
            
	</div>
                <?php if($model->isNewRecord!='1'){ ?>
                <div class="row">
                <?php echo CHtml::image(Yii::app()->request->baseUrl.'/banner/'.$model->imagePath,"imagePath",array("width"=>200)); ?>  
                </div>
                <?php }?>
        
        
                <?php
                    $this->widget('ext.imageSelect.ImageSelect',  array(
                    'path'=>'../images',
                    'alt'=>'alt text',
                    'uploadUrl'=>'../images',
                    'htmlOptions'=>array()
   ));
?>
        
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

                <?php  $this->endWidget(); ?>

</div><!-- form -->