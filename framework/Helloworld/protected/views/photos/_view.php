<?php
/* @var $this PhotosController */
/* @var $data Photos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagePath')); ?>:</b>
	<?php echo CHtml::encode($data->imagePath); ?>
	<br />


</div>