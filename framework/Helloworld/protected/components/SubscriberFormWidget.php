<?php

// protected/components/SubscriberFormWidget.php

class SubscriberFormWidget extends CWidget
{
    /**
     * @var CFormModel
     */
    public $form;

    public function run()
    {
        if (! $this->form instanceof CFormModel) {
            throw new RuntimeException('No valid form available.');
        }
        $this->render('subscriberFormWidget', array('form'=>$this->form));
    }
}

// protected/components/views/subscriberFormWidget.php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'home-newsletter-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
));

echo $form->textField($newsletterSubscribeForm, 'email');
echo $form->error($newsletterSubscribeForm, 'email');
echo CHtml::link("subscribe", "#", array('class'=>'btSubscribe'));
$this->endWidget();